<?php
/*  CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/

 include("debut.php"); ?>

<div class="corps">
<!-- Debut de la page -->
		
		<?php
		echo "
		<table summary=\"\" border=\"1\" width=\"90%\">
		<tr>
			<th><p>Conditions d'utilisation du site. (Version 2  MàJ: 07/2011)</p></th>
		</tr>
		<tr>
			<td class=t1><p>0. Préface</p></td>
		</tr>
		<tr>
			<td class=t3><p class='texte'>0.1 Définitions<br>
							Séliste ou Membre : Personne physique et moral inscrit et validé.<br>
							".$nom." : Le site ".$site." ainsi que les administrateurs.<br>
							Profil : Page internet ou s'affiche les données personnelles (email, pseudo, etc).<br>
							Administrateur: Séliste qui a accèe à la Base de données.<br>
							Modérateur : Séliste qui aura des fonctions spécifique et un rôle.<br>
							Compte Actif : Profil de séliste activé.<br>
							Compte Inactif : Profil de séliste désactivé (Invisible pour les sélistes et modérateurs, c'est le compte par défaut).</p></td>
		</tr>
		<tr>
			<td class=t3><p class='texte'>0.2 La simple utilisation et/ou consultation du site de ".$nom." implique, de façon automatique et inconditionnelle, votre acceptation pleine et entière de l'ensemble des présentes conditions d'utilisation ainsi que la charte.</p></td>
		</tr>
		<tr>
			<td class=t3><p class='texte'>0.3 Le site est gratuit et sans publicité, sous réserve que vous vous engagiez à respecter les présentes conditions d'utilisation qui peuvent être modifiées sans préavis.</p></td>
		</tr>
		<tr>
			<td class=t1><p>1. Responsabilités</p></td>
		</tr>
		<tr>
			<td class=t3><p class='texte'>1.1 Vous utilisez ce site à vos risques et périls, sous votre entière responsabilité.</p></td>
		</tr>
		<tr>
			<td class=t3><p class='texte'>1.2 Vous êtes conscient que toutes les informations et données, qu'elles soient portées à la connaissance du public par le biais du site ou transmises de manière privée, sont sous la seule responsabilité de la personne ayant émis leur contenu. Vous seul êtes entièrement responsable du contenu que vous décidez d’afficher, envoyer par courrier électronique ou transmettez de toute autre manière.</p></td>
		</tr>
		<tr>
			<td class=t3><p class='texte'>1.3 En mettant votre profil sur le site de ".$nom.", vous approuvez le fait que ce profil soit visible par les autres sélistes. Néanmoins, les données concernant votre profil ne sont encodées qu'une seule fois dans la base de données et sont gérées exclusivement par de ".$nom.". Vos données ne seront jamais transmise à des non sélistes.</p></td>
		</tr>
		<tr>
			<td class=t3><p class='texte'>1.4 Le grade Modérateur à un statut de modérateur bénévole et non soumis à la responsabilité civile et pénale.</p></td>
		</tr>
		<tr>
			<td class=t3><p class='texte'>1.5 Seul les Administrateurs peuvent avoir un statut d'administrateur de site.</p></td>
		</tr>
		<tr>
			<td class=t1><p>2. Engagement et retrait de profil</p></td>
		</tr>
		<tr>
			<td class=t3><p class='texte'>2.1 En créant un profil sur ce site ou en intervenant de quelque manière que ce soit, vous acceptez implicitement et explicitement que de ".$nom." puisse retirer votre profil et intervenir si celui-ci est contraire aux règles de conduite (charte & presente conditions d'utilisations), et ce sans préavis et sans que vous puissiez réclamer des dommages et intérêts de quelque nature que ce soit.</p></td>
		</tr>
		<tr>
			<td class=t3><p class='texte'>Les motifs de retrait de profil peuvent être les suivants :<br>
			2.2 Votre profil ou photo contient des données personnelles qui ne vous appartiennet pas.<br>
			2.3 Votre profil ou photo contient une connotation clairement sexuelle, agressive, raciste, ou xénophobe.<br>
			2.4 Vos interventions, que ce soit dans le forum, via messages publics, répliques ou messages privés, sont à connotation clairement sexuelle, agressive, raciste, ou xénophobe.<br>
			2.5 L'utilisation de votre profil est source de conflits sur le site.<br>
			2.6 Vous postez des messages à caractère commercial de quelque nature que ce soit.<br>
			2.7 Vous utilisez le site de ".$nom." dans un but de publicité, de promotion non sollicitée ou non autorisée (telle que le spamming ou l'envoi de lettres en chaîne etc.).<br>
			2.8 Votre profil est un faux profil qui pourrait perturber le fonctionnement normal du site : seront notamment poursuivis, toute fausse donnée destinée au harcèlement d'une tierce personne (adresse, numéro de téléphone), ou tout profil créé dans un but de concurrence déloyale.<br>
			2.9 Vous harcelez les autres utilisateurs.<br>
			2.10 Vous postez ou publiez des contenus qui constituent une violation des dispositions pénales réprimant notamment la pornographie enfantine, la pédophilie, la débauche, le racisme, la xénophobie, la publicité pour des offres de service à caractère sexuel, la vente de stupéfiants, etc.<br>
			2.11 Vous postez des contenus de nature, nuisible, menaçante, abusive, diffamatoire, haineuse, agressive, raciste, vulgaire, dénigrante, inconvenante, injurieuse, violente, obscène, pornographique, que ce soit dans les forums, via une réplique publique ou un message privé.<br>
			2.12 Vous partagez votre compte avec des personnes en infraction avec cette charte d'utilisation.<br>
			2.13 Vous utilisez le site pour racoler des utilisateurs pour quelque nature que se soit.<br>
			2.14 Votre compte est au delà de la limite de 1500 $monnaie pendant plus de deux mois et sans dérogation obtenu en réunion de fonctionnement.</p></td>
		</tr>
		<tr>
			<td class=t3><p class='texte'>2.15 Chaque séliste s'engage à partir de ".$nom." avec un compte le plus proche possible de Zero. </p></td>
		</tr>
		<tr>
			<td class=t3><p class='texte'>2.16 Tout séliste valide peut refuser une offre ou une demande.</p></td>
		</tr>
		<tr>
			<td class=t1><p>3. Majorité</p></td>
		</tr>
		<tr>
			<td class=t3><p class='texte'>3.1 Le site de ".$nom." est destiné aux plus de 18 ans. Afin d'assurer le respect de la sécurité et de la vie privée de nos utilisateurs, et plus particulièrement des enfants, les parents sont invités à surveiller l'utilisation faite par leurs enfants de l'accès à ces pages. En leur qualité de tuteurs légaux, il est de la responsabilité des parents de déterminer quel service est ou non approprié pour leur(s) enfant(s) et de surveiller l'utilisation qui en est faite.
</p></td>
		</tr>
		<tr>
			<td class=t3><p class='texte'>3.2 En aucun cas ".$nom." sera tenu responsable si un séliste ment sur son age.</p></td>
		</tr>
		<tr>
			<td class=t1><p>4. Vie privée</p></td>
		</tr>
		<tr>
			<td class=t3><p class='texte'>4.1 Les données personnelles que vous nous communiquez sont conservées dans notre base de données. Conformément à la Loi, vous disposez d'un droit de consultation, de modification et de rectification de toutes les données personnelles portées à notre connaissance (email et mot de passe personnels) lors de l'utilisation du service. Ce droit pourra être exercé en contactant un administrateur ou un modérateur ou encore via l'envoi d'un mail à webmaster@de ".$nom.". Ainsi, par sécurité, le site se donne le droit de garder en base de données vos informations personnelles, id de connexion, contenu de votre profil et/ou votre email de connexion. Vous disposez d'un droit de suppression de ces dernières en le demandant simplement par email à webmaster@de ".$nom.".</p></td>
		</tr>
		<tr>
			<td class=t3><p class='texte'>4.2 Le site de ".$nom." s'engage à conserver les informations personnelles à l'intérieur de ce site sur la base de donnée, créant ainsi un environnement confidentiel et sécurisé pour les utilisateurs du site de ".$nom.".</p></td>
		</tr>
		<tr>
			<td class=t3><p class='texte'>4.3 L'adresse E-mail que vous indiquerez, s'il y a lieu, sera uniquement utilisée pour vous rappeler votre mot de passe ou vos messages et pour vous connecter au site. Elle ne sera jamais transmise (aux autres utilisateurs, entreprises, particuliers..) sans votre accord.
</p></td>
		</tr>
		<tr>
			<td class=t3><p class='texte'>4.4 En acceptant ces conditions générales, vous autorisez les administrateurs de ".$nom." à vérifier le contenu de vos messages privés afin de contrôler une éventuelle infraction à toute législation en vigueur dans tout pays intéressé.</p></td>
		</tr>
		<tr>
			<td class=t3><p class='texte'>4.5 Le site est lisible par tout séliste. Ces messages étant publics, les modérateurs du site se réservent le droit de vérifier les messages envoyés dans les zones publiques du site et d'enlever tous messages jugés non conforme à la présente entente, ou qui enfreindrait la loi française.
</p></td>
		</tr>
		<tr>
			<td class=t3><p class='texte'>4.6 Vous êtes responsable de vos écrits et textes. ".$nom." rapportera auprès des autorités judiciaires toute infraction pénale qu'il aura pu constater si besoin est. Dans cet objectif, votre IP et date de connexion sont conservées afin de pouvoir formellement vous identifier.
</p></td>
		<tr>
			<td class=t3><p class='texte'>4.7 Les messages privés, les annonces que vous postez ou tout autre texte que vous publié sur le site se doit d'être en vigueur avec les droits d'auteur. 
</p></td>
		</tr>
		<tr>
			<td class=t3><p class='texte'>4.8 Les messages privés et les annonces que vous postez ont une durée de vie maximale de trois mois, au delà de cette limite de durée ils peuvent être effacés sans préavis. 
</p></td>
		</tr>
		<tr>
			<td class=t1><p>5. Indisponibilité - Non fonctionnement</p></td>
		</tr>
		<tr>
			<td class=t3><p class='texte'>5.1Afin de pouvoir utiliser l'ensemble des fonctionnalités de ".$site.", les utilisateurs doivent avoir à leur disposition une connexion Internet et un navigateur Internet. Les services pourront ainsi être momentanément interrompus pour maintenance, mise à jour, évolutions. Ces interruptions auront une durée indéterminée selon le désir de l'équipe de développement du site. Les membres de de ".$nom." renoncent à poursuivre de ".$nom." au titre du \"fonctionnement des services\" même pour une durée indéfinie.
</p></td>
		</tr>
		<tr>
			<td class=t1><p>6. Tarif et durée</p></td>
		</tr>
		<tr>
			<td class=t3><p class='texte'>6.1 de ".$nom." est gratuit et aucun abonnement obligatoire n'est demandé.</p></td>
		</tr>
		<tr>
			<td class=t3><p class='texte'>6.2 ".$nom." peut prèlève une cotisation en euros ou $monnaie tout les mois, le tarif de la cotisation est fixé en réunion et peut évolué dès la décision validé par la réunion.</p></td>
		</tr>
		<tr>
			<td class=t3><p class='texte'>6.3 En aucun cas un séliste peut demander de l'argent pour une annonce publié sur le site. La seul monnaie ayant cour sur ".$nom." est : le ".$monnaie.".</p></td>
		</tr>";?>
		

<!--   CETTE PARTIE EST INTERDITE DE MODIFICATION ET EST AFFICHEE SUR VOTRE SITE  © Copyright -->
		<?php  
		echo "<tr>
			<td class=t1><p>7. Propriété du site</p></td>
		</tr>
		<tr>
			<td class=t3><p class='texte'>7.1Vous êtes autorisé à accéder et utiliser le site pour une utilisation uniquement privée et non commerciale. De plus, utiliser tout ou partie du site sur un autre site Internet, un réseau informatique ou tout autre médium, est interdit. La création de produits dérivés, basés en tout ou partie sur les logiciels du site, est formellement interdite. Aucune partie du site ne peut être reproduite, publiée, copiée, enregistrée, postée, transmise, diffusée ou distribuée de quelque manière que ce soit, sauf autorisation spécifique préalable des Administrateurs du site (Voir Contributeurs). Cela inclus les graphiques et le code source. Le site étant sous licence Libre vous le trouverez sur www.reveland.fr</p></td>
		</tr>
		<tr>
			<td class=t1><p>8. Qui sommes-nous ?</p></td>
		</tr>
		<tr>
			<td class=t3><p class='texte'>8.1 Le site de ".$nom." est un site privé communautaire. C'est un ensemble de personnes visant à favoriser la création artistique et la communication entre les personnes, sans discrimination sociale ou culturelle. Ce site est disponible en logiciel libre sous licence CECILL-B sur www.reveland.fr, le créateur initial est M Petit Fabien, développeur web indépendant.</p></td>
		</tr>
		<tr>
			<td class=t1><p>9. Pour nous contacter :</p></td>
		</tr>
		<tr>
			<td class=t3><p class='texte'>9.1 www.reveland.fr pour le createur du site, ".$email." pour ".$nom."</p></td>
		</tr>";?>
		<!-- fin de © Copyright -->
		<?php 
		echo "
		<tr>
			<td class=t1><p>Copyright © 2007 (Màj en 2011) [Eolange-F.P.]– FRANÇAIS – Durée de validité indeterminée.</p></td>
		</tr>
		</table>";

?>


<!-- Fin de page -->
</div>
<?php include("fin.php"); ?>