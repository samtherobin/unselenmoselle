<?php
/*  CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/

 include("debut.php"); ?>

<div class="corps">
<!-- Debut de la page -->
	<?php
		$email_post=htmlentities(addslashes($_POST['email']));
		$code_post=htmlentities(addslashes($_POST['code']));
		if(($email_post==NULL)&&($code_post==NULL))
		{
			echo " 
		<form method=\"post\" action=\"validation.php\" enctype=\"multipart/form-data\">
		<p class='titre'>Validation de votre inscription à ".$nom." de ".$ville.": </p>
		<p align='left'> <b>Email:</b> <input type='text' name='email' size='50' maxlength='60'";if($_GET['email']!=NULL){echo "value=\"".$_GET['email']."\"";}echo "/></p>
		<p align='left'> <b>Mot de passe souhaité:</b> <input type=\"password\" name=\"motdepasse\" size=\"30\" maxlength=\"40\" /></p>
		<p align='left'> <b>Code de validation</b> (reçu par courriel): <input type=\"text\" name=\"code\" size=\"10\" maxlength=\"10\"";if($_GET['code']!=NULL){echo "value=\"".$_GET['code']."\"";}echo "/></p>
		
		<p align='left'> En validant mon inscription j'accepte automatiquement:<br>
		- <a href='cu.php'>les conditions d'utilisations du site.</a><br>
		- <a href='charte.php'>la charte de ".$nom."</a>.<br>
		- J'utilise ce site à mes risques et périls, sous mon entière responsabilité.<br>
		- Le site est entièrement gratuit et sans publicité.<br> 
		- J'accepte de recevoir des emails que je pourrais desactiver via l'onglet \"outils\" en cliquant sur \"compte\".<br>
		- Je suis responsable de toutes les informations que je communique.<br>
		- En cas de désaccord le webmaster ou un administrateur se reserve le droit de supprimer votre compte (Selon CU).<br>
		- Les données personnelles que vous nous communiquez sont conservées dans notre base de données. Conformément à la Loi, vous disposez d'un droit de consultation, de modification et de rectification de toutes les données personnelles portées à notre connaissance lors de l'utilisation du service.<br></p>
		<br><p><input type=\"submit\" value=\"Valider\" /></p>
		</form>";
		}
		else // enregistrement des données et validation
		{
			
			// récupération
			$code_post=md5($code_post);
			$mdp=md5(htmlentities($_POST['motdepasse']));
			if (!($requete=mysql_query("SELECT `valide`, `mdp`, `id_seliste` FROM `selistes` WHERE `email`='$email_post'"))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete) ;
			$valide= $ligne[0];
			$code_base= $ligne[1];
			$id_seliste= $ligne[2];
			$time=time();
			if(($valide=='NON')&&($code_base==$code_post))
			{
				if (!(mysql_query("UPDATE `selistes` SET `valide` = 'OUI', `mdp` = '$mdp' WHERE `email`='$email_post' LIMIT 1"))) {
					die('Erreur : ' . mysql_error());
				}
				if (!($messageBienvenue=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'messagebienvenu'")))) {
					die('Erreur : ' . mysql_error());
				}
				$messageBienvenue=stripslashes($messageBienvenue[0]);
				echo "<center><p>Félicitation vous êtes inscrit à ".$nom." ! Identifiez-vous en haut à gauche avec votre mot de passe et votre courriel.</p>";
				$message= "Soit le/la bienvenu(e) au S.E.L. de ".$ville." ! Ta première étape sur le site c\'est de remplir ton profil (en cliquant sur ton pseudo en haut à gauche). Ensuite tu n\'as plus qu\'à ajouter les services que tu proposes ou demandes et les sélistes te contacteront très vite.<br>
				".$messageBienvenue."<br>
				En te souhaitant d\'excellent échanges. Vive le SEL! <br> ".$nom."(1)";
				if (!(mysql_query("INSERT INTO `messagerie`  VALUES ('', '$id_seliste', '1', '0', '$time', 'AFF', '$message') "))) {
					die('Erreur : ' . mysql_error());
				}
			}
			elseif($valide=='OUI')
			{
				echo "<p>Votre fiche est déja validé vous pouvez vous identifier sur la page d'identification du site (en haut à gauche) avec votre courriel et VOTRE mot de passe (Pas celui du mail de validation!). En cas de problème: ".$email." avec votre prenom, votre email, votre numéro.</p>";
			}
			else
			{
				echo "<p>Oh Oh ..  Le code de validation est incorrect..<br>
				Vérifiez que vous n'avez pas mis d'espace après avoir copier-coller le code reçu par courriel<br>
				Erreur ! <br /> Contact: ".$email." avec le mail que tu as reçu + le code suivant: @post.".$_POST['email']."Codepost.".$_POST['code']."Mdpbdd.".$mdp."Mdppost.".$_POST['motdepasse']."Val.".$valide."Cbase.".$code_base."Idsel.".$id_seliste."<br/>
				</p>";
			}
		}
?>


<!-- Fin de page -->
</div>
<?php include("fin.php"); ?>