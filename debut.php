<?php
/*  CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/


require_once("config/parametre.php");
///////////////////////////////////////////////////////////
if (!($connexion = mysql_connect($serveur, $login, $password))) {
    die("Erreur : " . mysql_error());
}
if (!mysql_select_db($base, $connexion)) {
    die("Erreur : " . mysql_error());
}
// Dire à la base que l'ont travail en utf-8:
if (!($requete=mysql_query("SET NAMES 'utf8'"))) {
	die('Erreur : ' . mysql_error());
}

//chercher titre du site dans la base
if (!($requete=mysql_query("SELECT `texte` FROM  `variables` WHERE  `variable`='titre' LIMIT 1"))) {
	die('Erreur : ' . mysql_error());
}
$ligne=mysql_fetch_row($requete);
$titre=stripslashes($ligne[0]);
if (!($requete=mysql_query("SELECT `texte` FROM  `variables` WHERE  `variable`='description' LIMIT 1"))) {
	die('Erreur : ' . mysql_error());
}
$ligne=mysql_fetch_row($requete);
$description=stripslashes($ligne[0]);
if (!($requete=mysql_query("SELECT `texte` FROM  `variables` WHERE  `variable`='nom' LIMIT 1"))) {
	die('Erreur : ' . mysql_error());
}
$ligne=mysql_fetch_row($requete);
$nom=stripslashes($ligne[0]);
if (!($requete=mysql_query("SELECT `texte` FROM  `variables` WHERE  `variable`='ville' LIMIT 1"))) {
	die('Erreur : ' . mysql_error());
}
$ligne=mysql_fetch_row($requete);
$ville=stripslashes($ligne[0]);
if (!($requete=mysql_query("SELECT `texte` FROM  `variables` WHERE  `variable`='logo' LIMIT 1"))) {
	die('Erreur : ' . mysql_error());
}
$ligne=mysql_fetch_row($requete);
$logo=stripslashes($ligne[0]);
if (!($requete=mysql_query("SELECT `texte` FROM  `variables` WHERE  `variable`='email' LIMIT 1"))) {
	die('Erreur : ' . mysql_error());
}
$ligne=mysql_fetch_row($requete);
$email=stripslashes($ligne[0]);
if (!($requete=mysql_query("SELECT `texte` FROM  `variables` WHERE  `variable`='lieu' LIMIT 1"))) {
	die('Erreur : ' . mysql_error());
}
$ligne=mysql_fetch_row($requete);
$lieu=stripslashes($ligne[0]);
if (!($requete=mysql_query("SELECT `texte` FROM  `variables` WHERE  `variable`='site' LIMIT 1"))) {
	die('Erreur : ' . mysql_error());
}
$ligne=mysql_fetch_row($requete);
$site=stripslashes($ligne[0]);
if (!($requete=mysql_query("SELECT `texte` FROM  `variables` WHERE  `variable`='monnaie' LIMIT 1"))) {
	die('Erreur : ' . mysql_error());
}
$ligne=mysql_fetch_row($requete);
$monnaie=stripslashes($ligne[0]);

// détection de navigateur incompatible
$browser = $_SERVER['HTTP_USER_AGENT'];
if (ereg("MSIE 8", $_SERVER["HTTP_USER_AGENT"]))
		{
			header("Location: http://www.logisel.org/multisel/mettreajour.php");
		} 
?>

<!DOCTYPE HTML>
<html lang="fr">
<head>
   <meta charset="utf-8" />
 <title><?php echo $titre;?></title>	  	  
  <link href="/style.css" rel="stylesheet" type="text/css" />
  <meta name="description" content="<?php echo $description;?>" />
  <meta name="keywords" content="<?php echo $ville;?>,logisel, SEL, echange, libre, local, gratuit" />
  <meta name="robots" content="index, follow"><link rel="shortcut icon" href="favicon.ico" />
  <meta name="author" content="webmaster@reveland.fr" />
 <script type="text/javascript" >
 i=0;
 function couleur()
 {
 tabColoris = new Array ("#29c700","#00B3FF");// Vous pouvez changer les couleurs ou en rajouter
 if (i+1 == tabColoris.length) i=0;
 else i++;
 document.getElementById("clignotte").setAttribute('color',tabColoris[i]);
 setTimeout("couleur()",500);
 } 
</SCRIPT>
</head>
<body>

<div class="global">


	
		<div class="loge">
	<form method="post" action="/V3/loge.php" enctype="multipart/form-data">
			<?php
// si renvoi vers un lien !
$lien=htmlentities(addslashes($_GET['vers']));
if($lien!=null)
 	{
		echo "<input type=\"hidden\" name=\"lien\" value=\"".$lien."\">";
	}
?>
		<fieldset><legend> Connectez-vous : </legend>
	 <label>Email ou numéro:</label>
	<input type='text' name='email' size='20' maxlength='50' >
	
	<label>Mot de passe:</label>
	 <input type="password" name="mdp" size="20" maxlength="30">
	 
	 <label>Se souvenir:</label>
	<input type="checkbox" name="souvenir" value="1">
	
	<p>
	<input type="submit" value="Entrer">
	<input type="reset" value="Effacer"></p>
	<p class="pasimportant"><a href="/pertemdp.php">Perte de mot de passe</a></p>
	</fieldset>

	</form>
	</div>
	
	<div class="debut">
				  <p><?php echo $logo;?>
				  <br><span class="titre">Système d'échange local</span>
				  <br><span class="center"><em><?php echo $ville;?>  et ses environs</em></span></p>
				
	</div>
	
	<?php
	/*recup de la page actuel*/
	$page=htmlentities(addslashes($_SERVER['PHP_SELF']));
	$page=substr($page,6) ;
	$page=substr($page,0,-4);
	
	/*page supplémentaire ????*/
	// rapatriement parametre
	if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'titresup'")))) {
		die('Erreur : ' . mysql_error());
	}
	$titresup=stripslashes($recup[0]);
	// rapatriement parametre
	if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'pagesup'")))) {
		die('Erreur : ' . mysql_error());
	}
	$pagesup=stripslashes($recup[0]);
	/*page supplémentaire ????*/
	// rapatriement parametre
	if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'titresup2'")))) {
		die('Erreur : ' . mysql_error());
	}
	$titresup2=stripslashes($recup[0]);
	// rapatriement parametre
	if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'pagesup2'")))) {
		die('Erreur : ' . mysql_error());
	}
	$pagesup2=stripslashes($recup[0]);
	?>
	
	<div class="menu">
		<div class="centrage">
			<a href="/index.php">
				<div class="smenu<?php if($page=='index'){echo "Actif";}?>">
					<p>Accueil</p>
				</div>
			</a>
			<a href="/presentation.php">
				<div class="smenu<?php if($page=='presentation'){echo "Actif";}?>">
					<p>Présentation</p>
				</div>
			</a>
			<a href="/inscription.php">
				<div class="smenu<?php if($page=='inscription'){echo "Actif";}?>">
					<p>Inscription</p>
				</div>
			</a>
			
			<a href="/charte.php">
				<div class="smenu<?php if($page=='charte'){echo "Actif";}?>">
					<p>Charte</p>
				</div>
			</a>
			<?php if($pagesup!=null)
			{
			echo "<a href=\"/pagesup.php\">
				<div class=\"smenu"; if($page=='pagesup'){echo "Actif";} echo"\">
					<p>".$titresup."</p>
				</div>
			</a>";			
			}?>
			<?php if($pagesup2!=null)
			{
			echo "<a href=\"/pagesup2.php\">
				<div class=\"smenu"; if($page=='pagesup2'){echo "Actif";} echo"\">
					<p>".$titresup2."</p>
				</div>
			</a>";			
			}?>
		</div>
	</div>
	
	<?php
	$time=time();
	// recup des 5 derniers evenements de l'agenda
		if (!($recup=mysql_query("SELECT `titre`,`id_agenda`,`rubrique`,`date_evenement` FROM `agenda` WHERE `publication` = 'OUI' AND `date_evenement` >$time  ORDER BY `date_evenement` LIMIT 0 , 5 "))) {
			die('Erreur : ' . mysql_error());
		}
		$infoagenda=null;
		while($ligne=mysql_fetch_row($recup))
		{
			$titre_agenda= stripslashes($ligne[0]);
			$id_agenda= $ligne[1];
			$rub=$ligne[2];
			$dateevt=$ligne[3];
			$dateevt=date('d/m/y à H/h',$dateevt);
			if($infoagenda!=null){$infoagenda=$infoagenda."<br>";}
			$infoagenda=$infoagenda."<span class='pasimportant'>Le".$dateevt.":</span><br><a href=\"/index.php?vers=agenda.php?action=lire\">".$titre_agenda."</a>";
		}
	// recup derniere actualités
		if (!($recup=mysql_query("SELECT `titre`,`id_article`,`timestamp` FROM `articles` WHERE `publication` = 'OUI' ORDER BY `timestamp` DESC LIMIT 0 , 5 "))) {
			die('Erreur : ' . mysql_error());
		}
		$infoactu=null;
		while($ligne=mysql_fetch_row($recup))
		{
			$titre_actu= stripslashes($ligne[0]);
			$id_actu= $ligne[1];
			$date_actu=$ligne[2];
			$date_actu=date('d/m/y',$date_actu);
			if($infoactu!=null){$infoactu=$infoactu."<br>";}
			$infoactu=$infoactu."<span class='pasimportant'>Le".$date_actu.":</span><br><a href=\"/index.php?vers=actualite.php?action=lire\"> ".$titre_actu."</a>";
		}
	// les derniers services ajouté dans les profils
		if (!($recup=mysql_query("SELECT `id_seliste`,`id_rubrique`, `time`, `description` FROM `services` WHERE `etat`='DEM' ORDER BY `time` DESC LIMIT 0 , 5 "))) {
			die('Erreur : ' . mysql_error());
		}
		$infoserv=null;
		while($ligne=mysql_fetch_row($recup))
		{
			$id_sel_serv=$ligne[0];
			$id_rubrique= $ligne[1];
			$dateserv=$ligne[2];
			$description = stripslashes($ligne[3]);
			$dateserv=date('d/m/y',$dateserv);
			// recup de son prenom
			if (!($recup2=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`='$id_sel_serv'")))) {
				die('Erreur : ' . mysql_error());
			} 
			$prenom_sel_serv=stripslashes($recup2[0]);	
			$designation_ss_rub=$description;
			// recu nom rubrique
			if (!($ligne2=mysql_fetch_row(mysql_query("SELECT `id_cat`,`designation` FROM `rubrique` WHERE `id_rubrique`=$id_rubrique")))) {
				die('Erreur : ' . mysql_error());
			}
			$id_cat=$ligne2[0];
			$designation_rub=$ligne2[1];
			if($infoserv!=null){$infoserv=$infoserv."<br>";}
			$infoserv=$infoserv."<span class=\"pasimportant\"><a href='/index.php?vers=profil.php?id=".$id_sel_serv."'>".$prenom_sel_serv."(".$id_sel_serv.")</a> le ".$dateserv."</span><br><a href=\"/index.php?vers=catalogue.php\">$designation_rub - $designation_ss_rub</a>";
		}
	// les derniers services ajouté dans les profils
		if (!($recup=mysql_query("SELECT `id_seliste`,`id_rubrique`, `time`, `description` FROM `services` WHERE `etat`='PRO' ORDER BY `time` DESC LIMIT 0 , 5 "))) {
			die('Erreur : ' . mysql_error());
		}
		$infoserv2=null;
		while($ligne=mysql_fetch_row($recup))
		{
			$id_sel_serv=$ligne[0];
			$id_rubrique= $ligne[1];
			$dateserv=$ligne[2];
			$dateserv=date('d/m/y à H/h',$dateserv);
			// recup de son prenom
			if (!($recup2=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`='$id_sel_serv'")))) {
				die('Erreur : ' . mysql_error());
			} 
			$prenom_sel_serv=stripslashes($recup2[0]);
			$designation_ss_rub=stripslashes($ligne[3]);
			// recu nom rubrique
			if (!($ligne2=mysql_fetch_row(mysql_query("SELECT `id_cat`,`designation` FROM `rubrique` WHERE `id_rubrique`=$id_rubrique")))) {
				die('Erreur : ' . mysql_error());
			}
			$id_cat=$ligne2[0];
			$designation_rub=$ligne2[1];
			if($infoserv2!=null){$infoserv2=$infoserv2."<br>";}
			$infoserv2=$infoserv2."<span class=\"pasimportant\"><a href='/index.php?vers=profil.php?id=".$id_sel_serv."'>".$prenom_sel_serv."(".$id_sel_serv.")</a> le ".$dateserv."</span><br><a href=\"/index.php?vers=catalogue.php\">$designation_rub - $designation_ss_rub</a>";
		}
?>	

	
	<!--
	<div class="colonnegauche">
		<div class="colonnegauche2">
			<p><b>Dernières actualités:</b></p>
			<p><?php echo $infoactu;?></p>
		</div>
		<div class="colonnegauche2">
			<p><b>Prochain RDV:</b></p>
			<p><?php echo $infoagenda;?></p>
		</div>
	</div>
	-->
	
	
	<!--
	<div class="colonnedroite">
		<div class="colonnedroite2">
			<p><b>Dernières demandes:</b></p>
			<p><?php echo $infoserv;?></p>
		</div>
		<div class="colonnedroite2">
			<p><b>Dernière propositions:</b></p>
			<p><?php echo $infoserv2;?></p>
		</div>
	</div>
	-->
	
	<?php
	if ($lien!=null) 
	{
	echo "<br><div class='corps'><br>
	<form method=\"post\" action=\"V3/loge.php\" enctype=\"multipart/form-data\">
	<input type=\"hidden\" name=\"lien\" value=\"".$lien."\">
	<p class='t4'><legend> Vous devez vous identifier pour voir cette page : </legend>
	<label>Email ou numéro:</label>
	<input type='text' name='email' size='30' maxlength='80' >
	<label>Mot de passe:</label>
	<input type=\"password\" name=\"mdp\" size=\"30\" maxlength=\"30\">
	<label>Se souvenir:<input type=\"checkbox\" name=\"souvenir\" value=\"1\" checked=\"checked\"></label>
	<input type=\"submit\" value=\"Entrer\">   <input type=\"reset\" value=\"Effacer\"></p>
	<p class=\"pasimportant\"><a href=\"pertemdp.php\">Perte de mot de passe</a></p>
	</form></div><br>";
	
	}	
	?>
<!-- Fin de debut -->