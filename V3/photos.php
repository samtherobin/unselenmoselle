<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/

if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
 {								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
		switch ($grade)
		{
			case 'SELISTE' : include("lien/seliste.php");break;	
			case 'MODERATEUR' :include("lien/seliste.php"); include("lien/moderateur.php");break;
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page
		if($_GET['action']=='lire') //on affiche les photos enregistrés
		{	
			if (!($requete=mysql_query("SELECT * FROM `photos` WHERE id_seliste=$id_seliste ORDER BY ordre ASC"))) {
				die('Erreur : ' . mysql_error());
			}	
			echo "<br><br><div class='corps'><br><table summary=\"\" border=\"0\" width=\"100%\">";	
			$numero=0;			
			while($ligne=mysql_fetch_row($requete))
			{
				$id_photo=$ligne[0];
				if (!($maj=mysql_query("UPDATE `photos` SET `ordre` = '$numero' WHERE `id_photo` ='$id_photo' LIMIT 1") )) {
					die('Erreur : ' . mysql_error());
				}
				$numero=$numero+1;
				$nom=$ligne[1];
				$commentaire=stripslashes($ligne[4]);
				echo " 
				<tr>
  				<td class='t1'>
				<p><br>";
				if($numero!='1') { echo "<a href='photos.php?action=ordre&amp;id_photo=$id_photo&amp;ordre=Monter'><img width='15' src='images/flechehaut.gif'>Remonter la photo</a>&nbsp;&nbsp;";}
				echo "<a href='photos.php?action=ordre&amp;id_photo=$id_photo&amp;ordre=Decendre'><img width='15' src='images/flechebas.gif'>Descendre la photo</a>				
				<br>
				<br><a href='photos.php?action=supprimer&amp;id_photo=$id_photo'><img src='images/croix.jpg'> Supprimer cette photo </a><br/>
				<br>Balise: <b>[image=$nom]</b> 
				<br><span class='pasimportant'>(Sers à afficher la photo sur une actu, une annonce ou le chat...)</span></p></td>
				<td><img alt=\"photoducitoyen".$id_seliste."\" src=\"photos/".$nom.".jpg\" width=\"200\"></td>
 				</tr>
				<tr>
				<td><p class='t3'>Modifier le commentaire sur cette photo:</p></td>
				<td class='t1'><form method=\"post\" action=\"photos.php?action=commentaire&amp;id_photo=".$id_photo."\" enctype=\"multipart/form-data\"><input name='commentaire' value=\"".$commentaire."\" size=50 maxLength=50><br /></td>
				</tr>
				<tr><td colspan='2'><input type=\"submit\" name=\"envoi\" value=' Sauvegarder ' ></form><br><hr width='80%'></td></tr>";	  
			}
			echo  "
			<tr>
			<th><p>Ajouter une photo:</p>
			<p class=pasimportant>Attention les photos doivent être de moins de 1 Mo,<br>
			La miniature sera retaillée sous le format 1/2 c'est à dire 150 pixels sur 300. <br>
			Respectez la charte et les CU. Pas de sexe, ni de sang, pas d'atteinte à autrui etc.. Merci</p></th>
			<td class='t1'><form method=\"post\" action=\"photos.php?action=enregistre\" enctype=\"multipart/form-data\"><br><input type=\"file\" name=\"photo\" size=40><input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"300000\" /><br /><br /><input type=\"submit\" name=\"envoi\" value=' Envoyer la photo ' ></form></td>
			</tr></table><br>";
		}				
		elseif(($_GET['action']=='enregistre')&&(isset($_FILES['photo']))) 
		{//on enregistre
			echo "<br><br><div class='corps'><br>";
			$cle=rand(0,1000);
			$cle=$id_seliste."-".$cle;
			if(isset($_FILES['photo']))
			{// params
				unset($erreur);
				$extensions_ok = array('jpg', 'jpeg','JPG');
				$taille_max = 2000000;
				$dest_dossier = 'photos/';
				// vérifications
				if( !in_array( substr(strrchr($_FILES['photo']['name'], '.'), 1), $extensions_ok ) )
				{
					$erreur = 'Veuillez sélectionner un fichier de type jpg ou JPG ou jpeg !';
					echo "<p class='titre'>".$erreur.".</p>";
				}
				elseif( file_exists($_FILES['photo']['tmp_name'])and filesize($_FILES['photo']['tmp_name']) > $taille_max)
				{
					$erreur = 'Votre fichier doit faire moins de 2Mo !';
					echo "<p class='titre'>".$erreur.".</p>";
				}
				// copie du fichier
				if(!isset($erreur))
				{
					// copie du fichier
					$source=$_FILES['photo']['tmp_name'];
					$dest=$_FILES['photo']['tmp_name'];
					copy($source, $dest);
					move_uploaded_file($dest, $dest_dossier . $cle.".jpg");
					
					//Enregistrement miniature
					$source=$dest_dossier . $cle.".jpg";
					$dest=$dest_dossier."mini/".$cle.".jpg";
					
					$hauteur=100;
					$orig = imagecreatefromjpeg($source);
					$thumbY = $hauteur;
					$imageY = imagesy($orig);
					if ($imageY < $thumbY) {
					copy($source, $dest);
					} else {
					$imageX = imagesx($orig);
					$thumbX = $hauteur*$imageX/$imageY;
					$new = imagecreatetruecolor($thumbX, $thumbY);
					imagecopyresampled ($new, $orig, 0, 0, 0, 0, $thumbX, $thumbY, $imageX, $imageY);
					imagejpeg($new,$dest);
					imagedestroy($new);
					imagedestroy($orig);
					}
					move_uploaded_file($dest, $dest_dossier."mini/".$cle.".jpg");
					// enregistrement dans la base
					mysql_query("INSERT INTO `photos` ( `id_photo` , `nom` , `id_seliste` , `ordre` , `commentaire`) 
					VALUES ('', '$cle', '$id_seliste', '0', '')");
					echo "<p class='titre'>Fichier envoyé !</p>";
				}
			}
			echo "<br>";
		}
		elseif(($_GET['action']=='supprimer'))
		{
			$id_photo=$_GET['id_photo'];
			if (!($requete=mysql_query("SELECT `id_seliste`,`nom` FROM `photos` WHERE id_photo=$id_photo"))) {
				die('Erreur : ' . mysql_error());
			}	
			$ligne=mysql_fetch_row($requete) ;
			$id_base= $ligne[0];
			$nom= $ligne[1];
			if($id_seliste==$id_base)
			{
				echo "<br><br><div class='corps'><br><p class='titre'>Photo supprimé.<br><br>";
				$fichier = "photos/".$nom.".jpg";
				$fichier_mini="photos/mini/".$nom.".jpg";
				unlink($fichier); unlink($fichier_mini); 
				if (!(mysql_query("DELETE FROM `photos` WHERE `id_photo`=$id_photo LIMIT 1"))) {
					die('Erreur : ' . mysql_error());
				}  
			}
			else
			{
				header ("location:404.php");
				session_destroy();
			}
		}
		elseif(($_GET['action']=='ordre'))
		{
			$ordre=$_GET['ordre'];
			$id_photo=$_GET['id_photo'];
			if (!($requete=mysql_query("SELECT * FROM `photos` WHERE id_photo=$id_photo"))) {
				die('Erreur : ' . mysql_error());
			}	
			$ligne=mysql_fetch_row($requete) ;
			$id_photo= $ligne[0];
			$seliste_photo= $ligne[2];
			$ordre_photo= $ligne[3];
			if(($ordre=='Decendre')&&($id_seliste==$seliste_photo))
			{
				$ordrenew=$ordre_photo+1;
				if (!($maj=mysql_query("UPDATE `photos` SET `ordre` = '$ordre_photo' WHERE `id_seliste` ='$id_seliste' AND `ordre`='$ordrenew' LIMIT 1"))) {
					die('Erreur : ' . mysql_error());
				}
				if (!($maj=mysql_query("UPDATE `photos` SET `ordre` = '$ordrenew' WHERE `id_photo` ='$id_photo' LIMIT 1"))) {
					die('Erreur : ' . mysql_error());
				}
				echo "<br><br><div class='corps'><br>
				<br><p class='titre'>Ordre des photos modifiés<br><br>";
			}
			elseif(($ordre=='Monter')&&($id_seliste==$seliste_photo))
			{
				$ordrenew=$ordre_photo-1;
				if (!($maj=mysql_query("UPDATE `photos` SET `ordre` = '$ordre_photo' WHERE `id_seliste` ='$id_seliste' AND `ordre`='$ordrenew' LIMIT 1"))) {
					die('Erreur : ' . mysql_error());
				}
				if (!($maj=mysql_query("UPDATE `photos` SET `ordre` = '$ordrenew' WHERE `id_photo` ='$id_photo' LIMIT 1"))) {
					die('Erreur : ' . mysql_error());
				}
				echo "<br><br><div class='corps'><br>
				<br><p class='titre'>Ordre des photos modifiés<br><br>";
			}
			else
			{
				header ("location:404.php");
				session_destroy();
			}
		}
		elseif(($_GET['action']=='commentaire'))
		{
			$id_photo=$_GET['id_photo'];
			$commentaire=nl2br(htmlentities($_POST['commentaire'], ENT_QUOTES, "UTF-8"));
			if (!($requete=mysql_query("SELECT `id_seliste` FROM `photos` WHERE id_photo=$id_photo"))) {
				die('Erreur : ' . mysql_error());
			}	
			$ligne=mysql_fetch_row($requete) ;
			$id_base= $ligne[0];
			if($id_seliste==$id_base)
			{
				echo "<br><br><div class='corps'><br><p class='titre'>Commentaire enregistré.<br><br>";
				if (!($requete=mysql_query("UPDATE `photos` SET `commentaire` = '$commentaire' WHERE `id_photo` ='$id_photo' LIMIT 1") )) {
					die('Erreur : ' . mysql_error());
				}
				
			}
			else
			{
				header ("location:404.php");
				session_destroy();
			}
		}
		else
		{
			header ("location:404.php");
			session_destroy();
		}						
		echo "	
		<p><a href='photos.php?action=lire'>Retour Photos</a>  <a href='bureau.php'>Retour Bureau</a></p> 
		<br></div><br>";
	}
	else
	{ 	 // délai dépassé
		header ("location:troptard.php");
		session_destroy();
	}
}
else
{ 	 // pas de session
	header ("location:404.php");
	session_destroy();
}
mysql_close($connexion); 
echo "</center>";
include ("fin.php");	
?>
