<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/

if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
{								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade`, `prenom`, `email`, `grains` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		$prenom=$ligne[1];
		$email_seliste=$ligne[2];
		$grains_seliste=$ligne[3];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
		switch ($grade)
		{
			case 'SELISTE' : include("lien/seliste.php");break;	
			case 'MODERATEUR' :include("lien/seliste.php"); include("lien/moderateur.php");break;
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page
		echo "<br><div class=\"corps\"><br>";
		if(($_GET['action']=='valider')&&($_GET['id_ech']!=null))
		{ // valide l'echange
			$id_ech=htmlentities($_GET['id_ech'], ENT_QUOTES, "UTF-8");
			if (!($requete=mysql_query("SELECT * FROM `echanges` WHERE `id_echange`='$id_ech' LIMIT 1 "))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete) ;			
			$id_sel_fou=stripslashes($ligne[1]);
			$id_sel_dem=stripslashes($ligne[2]);
			$grains=$ligne[4];
			$etat=stripslashes($ligne[6]);
			$id_sel_enr=stripslashes($ligne[9]);
			if ($etat=='DEM')
			{ // si l'état est bien en cour de demande
				if (($id_sel_dem==$id_seliste)||($id_sel_fou==$id_seliste))
				{ // si c bien le fournisseur ou demandeur du service
					if ($id_sel_enr!=$id_seliste)
					{ // si c lui qui la enregistré il peu pas le validé!
						// test est-ce qu'on passe par un comptable ? on recupere le parametre
						if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'echauto'")))) {
							die('Erreur : ' . mysql_error());
						}
						$echauto=stripslashes($recup[0]);
						if($echauto=='OUI')
						{
							if (!(mysql_query("UPDATE `echanges` SET `etat`='VAL' WHERE `id_echange`='$id_ech' LIMIT 1"))) {
								die('Erreur : ' . mysql_error());
							}
							echo "<br><p class='t1'>Votre échange a été validé, le comptable le validera prochainement.</p><br><br>
							<p class='t4'><a href='echanges.php'>Retour aux échanges</a>&nbsp;&nbsp;&nbsp;<a href='bureau.php' title='Bureau'>Retour sur mon bureau</a></p><br><br>";
							// envoi du mail au modo
							if (!($requete=mysql_query("SELECT `texte` FROM  `variables` WHERE  `variable`='MOD1' LIMIT 1"))) {
								die('Erreur : ' . mysql_error());
							}
							$ligne=mysql_fetch_row($requete);
							$MOD1=stripslashes($ligne[0]);
							if (!($requete=mysql_query("SELECT `prenom`,`email` FROM  `selistes` WHERE  `id_seliste`='$MOD1' LIMIT 1"))) {
								die('Erreur : ' . mysql_error());
							}
							$ligne=mysql_fetch_row($requete);
							$PMOD1=stripslashes($ligne[0]);
							$MMOD1=stripslashes($ligne[1]);
							// envoi du mail:
							$message2=stripslashes("[".$nom."]MODERATEUR Comptabilité: Nouvel échange à valider");
							$headers = "From: comptabilité $nom <$email>\n";
							$headers .= "Reply-To: $nom\n";
							$headers .= "X-Sender: $nom\n";
							$headers .= "X-Author: Moderation $site\n";
							$headers .= "X-Priority:1\n";
							$headers .= "X-Mailer: PHP\n";
							$headers .= "Return_Path: <$email>\n";
							$headers .='Content-Type: text/html; charset="UTF-8"'."\n";
							$PMOD1=stripslashes($PMOD1);
							$titre=stripslashes($titre);
							$message=stripslashes($message);
							$email_dest=$ligne[1];
							// envoi du mail 								
							$mel ="<html><head><title></title></head><body>
							<p>Bonjour ".$PMOD1.", un nouvel échange vient d'être enregistré en modération par <a href=http://".$site."/index.php?vers=profil.php?id=".$id_seliste.">".$prenom."(".$id_seliste.")</a>.
							<br>Merci de vous connecter pour le valider ou le refuser.
							<br>Cordialement, <br> ".$nom.".
							<br><br><i>Vous recevez ces alertes suite à votre poste de Modérateur sur <a href=http://".$site."/> ".$site." </a>.</i></p></body></html>";
							sw_mail($MMOD1,$message2,$mel,$headers);
							// fin d'envoi du mail fin d'envoi de l'actu en moderation							
						}
						else
						{ // là on ne passe pas par un comptable on decompte les $monnaie tout de suite !
							if (!(mysql_query("UPDATE `echanges` SET `etat`='VAL' WHERE `id_echange`='$id_ech' LIMIT 1"))) {
								die('Erreur : ' . mysql_error());
							}
							// recup sous et prenom des selistes
							if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom`, `grains` FROM `selistes` WHERE `id_seliste`='$id_sel_fou'")))) {
								die('Erreur : ' . mysql_error());
							}
							$prenom_fou=stripslashes($recup[0]);
							$grains_fou=$recup[1];
							if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom`, `grains` FROM `selistes` WHERE `id_seliste`='$id_sel_dem'")))) {
								die('Erreur : ' . mysql_error());
							}
							$prenom_dem=stripslashes($recup[0]);
							$grains_dem=$recup[1];
							// mise a jour des comptes grains
							$grains_fou=($grains_fou)+($grains);
							$grains_dem=($grains_dem)-($grains);
							// enregistrement		
							if (!($requete=mysql_query("UPDATE `selistes` SET `grains` = '$grains_fou' WHERE `id_seliste` ='$id_sel_fou' LIMIT 1") )) {
								die('Erreur : ' . mysql_error());
							}
							if (!($requete=mysql_query("UPDATE `selistes` SET `grains` = '$grains_dem' WHERE `id_seliste` ='$id_sel_dem' LIMIT 1") )) {
								die('Erreur : ' . mysql_error());
							}
							if (!($requete=mysql_query("UPDATE `echanges` SET `etat` = 'OKI' WHERE `id_echange` ='$id_ech' LIMIT 1") )) {
								die('Erreur : ' . mysql_error());
							}
							echo "<br><p class='t1'>Votre échange a été validé.</p><br><br>
							<p class='t4'><a href='echanges.php'>Retour aux échanges</a>&nbsp;&nbsp;&nbsp;<a href='bureau.php' title='Bureau'>Retour sur mon bureau</a></p><br><br>";							
						}
						
					}
					else
					{
						echo "<br><p class='t1'>Vous ne pouvez pas valider un échange que vous avez enregistré.</p><br><br>
						<p class='t4'><a href='echanges.php'>Retour aux échanges</a>&nbsp;&nbsp;&nbsp;<a href='bureau.php' title='Bureau'>Retour sur mon bureau</a></p><br><br>";
					}
				}
				else
				{
					echo "<br><p class='t1'>Vous n'êtes ni le demandeur ni le fournisseur de cet échange.</p><br><br>
						<p class='t4'><a href='echanges.php'>Retour aux échanges</a>&nbsp;&nbsp;&nbsp;<a href='bureau.php' title='Bureau'>Retour sur mon bureau</a></p><br><br>";
				}
			}
			else
			{
				echo "<br><p class='t1'>L'échange est à l'état '$etat' il ne peut pas être validé.</p><br><br>
						<p class='t4'><a href='echanges.php'>Retour aux échanges</a>&nbsp;&nbsp;&nbsp;<a href='bureau.php' title='Bureau'>Retour sur mon bureau</a></p><br><br>";		
			}
		}
		elseif(($_GET['action']=='refuser')&&($_GET['id_ech']!=null))
		{ // refuser l'echange
			$id_ech=htmlentities($_GET['id_ech'], ENT_QUOTES, "UTF-8");
			if (!($requete=mysql_query("SELECT * FROM `echanges` WHERE `id_echange`='$id_ech' LIMIT 1 "))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete);			
			$id_seliste_dem=stripslashes($ligne[1]);
			$id_seliste_fou=stripslashes($ligne[2]);
			$etat=stripslashes($ligne[6]);
			$id_seliste_enr=stripslashes($ligne[9]);
			if ($etat=='DEM')
			{ // si l'état est bien en cour de demande
				if (($id_seliste_dem==$id_seliste)||($id_seliste_fou==$id_seliste))
				{ // si c bien le fournisseur ou demandeur du service
					if ($id_seliste_enr!=$id_seliste)
					{ // si c lui qui la enregistré il peu pas le validé!
						if (!(mysql_query("UPDATE `echanges` SET `etat`='REF' WHERE `id_echange`='$id_ech' LIMIT 1"))) {
							die('Erreur : ' . mysql_error());
						}
						echo "<br><p class='t1'>Votre échange à été refusé.</p><br><br>
						<p class='t4'><a href='echanges.php'>Retour aux échanges</a>&nbsp;&nbsp;&nbsp;<a href='bureau.php' title='Bureau'>Retour sur mon bureau</a></p><br><br>";	
					}
					else
					{
						echo "<br><p class='t1'>Vous ne pouvez pas refuser un échange que vous avez enregistré.</p><br><br>
						<p class='t4'><a href='echanges.php'>Retour aux échanges</a>&nbsp;&nbsp;&nbsp;<a href='bureau.php' title='Bureau'>Retour sur mon bureau</a></p><br><br>";
					}
				}
				else
				{
					echo "<br><p class='t1'>Vous n'êtes ni le demandeur ni le fournisseur de cette échange.</p><br><br>
						<p class='t4'><a href='echanges.php'>Retour aux échanges</a>&nbsp;&nbsp;&nbsp;<a href='bureau.php' title='Bureau'>Retour sur mon bureau</a></p><br><br>";
				}
			}
			else
			{
				echo "<br><p class='t1'>L'échange est à l'état '$etat' il ne peut pas être refusé.</p><br><br>
						<p class='t4'><a href='echanges.php'>Retour aux échanges</a>&nbsp;&nbsp;&nbsp;<a href='bureau.php' title='Bureau'>Retour sur mon bureau</a></p><br><br>";
			}
		}
		elseif($_GET['action']=='ajouter')
		{ // ajouter un echange
			$sens=htmlentities($_POST['sens'], ENT_QUOTES, "UTF-8");
			$id2=htmlentities($_POST['id2'], ENT_QUOTES, "UTF-8");
			$grains_ech=htmlentities($_POST['grains'], ENT_QUOTES, "UTF-8");
			$mois=htmlentities($_POST['mois'], ENT_QUOTES, "UTF-8");
			$jour=htmlentities($_POST['jour'], ENT_QUOTES, "UTF-8");
			$annee=htmlentities($_POST['annee'], ENT_QUOTES, "UTF-8");
			$detail=htmlentities($_POST['detail'], ENT_QUOTES, "UTF-8");
			
			if(($sens!=null)&&($grains_ech!=null)&&($id2!=null)&&($mois!=null)&&($jour!=null)&&($annee!=null))
			{
				$time_eve= mktime(2, $minutes,0,$mois,$jour,$annee); 
				if($sens=='ID1')
				{
					if (!(mysql_query("INSERT INTO `echanges` VALUES ('', '$id_seliste', '$id2', '$time', '$grains_ech', 'DEM','$detail', '$time_eve','$id_seliste')"))) {
						die('Erreur : ' . mysql_error());
					}
				}
				elseif($sens=='ID2')
				{
					if (!(mysql_query("INSERT INTO `echanges` VALUES ('', '$id2', '$id_seliste', '$time', '$grains_ech', 'DEM','$detail' ,'$time_eve','$id_seliste')"))) {
						die('Erreur : ' . mysql_error());
					}
				}
				if (!($requete=mysql_query("SELECT `prenom`, `email` FROM `selistes` WHERE `id_seliste`=$id2"))) {
					die('Erreur : ' . mysql_error());
				}	
				$ligne=mysql_fetch_row($requete) ;
				$prenom_dest=stripslashes($ligne[0]);
				$email_dest=$ligne[1];
				$message="Bonjour ".$prenom_dest.", un nouvel échange a été enregistré pour une valeur de ".$grains_ech."<br><br>
				<a href=\"echanges.php\"> Cliquez ici pour le valider ou le refuser</a><br>"; 
				if (!(mysql_query("INSERT INTO `messagerie` VALUES ('', '$id2', '1', '0', '$time', 'AFF', '$message')"))) {
					die('Erreur : ' . mysql_error());
				}
				$message="Bonjour ".$prenom_dest.", un nouvel échange a été enregistré pour une valeur de ".$grains_ech."<br>
				<a href=\"http://".$site."?lien=echanges.php\"> Cliquez ici pour le valider ou le refuser</a>"; 
				// envoi du mail 
				$message=stripslashes($message);
				$headers = "From: $nom <$email>\n";
				$headers .= "X-Sender: $nom\n";
				$headers .= "X-Author: Echange $site\n";
				$headers .= "X-Priority:1\n";
				$headers .= "X-Mailer: PHP\n";
				$headers .= "Return_Path: <$email_seliste>\n";
				$headers .='Content-Type: text/html; charset="UTF-8"'."\n";
				$message="Bonjour, un échange avec <a href=http://".$site."/index.php?vers=profil.php?id=".$id_seliste.">".$prenom."(".$id_seliste.")</a>  a été ajouté pour une valeur de ".$grains_ech." ".$monnaie.". Pour le valider ou le refuser rendez-vous sur <a href=http://".$site."/index.php?vers=echanges.php>".$site."</a>."; 
				$mel ='<html><head><title></title></head><body><p>'.$message.'<br><br><br><br><i>Vous recevez ces alertes parce que vous êtes inscrit sur :<br><a href=http://'.$site.'/index.php?vers=compte.php> '.$site.'</a>.</i></p></body></html>';
				sw_mail($email_dest,'['.$nom.'] Un nouvel échange a été ajouté par '.$prenom.' ('.$id_seliste.')',$mel,$headers);
				echo "<br><p class='t1'>Votre échange a bien été enregistré.</p><br><br>
						<p class='t4'><a href='echanges.php'>Retour aux échanges</a>&nbsp;&nbsp;&nbsp;<a href='bureau.php' title='Bureau'>Retour sur mon bureau</a></p><br><br>";
			}
			else   // tout les champs ne sont pas complété
			{
				echo "<br><p span class='rouge t1'>TOUT les champs doivent être complétés</p><br><br>
					<p class='t4'><a href='echanges.php'>Retour aux échanges</a>&nbsp;&nbsp;&nbsp;<a href='bureau.php' title='Bureau'>Retour sur mon bureau</a></p><br><br>";
			}
		}						
		else
		{ // voir mes echanges et  formulaire
			echo "
			<form method=\"post\" action=\"echanges.php?action=ajouter\" enctype=\"multipart/form-data\">
			<table summary=\"\" class='tablevu' width=\"90%\">
			<tr>
				<td colspan='3'><p align='center' class='titre'>Ajouter un échange:</p></td>
			</tr>";
			$listeactif='';
			if (!($requete=mysql_query("SELECT `id_seliste`, `prenom`, `nom` FROM `selistes` WHERE `valide`='OUI' ORDER BY `prenom`, `id_seliste` ASC "))) {
				die('Erreur : ' . mysql_error());
			}
			while($ligne=mysql_fetch_row($requete))
			{
				$id_membre= $ligne[0];
				$prenom_membre=stripslashes($ligne[1]);
				$nom_membre=stripslashes($ligne[2]);
				//if ($nom_membre != '')
				//    $prenom_membre = $prenom_membre.' '.mb_strtoupper($nom_membre, 'HTML-ENTITIES');
				$listeactif=$listeactif."<option value='$id_membre'>$prenom_membre ($id_membre)</option>";
			}
		// rapatriement parametre
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'monnaie'")))) {
				die('Erreur : ' . mysql_error());
			}
			$monnaie=stripslashes($recup[0]);	
			echo "
			<tr>
				<td class=t2><p><select name='sens'><option value='ID1'>J'ai</option>
										<option value='ID2'>On m'a</option></select> rendu un service ou cédé un objet.</p>
			</td>
			<tr>
				<td class=t2><p>L'échange s'est passé avec le/la séliste: <select name='id2'><option value=''>- Selection du seliste -</option>".$listeactif."</select>
				</p></td>
			</tr>
			<tr>
			<td class='t2'><p>Date de l'échange:
			<input type='text' name='jour' size='2' maxlength='2' /> /
			<input type='text' name='mois' size='2' maxlength='2' value='".$mois."' /> /
			<input type='text' name='annee' size='4' maxlength='4' value='".$annee."' /></p></td>
			</tr>
			<tr>
				<td class=t2><p>L'échange a pour valeur <input name='grains' size=5 maxLength=5> $monnaie.</p></td>
			</tr>
			<tr>
				<td class=t2><p>Description: <input name='detail' size=100 maxLength=200></p></td>
			</tr>
			<tr>
			<td colspan='3' class='t3'><input type='submit' value=' Sauvegarder ' ></td>
			</tr>";
			echo "</table></form><br>";
			// affichage des échanges
			if (!($requete1=mysql_query("SELECT * FROM `echanges` where `id_seliste_fou`='$id_seliste' AND NOT `etat`='OKI' ORDER BY `date_ech` ASC LIMIT 20"))) {
				die('Erreur : ' . mysql_error());
			}
			// rapatriement parametre
		if (!($query = mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'monnaie'"))) {
			die('Erreur : ' . mysql_error());
		}
		$recup=mysql_fetch_row($query);
		$monnaie=stripslashes($recup[0]);	
			echo "<table summary=\"\" class='tablevu' border='1' width=\"90%\">
			<tr>
				<td colspan='9'><p class='titre'>Mes échanges en cours:</p></td>
			</tr>
			<tr>
				<td class=t2><p>Seliste concerné:</p></td><td class=t2><p>Date enregistrement:</p></td><td class=t2><p>Date échange:</p></td><td class=t3><p>$monnaie:</p></td><td class=t3><p>Rubrique:</p></td><td class=t3><p>Description:</p></td><td class=t3><p>Etat:</p></td><td class=t3><p>Enregistré par:</p></td><td class=t3><p>Action:</p></td>
			<tr>
				<td class=t1 colspan='9'><p class='t4'>Mes services donnés:</p></td>
			</tr>";
			while($ligne=mysql_fetch_row($requete1))
			{
				$id_echange=$ligne[0];
			//	$id_seliste=$ligne[1];   cc'est moi !!!
				$id_sel=$ligne[2];
				$detail=stripslashes($ligne[7]);
				if (!($requete2=mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`='$id_sel'") )) {
					die('Erreur : ' . mysql_error());
				}
				$ligne2=mysql_fetch_row($requete2) ;
				$prenom_sel=stripslashes($ligne2[0]);
				$date=$ligne[3];
				$date=date('d/m/y à H\h i\m\i\n',$date);		
				$grains=$ligne[4];
				$id_rubrique=$ligne[5];
				$description=stripslashes($ligne[10]);
				$id_rubrique=$ligne3[1];
				if (!($requete3=mysql_query("SELECT `designation` FROM `rubrique` where `id_rubrique`='$id_rubrique'"))) {
					die('Erreur : ' . mysql_error());
				}
				$ligne3=mysql_fetch_row($requete3) ;
				$designation_rub=stripslashes($ligne3[0]);
				$etat=$ligne[6];
				$id_sel_enr=$ligne[9];
				if (!($requete2=mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`='$id_sel_enr'") )) {
					die('Erreur : ' . mysql_error());
				}
				$ligne2=mysql_fetch_row($requete2) ;
				$prenom_sel_enr=stripslashes($ligne2[0]);			
				if($etat=='DEM'){$etatd="Demande en cours, le deuxième séliste doit le valider.";}
				elseif($etat=='VAL'){$etatd="Attente de validation par le comptable.";}
				elseif($etat=='REF'){$etatd="ECHANGE REFUSE!";}	
				else {}
				$date_eve=$ligne[8];
				$date_mois=$date_eve;
				$date_eve=date('d/m/y',$date_eve);
				$time_mois=$time-2629743;
				if(($etat=='REF')&&($time_mois>$date_mois))
				{
				}
				else
				{
					echo "<tr height='80px'><td class=t2><p><a href='profil.php?id=".$id_sel."'>".$prenom_sel."(".$id_sel.")</a></p></td><td class=t2><p>".$date."</p></td><td class=t2><p>".$date_eve."</p></td><td class=t3><p>".$grains. " ".$monnaie."</p></td><td class=t3><p>".$description."</p></td><td class=t3><p class='pasimportant'>".$detail."</p></td><td class=t3><p>".$etatd."</p></td><td class=t3><p><a href='profil.php?id=".$id_sel_enr."'>".$prenom_sel_enr."(".$id_sel_enr.")</a></p></td>";
					if(($id_sel_enr==$id_seliste)||($etat!='DEM'))
					{
						echo "<td class=t3><p>Attente</p></td></tr>";
					}
					else
					{
						echo "<td class=t1 width='90px'>				
						<a href='echanges.php?action=valider&id_ech=$id_echange'><img src=\"images/coche.jpg\">Valider</a><br><br>
						<a href='echanges.php?action=refuser&id_ech=$id_echange'><img src=\"images/croix.jpg\">Refuser</a>
						</td></tr>";
					}				
					
				}
			}
			if (!($requete1=mysql_query("SELECT * FROM `echanges` where `id_seliste_dem`='$id_seliste' AND NOT `etat`='OKI' ORDER BY `date_ech` ASC"))) {
				die('Erreur : ' . mysql_error());
			}
			echo "
			<tr>
				<td colspan='9' class=t1><p class='t4'>Mes services reçus:</p></td>
			</tr>";
			while($ligne=mysql_fetch_row($requete1))
			{
				$id_echange=$ligne[0];
				$id_sel=$ligne[1]; 
				$detail=stripslashes($ligne[7]);
			//	$id_seliste=$ligne[2];  c'est moi !! 
				if (!($requete2=mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`='$id_sel'") )) {
					die('Erreur : ' . mysql_error());
				}
				$ligne2=mysql_fetch_row($requete2) ;
				$prenom_sel=stripslashes($ligne2[0]);
				$date=$ligne[3];
				$date=date('d/m/y à H\h i\m\i\n',$date);		
				$grains=$ligne[4];
				$id_rubrique=$ligne[5];
				$description=stripslashes($ligne[10]);
				if (!($requete3=mysql_query("SELECT `designation` FROM `rubrique` where `id_rubrique`='$id_rubrique'"))) {
					die('Erreur : ' . mysql_error());
				}
				$ligne3=mysql_fetch_row($requete3) ;
				$designation_rub=stripslashes($ligne3[0]);
				$etat=$ligne[6];
				$id_sel_enr=$ligne[9];
				if (!($requete2=mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`='$id_sel_enr'") )) {
					die('Erreur : ' . mysql_error());
				}
				$ligne2=mysql_fetch_row($requete2) ;
				$prenom_sel_enr=stripslashes($ligne2[0]);	
				if($etat=='DEM'){$etatd="Demande en cours, le deuxième séliste doit le valider.";}
				elseif($etat=='VAL'){$etatd="Attente de validation par le comptable.";}
				elseif($etat=='REF'){$etatd="ECHANGE REFUSE!";}	
				else {}
				$date_eve=$ligne[8];
				$date_mois=$date_eve;
				$date_eve=date('d/m/y',$date_eve);
				$time_mois=$time-2629743;
				if(($etat=='REF')&&($time_mois>$date_mois))
				{
				}
				else
				{
					echo "<tr height='80px'><td class=t2><p><a href='profil.php?id=".$id_sel."'>".$prenom_sel."(".$id_sel.")</a></p></td><td class=t2><p>".$date."</p></td><td class=t2><p>".$date_eve."</p></td><td class=t3><p>".$grains." ".$monnaie."</p></td><td class=t3><p>".$description."</p></td><td class=t3><p class='pasimportant'>".$detail."</p></td><td class=t3><p>".$etatd."</p></td><td class=t3><p><a href='profil.php?id=".$id_sel_enr."'>".$prenom_sel_enr."(".$id_sel_enr.")</a></p></td>";	
					if(($id_sel_enr==$id_seliste)||($etat!='DEM'))
					{
						echo "<td class=t3><p>Attente</p></td></tr>";
					}
					else
					{
						echo "<td class=t1 width='90px'>						
							<a href='echanges.php?action=valider&id_ech=$id_echange'><img src=\"images/coche.jpg\">Valider</a><br><br>
							<a href='echanges.php?action=refuser&id_ech=$id_echange'><img src=\"images/croix.jpg\">Refuser</a>
							</td></tr>";
					}				
					
				}
			}
			echo "</table>";
			echo "<br/>";
			// mes vieux echanges
			if (!($requete1=mysql_query("SELECT * FROM `echanges` where  (`etat`='OKI' AND `id_seliste_fou`='$id_seliste') OR (`etat`='OKI' AND `id_seliste_dem`='$id_seliste') ORDER BY `date_ech` DESC "))) {
				die('Erreur : ' . mysql_error());
			}
			echo "<table summary=\"\" class='tablevu' border='1' width=\"90%\">
			<tr>
				<td colspan='10'><p class='titre'>Mes anciens échanges:</p></td>
			</tr>
			<tr>
				<td class=t1><p>Séliste</p></td>			
				<td class=t1><p>Description</p></td>
				<td class=t1><p>Date échange</p></td>
				<td class=t1><p>Valeur<br/>($monnaie)</p></td>
				<td class=t1><p>Mon solde<br/>($monnaie)</p></td>
				<td class=t1><p>État</p></td>
			</tr>";
			$grains_cal=$grains_seliste;
			while($ligne=mysql_fetch_row($requete1))
			{
				$id_echange=$ligne[0];
				$id_sel2=$ligne[1];
				if (!($requete5=mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`='$id_sel2'") )) {
					die('Erreur : ' . mysql_error());
				}
				$ligne5=mysql_fetch_row($requete5) ;
				$prenom_sel2=stripslashes($ligne5[0]);
				$id_sel=$ligne[2];
				if (!($requete2=mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`='$id_sel'") )) {
					die('Erreur : ' . mysql_error());
				}
				$ligne2=mysql_fetch_row($requete2) ;
				$prenom_sel=stripslashes($ligne2[0]);
				$date=$ligne[3];
				$date=date('d/m/y',$date);		
				$grains=$ligne[4];
				$description=stripslashes($ligne[10]);
				$etat=$ligne[5];
				$detail=stripslashes($ligne[6]);
				if($etat=='DEM'){$etat="Demande en cours";}
				elseif($etat=='VAL'){$etat="Attente comptable";}
				elseif($etat=='REF'){$etat="Refusé";}
				elseif($etat=='OKI'){$etat="Validé";}	
				$date_eve=$ligne[7];
				$date_eve=date('d/m/y',$date_eve);
				$id_sel_enr=$ligne[8];
				if (!($requete2=mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`='$id_sel_enr'") )) {
					die('Erreur : ' . mysql_error());
				}
				$ligne2=mysql_fetch_row($requete2) ;
				$prenom_sel_enr=stripslashes($ligne2[0]);
				$couleur = 'bleu';
				$id_sel_aff = $id_sel;
				if($id_seliste==$id_sel)
				{
				    $couleur = 'rouge';
				    $id_sel_aff = $id_sel2;
				    $prenom_sel = $prenom_sel2;
				}
				// si  je fourni
				echo "<tr><td class=t2><p><a href='profil.php?id=".$id_sel_aff."'><span class=".$couleur.">".$prenom_sel."(".$id_sel_aff.")</span></a></p></td><td><p>".$detail."</p></td><td><p class='pasimportant'>".$date_eve."</p></td><td class=t3><p><span class='".$couleur."'>".$grains."</span></p></td><td class=t3><p>".$grains_cal."</p></td><td class=t3><p>".$etat."</p></td></tr>";
				if($id_seliste==$id_sel)
				{
				    $grains_cal=$grains_cal+$grains;
				} else {
				    $grains_cal=$grains_cal-$grains;
				}

				
			}
			echo "</table><br>";
		}
		echo "</div>";
	}
	else
	{ 	 // délai dépassé
		header ("location:troptard.php");
		session_destroy();
	}
 }
else
{ 	 // pas de session
	header ("location:404.php");
	session_destroy();
}
mysql_close($connexion);
include ("fin.php");	
?>
