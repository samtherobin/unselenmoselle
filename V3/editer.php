<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/
if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
{								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
		switch ($grade)
		{
			case 'SELISTE' : include("lien/seliste.php");break;	
			case 'MODERATEUR' :include("lien/seliste.php"); include("lien/moderateur.php");break;
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page
		//debut de l'edition
		if($_GET['action']=='edition') //on enregistre les modifs
		{	
			if (!($requete=mysql_query("SELECT `grade`, `prenom`, `nom`, `sexe`,`age`, `cdp`, `ville`, `pays`, `tel`, `email`, `telport`, `adresse`, `profil`, `nee`, `quartier`  FROM `selistes` WHERE `id_seliste`='$id_seliste'"))) {
				die('Erreur : ' . mysql_error());
			}	
			$ligne=mysql_fetch_row($requete) ;
			$grade=$ligne[0];
			$prenom=stripslashes($ligne[1]);
			$nom=stripslashes($ligne[2]);
			$sexe=$ligne[3];
			$age=$ligne[4];
			$cdp=stripslashes($ligne[5]);
			$ville=stripslashes($ligne[6]);
			$pays=stripslashes($ligne[7]);
			$tel=$ligne[8];
			$email=$ligne[9];
			$telport=$ligne[10];
			$adresse=stripslashes($ligne[11]);
			$profil=stripslashes($ligne[12]);
			$nee=stripslashes($ligne[13]);
			$quartier=stripslashes($ligne[14]);
			/*requete pour selectionner les villes*/
			if (!($requete2=mysql_query("SELECT `ville` FROM `selistes` WHERE `ville`!='' GROUP BY `ville` Order by `ville` ASC") )) {
				die('Erreur : ' . mysql_error());
			}
			echo  "<br><br><div class='corps'><br>
			<p class='titre'>Edition de votre profil</p><br>
			<form method=\"post\" action=\"editer.php?action=enregistre\" enctype=\"multipart/form-data\">
			<table summary=\"\" border='0' width='100%' cellpadding='3'>
			<tr>
			<td colspan='2' bgcolor='#FF0000'><p class='t1'>Sauvegardez votre profil régulièrement!</p></td>
			</tr>
			<tr>
			<td><p class='t3'><b>Prenom:</b></p></td>
			<td><p class='t2'><input name='prenom' value='".$prenom."' size=10 maxLength=30></p></td>
			</tr>
			<tr>
			<td><p class='t3'><b>Email:</b><br><span class='pasimportant'>Ne sera pas affiché sur votre profil.</span></p></td>
			<td><p class='t2'><input name='email' value='".$email."' size=30 maxLength=50></p></td>
			</tr>
			<tr>
			<td><p class='t3'><b>Nom:</b></p></td>
			<td><p class='t2'><input name='nom' value='".$nom."' size=10 maxLength=30></p></td>
			</tr>
			<tr>
			<td><p class='t3'><b>Date de naissance:</b><br><span class='pasimportant'>25/05/1980</span></p></td>
			<td><p class='t2'><input name='nee' value='".$nee."' size=10 maxLength=10></p></td>
			</tr>
			<tr>
			<td><p class='t3'><b>Age:</b></p></td>
			<td><p class='t2'><input name='age' value='".$age."' size=2 maxLength=2></p></td>
			</tr>
			<tr>
			<td><p class='t3'><b>Adresse:</b></p></td>
			<td><p class='t2'><input name='adresse' value='".$adresse."' size=50 maxLength=100></p></td>
			</tr>
			<tr>
			<td><p class='t3'><b>Code postal:</b></p></td>
			<td><p class='t2'><input name='cdp' value='".$cdp."' size=5 maxLength=5></p></td>
			</tr>
			<tr>
			<td><p class='t3'><b>Ville:</b></p></td>
			<td><p class='t2'><select name=\"ville\"><option value=\"\"> --- Ville ---</option>";
			// menu déroulant des villes
			while($ligne2=mysql_fetch_row($requete2))
		  	{	
				$ville2=stripslashes($ligne2[0]);
				echo "<option value=\"".$ville2."\"";
				if($ville==$ville2)
				{
					echo "selected";
				}
				echo " >".$ville2."</option>";
			}
			echo "</select><br>Votre ville n'est pas dans la liste? Ajoutez-la:<br><span class='pasimportant'>Attention à bien l'écrire ;)</span><br><input name='ville2' value='' size=20 maxLength=30></p></td>
			</tr>
			<tr>
			<td><p class='t3'><b>Quartier:</b><br><span class='pasimportant'>Facultatif</span></p></td>
			<td><p class='t2'><input name='quartier' value='".$quartier."' size=20 maxLength=30></p></td>
			</tr>
			<tr>
			<td><p class='t3'><b>Pays:</b></p></td>
			<td><p class='t2'><input name='pays' value='".$pays."' size=20 maxLength=20></p></td>
			</tr>
			<tr>
			<td><p class='t3'><b>Tél:</b><br><span class='pasimportant'>Facultatif</span></p></td>
			<td><p class='t2'><input name='tel' value='".$tel."' size=12 maxLength=10></p></td>
			</tr>
			<tr>
			<td><p class='t3'><b>Tél2:</b><br><span class='pasimportant'>Facultatif</span></p></td>
			<td><p class='t2'><input name='telport' value='".$telport."' size=12 maxLength=10></p></td>
			</tr>

			<tr>
			<td><p class='t3'><b>Genre:</b></p></td>
			<td><p class='t2'><select name='sexe'>
			<option value='".$sexe."' >".$sexe."</option>
			<option value='Femme' >Femme</option>
			<option value='Homme' >Homme</option>
			<option value='Couple' >Couple</option>
			</select></p></td>			
			</tr>
			<tr>
			<td><p class='t3'><b>Description:</b><br><span class='pasimportant'>Facultatif</span></p></td>
			<td><p class='t2'><textarea name='profil' cols=70 rows=10 >".$profil."</textarea></p></td>
			</tr>
			<tr>
			<td colspan='3' ><p class='t1'><input type='submit' value=' Sauvegarder ' ></p></td>
			</tr>
			</table>			
			</form><br></div><br>";
		}
		else //on enregistre
		{
			if($_GET['action']=='enregistre')
			{ //ok pour enregistre
				$prenom=htmlentities($_POST['prenom'], ENT_QUOTES, "UTF-8");
				$nom=htmlentities($_POST['nom'], ENT_QUOTES, "UTF-8");
				$email=htmlentities($_POST['email'], ENT_QUOTES, "UTF-8");
				$sexe=htmlentities($_POST['sexe'], ENT_QUOTES, "UTF-8");
				$age=htmlentities($_POST['age'], ENT_QUOTES, "UTF-8");
				$cdp=htmlentities($_POST['cdp'], ENT_QUOTES, "UTF-8");
				/*test si ajouter une ville ?*/
				$ville2=htmlentities($_POST['ville2'], ENT_QUOTES, "UTF-8");
				$ville=htmlentities($_POST['ville'], ENT_QUOTES, "UTF-8");
				if($ville2!=null){$ville=$ville2;}
				$nee=htmlentities($_POST['nee'], ENT_QUOTES, "UTF-8");
				$quartier=htmlentities($_POST['quartier'], ENT_QUOTES, "UTF-8");
				$pays=htmlentities($_POST['pays'], ENT_QUOTES, "UTF-8");
				$tel=htmlentities($_POST['tel'], ENT_QUOTES, "UTF-8");
				$telport=htmlentities($_POST['telport'], ENT_QUOTES, "UTF-8");
				$adresse=htmlentities($_POST['adresse'], ENT_QUOTES, "UTF-8");
				$profil=nl2br(htmlentities($_POST['profil'], ENT_QUOTES, "UTF-8"));
				if (!($requete=mysql_query("UPDATE `selistes` SET `email` = '$email', `prenom` = '$prenom', `nom` = '$nom', `age` = '$age', `pays` = '$pays', `ville` = '$ville', `sexe` = '$sexe', `adresse` = '$adresse', `tel` = '$tel',`telport` = '$telport',`cdp` = '$cdp', `profil` = '$profil', `nee` = '$nee', `quartier` = '$quartier' WHERE `id_seliste` ='$id_seliste' LIMIT 1"))) {
					die('Erreur : ' . mysql_error());
				}
				echo "<br><br><div class='corps'><br>
				<p class='titre'>Votre profil : <a href='profil.php?id=".$id_seliste."'>".$prenom."(".$id_seliste.")</a> est mis à jour merci.</p>
				<br><p><a href='bureau.php' title='Bureau'>Retour bureau</a></p><br></div><br>";
			}
			else
			{ 	//pirate
				header ("location:404.php");
				session_destroy();			
			}
		}
		//fin edition
	} 
	//delai depassé
	else
	{
		header ("location:troptard.php");
		session_destroy();
	}
}
// pas de sesion
else
{
	header ("location:404.php");
	session_destroy();
}
mysql_close($connexion); 
include ("fin.php");	
?>
