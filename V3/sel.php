<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/

if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
 {								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
		switch ($grade)
		{
			case 'SELISTE' : include("lien/seliste.php");break;	
			case 'MODERATEUR' :include("lien/seliste.php"); include("lien/moderateur.php");break;
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page
		// rapatriement texte
		if (!($recup=mysql_fetch_row(mysql_query("SELECT `description` FROM `autresel` WHERE `nom` = 'adressefrance'")))) {
			die('Erreur : ' . mysql_error());
		}
		$textenational=stripslashes($recup[0]);	
		if (!($recup=mysql_fetch_row(mysql_query("SELECT `description` FROM `autresel` WHERE `nom` = 'adresselocal'")))) {
			die('Erreur : ' . mysql_error());
		}
		$textelocal=stripslashes($recup[0]);	
		if (!($recup=mysql_fetch_row(mysql_query("SELECT `description` FROM `autresel` WHERE `nom` = 'selidaire'")))) {
			die('Erreur : ' . mysql_error());
		}
		$selidaire=stripslashes($recup[0]);	
		if (!($recup=mysql_fetch_row(mysql_query("SELECT `description` FROM `autresel` WHERE `nom` = 'routesel'")))) {
			die('Erreur : ' . mysql_error());
		}
		$routesel=stripslashes($recup[0]);	
		if (!($recup=mysql_fetch_row(mysql_query("SELECT `description` FROM `autresel` WHERE `nom` = 'routestage'")))) {
			die('Erreur : ' . mysql_error());
		}
		$routestage=stripslashes($recup[0]);	
		if (!($recup=mysql_fetch_row(mysql_query("SELECT `description` FROM `autresel` WHERE `nom` = 'fri'")))) {
			die('Erreur : ' . mysql_error());
		}
		$fri=stripslashes($recup[0]);
		echo "<br><div class=\"corps\"><br>
		
		<p class='titre'>Bonjour, bienvenue sur la page des autres SEL. </p>";
		if($grade=='ADMIN'||$grade=='MODERATEUR'){echo "<br><p><a class='amodo' href='editer_sel.php?action=editer'>Editer la page</a></p>";}	
		echo "
		<br>
		<div class='message'><p class='titre'>Niveau national:</p><p class='t2'>".$textenational."</p></div><br>
		<div class='message'><p class='titre'>Niveau régional:</p><p class='t2'>".$textelocal."</p></div><br>
		<div class='message'><p class='titre'>Sélidaire:</p><p class='t2'>".$selidaire."</p></div><br>
		<div class='message'><p class='titre'>Route des SEL:</p><p class='t2'>".$routesel."</p></div><br>
		<div class='message'><p class='titre'>Route des stages:</p><p class='t2'>".$routestage."</p></div><br>
		</div>";
	}
	else
	{ 	 // délai dépassé
		header ("location:troptard.php");
		session_destroy();
	}
 }
else
{ 	 // pas de session
	header ("location:404.php");
	session_destroy();
}
mysql_close($connexion); 
echo "</center>";
include ("fin.php");	
?>
