<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/
 
if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
{								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade`, `prenom`, `email`, `grains` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		$prenom=$ligne[1];
		$email=$ligne[2];
		$grains_seliste=$ligne[3];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
		switch ($grade)
		{
			case 'SELISTE' : header("location:404.php");break;		
			case 'MODERATEUR' :header("location:404.php");break;		
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page
		//  cettte page sers à initialiser le mot de passe du seliste les admin voit le mdp donné pas les modérateurs
		if(($_GET['action']=='initmdp')&&($_GET['id_sel']!=null))
			{ // changer le mot de passe
				$id_sel=$_GET['id_sel'];
				if (!($requete=mysql_query("SELECT `prenom`, `email` FROM `selistes` WHERE `id_seliste`='$id_sel' LIMIT 1"))) {
					die('Erreur : ' . mysql_error());
				}
				$ligne=mysql_fetch_row($requete);
				$email_dest=$ligne[1];
				$prenom_sel=$ligne[0];
				$id_sel=$_GET['id_sel'];
				$codealea=rand(10000,30000);
				$mdp=md5($codealea);
				// envoi du mail 
					$headers = "From: ".$nom." <".$email.">\n";
					$headers .= "Reply-To: ".$email."\n";
					$headers .= "X-Sender: ".$email."\n";
					$headers .= "X-Author: Administrateur ".$nom."\n";
					$headers .= "X-Priority:1\n";
					$headers .= "X-Mailer: PHP\n";
					$headers .= "Return_Path: <".$email.">\n";
					$headers .='Content-Type: text/html; charset="iso-8859-1"'."\n";
					$mel ='<html><head><title></title></head><body><p>Bonjour '.$prenom_sel.',<br>ton mot de passe vient d\'être changé suite à ta demande.
					Ton nouveau mot de passe est :<b>  '.$codealea.' </b><br>N\'hésite pas à le changer pour cela rendez-vous sur
					<a href=\'http://'.$site.'/index.php?vers=changemdp.php\'>'.$site.'</a> sur "changer mon mot de passe" en haut à gauche.
					<br><br>N\'hésite pas si tu as des remarques suggestions ou des questions ;) <br>@ Bientôt.<br><i>
					Si vous ne souhaitez plus avoir ces alertes envoyez un mail à: '.$email.'</i></p></body></html>';
					sw_mail($email_dest,'['.$nom.'] Alerte de sécurité votre mot de passe à changer',$mel,$headers);
					if (!($requete2=mysql_query("UPDATE `selistes` SET `mdp` = '$mdp', `reinscription`='$annee', `valide`='OUI' WHERE `id_seliste` ='$id_sel' LIMIT 1"))) {
						die('Erreur : ' . mysql_error());
					}
					echo "<br><div class='corps'><br>
					<p>Mot de passe Ré-initialisé pour ce séliste + Email à $email_dest + fiche valide et réinscription Ok<br>";
					// affichage du mdp si admin:
					if($grade=='ADMIN'){ echo " Voici son mot de passe: $codealea";}
					echo "</p></div><br>";		
			}
	}
	else
	{ 	 // délai dépassé
		header ("location:troptard.php");
		session_destroy();
	}
 }
else
{ 	 // pas de session
	header ("location:404.php");
	session_destroy();
}

include ("fin.php");	
?>
