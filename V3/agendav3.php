<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/ 
if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
{										  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade`, `nbr_art`, `inscription`, `prenom` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		$nbr_art=$ligne[1];
		$prenom=$ligne[3];
		$inscription=$ligne[2];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
	 	switch ($grade)
		{
			case 'SELISTE' : include("lien/seliste.php");break;				
			case 'MODERATEUR' :include("lien/seliste.php"); include("lien/moderateur.php");break;
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page

				
	// affichage calendrier :
	
	// savoir jour mois annee
				$time=time();
				$jour_actuel=date('d',$time);
				$mois_actuel=date('m',$time);
				$annee_actuel=date('y',$time);
				$jour=1;
				$resultat=0;
				if($_GET['mois']!=null)
				{
					$moisdemanderapres=addslashes($_GET['mois']);
					$anneedemander=addslashes($_GET['annee']);
					if ((strval(intval($moisdemanderapres)) == strval($moisdemanderapres))&&(strval(intval($anneedemander)) == strval($anneedemander)))
					{
						$mois=$moisdemanderapres;
						$annee=$anneedemander;
						if($mois>12)
						{
							$annee = $annee+1;
							$mois=1;
						}
						if($mois=='0')
						{
							$annee=$annee-1;
							$mois=12;
						}
					}
					else
					{
						$mois=$mois_actuel;
						$annee=$annee_actuel;
					}
				}
				else
				{
					$mois=$mois_actuel;
					$annee=$annee_actuel;	
				}			
				$time_premierjouraafficher = mktime(0, 0, 0, $mois, 1, $annee);
				$jourtexte=date('D',$time_premierjouraafficher);
				$moisprochain=$mois+1;		
				$moisavant=$mois-1;
				echo "<br><div class='corps'><br><p class='titre'>L'agenda de ".$nom.":</p>			
			<br>
			<p><a href=\"agenda.php?action=lire\">Voir l'agenda sous forme de liste</a>&nbsp;&nbsp;&nbsp;
			<a href=\"agenda.php?action=proposer\"><img src=\"images/editer.gif\"> Proposer un évenement dans l'agenda</a><br></p>
			<br><br><table class='tablevu' summary=\"\" border='1' cellpadding='2'>";
				// transforme jour en texte francais
				if($jourtexte=="Mon"){$jourtexte="L";}
				elseif($jourtexte=="Tue"){$jourtexte="Ma";}
				elseif($jourtexte=="Wed"){$jourtexte="Me";}
				elseif($jourtexte=="Thu"){$jourtexte="J";}
				elseif($jourtexte=="Fri"){$jourtexte="V";}
				elseif($jourtexte=="Sat"){$jourtexte="S";}
				elseif($jourtexte=="Sun"){$jourtexte="D";}
				else {}
				// transforme chiffre en mois texte
				if($mois==1){$moistexte="Janvier";}
				elseif($mois==2){$moistexte="Février";}
				elseif($mois==3){$moistexte="Mars";}
				elseif($mois==4){$moistexte="Avril";}
				elseif($mois==5){$moistexte="Mai";}
				elseif($mois==6){$moistexte="Juin";}
				elseif($mois==7){$moistexte="Juillet";}
				elseif($mois==8){$moistexte="Aout";}
				elseif($mois==9){$moistexte="Septembre";}
				elseif($mois==10){$moistexte="Octobre";}
				elseif($mois==11){$moistexte="Novembre";}
				elseif($mois==12){$moistexte="Decembre";}
				else {}
				// calcul nbre de jour dans un mois
				echo"<tr class='teinte1'><td colspan='7' height='10'><p class='titre' ><a class='aphoto' href='agendav3.php?mois=$moisavant&annee=$annee'> <img src='images/flechegauche.png' width='25px'> </a> $moistexte 20$annee <a  class='aphoto' href='agendav3.php?mois=$moisprochain&annee=$annee'> <img src='images/flechedroite.png' width='25px'> </a></th></tr>
				<tr class='teinte2'><td width='85'><p class='t1'>Lundi</p></td><td width='85'><p class='t1'>Mardi</p></td><td width='85' ><p class='t1'>Mercredi</p></td><td width='85'><p class='t1'>Jeudi</p></td><td width='85' ><p class='t1'>Vendredi</p></td><td width='85'><p class='t1'>Samedi</p></td><td 'width='85'><p class='t1'>Dimanche</p></td></tr>";
					if(($mois==1)||($mois==3)||($mois==5)||($mois==7)||($mois==8)||($mois==10)||($mois==12))
					{
						$nbrjourmois=31;
					}
					elseif($mois==2)
					{
						// si annee bissextil
						if( (is_int($annee/4) && !is_int($annee/100)) || is_int($annee/400))
						{
							$nbrjourmois=29;
						}
						else
						{
							$nbrjourmois=28;
						}
					}
					else
					{
						$nbrjourmois=30;
					}	
					$message="";
					// affichage des jours du mois
					while($jour<=$nbrjourmois)
					{ 
						// cherche si evenement ce jour
						// time du jour a 0:00
						$time_jour=strtotime($annee."-".$mois."-".$jour);
						$time_jour24=$time_jour+(24*60*60)-1;
						// recup du titre dans le journée
						$actif="non";
						if (!($recup=mysql_query("SELECT `titre`,`id_agenda`, `date_evenement`, `rubrique` FROM `agenda` WHERE `date_evenement` <=$time_jour24 AND `date_evenement` >=$time_jour AND `publication` = 'OUI' LIMIT 0 , 5 "))) {
							die('Erreur : ' . mysql_error());
						}
						while($ligne=mysql_fetch_row($recup))
						{
							$titre_agenda= stripslashes($ligne[0]);
							$id_agenda= $ligne[1];
							$date=date("d/m/Y",$ligne[2]);
							$rub= $ligne[3];
							if($rub=='INS'){$couleur="noir";}
						elseif($rub=='AUB'){$couleur="vert";}
						elseif($rub=='RAN'){$couleur="jaune";}
						elseif($rub=='REU'){$couleur="rouge";}
						elseif($rub=='SOR'){$couleur="bleu";}
						else {}
							$message=$message."<tr><td><p class='t3'>$date</p></td>
							<td><p class='t2'><a href='agenda.php?action=reponses&id=$id_agenda'><span class='$couleur'>$titre_agenda</span></a></p></td></tr>";
							$actif="OUI";
						}
						// pour placer le premier jour
						if($jour==1)
						{
							if($jourtexte=="Ma") { echo "<td>&nbsp;</td>";}
							if($jourtexte=="Me") { echo "<td>&nbsp;</td><td>&nbsp;</td>";}
							if($jourtexte=="J") { echo "<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>";}
							if($jourtexte=="V") { echo "<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>";}
							if($jourtexte=="S") { echo "<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>";}
							if($jourtexte=="D") { echo "<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>";}
						}
						if($actif=='OUI')
						{
							echo"<td class='teinte3'><p class='t1'><a href='agenda.php?action=reponses&id=$id_agenda'>$jour</a></p></td>";
							if($jourtexte=="D") { echo "</tr><tr>";}
						}
						else
						{
							// changement si c'est aujourd'hui
							if(($jour_actuel==$jour)&&($mois==$mois_actuel)&&($annee==$annee_actuel))
							{
								echo"<td style=\"border:3px solid black\" class='t1'><p class='t1'>$jour</p></td>";
								if($jourtexte=="D") { echo "</tr><tr>";}
							}
							else
							{
								// si dimanche on le met gras
								if($jourtexte=="D")
								{
									echo"
										<td><p class='t4'><b>$jour</b></p></td>
									</tr>
									<tr>
									";
								}
								else
								{
								 echo"
									<td><p class='t4'>$jour</p></td>";
								}					
							}
											
							
						}
						$jour++;
						// transforme jour +1
						if($jourtexte=="L"){$jourtexte="Ma";}
						elseif($jourtexte=="Ma"){$jourtexte="Me";}
						elseif($jourtexte=="Me"){$jourtexte="J";}
						elseif($jourtexte=="J"){$jourtexte="V";}
						elseif($jourtexte=="V"){$jourtexte="S";}
						elseif($jourtexte=="S"){$jourtexte="D";}
						elseif($jourtexte=="D"){$jourtexte="L";}
						else {}
					}
					echo"</tr></table>";
					
	echo "<br><br>
		<table class='tablevu' border='0'>
			<tr>
				<td colspan='2' class='teinte1'><p class='titre'>Evènements à venir du mois:</p></td>
			</tr>
			<tr class='teinte2'>
				<td width='15%'><p class='t1'>Date</p></td>
				<td><p class='t1'>Titre</p></td>
			</tr>
				".$message."
			</table><br><br>
			<div class='message'>
			<br><p>Code couleurs:<br>
			<span class='noir'>Prochaines réunion d'inscription.<br></span>
			<span class='vert'>Les auberges et soirées.<br></span>
			<span class='jaune'>Les randonnées et sorties nature.<br></span>
			<span class='rouge'>Réunion de ".$nom.".<br></span>
			<span class='bleu'>Toutes les autres sorties.<br></span></p>
		<br></div><br></div><br>";
		
	}
	else
	{ 	 //délai dépassé
		header ("location:troptard.php");
		session_destroy();
	}
 }
else
{ 	 // pas de session
	header ("location:404.php");
	session_destroy();
}
mysql_close($connexion); 
include ("fin.php");	
?>



