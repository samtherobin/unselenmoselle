/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	config.language = 'fr';
	config.uiColor = '#cdffce';
	config.skin = 'kama';
	config.toolbar = 'MyToolbar';
 	config.toolbar_MyToolbar =
	[
		{ name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
		{ name: 'editing', items : [ 'Scayt' ] },
		{ name: 'insert', items : [ 'Image','Table','HorizontalRule','Smiley','SpecialChar'] },
                '/',
		{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','-','Subscript','Superscript','-','RemoveFormat' ] },
		{ name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote',
		'-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock' ] },
		{ name: 'links', items : [ 'Link','Unlink','Anchor' ] },
		{ name: 'tools', items : [ 'Maximize', 'Source' ] },
		'/',
		{ name: 'styles', items : [ 'Styles','Font','FontSize' ] },
		{ name: 'colors', items : [ 'TextColor','BGColor' ] }
	];
	config.docType = '<!DOCTYPE HTML>';
	config.stylesSet = 'my_styles';
	config.height = 400;
	config.width = 800;
};
CKEDITOR.stylesSet.add( 'my_styles', [
    // Block-level styles.
    { name: 'Titre', element: 'h1', styles: { 'font-size':'130%', 'color':'#000000','font-family': 'Comic sans MS','text-align': 'center', 'font-weight':'bold'} },
	{ name: 'Sous titre', element: 'h2', styles: { 'font-size':'110%', 'color':'#111111','text-align': 'center','font-weight':'bold','font-family': 'Comic sans MS'} },
    { name: 'Normal',  element: 'p', styles: {'font-size':'100%', 'color':'#222222','font-family': 'Comic sans MS'} },
	{ name: 'Peu important', element: 'p', styles: { 'font-size':'80%','color':'#666666','font-family': 'Comic sans MS'} },
    { name: 'Bloc Bleu', element: 'div', styles: { 'border-radius':'20px 0 20px 0','margin':'10px','padding':'10px','background-color':'#51a8ff', 'color':'#222222', 'font-size':'130%', 'font-weight':'bold', 'text-align':'center','font-family': 'Comic sans MS'} },
	{ name: 'Bloc Vert', element: 'div', styles: { 'border-radius':'20px 0 20px 0','margin':'10px','padding':'10px','background-color':'#4ecb66', 'color':'#222222', 'font-size':'130%', 'font-weight':'bold', 'text-align':'center','font-family': 'Comic sans MS'} },
	{ name: 'Bloc Jaune', element: 'div', styles: { 'border-radius':'20px 0 20px 0','margin':'10px','padding':'10px','background-color':'Yellow', 'color':'#222222', 'font-size':'130%', 'font-weight':'bold', 'text-align':'center','font-family': 'Comic sans MS'} },
	{ name: 'Bloc Orange', element: 'div', styles: { 'border-radius':'20px 0 20px 0','margin':'10px','padding':'10px','background-color':'#ffd900', 'color':'#222222', 'font-size':'130%', 'font-weight':'bold', 'text-align':'center','font-family': 'Comic sans MS'} },
    // Inline styles.
	{ name: 'Marqueur fluo', element: 'span', styles: { 'background-color': 'Yellow'} },
	{ name: 'Marqueur bleu', element: 'span', styles: { 'background-color': '#51a8ff'} },
	{ name: 'Marqueur vert', element: 'span', styles: { 'background-color': '#4ecb66'} },
	{ name: 'Marqueur orange', element: 'span', styles: { 'background-color': '#ffd900'} }
]);

