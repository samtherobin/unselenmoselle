<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/

if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
{								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
		switch ($grade)
		{
			case 'SELISTE' : include("lien/seliste.php");break;	
			case 'MODERATEUR' :include("lien/seliste.php"); include("lien/moderateur.php");break;
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page
		// d'abord on recupére le parametre pour savoir si on affiche le solde ou pas
		if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'affsolde'")))) {
			die('Erreur : ' . mysql_error());
		}
		$affsolde=stripslashes($recup[0]);
		echo "<br><div class=\"corps\"><br>";
		//debut du scan
		$scan=$_GET['scan'];
		if($scan=='inactif')
		{
			if (!($requete=mysql_query("SELECT `id_seliste`, `connection`, `prenom`, `tel`, `grains`, `age`, `ville`, `quartier`  FROM `selistes` WHERE `valide`='NON' ORDER BY `id_seliste` DESC "))) {
				die('Erreur : ' . mysql_error());
			}
		}
		elseif($scan=='monnaie')
		{
			if (!($requete=mysql_query("SELECT `id_seliste`, `connection`, `prenom`, `tel`, `grains`, `age`, `ville`, `quartier`  FROM `selistes` WHERE `valide`='OUI' ORDER BY `grains` DESC "))) {
				die('Erreur : ' . mysql_error());
			}
		}
		elseif($scan=='pseudo')
		{
			if (!($requete=mysql_query("SELECT `id_seliste`, `connection`, `prenom`, `tel`, `grains`, `age`, `ville`, `quartier`  FROM `selistes` WHERE `valide`='OUI' ORDER BY `prenom` ASC "))) {
				die('Erreur : ' . mysql_error());
			}
		}
		elseif($scan=='connect')
		{
			if (!($requete=mysql_query("SELECT `id_seliste`, `connection`, `prenom`, `tel`, `grains`, `age`, `ville`, `quartier`  FROM `selistes` WHERE `valide`='OUI' ORDER BY `connection` DESC "))) {
				die('Erreur : ' . mysql_error());
			}
		}
		elseif($scan=='age')
		{
			if (!($requete=mysql_query("SELECT `id_seliste`, `connection`, `prenom`, `tel`, `grains`, `age`, `ville`, `quartier`  FROM `selistes` WHERE `valide`='OUI' ORDER BY `age` DESC "))) {
				die('Erreur : ' . mysql_error());
			}
		}
		else  // actif
		{
			if (!($requete=mysql_query("SELECT `id_seliste`, `connection`, `prenom`, `tel`, `grains`, `age`, `ville`, `quartier`  FROM `selistes` WHERE `valide`='OUI' ORDER BY `id_seliste` DESC "))) {
				die('Erreur : ' . mysql_error());
			}
		}
		if($affsolde=='NON'){$colspan=6;}else{$colspan=7;}
		echo "
		<table class='tablevu' border='1' cellpadding='5'>
		<tr>
			<td colspan='".$colspan."'><p class='titre'>Les sélistes:</p></td>
		</tr>
		<tr>
			<td class='teinte2' colspan='".$colspan."'><p>Ne vous inquiétez pas si votre compte est négatif ou positif au-dessus des limites théoriques,<br> cela signifie juste que... vous avez fait des échanges et on vous en remercie :)</p></td>
		</tr>
		<tr>
			<td class='teinte1'><p class='t1'><a href='liste_selistes.php?scan=pseudo'>Pseudo(numéro):</a><br></p></th>
			<td class='teinte1'><p class='t1'><a href='liste_selistes.php?scan=connect'>Dernière connection:</a></p></td>
			<td class='teinte1'><p class='t1'>Contact:</p></td>
			";
			if($affsolde=='OUI'){echo "<td class='teinte1'><p class='t1'><a href='liste_selistes.php?scan=monnaie'>$monnaie:</a></p></td>";}
			echo "
			<td class='teinte1'><p class='t1'>Numéro de Téléphone:</p></td>
			<td class='teinte1'><p class='t1'><a href='liste_selistes.php?scan=age'>Age:</a></p></td>
			<td class='teinte1'><p class='t1'>Ville:</p></td>
		</tr>";
		while($ligne=mysql_fetch_row($requete))
		{
			$id_sel= $ligne[0];
			$connection= $ligne[1];
			$prenom_sel=stripslashes($ligne[2]);
			$tel=stripslashes($ligne[3]);
			$grains=stripslashes($ligne[4]);
			$age=stripslashes($ligne[5]);
			$ville=stripslashes($ligne[6]);
			$quartier=stripslashes($ligne[7]);
			if(($grains<-1500)||($grains>1500))
			{
				$grains="<p class=rouge>".$grains."</p>";
			}
			elseif(($grains<-1000)||($grains>1000))
			{
			 $grains="<p class=bleu>".$grains."</p>";
			}
			elseif(($grains<-500)||($grains>500))
			{
			 $grains="<p class=vert>".$grains."</p>";
			}
			elseif($grains>0)
			{
			 $grains="<p class=jaune>".$grains."</p>";
			}
			elseif($grains<0)
			{
			 $grains="<p class=jaune>".$grains."</p>";
			}
			// recup tps de connection
			$seconde_connect=$time-$connection;
				if ($seconde_connect<180) //3minutes = online 
				{
					$seconde_connect='Connecté';
				}
				elseif($seconde_connect<3600)
				{
					$seconde_connect=floor($seconde_connect/60)."Min";
				}
				elseif($seconde_connect<86400)
				{
					$seconde_connect=floor($seconde_connect/60/60)."H";
				}
				else
				{
					$seconde_connect=floor($seconde_connect/60/60/24);
					if($seconde_connect==1)
					{
						$pluriel="Jour"; 
					} 
					else 
					{
						$pluriel="Jours";
					}
					$seconde_connect=$seconde_connect." ".$pluriel;
				}	
			echo "<tr>
			<td class='teinte2'><p class='t4'><a href=\"profil.php?id=".$id_sel."\">".$prenom_sel."(".$id_sel.")</a></p></td>
			<td><p>".$seconde_connect."</p></td>
			<td><p><a class='aphoto' href=\"messagerie.php?action=ecrire&amp;id=".$id_sel."\"><img src=\"images/lettreNormal.jpg\" alt=\"lettre\"></a></p></td>
			";
			if($affsolde=='OUI'){echo "<td>".$grains."</td>";};
			echo "
			<td><p>".$tel."</p></td>
			<td><p>";if($age!=0){ echo $age." ans";}echo "</p></td>
			<td><p>".$ville."<br><span class='pasimportant'><i>".$quartier."</i></span></p></td>
			</tr>";
		}
		if (!($requete1=mysql_query("SELECT AVG(`age`) FROM `selistes` WHERE NOT `age`='0' ") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$agemoy= round($ligne[0],2);
		if (!($requete1=mysql_query("SELECT AVG(`grains`) FROM `selistes` WHERE `valide`='NON' AND `id_seliste`>'5' ") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete2=mysql_query("SELECT AVG(`grains`) FROM `selistes` WHERE `valide`='OUI' AND `id_seliste`>'5' ") )) {
			die('Erreur : ' . mysql_error());
		}
				
		//  calcul des stat 
		$ligne=mysql_fetch_row($requete1) ;
		$grainmoyinactif= round($ligne[0],2);
		$ligne=mysql_fetch_row($requete2) ;
		$grainmoyactif= round($ligne[0],2);
		echo "</table>
		<p>Age moyen: $agemoy ans</br>
		Nombre de $monnaie moyen par séliste inactif(sans les comptes 1 2 3 4 5): $grainmoyinactif $monnaie</br>
		Nombre de $monnaie moyen par séliste actif(sans les comptes 1 2 3 4 5): $grainmoyactif $monnaie</br></p>";		
		echo "
				<br><a href=\"liste_selistes.php\">Selistes actifs</a>&nbsp;&nbsp;<a href=\"liste_selistes.php?scan=inactif\">Sélistes inactifs</a><br><br>";
		
		echo "<br></div>";
	}
	else
	{ 	 // délai dépassé
		header ("location:troptard.php");
		session_destroy();
	}
 }
else
{ 	 // pas de session
	header ("location:404.php");
	session_destroy();
}
mysql_close($connexion);
include ("fin.php");	
?>
