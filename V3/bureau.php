<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/
 
if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
 {								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete1=mysql_query("SELECT `grade`, `grains`, `prenom` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete=mysql_query("UPDATE `selistes` SET `connection` = '$time' WHERE `id_seliste` ='$id_seliste' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		$grains= $ligne[1];	
		$prenom= $ligne[2];		
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);		
		switch ($grade)
		{
			case 'SELISTE' : include("lien/seliste.php");break;
			case 'MODERATEUR' :include("lien/seliste.php"); include("lien/moderateur.php");break;		
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// début du bureau			
		// test si le membres à 3 services
		if (!($requetedem=mysql_query("SELECT * FROM `services` WHERE `id_seliste`='$id_seliste' AND `etat`='DEM'"))) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requetepro=mysql_query("SELECT * FROM `services` WHERE `id_seliste`='$id_seliste' AND `etat`='PRO'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nb_dem=0;
		$nb_dem=mysql_num_rows($requetedem);
		$nb_pro=0;
		$nb_pro=mysql_num_rows($requetepro);
		if($nb_pro<3)
		{
			echo "<br><p align='center'><a href='services.php?action=edition'><span class=rouge>Attention vous n'avez pas 3 services proposés.</span></a></p>";
		}
		if($nb_dem<3)
		{
			echo "<br><p class=rouge align='center'><a href='services.php?action=edition'><span class=rouge>Attention vous n'avez pas 3 services demandés.</span></a></p>";
		}
		// recup des 5 derniers evenements de l'agenda
		if (!($recup=mysql_query("SELECT `titre`,`id_agenda`,`rubrique`,`date_evenement` FROM `agenda` WHERE `publication` = 'OUI' AND `date_evenement` >$time  ORDER BY `date_evenement` LIMIT 0 , 5 "))) {
			die('Erreur : ' . mysql_error());
		}
		$infoagenda=null;
		while($ligne=mysql_fetch_row($recup))
		{
			$titre_agenda= stripslashes($ligne[0]);
			$id_agenda= $ligne[1];
			$rub=$ligne[2];
			$dateevt=$ligne[3];
			$dateevt=date('d/m/y à H\h',$dateevt);
			if($rub=='INS'){$couleur="noir";}
			elseif($rub=='AUB'){$couleur="vert";}
			elseif($rub=='RAN'){$couleur="jaune";}
			elseif($rub=='REU'){$couleur="rouge";}
			elseif($rub=='SOR'){$couleur="bleu";}
			else {}
			// recup nbr replique
			if (!($nb_replique=mysql_num_rows(mysql_query("SELECT * FROM `agenda_repliques` WHERE `id_agenda`=$id_agenda")))) {
				die('Erreur : ' . mysql_error());
			}						
			if($infoagenda!=null){$infoagenda=$infoagenda."</p><hr width=50%><p>";}
			$infoagenda=$infoagenda."<span class='pasimportant'>Le".$dateevt.":</span><br><a href=\"agenda.php?action=reponses&amp;id=".$id_agenda."\"> <span class='$couleur'>".$titre_agenda."</span></a> ($nb_replique)";
		}
		// recup derniere actualités
		if (!($recup=mysql_query("SELECT `titre`,`id_article`,`timestamp` FROM `articles` WHERE `publication` = 'OUI' ORDER BY `timestamp` DESC LIMIT 0 , 5 "))) {
			die('Erreur : ' . mysql_error());
		}
		$infoactu=null;
		while($ligne=mysql_fetch_row($recup))
		{
			$titre_actu= stripslashes($ligne[0]);
			$id_actu= $ligne[1];
			$date_actu=$ligne[2];
			$date_actu=date('d/m/y à H\h',$date_actu);
			// recup nbr replique
			if (!($query = mysql_query("SELECT * FROM `articles_repliques` WHERE `id_article`=$id_actu"))) {
				die('Erreur : ' . mysql_error());
			}
			$nb_replique = mysql_num_rows($query);
			if($infoactu!=null){$infoactu=$infoactu."</p><hr width=50%><p>";}
			$infoactu=$infoactu."<span class='pasimportant'>Le".$date_actu.":</span><br><a href=\"actualites.php?action=reponses&id=".$id_actu."\"> ".$titre_actu."</a> ($nb_replique)";
		}
		// les derniers services ajouté dans les profils
		if (!($recup=mysql_query("SELECT `id_seliste`, `id_rubrique`, `time`, `description` FROM `services` WHERE `etat`='DEM' ORDER BY `time` DESC LIMIT 0 , 5 "))) {
			die('Erreur : ' . mysql_error());
		}
		$infoserv=null;
		while($ligne=mysql_fetch_row($recup))
		{
			$id_sel_serv=$ligne[0];
			$id_rubrique= $ligne[1];
			$dateserv=$ligne[2];
			$description=stripslashes($ligne[3]);
			$dateserv=date('d/m/y à H\h',$dateserv);
			// recup de son prenom
			if (!($recup2=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`='$id_sel_serv'")))) {
				die('Erreur : ' . mysql_error());
			} 
			$prenom_sel_serv=stripslashes($recup2[0]);	
			$designation_ss_rub=$description;
			// recu nom rubrique
			if (!($ligne2=mysql_fetch_row(mysql_query("SELECT `id_cat`,`designation` FROM `rubrique` WHERE `id_rubrique`=$id_rubrique")))) {
				die('Erreur : ' . mysql_error());
			}
			$id_cat=$ligne2[0];
			$designation_rub=$ligne2[1];
			if($infoserv!=null){$infoserv=$infoserv."</p><hr width=50%><p>";}
			$infoserv=$infoserv."<span class=\"pasimportant\"><a href='profil.php?id=".$id_sel_serv."'>".$prenom_sel_serv."(".$id_sel_serv.")</a> le ".$dateserv."</span><br><a href='selistes.php?type=menu&catalogue=$id_cat&rubrique=$id_rubrique'>$designation_rub</a> - $designation_ss_rub";
		}
		// les derniers services ajouté dans les profils
		if (!($recup=mysql_query("SELECT `id_seliste`,`id_rubrique`, `time`, `description` FROM `services` WHERE `etat`='PRO' ORDER BY `time` DESC LIMIT 0 , 5 "))) {
			die('Erreur : ' . mysql_error());
		}
		$infoserv2=null;
		while($ligne=mysql_fetch_row($recup))
		{
			$id_sel_serv=$ligne[0];
			$id_rubrique= $ligne[1];
			$dateserv=$ligne[2];
			$description=stripslashes($ligne[3]);
			$dateserv=date('d/m/y à H\h',$dateserv);
			// recup de son prenom
			if (!($recup2=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`='$id_sel_serv'")))) {
				die('Erreur : ' . mysql_error());
			} 
			$prenom_sel_serv=stripslashes($recup2[0]);	
			$designation_ss_rub=$description;
			// recu nom rubrique
			if (!($ligne2=mysql_fetch_row(mysql_query("SELECT `id_cat`,`designation` FROM `rubrique` WHERE `id_rubrique`=$id_rubrique")))) {
				die('Erreur : ' . mysql_error());
			}
			$id_cat=$ligne2[0];
			$designation_rub=$ligne2[1];
			if($infoserv2!=null){$infoserv2=$infoserv2."</p><hr width=50%><p>";}
			$infoserv2=$infoserv2."<span class=\"pasimportant\"><a href='profil.php?id=".$id_sel_serv."'>".$prenom_sel_serv."(".$id_sel_serv.")</a> le ".$dateserv."</span><br><a href='selistes.php?type=menu&catalogue=$id_cat&rubrique=$id_rubrique'>$designation_rub</a> - $designation_ss_rub";
		}
		// recup de sa liste d'amis + jointure de la table seliste
		$infoamis=null;
		if (!($requete=mysql_query("SELECT `contact`.`id_contact`, `selistes`.`prenom`, `selistes`.`connection`, `selistes`.`valide` FROM `contact` INNER JOIN `selistes` ON `id_contact`=`selistes`.`id_seliste` WHERE `contact`.`id_seliste`='$id_seliste' AND `ami`='A' ORDER BY  `selistes`.`connection` DESC"))) {
			die('Erreur : ' . mysql_error());
		}
		while($ligne=mysql_fetch_row($requete))
		{
			$id_ami=$ligne[0];
			$prenom_ami=stripslashes ($ligne[1]);		
			$connection_fiche=$ligne[2];
			$valide_fiche=$ligne[3];
			// recup tps de connection
			$seconde_connect=$time-$connection_fiche;
			if ($seconde_connect<180) //3minutes = online 
			{$tps_connect='<b>En ligne</b>'; }
			else 
			{ 

				switch($seconde_connect)
				{
					case ($seconde_connect<180): {$tps_connect='<b>En ligne</b>';break;}
					case ($seconde_connect<3600): {$tps_connect=floor($seconde_connect/60)." min";break;}
					case ($seconde_connect<86400): {$tps_connect=floor($seconde_connect/60/60)." H";break;}
					case ($seconde_connect<2635200):{$tps_connect=floor($seconde_connect/60/60/24)." J";break;}
					case ($seconde_connect>=2635200):{$tps_connect=floor($seconde_connect/60/60/24/30.5)." Mois";break;}
				}
			}
			$infoamis=$infoamis."<p>
			<span class='col1'><a href='messagerie.php?action=ecrire&amp;id=".$id_ami."' class='aphoto'><img src=\"images/lettreNormal.jpg\"  width=\"20px\"></a>		
			</span><span class='col2'><a href=\"profil.php?id=".$id_ami."\">";
			if($valide_fiche=='NON') {$infoamis=$infoamis."<span class='gris'>";}
			$infoamis=$infoamis."&nbsp;".$prenom_ami."(".$id_ami.")";
			if($valide_fiche=='NON') {$infoamis=$infoamis."</span>";}
			$infoamis=$infoamis."</a>
			</span><span class='col3'><i>".$tps_connect."</i></span></p>";
		}
				// recup de sa liste d'amis + jointure sur table seliste
		$infocontacts=null;
		if (!($requete=mysql_query("SELECT `contact`.`id_contact`, `selistes`.`prenom`, `selistes`.`connection`, `selistes`.`valide` FROM `contact` INNER JOIN `selistes` ON `id_contact`=`selistes`.`id_seliste` WHERE `contact`.`id_seliste`='$id_seliste' AND `ami`='C' ORDER BY  `selistes`.`connection` DESC"))) {
			die('Erreur : ' . mysql_error());
		}
		while($ligne=mysql_fetch_row($requete))
		{
			$id_ami=$ligne[0];
			$prenom_ami=stripslashes ($ligne[1]);		
			$connection_fiche=$ligne[2];
			$valide_fiche=$ligne[3];
			// recup tps de connection
			$seconde_connect=$time-$connection_fiche;
			if ($seconde_connect<180) //3minutes = online 
			{$tps_connect='<b>En ligne</b>'; }
			else 
			{ 

				switch($seconde_connect)
				{
					case ($seconde_connect<180): {$tps_connect='<b>En ligne</b>';break;}
					case ($seconde_connect<3600): {$tps_connect=floor($seconde_connect/60)." min";break;}
					case ($seconde_connect<86400): {$tps_connect=floor($seconde_connect/60/60)." H";break;}
					case ($seconde_connect<2635200):{$tps_connect=floor($seconde_connect/60/60/24)." J";break;}
					case ($seconde_connect>=2635200):{$tps_connect=floor($seconde_connect/60/60/24/30.5)." Mois";break;}
				}
			}
			$infocontacts=$infocontacts."<p>
			<span class='col1'><a href='messagerie.php?action=ecrire&amp;id=".$id_ami."' class='aphoto'><img src=\"images/lettreNormal.jpg\"  width=\"20px\"></a>		
			</span><span class='col2'><a href=\"profil.php?id=".$id_ami."\">";
			if($valide_fiche=='NON') {$infocontacts=$infocontacts."<span class='gris'>";}
			$infocontacts=$infocontacts."&nbsp;".$prenom_ami."(".$id_ami.")";
			if($valide_fiche=='NON') {$infocontacts=$infocontacts."</span>";}
			$infocontacts=$infocontacts."</a>
			</span><span class='col3'><i>".$tps_connect."</i></span></p>";
		}
		
		
			echo"<br>
					<div class=\"colonnegauche\">
						<div class=\"colonnegauche2\">
							<p><b>Dernières actualités:</b></p>
							<p>".$infoactu."</p>
						</div>
						<div class=\"colonnegauche2\">
							<p><b>Dernières demandes:</b></p>
							<p>".$infoserv."</p>
						</div>
						<div class=\"colonnegauche2\">
							<p><b>Dernière propositions:</b></p>
							<p>".$infoserv2."</p>
						</div>
					</div>			

					<div class=\"colonnedroite\">
						<div class=\"colonnedroite2\">
							<p><span class='t1'>Agenda:</span>
							"; include("agendav3mini.php");echo "
							".$infoagenda."</p>
						</div>
						<div class=\"colonnedroite2\">
							<p><b>Vos Amis:</b>&nbsp;<a href='amis.php?action=lire'><img src='images/editer.gif'></a></p>
							<div class='tableau'>".$infoamis."</div>
						</div>
						<div class=\"colonnedroite2\">
							<p><b>Vos Contacts:</b>&nbsp;<a href='amis.php?action=lire'><img src='images/editer.gif'></a></p>
							<div class='tableau'>".$infocontacts."</div>
						</div>
						
					</div>

					
					<div class=\"corps2\"><br>
				
						";												
		if (!($requete3=mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF' ORDER BY `time` DESC LIMIT 5"))) {
			die('Erreur : ' . mysql_error());
		}  
		$nb=0;
		$nb=mysql_num_rows($requete3);	  
		if($nb!=0) //Si une ligne alors il a un message mini
		{	   
			while($ligne=mysql_fetch_row($requete3))
			{
				$id_mess= $ligne[0];
				$id_expe= $ligne[2];	   
				if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste` =$id_expe")))) {
					die('Erreur : ' . mysql_error());
				}
				$prenom_exp=stripslashes($recup[0]);		
				$idancien= $ligne[3];
				$time_mess= $ligne[4];
				$message=stripslashes($ligne[6]);	  
				include("smileyscompagnie.php" )	;
				$date=date('d/m/y',$time_mess);		
				echo "<div class='message'><p><b>Message de <a href=\"profil.php?id=".$id_expe."\">".$prenom_exp."(".$id_expe.")</a> le ".$date."</b>&nbsp
					<a href='messagerie.php?action=repondre&amp;id_mess=".$id_mess."'><img src=\"images/lettreNormal.jpg\" alt='repondre'></a>&nbsp;
					<a href='messagerie.php?action=supprimer&amp;id_mess=".$id_mess."'><img src=\"images/croix.jpg\" alt='supprimer'></a>&nbsp;
					<a href='messagerie.php?action=sauvegarder&amp;id_mess=".$id_mess."'><img src=\"images/disquette.jpg\" alt='sauvegarder'></a></p>
					<hr width=50%>
 				<p>".$message."</p></div><br>";	  
			}	  
		}  
		else //si il a pas de message
		{
			echo "<p>Vous n'avez pas de message.</p>";
		}		
		if($nbrmess>5)
		{
			$nbrmess2=$nbrmess-5;
			echo "<div class='message'><p><a href='messagerie.php?action=lire'>Voir les $nbrmess2 messages suivants</a></p></div><br>";
		}
		echo "</div>"; 
		//fin du bureau
		mysql_close($connexion); 
		include ("fin.php");
	}
	else
	{ 	// trop tard
		include("troptard.php");
		session_destroy();
	}
}
else
{ 	// voleur
	header ("location:404.php");
	session_destroy();
}
?>