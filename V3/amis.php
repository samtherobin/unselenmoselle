<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/

if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
{								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade`, `prenom` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		$prenom= $ligne[1];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
	 	switch ($grade)
		{
			case 'SELISTE' : include("lien/seliste.php");break;	
			case 'MODERATEUR' :include("lien/seliste.php"); include("lien/moderateur.php");break;
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		echo "<br><div class=\"corps\"><br>";
		// Debut de la page
		$action=$_GET['action'];
		if($action=='lire')
		{
			//liste ami
			if (!($requete=mysql_query("SELECT `id_contact` FROM `contact` WHERE `id_seliste`=$id_seliste AND `ami`='A'"))) {
				die('Erreur : ' . mysql_error());
			}
			$cpt_req++;
			echo "					
			<table class='tablevu' border='1' cellpadding=\"2\">
			<tr><th colspan=4>Liste des amis:</th></tr>";
			while($ligne=mysql_fetch_row($requete))
	      {
				$id_seliste_ami=$ligne[0];
				if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`='$id_seliste_ami'")))) {
					die('Erreur : ' . mysql_error());
				} 
				$prenom_ami=stripslashes($recup[0]);		
				echo "
				<tr>
				<td class='t1'><br><a href=\"profil.php?id=".$id_seliste_ami."\">".$prenom_ami."(".$id_seliste_ami.")</a><br><br></td>  
				<td class='t1'><br><a href=\"amis.php?action=supprimer&amp;id=".$id_seliste_ami."\"><img src='images/croix.jpg'>Supprimer</a><br><br></td>
				<td class='t1'><br><a href=\"amis.php?action=listecontact&amp;id=".$id_seliste_ami."\">Liste Contact</a><br><br></td>
				</tr>";
			}
			// liste contact (ami=c)
			if (!($requete=mysql_query("SELECT `id_contact` FROM `contact` WHERE `id_seliste`=$id_seliste AND `ami`='C'"))) {
				die('Erreur : ' . mysql_error());
			}
			$cpt_req++;
			echo "				
			<tr><th colspan=4>Liste des contacts:</th></tr>";
			while($ligne=mysql_fetch_row($requete))
	      {
				$id_seliste_ami=$ligne[0];
				if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`='$id_seliste_ami'")))) {
					die('Erreur : ' . mysql_error());
				} 
				$prenom_ami=stripslashes($recup[0]);		
				echo "
				<tr>
				<td class='t1'><br><a href=\"profil.php?id=".$id_seliste_ami."\">".$prenom_ami."(".$id_seliste_ami.")</a><br><br></td>  
				<td class='t1'><br><a href=\"amis.php?action=supprimer&amp;id=".$id_seliste_ami."\"><img src='images/croix.jpg'>Supprimer</a><br><br></td>
				<td class='t1'><br><a href=\"amis.php?action=listeami&amp;id=".$id_seliste_ami."\">Liste Ami</a><br><br></td>
				</tr>";
			}
			echo "</table>";
		}
		elseif($action=='ajouter')
		{
			$id_seliste_ajout=$_GET['id'];
			if (!($requete=mysql_query("INSERT INTO `contact` VALUES ('".$id_seliste."', '".$id_seliste_ajout."', 'C') "))) {
				die('Erreur : ' . mysql_error());
			}
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`='$id_seliste_ajout'")))) {
				die('Erreur : ' . mysql_error());
			}
			$prenom_ami=stripslashes($recup[0]);		
			//envoi message privé
			$message="<i>Information:</i> <a href=\"profil.php?id=".$id_seliste."\">".$prenom."(".$id_seliste.")</span></a> t\'a ajouté dans sa liste d\'amis.";
			if (!($requete=mysql_query("INSERT INTO `messagerie` VALUES ('', '".$id_seliste_ajout."', '".$id_seliste."', '' , '".$time."','AFF', '".$message."')"))) {
				die('Erreur : ' . mysql_error());
			}
			echo "<br><p class='titre'><br><a href=\"profil.php?id=".$id_seliste_ajout."\">".$prenom_ami."(".$id_seliste_ajout.")</a> Ajouté!</p><br><br><br>";
		}
		elseif($action=='supprimer')
		{
			$id_seliste_sup=$_GET['id'];
			if (!($requete=mysql_query("DELETE FROM `contact` WHERE `id_contact`=$id_seliste_sup AND `id_seliste`=$id_seliste LIMIT 1"))) {
				die('Erreur : ' . mysql_error());
			}
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`='$id_seliste_sup'")))) {
				die('Erreur : ' . mysql_error());
			} 
			$prenom_ami=stripslashes($recup[0]);		
			echo "	<br><p class='titre'><br><a href=\"profil.php?id=".$id_seliste_sup."\">".$prenom_ami."(".$id_seliste_sup.")</a> suprimé!</p><br><br><br>";
		}
		elseif($action=='listeami')
		{
			$id_seliste_ami=$_GET['id'];
			if (!($requete=mysql_query("UPDATE `contact` SET `ami` = 'A' WHERE `id_seliste` =$id_seliste AND `id_contact` =$id_seliste_ami LIMIT 1"))) {
				die('Erreur : ' . mysql_error());
			}
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`='$id_seliste_ami'")))) {
				die('Erreur : ' . mysql_error());
			} 
			$prenom_ami=stripslashes($recup[0]);		
			echo "<br><p class='titre'><br><a href=\"profil.php?id=".$id_seliste_ami."\">".$prenom_ami."(".$id_seliste_ami.")</a> devient votre ami.</p><br><br><br>";
		}
		elseif($action=='listecontact')
		{
			$id_seliste_ami=$_GET['id'];
			if (!($requete=mysql_query("UPDATE `contact` SET `ami` = 'C' WHERE `id_seliste` =$id_seliste AND `id_contact` =$id_seliste_ami LIMIT 1"))) {
				die('Erreur : ' . mysql_error());
			}
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`='$id_seliste_ami'")))) {
				die('Erreur : ' . mysql_error());
			} 
			$prenom_ami=stripslashes($recup[0]);		
			$grade_ami=$recup[1];
			echo "<br><p class='titre'><br><a href=\"profil.php?id=".$id_seliste_ami."\">".$prenom_ami."(".$id_seliste_ami.")</a> devient votre contact.</p><br><br><br>";
		}
		else
		{ 	 // heu voleur:p
			header ("location:404.php");
			session_destroy();
		}

		echo "<br><p><br><a href='bureau.php' title='Bureau'>Retour bureau</a></p><br><br></div>";	
} 
	else
	{ 	//délai dépassé
		header ("location:troptard.php");
		session_destroy();
	}
}
else
{ 	// voleur
	header ("location:404.php");
	session_destroy();
}
mysql_close($connexion);
include ("fin.php");	
?>
