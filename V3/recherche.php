<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/

if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
{								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
		switch ($grade)
		{
			case 'SELISTE' : include("lien/seliste.php");break;				
			case 'MODERATEUR' :include("lien/seliste.php"); include("lien/moderateur.php");break;
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page
		// rapatriement parametre21
		// d'abord on recupére le parametre pour savoir si on affiche le solde ou pas
		if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'affsolde'")))) {
			die('Erreur : ' . mysql_error());
		}
		$affsolde=stripslashes($recup[0]);
		echo "<br><div class=\"corps\"><br>";
		//debut de la recherche
		// par id
		if($_GET['id']!= NULL)
		{
			echo "<table summary=\"\" width=\"90%\" border=\"0\" >
			<tr>
			<th colspan='2'>Recherche par numéro d'identifiant:</th>
			</tr>
			<tr>";
			$id_sel=nl2br(htmlentities($_GET['id'], ENT_QUOTES, "UTF-8"));
			if (!($requete=mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`=$id_sel"))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete) ;
			$prenom_sel=stripslashes($ligne[0]);
			echo "
			<td class='t1'><br><a href='profil.php?id=".$id_sel."'>".$prenom_sel."(".$id_sel.")</a><br><br></td>
			</tr></table>";
		}
		//par  prenom
		elseif($_GET['prenom']!= NULL)
		{	
			if($affsolde=='NON'){$colspan=6;}else{$colspan=7;}
			echo "<table summary=\"\" class='tablevu' border=\"1\" >
			<tr>
			<th colspan='".$colspan."'><p>Recherche par prénom: (50 derniers connectés)</p></th>
			</tr>
			<tr>
			<th><br><a href='liste_selistes.php?scan=pseudo'>Pseudo(numéro):</a><br><br></th>
			<th><br><a href='liste_selistes.php?scan=connect'>Dernière connection:</a><br><br></th>
			<th>Contact:</th>";
			if($affsolde=='OUI'){echo "<th><br><a href='liste_selistes.php?scan=monnaie'>$monnaie:</a><br><br></th>";}
			echo "<th>Numéro de Téléphone:</th>
			<th><br><a href='liste_selistes.php?scan=age'>Age:</a><br><br></th>
			<th>Ville:</th>
			</tr>";
			$prenom_sel=nl2br(htmlentities($_GET['prenom'], ENT_QUOTES, "UTF-8"));
			if (!($requete=mysql_query("SELECT `id_seliste`, `connection`, `prenom`, `tel`, `grains`, `age`, `ville`, `quartier` FROM `selistes` WHERE  `prenom` REGEXP '$prenom_sel' AND `valide`='OUI' ORDEr by `connection` DESC LIMIT 0,50"))) {
				die('Erreur : ' . mysql_error());
			}	
			while($ligne=mysql_fetch_row($requete))
			{
				$id_sel= $ligne[0];
			$connection= $ligne[1];
			$prenom_sel=stripslashes($ligne[2]);
			$tel=stripslashes($ligne[3]);
			$grains=stripslashes($ligne[4]);
			$age=stripslashes($ligne[5]);
			$ville=stripslashes($ligne[6]);
			$quartier=stripslashes($ligne[7]);
			if(($grains<-1500)||($grains>1500))
			{
				$grains="<p class=fond_rouge><span class=ROUGE>".$grains."</span></p>";
			}
			elseif(($grains<-1000)||($grains>1000))
			{
			 $grains="<p class=fond_bleu><span class=BLEU>".$grains."</span></p>";
			}
			elseif(($grains<-500)||($grains>500))
			{
			 $grains="<p class=fond_vert><span class=VERT>".$grains."</span></p>";
			}
			elseif($grains>0)
			{
			 $grains="<p class=fond_jaune><span class=JAUNE>".$grains."</span></p>";
			}
			elseif($grains<0)
			{
			 $grains="<p class=fond_jaune><span class=JAUNE>".$grains."</span></p>";
			}
			// recup tps de connection
			$seconde_connect=$time-$connection;
				if ($seconde_connect<180) //3minutes = online 
				{
					$seconde_connect='Connecté';
				}
				elseif($seconde_connect<3600)
				{
					$seconde_connect=floor($seconde_connect/60)."Min";
				}
				elseif($seconde_connect<86400)
				{
					$seconde_connect=floor($seconde_connect/60/60)."H";
				}
				else
				{
					$seconde_connect=floor($seconde_connect/60/60/24);
					if($seconde_connect==1)
					{
						$pluriel="Jour"; 
					} 
					else 
					{
						$pluriel="Jours";
					}
					$seconde_connect=$seconde_connect." ".$pluriel;
				}	
			echo "<tr>
			<td class=\"t1\" width=\"20%\"><p><br><a href=\"profil.php?id=".$id_sel."\">".$prenom_sel."(".$id_sel.")</a><br><br></p></td>
			<td class=\"t2\"><p>".$seconde_connect."</p></td>
			<td class=\"t2\"><p><br><a href=\"messagerie.php?action=ecrire&amp;id=".$id_sel."\"><img src=\"images/lettreNormal.jpg\" alt=\"lettre\"></a><br></p></td>
			";
			if($affsolde=='OUI'){echo "<td class=t3>".$grains."</td>";}
			echo "<td class=t2><p>".$tel."</p></td>
			<td class=t3><p>";if($age!=0){ echo $age." ans";}echo "</p></td>
			<td class=t3><p>".$ville."<br><span class='pasimportant'><i>".$quartier."</i></span></p></td>
			</tr>";
			}
			echo "</table><br>";				
		}
		elseif($_POST['message']!= NULL)
		{
			echo "<table summary=\"\" class='tablevu' border=\"1\" >
			<tr>
			<th colspan='5'>Recherche par messagerie privé:</th>
			</tr>
			<tr>
				<td class='t1'>Destinataire</td>
				<td class='t1'>Expediteur</td>
				<td class='t1'>Date</td>
				<td class='t1'>Statut</td>
				<td class='t1' width='40%'>Message</td>
			</tr>";
			$message=nl2br(htmlentities($_POST['message'], ENT_QUOTES, "UTF-8"));
			if (!($requete=mysql_query("SELECT * FROM `messagerie` WHERE  `message` REGEXP '$message' "))) {
				die('Erreur : ' . mysql_error());
			}	
			while($ligne=mysql_fetch_row($requete))
			{
				$id_dest= $ligne[1];
				if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`=$id_dest")))) {
					die('Erreur : ' . mysql_error());
				} 
				$prenom_dest=$recup[0];		
				$id_exp= $ligne[2];
				if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`=$id_exp")))) {
					die('Erreur : ' . mysql_error());
				} 
				$prenom_exp=$recup[0];		
				$time_mess= date('d/m/y à H\h i\m\i\n',$ligne[4]);
				$rub= $ligne[5];
				$message= stripslashes($ligne[6]);
				echo "<tr>
				<td class='t2'><br><a href='profil.php?id=".$id_dest."'>".$prenom_dest."(".$id_dest.")</a><br></td>
				<td class='t2'><br><a href='profil.php?id=".$id_exp."'>".$prenom_exp."(".$id_exp.")</a><br></td>
				<td class='t2'>".$time_mess."</td>
				<td class='t4'>".$rub."</td>
				<td class='t2' width='30%'>".$message."</td>
				</tr>";
			}
			echo "</table><br>";				
		}
		//par ville
		elseif($_GET['ville']!= NULL)
		{
			if($affsolde=='NON'){$colspan=6;}else{$colspan=7;}
			echo "<table summary=\"\" class='tablevu' border=\"1\" >
			<tr>
			<th colspan='".$colspan."'><p>Recherche par ville: (50 derniers connectés)</p></th>
			</tr>
			<tr>
			<th><br><a href='liste_selistes.php?scan=pseudo'>Pseudo(numéro):</a><br><br></th>
			<th><br><a href='liste_selistes.php?scan=connect'>Dernière connection:</a><br><br></th>
			<th>Contact:</th>
			";
			if($affsolde=='OUI'){echo "<th><br><br><a href='liste_selistes.php?scan=monnaie'>$monnaie:</a><br><br></th>";}
			echo "			
			<th>Numéro de Téléphone:</th>
			<th><br><a href='liste_selistes.php?scan=age'>Age:</a><br><br></th>
			<th>Ville:</th>
			</tr>";
			$ville_sel=rawurldecode($_GET['ville']);
			if (!($requete=mysql_query("SELECT `id_seliste`, `connection`, `prenom`, `tel`, `grains`, `age`, `ville`, `quartier`  FROM `selistes` WHERE `ville` REGEXP '$ville_sel' AND `valide`='OUI' ORDER BY `connection` DESC LIMIT 0,50"))) {
				die('Erreur : ' . mysql_error());
			}	
			while($ligne=mysql_fetch_row($requete))
			{
				$id_sel= $ligne[0];
			$connection= $ligne[1];
			$prenom_sel=stripslashes($ligne[2]);
			$tel=stripslashes($ligne[3]);
			$grains=stripslashes($ligne[4]);
			$age=stripslashes($ligne[5]);
			$ville=stripslashes($ligne[6]);
			$quartier=stripslashes($ligne[7]);
			if(($grains<-1500)||($grains>1500))
			{
				$grains="<p class=fond_rouge><span class=ROUGE>".$grains."</span></p>";
			}
			elseif(($grains<-1000)||($grains>1000))
			{
			 $grains="<p class=fond_bleu><span class=BLEU>".$grains."</span></p>";
			}
			elseif(($grains<-500)||($grains>500))
			{
			 $grains="<p class=fond_vert><span class=VERT>".$grains."</span></p>";
			}
			elseif($grains>0)
			{
			 $grains="<p class=fond_jaune><span class=JAUNE>".$grains."</span></p>";
			}
			elseif($grains<0)
			{
			 $grains="<p class=fond_jaune><span class=JAUNE>".$grains."</span></p>";
			}
			// recup tps de connection
			$seconde_connect=$time-$connection;
				if ($seconde_connect<180) //3minutes = online 
				{
					$seconde_connect='Connecté';
				}
				elseif($seconde_connect<3600)
				{
					$seconde_connect=floor($seconde_connect/60)."Min";
				}
				elseif($seconde_connect<86400)
				{
					$seconde_connect=floor($seconde_connect/60/60)."H";
				}
				else
				{
					$seconde_connect=floor($seconde_connect/60/60/24);
					if($seconde_connect==1)
					{
						$pluriel="Jour"; 
					} 
					else 
					{
						$pluriel="Jours";
					}
					$seconde_connect=$seconde_connect." ".$pluriel;
				}	
			echo "<tr>
			<td class=\"t1\" width=\"20%\" height='20px'><p><a href=\"profil.php?id=".$id_sel."\">".$prenom_sel."(".$id_sel.")</a></p></td>
			<td class=\"t2\"><p>".$seconde_connect."</p></td>
			<td class=\"t2\"><p><a href=\"messagerie.php?action=ecrire&amp;id=".$id_sel."\"><img src=\"images/lettreNormal.jpg\" alt=\"lettre\"></a></p></td>
			";
			if($affsolde=='OUI'){echo "<td class=t3>".$grains."</td>";}
			echo "
			<td class=t2><p>".$tel."</p></td>
			<td class=t3><p>";if($age!=0){ echo $age." ans";}echo "</p></td>
			<td class=t3><p>".$ville."<br><span class='pasimportant'><i>".$quartier."</i></span></p></td>
			</tr>";
			}
			echo "</table><br>";	
		}
		//par courriel
		elseif(($_GET['courriel']!= NULL)&&(($grade=='MODERATEUR')||($grade=='ADMIN')))
		{
			if($affsolde=='NON'){$colspan=7;}else{$colspan=8;}
			echo "<table summary=\"\" class='tablevu' border=\"1\" >
			<tr>
			<th colspan='".$colspan."'><p>Recherche par Courriel:</p></th>
			</tr>
			<tr>
			<th><br><a href='liste_selistes.php?scan=pseudo'>Pseudo(numéro):</a><br><br></th>
			<th><br><a href='liste_selistes.php?scan=connect'>Dernière connection:</a><br><br></th>
			<th>Email:</th>
			<th>Contact:</th>
			";
			if($affsolde=='OUI'){echo "<th><br><br><a href='liste_selistes.php?scan=monnaie'>$monnaie:</a><br><br></th>";}
			echo "			
			<th>Numéro de Téléphone:</th>
			<th><br><a href='liste_selistes.php?scan=age'>Age:</a><br><br></th>
			<th>Ville:</th>
			</tr>";
			$email_sel=nl2br(htmlentities($_GET['courriel'], ENT_QUOTES, "UTF-8"));
			if (!($requete=mysql_query("SELECT `id_seliste`, `connection`, `prenom`, `tel`, `grains`, `age`, `ville`, `quartier`, `email`  FROM `selistes` WHERE `email` REGEXP '$email_sel' ORDER BY `connection` DESC LIMIT 0,50"))) {
				die('Erreur : ' . mysql_error());
			}	
			while($ligne=mysql_fetch_row($requete))
			{
				$id_sel= $ligne[0];
			$connection= $ligne[1];
			$prenom_sel=stripslashes($ligne[2]);
			$tel=stripslashes($ligne[3]);
			$grains=stripslashes($ligne[4]);
			$age=stripslashes($ligne[5]);
			$ville=stripslashes($ligne[6]);
			$quartier=stripslashes($ligne[7]);
			$email=stripslashes($ligne[8]);
			if(($grains<-1500)||($grains>1500))
			{
				$grains="<p class=fond_rouge><span class=ROUGE>".$grains."</span></p>";
			}
			elseif(($grains<-1000)||($grains>1000))
			{
			 $grains="<p class=fond_bleu><span class=BLEU>".$grains."</span></p>";
			}
			elseif(($grains<-500)||($grains>500))
			{
			 $grains="<p class=fond_vert><span class=VERT>".$grains."</span></p>";
			}
			elseif($grains>0)
			{
			 $grains="<p class=fond_jaune><span class=JAUNE>".$grains."</span></p>";
			}
			elseif($grains<0)
			{
			 $grains="<p class=fond_jaune><span class=JAUNE>".$grains."</span></p>";
			}
			// recup tps de connection
			$seconde_connect=$time-$connection;
				if ($seconde_connect<180) //3minutes = online 
				{
					$seconde_connect='Connecté';
				}
				elseif($seconde_connect<3600)
				{
					$seconde_connect=floor($seconde_connect/60)."Min";
				}
				elseif($seconde_connect<86400)
				{
					$seconde_connect=floor($seconde_connect/60/60)."H";
				}
				else
				{
					$seconde_connect=floor($seconde_connect/60/60/24);
					if($seconde_connect==1)
					{
						$pluriel="Jour"; 
					} 
					else 
					{
						$pluriel="Jours";
					}
					$seconde_connect=$seconde_connect." ".$pluriel;
				}	
			echo "<tr>
			<td class=\"t1\" width=\"20%\" height='20px'><p><a href=\"profil.php?id=".$id_sel."\">".$prenom_sel."(".$id_sel.")</a></p></td>
			<td class=\"t2\"><p>".$seconde_connect."</p></td>
			<td class=\"t2\"><p>".$email."</p></td>
			<td class=\"t2\"><p><a href=\"messagerie.php?action=ecrire&amp;id=".$id_sel."\"><img src=\"images/lettreNormal.jpg\" alt=\"lettre\"></a></p></td>
			";
			if($affsolde=='OUI'){echo "<td class=t3>".$grains."</td>";}
			echo "	
			<td class=t2><p>".$tel."</p></td>
			<td class=t3><p>";if($age!=0){ echo $age." ans";}echo "</p></td>
			<td class=t3><p>".$ville."<br><span class='pasimportant'><i>".$quartier."</i></span></p></td>
			</tr>";
			}
			echo "</table><br>";	
		}
		elseif($_POST['annonce']!= NULL)
		{
			echo "<table summary=\"\" class='tablevu' border=\"1\" >
			<tr>
			<th colspan='2'>Recherche dans les annonces:</th>
			</tr>";
			$annonce_sel=nl2br(htmlentities($_POST['annonce'], ENT_QUOTES, "UTF-8"));
			if (!($requete=mysql_query("SELECT * FROM `petite_annonce` WHERE `message` REGEXP '$annonce_sel' LIMIT 0,20"))) {
				die('Erreur : ' . mysql_error());
			}	
			while($ligne=mysql_fetch_row($requete))
			{
				$id_sel= $ligne[1];
				if (!($requete2=mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`='$id_sel' LIMIT 1"))) {
					die('Erreur : ' . mysql_error());
				}	
				$ligne2=mysql_fetch_row($requete2) ;
				$prenom_sel=stripslashes($ligne2[0]);
				$time_ann=$date=date('d/m/y',$ligne[2]);
				$rub_ann= $ligne[3];
				if($rub_ann=='PRO')
				{
					$rub_ann="propose";
				}
				else
				{
					$rub_ann="demande";
				}
				$message_ann= stripslashes($ligne[4]);
				echo "<tr>
				<td width=30% class='t2' height='22px'><a href='profil.php?id=".$id_sel."'>".$prenom_sel."(".$id_sel.")</a> vous ".$rub_ann.":</td>
				<td width=30% class='t2'>".$message_ann."</td>
				</tr>";
			}
			echo "</table><br>";	
		}
		elseif($_POST['catalogue']!= NULL)
		{
			echo "<table summary=\"\" class='tablevu' border=\"1\" >
			<tr>
			<th colspan='2'>Recherche dans les catalogues:</th>
			</tr>";
			$recherche=nl2br(htmlentities($_POST['catalogue'], ENT_QUOTES, "UTF-8"));
			if (!($requete=mysql_query("SELECT * FROM `catalogue` WHERE `designation` REGEXP '$recherche' LIMIT 0,20"))) {
				die('Erreur : ' . mysql_error());
			}	
			while($ligne=mysql_fetch_row($requete))
			{
				$id_cat= $ligne[0];
				$designation_cat=stripslashes($ligne[1]);
				echo "<tr>
				<td width=30% class='t2' colspan='2'><p class=left><a href='catalogue2.php?id_cat=$id_cat'><img src='images/flechedroite.gif' width='20'>&nbsp;<b><span class='ROUGE'>".$designation_cat."</span></b></a></p></td>				</tr>";
			}
			echo "</table><br>";
			// recherche dans les rubriques
			echo "<table summary=\"\" class='tablevu' border=\"1\" >
			<tr>
			<th colspan='2'>Recherche dans les rubriques:</th>
			</tr>
			<td width=30% class='t1'>Nom du catalogue:</td>
			<td width=70% class='t1'>Nom de la rubrique:</td>";
			if (!($requete=mysql_query("SELECT * FROM `rubrique` WHERE `designation` REGEXP '$recherche' LIMIT 0,20"))) {
				die('Erreur : ' . mysql_error());
			}	
			while($ligne=mysql_fetch_row($requete))
			{
				$id_cat= $ligne[1];
				$designation_rub=stripslashes($ligne[2]);
				if (!($designation_cat=mysql_fetch_row(mysql_query("SELECT `designation` FROM `catalogue` WHERE `id_cat`='$id_cat'")))) {
					die('Erreur : ' . mysql_error());
				}
				$designation_cat=stripslashes($designation_cat[0]);
				echo "<tr>
				<td class='t2'><p class=left><a href='catalogue2.php?id_cat=$id_cat'><img src='images/flechedroite.gif' width='20'>&nbsp;<b><span class='ROUGE'>".$designation_cat."</span></b></a></p></td>
				<td class='t2'><p><a href='catalogue2.php?id_cat=$id_cat'><b>".$designation_rub."</span></b></a></p></td></tr>";
			}
			echo "</table><br>";
			// recherche dans les sous rubriques
			echo "<table summary=\"\" class='tablevu' border=\"1\" >
			<tr>
			<th colspan='3'>Recherche dans les sous-rubriques:</th>
			</tr>
			<tr>
			<td width=30% class='t1'>Nom du catalogue:</td>
			<td width=30% class='t1'>Nom de la rubrique:</td>
			<td width=40% class='t1'>Nom de la sous-rubrique:</td>";
			if (!($requete=mysql_query("SELECT * FROM `sous_rubrique` WHERE `designation` REGEXP '$recherche' LIMIT 0,20"))) {
				die('Erreur : ' . mysql_error());
			}	
			while($ligne=mysql_fetch_row($requete))
			{
				$id_rub= $ligne[1];
				$id_ss_rub= $ligne[0];
				$designation_ss_rub=stripslashes($ligne[2]);
				if (!($designation_rub=mysql_fetch_row(mysql_query("SELECT `id_cat`,`designation` FROM `rubrique` WHERE `id_rubrique`='$id_rub'")))) {
					die('Erreur : ' . mysql_error());
				}
				$id_cat=$designation_rub[0];
				$designation_rub=stripslashes($designation_rub[1]);					
				if (!($designation_cat=mysql_fetch_row(mysql_query("SELECT `designation` FROM `catalogue` WHERE `id_cat`='$id_cat'")))) {
					die('Erreur : ' . mysql_error());
				}
				$designation_cat=stripslashes($designation_cat[0]);
				echo "<tr>
				<td class='t2'><p class=left><a href='catalogue2.php?id_cat=$id_cat'><img src='images/flechedroite.gif' width='20'>&nbsp;<b><span class='ROUGE'>".$designation_cat."</span></b></a></p></td>
				<td class='t2'><p><a href='catalogue2.php?id_cat=$id_cat'><b>".$designation_rub."</b></a></p></td>
				<td class='t2'><p><a href='selistes.php?type=menu&catalogue=$id_cat&rubrique=$id_rub&ssrubrique=$id_ss_rub'>".$designation_ss_rub."</a></p></td></tr>";
			}
			echo "</table><br>";
			// recherche dans les détail de services
			echo "<table summary=\"\" class='tablevu' border=\"1\" >
			<tr>
			<th colspan='4'>Détails des demandes et propositions de services:</th>
			</tr>
			<tr>
			<td width=20% class='t1'>Nom du catalogue:</td>
			<td width=20% class='t1'>Nom de la rubrique:</td>
			<td width=20% class='t1'>Nom de la sous-rubrique:</td>
			<td width=40% class='t1'>Détails:</td>";
			if (!($requete=mysql_query("SELECT * FROM `services` WHERE `detail` REGEXP '$recherche' LIMIT 0,50"))) {
				die('Erreur : ' . mysql_error());
			}	
			while($ligne=mysql_fetch_row($requete))
			{
				$id_sel=$ligne[0];
				$id_ss_rub=$ligne[1];
				$detail=stripslashes($ligne[4]);
				if (!($ligne1=mysql_fetch_row(mysql_query("SELECT * FROM `sous_rubrique` WHERE `id_ss_rub`='$id_ss_rub'")))) {
					die('Erreur : ' . mysql_error());
				}
				$id_rub= $ligne1[1];
				$designation_ss_rub=stripslashes($ligne1[2]);
				if (!($id_cat=mysql_fetch_row(mysql_query("SELECT `id_cat`,`designation` FROM `rubrique` WHERE `id_rubrique`='$id_rub'")))) {
					die('Erreur : ' . mysql_error());
				}
				$designation_rub=stripslashes($id_cat[1]);					
				$id_cat=$id_cat[0];
				if (!($designation_cat=mysql_fetch_row(mysql_query("SELECT `designation` FROM `catalogue` WHERE `id_cat`='$id_cat'")))) {
					die('Erreur : ' . mysql_error());
				}
				$designation_cat=stripslashes($designation_cat[0]);
				if (!($prenom_sel=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`='$id_sel'")))) {
					die('Erreur : ' . mysql_error());
				}
				$prenom_sel=stripslashes($prenom_sel[0]);
				echo "<tr>
				<td class='t2'><p class=left><a href='catalogue2.php?id_cat=$id_cat'><img src='images/flechedroite.gif' width='20'>&nbsp;<b><span class='ROUGE'>".$designation_cat."</span></b></a></p></td>
				<td class='t2'><p><a href='catalogue2.php?id_cat=$id_cat'><b>".$designation_rub."</b></a></p></td>
				<td class='t2'><p><a href='selistes.php?type=menu&catalogue=$id_cat&rubrique=$id_rub&ssrubrique=$id_ss_rub'>".$designation_ss_rub."</a></p></td>
				<td class='t2'><p><a href='profil.php?id=$id_sel'>".$prenom_sel."(".$id_sel.")</a><br><span class='pasimportant'>".$detail."</span></p></td></tr>";
			}
			echo "</table><br>";			
		}
		// formulaire
		else
		{
			echo "
			<form method=\"get\" action=\"recherche.php\" enctype=\"multipart/form-data\">
			<table summary=\"\" width=\"50%\" border=\"0\" >
			<tr>
			<th colspan='2'>Recherche par numéro:</th>
			</tr>
			<tr>
			<td width=\"30%\" class='t1' style=\"text-align: right\">Numéro du séliste:</td>
			<td class='t2' style=\"text-align: left\"><input type='text' name='id' size='20' maxlength='20' /></td>
			</tr>
			<tr>
			<th colspan='2'>Recherche par prénom:</th>
			</tr>
			<tr>
			<td class='t1' style=\"text-align: right\">Prénom du séliste:</td>
			<td class='t2' style=\"text-align: left\"><input type='text' name='prenom' size='20' maxlength='20' /></td>
			</tr>
			<tr>
			<th colspan='2'>Recherche par ville:</th>
			</tr>
			<tr>
			<td class='t1' style=\"text-align: right\">Ville du séliste:</td>
			<td class='t2' style=\"text-align: left\"><input type='text' name='ville' size='20' maxlength='40' /></td>
			</tr>
			";
			if(($grade=='MODERATEUR')||($grade=='ADMIN'))
			{
			echo "
			<tr>
			<th colspan='2 bleu'><hr width='70%'>Modérateur uniquement:</th>
			</tr>
			<tr>
			<td class='t1' style=\"text-align: right\"><p class='bleu'>Recherche par Courriel:</p></td>
			<td class='t2' style=\"text-align: left\"><input type='text' name='courriel' size='30' maxlength='50' /></td>
			</tr>";
			}
			echo "
			<tr>
			<td class='t1' colspan='2'><input type=\"submit\" value=\"Lancer les recherches \"></td>
			</tr>
			</table></form>";
			if($grade=='ADMIN')
			{
			echo "
			<form method=\"post\" action=\"recherche.php\" enctype=\"multipart/form-data\">
			<table summary=\"\" width=\"50%\" border=\"0\" >
			<tr>
			<th colspan='2 rouge'><hr width='70%'>Admin uniquement:</th>
			</tr>
			<tr>
			<td class='t1' style=\"text-align: right\"><p class='rouge'>Recherche dans les message privés:</p></td>
			<td class='t2' style=\"text-align: left\"><input type='text' name='message' size='30' maxlength='50' /></td>
			</tr>
			<tr>
			<td class='t1' colspan='2'><input type=\"submit\" value=\"Lancer les recherches Admin\"></td>
			</tr>
			</table></form>";
			}			
			
		}
		echo "</div><br>";
	}
	else
	{ // délai dépassé
		header ("location:troptard.php");
		session_destroy();
	}
}
else
{ 	 // pas de session
	header ("location:404.php");
	session_destroy();
}
mysql_close($connexion);
include ("fin.php");	
?>
