<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/

if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
{								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade`, `prenom`, `email` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		$prenom=$ligne[1];
		$email_seliste=$ligne[2];
		if (!($query=mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess=mysql_num_rows($query);
		switch ($grade)
		{
			case 'SELISTE' : include("lien/seliste.php");break;	
			case 'MODERATEUR' :include("lien/seliste.php"); include("lien/moderateur.php");break;
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page
		if(($grade=='ADMIN')||($grade=='MODERATEUR'))
		{ // voir les echanges d'un membre
			$id_membre=htmlentities($_GET['id'], ENT_QUOTES, "UTF-8");
			if (!($requete1=mysql_query("SELECT * FROM `echanges` where  `id_seliste_fou`='$id_membre' OR `id_seliste_dem`='$id_membre' ORDER BY `date_ech` DESC LIMIT 20 "))) {
			   die('Erreur : ' . mysql_error()); 
			}
			if (!($requete=mysql_query("SELECT *  FROM `selistes` WHERE `id_seliste`='$id_membre'"))) {
			    die('Erreur : ' . mysql_error());
			}	
			$ligne=mysql_fetch_row($requete) ;
			$prenom_sel=stripslashes($ligne[5]);
			echo "<br><div class='corps'><br>
			<p class='titre'>Les 20 derniers échanges de ".$prenom_sel."(".$id_membre."):</p><br>
			<table summary=\"\" border=\"1\" width=\"100%\">
			<tr>
				<td class='t1 teinte1'><p>Au débit de</p></td>
				<td class='t1 teinte1'><p>Au profit de</p></td>
				<td class='t1 teinte1'><p>Date de l'échange</p></td>
				<td class='t1 teinte1'><p>$monnaie</p></td>
				<td class='t1 teinte1'><p>Catégorie</p></td>
				<td class='t1 teinte1'><p>Rubrique</p></td>		
				<td class='t1 teinte1'><p>Description</p></td>						
				<td class='t1 teinte1'><p>Détail</p></td>
				<td class='t1 teinte1'><p>État</p></td>
				<td class='t1 teinte1'><p>Enregistré par</p></td>
				<td class='t1 teinte1'><p>Date enregistrement</p></td>
			</tr>";
			while($ligne=mysql_fetch_row($requete1))
			{
				$id_echange=$ligne[0];
				$id_sel2=$ligne[1];
				if (!($requete5=mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`='$id_sel2'"))) {
					die('Erreur : ' . mysql_error());
				}
				$ligne5=mysql_fetch_row($requete5) ;
				$prenom_sel2=stripslashes($ligne5[0]);
				$id_sel=$ligne[2];
				if (!($requete2=mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`='$id_sel'"))) {
					die('Erreur : ' . mysql_error());
				}
				$ligne2=mysql_fetch_row($requete2) ;
				$prenom_sel=stripslashes($ligne2[0]);
				$date=$ligne[3];
				$date=date('d/m/y à H\h i\m\i\n',$date);		
				$grains=$ligne[4];
				$id_rubrique=$ligne[5];
				$etat=$ligne[6];
				$detail=stripslashes($ligne[7]);
				$description=$ligne[10];
				if (!($requete3=mysql_query("SELECT `designation`, `id_cat` FROM `rubrique` WHERE `id_rubrique`='$id_rubrique'"))) {
					die('Erreur : ' . mysql_error());
				}
				$ligne3=mysql_fetch_row($requete3);
				$designation_rub=stripslashes($ligne3[0]);
				$id_cat=$ligne3[1];
				if (!($requete4=mysql_query("SELECT `designation` FROM `catalogue` WHERE `id_cat`='$id_cat'"))) {
					die('Erreur : ' . mysql_error());
				}
				$ligne4=mysql_fetch_row($requete4);
				$designation_cat=stripslashes($ligne4[0]);
				if($etat=='DEM'){$etat="<span class='bleu'>Demande en cours</span>";}
				elseif($etat=='VAL'){$etat="<span class='bleu'>Attente comptable</span>";}
				elseif($etat=='REF'){$etat="<span class='rouge'><b>DEMANDE REFUSE</b></span>";}	
				elseif($etat=='OKI'){$etat="<span class='vert'><b>Ok</b></span>";}
				else {}
				$date_eve=$ligne[8];
				$date_eve=date('d/m/y',$date_eve);
				$id_sel_enr=$ligne[9];
				if (!($requete2=mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`='$id_sel_enr'"))) {
					die('Erreur : ' . mysql_error());
				}
				$ligne2=mysql_fetch_row($requete2) ;
				$prenom_sel_enr=stripslashes($ligne2[0]);	
				echo "<tr><td class=t2><p><a href='profil.php?id=".$id_sel."'>".$prenom_sel."(".$id_sel.")</a></p></td><td class=t2><p><a href='profil.php?id=".$id_sel2."'>".$prenom_sel2."(".$id_sel2.")</a></p></td><td class=t2><p>".$date_eve."</p></td><td class=t3><p>".$grains." ".$monnaie."</p></td><td class=t3><p>".$designation_cat."</p></td><td class=t3><p>".$designation_rub."</p></td><td class=t3><p>".$description."</p></td><td class=t3><p>".$detail."</p></td><td class=t3><p>".$etat."</p></td><td class=t3><p><a href='profil.php?id=".$id_sel_enr."'>".$prenom_sel_enr."(".$id_sel_enr.")</a></p></td><td class=t3><p>".$date."</p></td></tr>";		
			}
			echo "</table><br></div><br>";
		}
	}
	else
	{ 	 // délai dépassé
		header ("location:troptard.php");
		session_destroy();
	}
 }
else
{ 	 // pas de session
	header ("location:404.php");
	session_destroy();
}
mysql_close($connexion);
include ("fin.php");	
?>
