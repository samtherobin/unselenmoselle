<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/

if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
{								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
		switch ($grade)
		{
			case 'SELISTE' : include("lien/seliste.php");break;	
			case 'MODERATEUR' :include("lien/seliste.php"); include("lien/moderateur.php");break;
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page
			echo "<br><div class=\"corps\"><br>";
		//debut de l'edition
		if($_GET['action']=='edition') 
		{	
			if (!($requete5=mysql_query("SELECT `id_cat`, `designation` FROM `catalogue` ORDER BY `designation` ASC"))) {
				die('Erreur : ' . mysql_error());
			}
			$cpt_req++;
			echo "
			<table class='tablevu'>
			<tr>
			<td><p class='titre'>Ajouter un service sur mon profil:</p></td>
			</tr>
			<tr>
				<td class=\"t1\"><p>Vous souhaitez ajouter un service sur votre profil ?<br>
				Choisissez dans le menu déroulant, si c'est un service que vous proposez/pouvez fournir aux autres membres, ou que vous demandez.</td>
			</tr>
			<tr><td class=t1>
			<select name=\"etat\" onChange=\"document.location='services.php?action=edition&amp;etat='+this.value\">
				<option value=\"\">--- Ajouter un service que je ... ---</option>
				<option value=\"PRO\" ";
				if($_GET['etat']=='PRO')
						{
							echo "selected";
						}
				echo ">Propose</option>
				<option value=\"DEM\"";
				if($_GET['etat']=='DEM')
						{
							echo "selected";
						}
				echo ">Demande</option></select>";
			// menu déroulant catalogue
			if( $_GET['etat']!=null)
			{
				$etat=$_GET['etat'];
				echo "<select name=\"catalogue\" onChange=\"document.location='services.php?action=edition&amp;etat=$etat&amp;catalogue='+this.value\">
				<option value=\"\"> --- dans le catalogue ---</option>";
				// menu déroulant catalogue	
				while($ligne=mysql_fetch_row($requete5))
				  	{
						echo "<option value='".$ligne[0]."' ";
						if($_GET['catalogue']==$ligne[0])
						{
							echo "selected";
						}
						echo " >".$ligne[1]."</option>";
					}
				echo "</select>";
			}
		
			// menu déroulant rubrique
			if( $_GET['catalogue']!=null)
			{
				$id_cat=$_GET['catalogue'];
				$etat=$_GET['etat'];
				echo "<select name='rubrique' 
				onChange=\"document.location='services.php?action=ajouter&amp;etat=$etat&amp;id_rubrique='+this.value\">
				<option value=\"\"> -- dans la rubrique --</option> ";
				if (!($requete6=mysql_query("SELECT `id_rubrique`, `designation` FROM `rubrique` where `id_cat`='$id_cat' ORDER BY `designation` ASC"))) {
					die('Erreur : ' . mysql_error());
				}
				$cpt_req++;
				while($ligne=mysql_fetch_row($requete6))
			  	{
					echo "<option value='".$ligne[0]."'";
					if($_GET['rubrique']==$ligne[0])
					{
						echo "selected";
					}
						echo ">".$ligne[1]."</option>";
				}
			echo "</select>";
			}
		    
			echo "</table><br><br>";
			
			// forumulaire ajout dans le catalogue
			echo "
			<form method=\"post\" action=\"services.php?action=proposer\" enctype=\"multipart/form-data\">
			<table summary=\"\" class='tablevu' cellpadding='5'>
			<tr>
				<td><p class='titre'>Le service que vous proposez/demandez n'est pas dans la catalogue ?</p>
				<p class='pasimportant'>Décrivez le service à rajouter au catalogue et dans quelle rubrique le mettre.<br>
			Votre demande sera traitée par un modérateur dans quelques jours. Si le service existe dans le catalogue il sera forcément refusé.</p></td>
			</tr>
			<tr>
				<td><p class=t4><textarea  name='service' cols='80' rows='3' ></textarea><br>
								<input type='submit' value='Demander à ajouter ce nouveau service dans le catalogue'></p></td>
			</tr></table></form><br>";
				
		
			//on enregistre les modifs
			echo "<table summary=\"\" class='tablevu' border='1' cellpadding='5' width='80%'>
			<tr>
			<th colspan='5'><p><span class='bleu'>Les services que je propose :</span></p></th>
			</tr>";
			// service proposés
			if (!($requete1=mysql_query("SELECT `id_service`, `rubrique`.`id_rubrique`, `rubrique`.`designation`, `catalogue`.`id_cat`, `catalogue`.`designation`, `description` , `detail` FROM `services`, `rubrique`, `catalogue` WHERE `rubrique`.`id_rubrique`=`services`.`id_rubrique` AND `rubrique`.`id_cat`=`catalogue`.`id_cat` AND `id_seliste`='$id_seliste' AND `etat`='PRO' ORDER BY `catalogue`.`designation`, `rubrique`.`designation`"))) {
			    die('Erreur : ' . mysql_error());
			}
			$id_rubrique_precedent = 0;
			$id_cat_precedent = 0;
			while($ligne=mysql_fetch_row($requete1))
			{
			    $id_service = $ligne[0];
				$id_rubrique = $ligne[1];
				$designation_rubrique = stripslashes($ligne[2]);
				$id_cat = $ligne[3];
				$designation_cat = stripslashes($ligne[4]);
				$description = stripslashes($ligne[5]);
				$detail = stripslashes($ligne[6]);
				$message = $detail;
				include("smileyscompagnie.php");
				$detail = $message;
				if ($id_cat != $id_cat_precedent) {
				    echo "<tr class='teinte1'><td class='t1' colspan='3'><p class='important'>$designation_cat</p></td></tr>";
				    $id_cat_precedent = $id_cat;
				}
				if ($id_rubrique != $id_rubrique_precedent) {
				    echo "<tr class='teinte2'><td class='t1' colspan='3'><p class='pasimportant'>$designation_rubrique</p></td></tr>";
				    $id_rubrique_precedent = $id_rubrique;
				}
				echo "<tr><td class=t3 width='30%'><p>".$description."</p></td><td class=t4 width='50%'><p class='pasimportant'>".$detail."</p></td><td class=t2>
					<a href=\"services.php?action=supprimer&amp;id_service=$id_service\"><img src=\"images/croix.jpg\">Supprimer</a><br/><br/>
					<a href=\"details.php?action=editer&amp;id_service=$id_service\"><img src=\"images/editer.gif\">Editer les détails</a>
					</td>";
			}
			echo "
			<tr>
			<th colspan='3'><p><span class='rouge'>Les services que je demande :</span></p></th>
			</tr>";
			// service demandes
			if (!($requete1=mysql_query("SELECT `id_service`, `rubrique`.`id_rubrique`, `rubrique`.`designation`, `catalogue`.`id_cat`, `catalogue`.`designation`, `description` , `detail` FROM `services`, `rubrique`, `catalogue` WHERE `rubrique`.`id_rubrique`=`services`.`id_rubrique` AND `rubrique`.`id_cat`=`catalogue`.`id_cat` AND `id_seliste`='$id_seliste' AND `etat`='DEM' ORDER BY `catalogue`.`designation`, `rubrique`.`designation`"))) {
			    die('Erreur : ' . mysql_error());
			}
			$id_rubrique_precedent = 0;
			$id_cat_precedent = 0;
			while($ligne=mysql_fetch_row($requete1))
			{
			    $id_service = $ligne[0];
				$id_rubrique = $ligne[1];
				$designation_rubrique = $ligne[2];
				$id_cat = $ligne[3];
				$designation_cat = $ligne[4];
				$description = stripslashes($ligne[5]);
				$detail = stripslashes($ligne[6]);
				$message = $detail;
				include("smileyscompagnie.php");
				$detail = $message;
				if ($id_cat != $id_cat_precedent) {
				    echo "<tr class='teinte1'><td class='t1' colspan='3'><p class='important'>$designation_cat</p></td></tr>";
				    $id_cat_precedent = $id_cat;
				}
				if ($id_rubrique != $id_rubrique_precedent) {
				    echo "<tr class='teinte2'><td class='t1' colspan='3'><p class='pasimportant'>$designation_rubrique</p></td></tr>";
				    $id_rubrique_precedent = $id_rubrique;
				}
				echo "<tr><td class=t3><p>".$description."</p></td><td class=t3><p class='pasimportant'>".$detail."</p></td><td class=t2><br>
					<a href=\"services.php?action=supprimer&amp;id_service=$id_service\"><img src=\"images/croix.jpg\">Supprimer</a><br><br>
					<a href=\"details.php?action=editer&amp;id_service=$id_service\"><img src=\"images/editer.gif\">Editer les détails</a><br>
					</td>";
			}
    		echo "</table><br>";
		}
		elseif($_GET['action']=='proposer')
		{ //enregistré la demande d'ajout de service 
			$description=nl2br(htmlentities($_POST['service'], ENT_QUOTES, "UTF-8"));
			if (!(mysql_query("INSERT INTO `demande_services` VALUES ('', '$id_seliste', '$time', '$description')"))) {
				die('Erreur : ' . mysql_error());
			}
			echo "<br><p class='titre'>Votre demande d'ajout de service à été transmise au modérateur qui s'occupe du catalogue.</p>	
				<br><p class='t4'><a href='bureau.php' title='Bureau'>Retour sur mon bureau</a>&nbsp;&nbsp;<a href='services.php?action=edition'>Retour services</a></p><br><br> ";
			// envoi du mail au modo
			if (!($requete=mysql_query("SELECT `texte` FROM  `variables` WHERE  `variable`='MOD4' LIMIT 1"))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete);
			$MOD4=stripslashes($ligne[0]);
			if (!($requete=mysql_query("SELECT `prenom`,`email` FROM  `selistes` WHERE  `id_seliste`='$MOD4' LIMIT 1"))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete);
			$PMOD4=stripslashes($ligne[0]);
			$MMOD4=stripslashes($ligne[1]);
			// envoi du mail:
			$message2=stripslashes("[".$nom."]MODERATEUR Catalogue: Nouvelle demande d'ajout à moderer");
			$headers = "From: Catalogue $nom <$email>\n";
			$headers .= "Reply-To: $nom\n";
			$headers .= "X-Sender: $nom\n";
			$headers .= "X-Author: Moderation $site\n";
			$headers .= "X-Priority:1\n";
			$headers .= "X-Mailer: PHP\n";
			$headers .= "Return_Path: <$email>\n";
			$headers .='Content-Type: text/html; charset="UTF-8"'."\n";
			$PMOD4=stripslashes($PMOD4);
			$message=stripslashes($description);
			// envoi du mail 								
			$mel ="<html><head><title></title></head><body>
			<p>Bonjour ".$PMOD4.", une nouvelle demande d'ajout viens d'être formulé dans la gestion du catalogue par <a href=http://".$site."/index.php?vers=profil.php?id=".$id_seliste.">".$prenom."(".$id_seliste.")</a>.
			<br><hr width=90%><br>".$message."
			<br><hr width=90%>
			<br>Merci de vous connecter pour l'ajouter ou pas dans le catalogue.
			<br>Cordialement, <br> ".$nom.".
			<br><br><i>Vous recevez ces alertes suite à votre poste de Modérateur sur <a href=http://".$site."/> ".$site." </a>.</i></p></body></html>";
			sw_mail($MMOD4,$message2,$mel,$headers);
			// fin d'envoi du mail fin d'envoi de l'actu en moderation			
		}
		elseif(($_GET['action']=='supprimer')&&($_GET['id_service']!=null))
		{ //Suppresion d'un service
			$id_service=htmlentities($_GET['id_service'], ENT_QUOTES, "UTF-8");
			if (!mysql_query("DELETE FROM `services` WHERE `id_service`='$id_service'")) {
				die('Requête invalide : ' . mysql_error());
			}
			echo "<br><p class='titre'>Service supprimé de votre profil.</p>	
				<br><p class='t4'><a href='bureau.php' title='Bureau'>Retour sur mon bureau</a>&nbsp;&nbsp;<a href='services.php?action=edition'>Retour services</a></p><br><br>";
		}
		elseif(($_GET['action']=='ajouter')&&($_GET['etat']!=null)&&($_GET['id_rubrique']!=null))
		{ //ajout d'un service
			$id_rubrique=htmlentities($_GET['id_rubrique'], ENT_QUOTES, "UTF-8");
			$etat=htmlentities($_GET['etat'], ENT_QUOTES, "UTF-8");
			if (($etat=='PRO')||($etat=='DEM')	)
			{
			    $description=htmlentities($_POST['description'], ENT_QUOTES, "UTF-8");
				$detail=htmlentities($_POST['detail'], ENT_QUOTES, "UTF-8");
				if($description!=null)
				{
					if (!mysql_query("INSERT INTO `services` (id_seliste, id_rubrique, etat, time, description, detail) VALUES ('$id_seliste', '$id_rubrique', '$etat', '$time','$description', '$detail')")) {
						die('Requête invalide : ' . mysql_error());
					}
					
					echo "<br><p class='titre'>Service ajouté sur votre profil.</p>	
				<br><p class='t4'><a href='bureau.php' title='Bureau'>Retour sur mon bureau</a>&nbsp;&nbsp;<a href='services.php?action=edition'>Retour services</a></p><br><br>";
				}
				else
				{
					echo "
					
					<form method=\"post\" action=\"services.php?action=ajouter&amp;etat=$etat&amp;id_rubrique=$id_rubrique\" enctype=\"multipart/form-data\">
					<table summary=\"\" width='40%'>
					<tr>
						<td><p class='titre'>Ajout d'une ";
				    if ($etat == 'PRO') {
				        echo "proposition";
				    }
				    else if ($etat == 'DEM') {
				        echo "demande";
				    }
					echo " de service<br/></p><br/></td>	
					</tr>";
					if (!($requete3=mysql_query("SELECT `designation`, `id_cat` FROM `rubrique` WHERE `id_rubrique`='$id_rubrique'") )) {
						die('Erreur : ' . mysql_error());
					}
					$ligne3=mysql_fetch_row($requete3);
					$designation_rub=stripslashes($ligne3[0]);
					$id_cat=$ligne3[1];
					if (!($requete4=mysql_query("SELECT `designation` FROM `catalogue` WHERE `id_cat`='$id_cat'") )) {
						die('Erreur : ' . mysql_error());
					}
					$ligne4=mysql_fetch_row($requete4);
					$designation_cat=stripslashes($ligne4[0]);
					echo "
					<tr><td class='t2'><p class='important'>Catégorie : <span class='gris'>$designation_cat</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rubrique : <span class='gris'>$designation_rub</span></p></td></tr>
					<tr>
						<td class=t2>
						<p class='important'>Description du service : </p><input name='description' type='text' style='width: 100%;'/><br/><br/>
						<p class='important'>Détails :</p>
						<textarea  name='detail' cols='80' rows='4' ></textarea><br/>
						<p class='important'><input type='submit' value='Ajouter'></p></td>
					</tr>
					<tr>
						<td class='t1'>
						<br><a href='bureau.php'>Retour sur mon bureau</a>&nbsp;&nbsp;&nbsp;
						<a href='services.php?action=edition'>Retour services</a><br>
						</td> 
					</tr>
					</table></form>";
				}
			}
			else 
			{//pirate
				header ("location:404.php");
				session_destroy();	
			}
		}
		else
		{ 	//pirate
			header ("location:404.php");
			session_destroy();			
		}
		echo "</div>";
		//fin edition
	} 
	//delai depassé
	else
	{
		header ("location:troptard.php");
		session_destroy();
	}
}
// pas de sesion
else
{
	header ("location:404.php");
	session_destroy();
}
mysql_close($connexion); 
include ("fin.php");	
?>
