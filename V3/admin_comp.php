<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/
if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
{								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
	 	switch ($grade)
		{
			case 'SELISTE' : header("location:404.php");break;	
			case 'MODERATEUR' :include("lien/seliste.php"); include("lien/moderateur.php");break;
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page
		echo "<br><br><div class='corps'><br>";
		if (($_GET['action']=='valider')&&($_GET['id']!=null))
		{
			$id_ech=$_GET['id'];
			if (!($requete=mysql_query("SELECT * FROM `echanges` WHERE `id_echange`='$id_ech'"))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete);
			$id_sel_fou=$ligne[1];
			$id_sel_dem=$ligne[2];
			$grains=$ligne[4];
			$etat=$ligne[6];
			// recup sous et prenom des selistes
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom`, `grains` FROM `selistes` WHERE `id_seliste`='$id_sel_fou'")))) {
				die('Erreur : ' . mysql_error());
			}
			$prenom_fou=stripslashes($recup[0]);
			$grains_fou=$recup[1];
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom`, `grains` FROM `selistes` WHERE `id_seliste`='$id_sel_dem'")))) {
				die('Erreur : ' . mysql_error());
			}
			$prenom_dem=stripslashes($recup[0]);
			$grains_dem=$recup[1];
			// mise a jour des comptes grains
			$grains_fou=($grains_fou)+($grains);
			$grains_dem=($grains_dem)-($grains);
			// enregistrement		
			if($etat=='VAL')
			{
				if (!($requete=mysql_query("UPDATE `selistes` SET `grains` = '$grains_fou' WHERE `id_seliste` ='$id_sel_fou' LIMIT 1") )) {
					die('Erreur : ' . mysql_error());
				}
				if (!($requete=mysql_query("UPDATE `selistes` SET `grains` = '$grains_dem' WHERE `id_seliste` ='$id_sel_dem' LIMIT 1") )) {
					die('Erreur : ' . mysql_error());
				}
				if (!($requete=mysql_query("UPDATE `echanges` SET `etat` = 'OKI' WHERE `id_echange` ='$id_ech' LIMIT 1") )) {
					die('Erreur : ' . mysql_error());
				}
				echo "<br><div class='message'><br>
				<p class='t1'>L'échange numéro ".$id_ech." à été validé et comptabilisé.</p>
				<br><p class='t4'>Le compte de ".$prenom_fou."(".$id_sel_fou.") à été crédité de $grains il passe à $grains_fou</p>
				<br><p class='t4'>Le compte de ".$prenom_dem."(".$id_sel_dem.") à été débité de $grains il passe à $grains_dem</p>
				<br>
				<p><a href='admin_comp.php' class='amodo'>Retour validaton des échanges</a>&nbsp;&nbsp;
				<a href='bureau.php'>Retour au bureau</a></p><br></div>";	
			}
			else
			{
				echo "<br><div class='message'><br>
				<p class='t1'>L'échange numéro ".$id_ech." à déja été validé et comptabilisé doucement avec le double clic!</p>
				<br><p class='t4'>L'erreur comptable a été évité !!!!!!!!</p>
				<br>
				<p><a href='admin_comp.php' class='amodo'>Retour validaton des échanges</a>&nbsp;&nbsp;
				<a href='bureau.php'>Retour au bureau</a></p><br></div>";	
			}
		}
		
		if (!($requete=mysql_query("SELECT * FROM `echanges` WHERE `etat`='VAL'"))) {
			die('Erreur : ' . mysql_error());
		}
		echo "<p class='titre'>Validation des échanges</p><br>
		<p><a href='modo_ajout_ech.php'>Ajouter un échange manuellement</a></p><br>
		<table summary=\"\" class='tablevu' border=\"1\" width=\"100%\" cellpadding=\"2\">
		<tr><td colspan=9><p class='t1'>Liste des échanges à valider:</p></td></tr>
		<tr>
			<td class='t1'><p>Séliste qui fournit:</p></td>
			<td class='t1'><p>Séliste qui demande:</p></td>
			<td class='t1'><p>Enregistré le:</p></td>
			<td class='t1'><p>Echange du:</p></td>
			<td class='t1'><p>$monnaie:</p></td>
			<td class='t1'><p>Rubrique:</p></td>
			<td class='t1'><p>Détail:</p></td>
			<td class='t1'><p>Enregistré par:</p></td>
			<td class='t1' width='10%'><p>Validation:</p></td>
		</tr>";
		while($ligne=mysql_fetch_row($requete))
		{
			$id_ech=$ligne[0];
			$id_sel_fou=$ligne[1];
			$id_sel_dem=$ligne[2];
			$time_ech=$ligne[3];
			$date=date('d/m/y',$time_ech);
			$date2=date('à H\hi',$time_ech);
			$date=$date."<br>".$date2;
			$grains=$ligne[4];
			$id_ss_rub=$ligne[5];
			$detail_ech=stripslashes($ligne[7]);
			$date_ech=$ligne[8];
			$date_ech=date('d/m/y',$date_ech);
			$date2=date('à H\hi',$date_ech);
			$date_ech=$date_ech."<br>".$date2;
			$id_sel_enr=$ligne[9];
			// recup info seliste
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`='$id_sel_fou'")))) {
				die('Erreur : ' . mysql_error());
			}
			$prenom_fou=stripslashes($recup[0]);
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`='$id_sel_dem'")))) {
				die('Erreur : ' . mysql_error());
			}
			$prenom_dem=stripslashes($recup[0]);
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`='$id_sel_enr'")))) {
				die('Erreur : ' . mysql_error());
			}
			$prenom_enr=stripslashes($recup[0]);
			if (!($requete2=mysql_query("SELECT `designation`, `id_rubrique` FROM `sous_rubrique` WHERE `id_ss_rub`='$id_ss_rub'") )) {
				die('Erreur : ' . mysql_error());
			}
			$ligne2=mysql_fetch_row($requete2);
			$rubrique=stripslashes($ligne2[0]);
			$id_rubrique=stripslashes($ligne2[1]);
			// recherche des catalogue et rubrique
			if (!($requete3=mysql_query("SELECT `designation`, `id_cat` FROM `rubrique` WHERE `id_rubrique`='$id_rubrique'") )) {
				die('Erreur : ' . mysql_error());
			}
			$ligne3=mysql_fetch_row($requete3);
			$designation_rub=stripslashes($ligne3[0]);
			$id_cat=$ligne3[1];
			if (!($requete4=mysql_query("SELECT `designation` FROM `catalogue` WHERE `id_cat`='$id_cat'") )) {
				die('Erreur : ' . mysql_error());
			}
			$ligne4=mysql_fetch_row($requete4);
			$designation_cat=stripslashes($ligne4[0]);
			echo "<tr>
			<td class='t4'><p><a href='profil.php?id=".$id_sel_fou."'>".$prenom_fou."(".$id_sel_fou.")</a></p></td>
			<td class='t4'><p><a href='profil.php?id=".$id_sel_dem."'>".$prenom_dem."(".$id_sel_dem.")</a></p></td>
			<td class='t2'><p>".$date."</p></td>
			<td class='t2'><p>".$date_ech."</p></td>
			<td class='t2'><p>".$grains."</p></td>
			<td class='t2'><p><b>".$designation_cat."</b><br>".$designation_rub."<br><i>".$rubrique."</i></p></td>
			<td class='t4'><p>".$detail_ech."</p></td>
			<td class='t4'><p><a href='profil.php?id=".$id_sel_enr."'>".$prenom_enr."(".$id_sel_enr.")</p></td>
			<td class='t4'>
					<a class='amodo' href=\"admin_comp.php?action=valider&amp;id=$id_ech\"><img src='images/coche.jpg'>Valider</a><br><br>
					<a class='amodo' href=\"admin_modif_ech.php?id=$id_ech\"><img src='images/editer.gif'>Modifier</a>					
					</td>
			</tr>";
		}
		echo "</table><br>";
		// rapatriement parametre
		if (!($requete=mysql_query("SELECT * FROM `echanges` WHERE NOT `etat`='OKI' AND NOT `etat`='VAL' order by `time` desc"))) {
			die('Erreur : ' . mysql_error());
		}
		echo "<br>
		<table summary=\"\" class='tablevu' border=\"1\" width=\"100%\" cellpadding=\"2\">
		<tr><td colspan=10><p class='t1'>Liste des échanges en attente d'action:</p></td></tr>
		<tr>
			<td class='t1'><p>Séliste qui fournit:</p></td>
			<td class='t1'><p>Séliste qui demande:</p></td>
			<td class='t1'><p>Enregistré le:</p></td>
			<td class='t1'><p>Echange du:</p></td>
			<td class='t1'><p>$monnaie:</p></td>
			<td class='t1'><p>Rubrique:</p></td>
			<td class='t1'><p>Détail:</p></td>
			<td class='t1'><p>Etat:</p></td>
			<td class='t1'><p>Enregistré par:</p></td>
			<td class='t1' width='10%'><p>Modification:</p></td>
		</tr>";
		while($ligne=mysql_fetch_row($requete))
		{
			$id_ech=$ligne[0];
			$id_sel_fou=$ligne[1];
			$id_sel_dem=$ligne[2];
			$time_ech=$ligne[3];
			$date=date('d/m/y',$time_ech);
			$date2=date('à H\hi',$time_ech);
			$date=$date."<br>".$date2;
			$grains=$ligne[4];
			$id_ss_rub=$ligne[5];
			$etat=$ligne[6];
			$detail_ech=stripslashes($ligne[7]);
			$date_ech=$ligne[8];
			$date_ech=date('d/m/y',$date_ech);
			$date2=date('à H\hi',$date_ech);
			$date_ech=$date_ech."<br>".$date2;
			$id_sel_enr=$ligne[9];
			// recup info seliste
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`='$id_sel_fou'")))) {
				die('Erreur : ' . mysql_error());
			}
			$prenom_fou=stripslashes($recup[0]);
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`='$id_sel_dem'")))) {
				die('Erreur : ' . mysql_error());
			}
			$prenom_dem=stripslashes($recup[0]);
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`='$id_sel_enr'")))) {
				die('Erreur : ' . mysql_error());
			}
			$prenom_enr=stripslashes($recup[0]);
			if (!($requete2=mysql_query("SELECT `designation`,`id_rubrique` FROM `sous_rubrique` WHERE `id_ss_rub`='$id_ss_rub'") )) {
				die('Erreur : ' . mysql_error());
			}
			$ligne2=mysql_fetch_row($requete2);
			$rubrique=stripslashes($ligne2[0]);
			$id_rubrique=stripslashes($ligne2[1]);
			// recherche des catalogue et rubrique
			if (!($requete3=mysql_query("SELECT `designation`, `id_cat` FROM `rubrique` WHERE `id_rubrique`='$id_rubrique'") )) {
				die('Erreur : ' . mysql_error());
			}
			$ligne3=mysql_fetch_row($requete3);
			$designation_rub=stripslashes($ligne3[0]);
			$id_cat=$ligne3[1];
			if (!($requete4=mysql_query("SELECT `designation` FROM `catalogue` WHERE `id_cat`='$id_cat'") )) {
				die('Erreur : ' . mysql_error());
			}
			$ligne4=mysql_fetch_row($requete4);
			$designation_cat=stripslashes($ligne4[0]);
			// 1 semaine = pas afficher REF
			$time2=$time-(604800);
			if(($etat=='REF')&&($time_ech>$time2))
			{
				echo "<tr>
				<td class='t4'><p><a href='profil.php?id=".$id_sel_fou."'>".$prenom_fou."(".$id_sel_fou.")</a></p></td>
				<td class='t4'><p><a href='profil.php?id=".$id_sel_dem."'>".$prenom_dem."(".$id_sel_dem.")</a></p></td>
				<td class='t2'><p>".$date."</p></td>
				<td class='t2'><p>".$date_ech."</p></td>
				<td class='t4'><p>".$grains."</p></td>
				<td class='t2'><p><b>".$designation_cat."</b><br>".$designation_rub."<br><i>".$rubrique."</i></p></p></td>
				<td class='t2'><p>".$detail_ech."</p></td>
				<td class='t1 teinte2'><p>".$etat."</p></td>
				<td class='t4'><p><a href='profil.php?id=".$id_sel_enr."'>".$prenom_enr."(".$id_sel_enr.")</a></p></td>
				<td class='t4'><a class='amodo' href=\"admin_modif_ech.php?id=$id_ech\"><img src='images/editer.gif'>Modifier</a>
				</td>
				</tr>";
			}
			elseif($etat=='REF')
			{
			}
			else
			{
				echo "<tr>
				<td class='t4'><p><a href='profil.php?id=".$id_sel_fou."'>".$prenom_fou."(".$id_sel_fou.")</a></p></td>
				<td class='t4'><p><a href='profil.php?id=".$id_sel_dem."'>".$prenom_dem."(".$id_sel_dem.")</a></p></td>
				<td class='t2'><p>".$date."</p></td>
				<td class='t2'><p>".$date_ech."</p></td>
				<td class='t4'><p>".$grains."</p></td>
				<td class='t2'><p><b>".$designation_cat."</b><br>".$designation_rub."<br><i>".$rubrique."</i></p></p></td>
				<td class='t2'><p>".$detail_ech."</p></td>
				<td class='teinte1 t1'><p>".$etat."</p></td>
				<td class='t4'><p><a href='profil.php?id=".$id_sel_enr."'>".$prenom_enr."(".$id_sel_enr.")</a></p></td>
				<td class='t4'><a class='amodo' href=\"admin_modif_ech.php?id=$id_ech\"><img src='images/editer.gif'>Modifier</a>
				</td>
				</tr>";
			}
		}
		
		echo "</table><img src='images/principecompta2.jpg' width='700'>
		</br></div>";
	}
	else
	{ 	 //délai dépassé
		header ("location:troptard.php");
		session_destroy();
	}
 }
else
{ 	 // pas de session
	header ("location:404.php");
	session_destroy();
}
mysql_close($connexion); 
include ("fin.php");	
?>
