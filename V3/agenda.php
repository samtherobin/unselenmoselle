<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/ 
if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
{								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade`, `nbr_art`, `inscription`, `prenom` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		$nbr_art=$ligne[1];
		$prenom=$ligne[3];
		$inscription=$ligne[2];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
	 	switch ($grade)
		{
			case 'SELISTE' : include("lien/seliste.php");break;				
			case 'MODERATEUR' :include("lien/seliste.php"); include("lien/moderateur.php");break;
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page
		$action=$_GET['action'];
		//afficher les article
		if($action=='lire')
		{
			echo "<br><div class=\"corps\"><br>
			
			<p class='titre'>L'agenda de ".$nom.":</p>			
			<br>
			<p><a href=\"agendav3.php\">Voir l'agenda sous forme Calendrier</a>&nbsp;&nbsp;&nbsp;
			<a href=\"agenda.php?action=proposer\"><img src=\"images/editer.gif\"> Proposer un évenement dans l'agenda</a><br></p>";
			$i=0;
			while($i<5)
			{
				$i++;
				if($i==1){$rub='INS'; $texte='Prochaines réunion d\'inscription:';}
				elseif($i==2){$rub='AUB'; $texte='Les auberges et soirées:';} 
				elseif($i==3){$rub='RAN'; $texte='Les randonnées et sorties nature:';} 
				elseif($i==4){$rub='REU'; $texte='Réunion de fonctionnement:';} 
				elseif($i==5){$rub='SOR'; $texte='Toutes les autres sorties:';} 
				else 
				{
					header ("location:troptard.php");
					session_destroy();
				};		
					if (!($requete=mysql_query("SELECT * FROM `agenda` WHERE `rubrique`='$rub' AND `publication`='OUI' AND `date_evenement`> $time ORDER BY `date_evenement` ASC"))) {
						die('Erreur : ' . mysql_error());
					}
					$f=0;
					while($ligne=mysql_fetch_row($requete))
					{
						if($f==0){echo "<br><div class='teinte3 message'><p class='titre'>".$texte."</p><hr width='20%'><br>"; $f=1;}
						$id_agenda= $ligne[0];
						$date_creation= $ligne[3];
						$date_creation=date('d/m/y à H\h i',$date_creation);
						$id_sel= $ligne[4];
						$date_evenement= $ligne[5];
						$date_evenement=date('d/m/y à H\h i',$date_evenement);	
						$titre= stripslashes($ligne[6]);
						$article= stripslashes($ligne[7]);
						$message=$article;
						include("smileyscompagnie.php" );
						$article=$message;
						//récupération du seliste
						if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`=$id_sel")))) {
							die('Erreur : ' . mysql_error());
						} 
						$prenom_seliste=stripslashes($recup[0]);	
						// recup nbr replique
						if (!($nb_replique=mysql_num_rows(mysql_query("SELECT * FROM `agenda_repliques` WHERE `id_agenda`=$id_agenda")))) {
							die('Erreur : ' . mysql_error());
						}
						echo "<div style='background-color:#ffffff' class='message'><p class=titre>".$titre." <br>
						<span class='pasimportant'>Evenement écrit par <a href=\"profil.php?id=".$id_sel."\">".$prenom_seliste."(".$id_sel.")</a> le ".$date_creation."</span></p>
						<p>Prévu le : ".$date_evenement."</p>
						<hr width=70%>						
						<span class='t2'>".$article."</span>
						<br><p><a href=\"agenda.php?action=reponses&amp;id=".$id_agenda."\"><img src=\"images/lettres.jpg\"> Voir les ".$nb_replique." Réponse";if($nb_replique>1){echo "s";}echo "</a>
						<a href=\"agenda.php?action=repondre&amp;id=".$id_agenda."\" title=\"Repondre\"><img src=\"images/lettre.jpg\"> Répondre</a>					
						"; // si il a le droit d'éditer l'article 
						if(($grade=='MODERATEUR')||($grade=='ADMIN')){echo "						
						<a class='amodo' href=\"agenda.php?action=editer_article&amp;id=".$id_agenda."\" title=\"Editerarticle\"><img src=\"images/editer.gif\"> Editer cet évenement</a>					
						<a class='amodo' href=\"agenda.php?action=supprimer_article&amp;id=".$id_agenda."\" title=\"supprimearticle\">Mettre cet évenement en modération</a>
						";}
						echo "</p></div><br>";
					}
					if($f!=0){ echo "</div>";}
				}
				echo"<br></div>";
			}
		//répondre a une article agenda
		elseif($action=='repondre')
		{
			$id_agenda=nl2br(htmlentities($_GET['id'], ENT_QUOTES, "UTF-8"));
			if (!($requete=mysql_fetch_row(mysql_query("SELECT `publication` FROM `agenda` WHERE `id_agenda`=$id_agenda")))) {
				die('Erreur : ' . mysql_error());
			}
			$publication=$requete[0];
			if($publication=='OUI')
			{
				if (!($recup=mysql_fetch_row(mysql_query("SELECT `titre` FROM `agenda` WHERE `publication`='OUI' AND `id_agenda`='$id_agenda'")))) {
					die('Erreur : ' . mysql_error());
				}
				$titre=stripslashes($recup[0]);
				//formulaire 
				echo "<br><div class=\"corps\"><br>
				<form method='post' action='agenda.php?action=envoye&amp;id=$id_agenda' enctype='multipart/form-data'>
					<input type='hidden' name='id_agenda' value=$id_agenda >
					<p class='titre'>Répondre à l'évenement: \" ".$titre." \"</p><br>
					<p><textarea name='message' cols='80' rows='10'></textarea>
					<br><input type='submit' value=' Répondre '></p><br></form>
					<table summary=\"\" border=\"1\" width=\"90%\">
					<tr class='t1'>
					<td><img alt=\"smile\" src='smiles/smile1.gif'></td><td><img alt=\"smile\" src='smiles/smile2.gif'></td><td><img alt=\"smile\" src='smiles/smile3.gif'></td>
		   		<td><img alt=\"smile\" src='smiles/smile4.gif'></td><td><img alt=\"smile\" src='smiles/smile5.gif'></td><td><img alt=\"smile\" src='smiles/smile6.gif'></td>
		   		<td><img alt=\"smile\" src='smiles/smile7.gif'></td><td><img alt=\"smile\" src='smiles/smile8.gif'></td><td><img alt=\"smile\" src='smiles/smile9.gif'></td>
		  			<td><img alt=\"smile\" src='smiles/smile10.gif'></td><td><img alt=\"smile\" src='smiles/smile11.gif'></td><td><img alt=\"smile\" src='smiles/smile12.gif'></td>
		   		<td><img alt=\"smile\" src='smiles/smile13.gif'></td><td><img alt=\"smile\" src='smiles/smile27.gif'></td><td><img alt=\"smile\" src='smiles/smile28.gif'></td>
		   		<td><img alt=\"smile\" src='smiles/smile36.gif'></td><td><img alt=\"smile\" src='smiles/smile25.gif'></td>
					</tr>	
					<tr class='t2'>
					<td>:)</td><td>:(</td><td>;)</td><td>:D</td><td>:*</td><td>B)</td><td>:o</td><td>:$</td>
					<td>:p</td><td>:x</td><td>(k)</td><td>:-/</td><td>Zzz</td><td>O:]</td><td>(bain)</td><td>(sante)</td>
					<td>(love)</td>
					</tr>
					<tr class='t1'>
		   		<td><img alt=\"smile\" src='smiles/smile19.gif'></td><td><img alt=\"smile\" src='smiles/smile20.gif'></td><td><img alt=\"smile\" src='smiles/smile18.gif'></td>
		   		<td><img alt=\"smile\" src='smiles/smile22.gif'></td><td><img alt=\"smile\" src='smiles/smile23.gif'></td><td><img alt=\"smile\" src='smiles/smile24.gif'></td>
		   		<td><img alt=\"smile\" src='smiles/smile17.gif'></td><td><img alt=\"smile\" src='smiles/smile16.gif'></td><td><img alt=\"smile\" src='smiles/smile14.gif'></td>
		   		<td><img alt=\"smile\" src='smiles/smile15.gif'></td><td><img alt=\"smile\" src='smiles/smile29.gif'></td><td><img alt=\"smile\" src='smiles/smile30.gif'></td>
		   		<td><img alt=\"smile\" src='smiles/smile31.gif'></td><td><img alt=\"smile\" src='smiles/smile34.gif'></td><td><img alt=\"smile\" src='smiles/smile32.gif'></td>
					<td><img alt=\"smile\" src='smiles/smile26.gif'></td><td><img alt=\"smile\" src='smiles/smile21.gif'>
					</tr>
					<tr class='t2'>
					<td>:F</td><td>:X</td><td>(l)</td><td>:P</td><td>:!</td><td>:i</td>
					<td>;x</td><td>:-*</td><td>8)</td><td>:#</td><td>;(</td><td>:@</td><td>(mur)</td>
					<td>O:}</td><td>(jtm)</td><td>(merci)</td><td>(gne)</td>	
					</tr>										
					</table></div>";
			}
			else
			{
				header ("location:404.php");
				session_destroy();
			}
		}
		//envoyer la reponse
		elseif($action=='envoye')
		{
			$id_agenda_url=nl2br(htmlentities($_GET['id'], ENT_QUOTES, "UTF-8"));
			$id_agenda=nl2br(htmlentities($_POST['id_agenda'], ENT_QUOTES, "UTF-8"));
			$message=nl2br(htmlentities($_POST['message'], ENT_QUOTES, "UTF-8"));
			if (!($requete=mysql_fetch_row(mysql_query("SELECT `publication` FROM `agenda` WHERE `id_agenda`='$id_agenda'")))) {
				die('Erreur : ' . mysql_error());
			}
			$publication=$requete[0];
			if(($id_agenda_url==$id_agenda)&&($publication=='OUI'))
			{
				if( strlen( $message ) > 10 ) 
				{
					if (!mysql_query("INSERT INTO `agenda_repliques` VALUES ('', '$id_agenda', '$id_seliste', '$time', '$message')")) {
						die('Requête invalide : ' . mysql_error());
					}
					echo "<br><div class=\"corps\"><br>
						<p>Votre réponse à bien été envoyée.<br><br>
					<a href='bureau.php' title='bureau'>Retour bureau</a></td>
					<a href='agenda.php?action=lire'>Revoir les évenements de l'agenda</a><br>
					</p><br></div>";
				}
				// Message trop court
				else 
				{
					echo "<br><div class=\"corps\"><br>
						<p>Votre réponse est trop courte.<br><br>
					<a href='bureau.php' title='bureau'>Retour bureau</a></td>
					<a href='agenda.php?action=lire'>Revoir les évenements de l'agenda</a><br>
					</p><br></div>";
				}				
			}
			else
			{
				header ("location:404.php");
				session_destroy();
			}
		}
		//voir les reponses
		elseif($action=='reponses')
		{
			$id_agenda=nl2br(htmlentities($_GET['id'], ENT_QUOTES, "UTF-8"));
			if (!($requete=mysql_fetch_row(mysql_query("SELECT `publication` FROM `agenda` WHERE `id_agenda`='$id_agenda'")))) {
				die('Erreur : ' . mysql_error());
			}
			$publication=$requete[0];
			//si elle est publié
			if($publication=='OUI')
			{
				if (!($requete=mysql_query("SELECT * FROM `agenda` WHERE `id_agenda`='$id_agenda'"))) {
					die('Erreur : ' . mysql_error());
				}	
				$ligne=mysql_fetch_row($requete);
				$id_agenda=$ligne[0];
				$date_creation=$ligne[3];
				$date_creation=date('d/m/y à H\h i',$date_creation);
				$id_sel= $ligne[4];
				$date_evenement= $ligne[5];
				$date_evenement=date('d/m/y à H\h i',$date_evenement);	
				$titre= stripslashes($ligne[6]);
				$article= stripslashes($ligne[7]);
				$message=$article;
				include("smileyscompagnie.php" );
				$article=$message;
				//recuperation du seliste
				if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`=$id_sel")))) {
					die('Erreur : ' . mysql_error());
				} 
				$prenom_seliste=stripslashes($recup[0]);	
				echo "<br><div class=\"corps\"><br>
				<div class='message'><br><p class=titre>".$titre." <br><br>
					<span class='pasimportant'>Evenement écrit par <a href=\"profil.php?id=".$id_sel."\">".$prenom_seliste."(".$id_sel.")</a> le ".$date_creation."</span></p>
					<p>Prévu le : ".$date_evenement."</p>
					<hr width=70%>						
					<span class='t2'>".$article."</span>					
					<hr width=70%><br><p>
					<a href=\"agenda.php?action=repondre&amp;id=".$id_agenda."\" title=\"Repondre\"><img src=\"images/lettre.jpg\"> Répondre</a>&nbsp; ";
					if(($grade=='MODERATEUR')||($grade=='ADMIN')){echo "
					<a class='amodo' href=\"agenda.php?action=editer_article&amp;id=".$id_agenda."\" title=\"Editerarticle\"><img src=\"images/editer.gif\"> Editer l'évenement</a>
					<a class='amodo' href=\"agenda.php?action=supprimer_article&amp;id=".$id_agenda."\" title=\"supprimearticle\">Mettre l'évenement en modération</a>";}
					echo "					
					</p><br>
					</div><br>";			
					// recup des repliques
				if (!($requete=mysql_query("SELECT * FROM `agenda_repliques` WHERE `id_agenda`='$id_agenda' ORDER BY `timestamp` ASC"))) {
					die('Erreur : ' . mysql_error());
				}	
				while($ligne=mysql_fetch_row($requete))
				{
					$id_replique= $ligne[0];
					$id_ecriveur= $ligne[2];
					$timestamp= $ligne[3];
					$date=date('d/m/y à H\h i\m\i\n',$timestamp);	
					$message=stripslashes($ligne[4]);
					include("smileyscompagnie.php" );
					if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`=$id_ecriveur")))) {
						die('Erreur : ' . mysql_error());
					} 
					$prenom_ecriveur=stripslashes($recup[0]);		
					$grade_ecriveur=$recup[1];
					echo "<div class='message'><p class=\"pasimportant\">Réponse de <a href='profil.php?id=".$id_ecriveur."'>".$prenom_ecriveur."(".$id_ecriveur.")</a> le ".$date."</p><br>
					<p>".$message."</p>";
					if(($grade=='MODERATEUR')||($grade=='ADMIN')){echo " <br>
					<a class='amodo' href='agenda.php?action=sup_rep&amp;id=".$id_replique."'>Supprimer cette réponse</a>&nbsp;&nbsp;
					<a class='amodo' href='agenda.php?action=edit_rep&amp;id=".$id_replique."'>Editer cette réponse</a>";}
					echo "<br></div><br>";
				}
				echo "</div>";
			}
			//si elle est pas publié!
			else
			{
				header ("location:404.php");
				session_destroy();
			}
		}
		//proposer un article
		elseif($action=='proposer')
		{
			if($_GET['action2']=='envoye')
			{
				$titre=nl2br(htmlentities($_POST['titre'], ENT_QUOTES, "UTF-8"));
				$message=$_POST['article'];
				$titrebase=nl2br(htmlentities($_POST['titre'], ENT_QUOTES, "UTF-8"));
				$messagebase=$_POST['article'];
				$rubrique=htmlentities($_POST['rubrique'], ENT_QUOTES, "UTF-8");				
				$heure=htmlentities($_POST['heure'], ENT_QUOTES, "UTF-8");
				$minutes=htmlentities($_POST['minute'], ENT_QUOTES, "UTF-8");
				$mois=htmlentities($_POST['mois'], ENT_QUOTES, "UTF-8");
				$jour=htmlentities($_POST['jour'], ENT_QUOTES, "UTF-8");
				$annee=htmlentities($_POST['annee'], ENT_QUOTES, "UTF-8");
				$time_eve= mktime($heure, $minutes,0,$mois,$jour,$annee); 
				$imm=nl2br(htmlentities($_POST['immediat'], ENT_QUOTES, "UTF-8"));
				if(( strlen( $message ) > 50 )&&( $mois!=null )&&( $heure!=null )&&( $titre!=null )) 
				{
					if($imm=='OUI')
					{
						if (!mysql_query("INSERT INTO `agenda` VALUES ('', '$rubrique', 'OUI', '$time', '$id_seliste', '$time_eve', '$titrebase', '$messagebase')")) {
							die('Requête invalide : ' . mysql_error());
						}
						if (!(mysql_query("UPDATE `agenda` SET `publication`='OUI' WHERE `id_agenda`=$id_agenda LIMIT 1") )) {
							die('Erreur : ' . mysql_error());
						}
						// l'enregistré dans le log 
						if (!mysql_query("INSERT INTO `log` VALUES ('', '$id_seliste', '$id_agenda', '$time', 'agen_PUBIN', '$titre')")) {
							die('Requête invalide : ' . mysql_error());
						}
						// envoi du mail:
						if (!($requete=mysql_query("SELECT `prenom`, `email` FROM `selistes` WHERE `valide`='OUI' AND `mail_age`='OUI'"))) {
							die('Erreur : ' . mysql_error());
						}
						$message2=stripslashes("[".$nom."] Evenement: \" ".$titre." \" ");
								$headers = "From: Evenement $nom <$email>\n";
								$headers .= "Reply-To: $nom\n";
								$headers .= "X-Sender: $nom\n";
								$headers .= "X-Author: Evenement $site\n";
								$headers .= "X-Priority:1\n";
								$headers .= "X-Mailer: PHP\n";
								$headers .= "Return_Path: <$email>\n";
								$headers .='Content-Type: text/html; charset="UTF-8"'."\n";
						while($ligne=mysql_fetch_row($requete))
						{
							$prenom_dest=stripslashes($ligne[0]);
							$email_dest=$ligne[1];
							// envoi du mail 
								
					$mel ="
	<html><head><title></title></head><body>
	<p>Bonjour ".$prenom_dest.", un nouvel évenement qui aura lieu le ".$jour."/".$mois."/".$annee."  vient d'être publié par <a href=http://".$site."/index.php?vers=profil.php?id=".$id_seliste.">".$prenom."(".$id_seliste.")</a> en publication immédiate.
	<br><br><b>".$titre."</b>
	<br><hr width=90%><br>".$message."
	<br><hr width=90%>
	<br>Pour le lire sur le site <a href=http://".$site."/index.php?vers=agenda.php?action=lire> cliquez-ici </a>
	<br>Cordialement, <br> ".$nom.".
	<br><br><i>Si vous ne souhaitez plus avoir ces alertes:<br>
	<a href=http://".$site."/index.php?vers=compte.php> Cliquez ici </a> ou via la page notifications (en haut à gauche), pour desactiver ces alertes.</i></p></body></html>";
								sw_mail($email_dest,$message2,$mel,$headers);
						}
						echo "<br><div class=\"corps\"><br>
						<p class='t1'>Votre évenement a bien été publié il est en ligne!</p><br>
						<p class='t4'><a href='bureau.php' title='bureau'>Retour bureau</a>&nbsp;&nbsp;
						<a href='agenda.php?action=lire'>Revoir les évenements de l'agenda</a><br>
						</p><br></div>";
					}
					else
					{ // pas immediat					
						if (!mysql_query("INSERT INTO `agenda` VALUES ('', '$rubrique', 'NON', '$time', '$id_seliste', '$time_eve', '$titrebase', '$messagebase')")) {
							die('Requête invalide : ' . mysql_error());
						}
						echo "<br><div class=\"corps\"><br>
						<p>Votre évenement a bien été proposée à la modération.<br><br>
						<a href='bureau.php' title='bureau'>Retour bureau</a></td>
						<a href='agenda.php?action=lire'>Revoir les évenements de l'agenda</a><br>
						</p><br></div>";
						// envoi du mail au modo
						if (!($requete=mysql_query("SELECT `texte` FROM  `variables` WHERE  `variable`='MOD3' LIMIT 1"))) {
							die('Erreur : ' . mysql_error());
						}
						$ligne=mysql_fetch_row($requete);
						$MOD3=stripslashes($ligne[0]);
						if (!($requete=mysql_query("SELECT `prenom`,`email` FROM  `selistes` WHERE  `id_seliste`='$MOD3' LIMIT 1"))) {
							die('Erreur : ' . mysql_error());
						}
						$ligne=mysql_fetch_row($requete);
						$PMOD3=stripslashes($ligne[0]);
						$MMOD3=stripslashes($ligne[1]);
						// envoi du mail:
						$message2=stripslashes("[".$nom."]MODERATEUR Actu-Agenda: Nouvel évenement à moderer");
						$headers = "From: Evenement $nom <$email>\n";
						$headers .= "Reply-To: $nom\n";
						$headers .= "X-Sender: $nom\n";
						$headers .= "X-Author: Moderation $site\n";
						$headers .= "X-Priority:1\n";
						$headers .= "X-Mailer: PHP\n";
						$headers .= "Return_Path: <$email>\n";
						$headers .='Content-Type: text/html; charset="UTF-8"'."\n";
						$PMOD3=stripslashes($PMOD3);
						$titre=stripslashes($titre);
						$message=stripslashes($message);
						$email_dest=$ligne[1];
						// envoi du mail 								
						$mel ="<html><head><title></title></head><body>
						<p>Bonjour ".$PMOD3.", un nouvel évenement vient d'être mis en modération par <a href=http://".$site."/index.php?vers=profil.php?id=".$id_seliste.">".$prenom."(".$id_seliste.")</a>.
						<br><br><b>".$titre."</b>
						<br><hr width=90%><br>".$message."
						<br><hr width=90%>
						<br>Merci de vous connecter pour le valider ou le refuser.
						<br>Cordialement, <br> ".$nom.".
						<br><br><i>Vous recevez ces alertes suite à votre poste de Modérateur sur <a href=http://".$site."/> ".$site." </a>.</i></p></body></html>";
						sw_mail($MMOD3,$message2,$mel,$headers);
						// fin d'envoi du mail fin d'envoi de l'actu en moderation	
					}
				}
				// Message trop court
				else 
				{
					echo "<br><div class=\"corps\"><br>
					<p>Votre proposition d'évenement est soit trop courte soit il manque une information, verifier le titre, la date et l'heure de votre évenement.<br><br>
					<a href='bureau.php' title='bureau'>Retour bureau</a></td>
					<a href='agenda.php?action=lire'>Revoir les évenements de l'agenda</a><br>
					</p><br></div>";
				}	
			}
			else
			{
				// date aujordhui - date   inscription  =  + de 3 mois  7 889 231 sec
				$date2=$time-$inscription;
				$anneeactuel=date('Y');
				echo "<br><div class=\"corps\"><br><script type=\"text/javascript\" src=\"ckeditor/ckeditor.js\"></script>
				<p class='titre'>Proposition d'évenement.</p><br>
				<div class='message'><p class='rouge t2'><b>Utilisez l'agenda uniquement si votre sujet :<br>
				- Comporte une date et un horaire précis.<br>
				- Vous participez ou organisez l'événement.<br>
				Pour tous les autres cas: c'est une actualité qui ne doit pas figurer dans l'agenda. Si vous voulez informer les selistes sur plusieurs événements, merci de publier vos événements dans Actualité et en un seul sujet. Merci.</b></p></div><br>
				<form method='post' action='agenda.php?action=proposer&amp;action2=envoye' enctype='multipart/form-data'>
				<p>Type d'évenement:
				<select name='rubrique'>
				<option value='AUB' >Auberges et Soirées</option>
				<option value='INS' >Réunion d'inscription</option>
				<option value='REU' >Réunion de ".$nom."</option>
				<option value='RAN' >Randonnées et sorties nature</option>
				<option value='SOR' >Toutes les autres sorties</option>
				</select></p>			
				<p>Date de l'évenement:
				<input type='text' name='jour' size='2' maxlength='2' /> /
				<input type='text' name='mois' size='2' maxlength='2' /> /
				<input type='text' name='annee' size='4' maxlength='4' value='".$anneeactuel."' /> à
				<input type='text' name='heure' size='2' maxlength='2' /> h
				<input type='text' name='minute' size='2' maxlength='2' value='00' /></p>
				<p>Titre:<input type='text' name='titre' size='50' maxlength='80' /></p>
				<center><br><textarea name='article' class=\"ckeditor\" id=\"editeur\" cols='80' rows='30'>Tapez votre proposition ici.</textarea></center><br>
				<hr width='70%'>
					<p>Un modérateur va vérifier votre évenement proposé et le mettre en ligne<br>
					<br><input type='submit' value=' Proposez cet évenement aux modérateurs'></p><hr width='70%'>";
					echo "</form>
				<table summary=\"\" border=\"1\" width=\"90%\">
				<tr class='t1'>
				<td><img alt=\"smile\" src='smiles/smile1.gif'></td><td><img alt=\"smile\" src='smiles/smile2.gif'></td><td><img alt=\"smile\" src='smiles/smile3.gif'></td>
			<td><img alt=\"smile\" src='smiles/smile4.gif'></td><td><img alt=\"smile\" src='smiles/smile5.gif'></td><td><img alt=\"smile\" src='smiles/smile6.gif'></td>
			<td><img alt=\"smile\" src='smiles/smile7.gif'></td><td><img alt=\"smile\" src='smiles/smile8.gif'></td><td><img alt=\"smile\" src='smiles/smile9.gif'></td>
				<td><img alt=\"smile\" src='smiles/smile10.gif'></td><td><img alt=\"smile\" src='smiles/smile11.gif'></td><td><img alt=\"smile\" src='smiles/smile12.gif'></td>
			<td><img alt=\"smile\" src='smiles/smile13.gif'></td><td><img alt=\"smile\" src='smiles/smile27.gif'></td><td><img alt=\"smile\" src='smiles/smile28.gif'></td>
			<td><img alt=\"smile\" src='smiles/smile36.gif'></td><td><img alt=\"smile\" src='smiles/smile25.gif'></td>
				</tr>	
				<tr class='t2'>
				<td>:)</td><td>:(</td><td>;)</td><td>:D</td><td>:*</td><td>B)</td><td>:o</td><td>:$</td>
				<td>:p</td><td>:x</td><td>(k)</td><td>:-/</td><td>Zzz</td><td>O:]</td><td>(bain)</td><td>(sante)</td>
				<td>(love)</td>
				</tr>
				<tr class='t1'>
			<td><img alt=\"smile\" src='smiles/smile19.gif'></td><td><img alt=\"smile\" src='smiles/smile20.gif'></td><td><img alt=\"smile\" src='smiles/smile18.gif'></td>
			<td><img alt=\"smile\" src='smiles/smile22.gif'></td><td><img alt=\"smile\" src='smiles/smile23.gif'></td><td><img alt=\"smile\" src='smiles/smile24.gif'></td>
			<td><img alt=\"smile\" src='smiles/smile17.gif'></td><td><img alt=\"smile\" src='smiles/smile16.gif'></td><td><img alt=\"smile\" src='smiles/smile14.gif'></td>
			<td><img alt=\"smile\" src='smiles/smile15.gif'></td><td><img alt=\"smile\" src='smiles/smile29.gif'></td><td><img alt=\"smile\" src='smiles/smile30.gif'></td>
			<td><img alt=\"smile\" src='smiles/smile31.gif'></td><td><img alt=\"smile\" src='smiles/smile34.gif'></td><td><img alt=\"smile\" src='smiles/smile32.gif'></td>
				<td><img alt=\"smile\" src='smiles/smile26.gif'></td><td><img alt=\"smile\" src='smiles/smile21.gif'>
				</tr>
				<tr class='t2'>
				<td>:F</td><td>:X</td><td>(l)</td><td>:P</td><td>:!</td><td>:i</td>
				<td>;x</td><td>:-*</td><td>8)</td><td>:#</td><td>;(</td><td>:@</td><td>(mur)</td>
				<td>O:}</td><td>(jtm)</td><td>(merci)</td><td>(gne)</td>	
				</tr>										
				</table><br></div>";
			}
		}
		//editer une replique
		elseif(($action=='edit_rep')&&(($grade=='MODERATEUR')||($grade=='ADMIN')))
		{
			if($_POST['id_rep']==NULL)
			{
				// formulaire
				$id_replique=$_GET['id'];
				if (!($requete=mysql_fetch_row(mysql_query("SELECT * FROM `agenda_repliques` WHERE `id_replique`=$id_replique")))) {
					die('Erreur : ' . mysql_error());
				}
				$id_sel=$requete[2];
				$timestamp=$requete[3];
				$message=stripslashes($requete[4]);
				$message=str_replace("<br />","",$message);
				if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`=$id_sel")))) {
					die('Erreur : ' . mysql_error());
				} 
				$prenom_sel=stripslashes($recup[0]);		
				echo "<br><div class=\"corps\"><br>
				<form method='post' action='agenda.php?action=edit_rep&amp;id=$id_replique' enctype='multipart/form-data'>
				<input type='hidden' name='id_rep' value=$id_replique >
				<p class='titre'>Edition de la réponse de <a href='profil.php?id=".$id_sel."'>".$prenom_sel."(".$id_sel.")</a></a>.</p><br>
				<p>Réponse:<br><textarea name='replique' cols='80' rows='10'>".$message."</textarea><br>
				<input type='submit' value=' Enregistrer les modifications '></form><br>
					
				<a href='bureau.php' title='bureau'>Retour bureau</a>&nbsp;&nbsp;
				<a href='agenda.php?action=lire'>Revoir les évenements de l'agenda</a></p><br></div>";
			}
			else
			{
				// enregistre l'edition
				$id_rep=$_GET['id'];
				$id_rep_post=$_POST['id_rep'];
				if($id_rep==$id_rep_post)
				{
					// transformation des bbcodes et smileys
					$message=nl2br(htmlentities($_POST['replique'], ENT_QUOTES, "UTF-8"));
					if (!($requete=mysql_query("UPDATE `agenda_repliques` SET `replique` = '$message' WHERE `id_replique` ='$id_rep' LIMIT 1") )) {
						die('Erreur : ' . mysql_error());
					}
					echo "<br><div class=\"corps\"><br>		
						<p class='titre'>Edition enregistrée.</p><br>
						<p><a href='bureau.php' title='bureau'>Retour bureau</a>&nbsp;&nbsp;
				<a href='agenda.php?action=lire'>Revoir les évenements de l'agenda</a></p><br></div>";
				}
			}			
		}
		//supprimer une replique
		elseif(($action=='sup_rep')&&(($grade=='MODERATEUR')||($grade=='ADMIN')))
		{
			$id_rep=$_GET['id'];
			if (!($requete=mysql_fetch_row(mysql_query("SELECT * FROM `agenda_repliques` WHERE `id_seliste`=$id_rep")))) {
				die('Erreur : ' . mysql_error());
			}
			$id_agenda=$requete[1];
			$id_ecriveur=$requete[2];
			$time=$requete[3];
			$message=$requete[4];
			// suppression de la replique
			if (!(mysql_query("DELETE FROM `agenda_repliques` WHERE `id_replique`=$id_rep LIMIT 1"))) {
				die('Erreur : ' . mysql_error());
			}
			echo "<br><div class=\"corps\"><br>		
						<p class='titre'>La réponse est supprimée.</p><br>
						<p><a href='bureau.php' title='bureau'>Retour bureau</a>&nbsp;&nbsp;
				<a href='agenda.php?action=lire'>Revoir les évenements de l'agenda</a></p><br></div>";
		}
		//supprimer une article
		elseif(($action=='supprimer_article')&&(($grade=='MODERATEUR')||($grade=='ADMIN')))
		{
			$id_agenda=htmlentities($_GET['id'], ENT_QUOTES, "UTF-8");
			// rendre non public.
			if (!(mysql_query("UPDATE `agenda` SET `publication` = 'NON' WHERE `id_agenda` ='$id_agenda' LIMIT 1 "))) {
				die('Erreur : ' . mysql_error());
			}
			echo "<br><div class=\"corps\"><br>		
						<p class='titre'>L'évenement est rendu privé et visible dans l'administration de l'agenda.</p><br>
						<p><a href='bureau.php' title='bureau'>Retour bureau</a>&nbsp;&nbsp;
				<a href='agenda.php?action=lire'>Revoir les évenements de l'agenda</a></p><br></div>";
		}
		//edition de la article
		elseif(($action=='editer_article')&&(($grade=='MODERATEUR')||($grade=='ADMIN')))
		{
			if($_POST['id_agenda']==NULL)
			{
				// formulaire
				$id_agenda=$_GET['id'];
				if (!($requete=mysql_fetch_row(mysql_query("SELECT * FROM `agenda` WHERE `id_agenda`=$id_agenda")))) {
					die('Erreur : ' . mysql_error());
				}
				$id_rub=$requete[1];
				$id_ecriveur=$requete[4];
				$date_evt=$requete[5];
				$jour_evt=date('d',$date_evt);
				$mois_evt=date('m',$date_evt);
				$annee_evt=date('y',$date_evt);
				$heure_evt=date('H',$date_evt);
				$minute_evt=date('i',$date_evt);
				$titre=stripslashes($requete[6]);
				$article=stripslashes($requete[7]);
				$article=str_replace("<br />","",$article);
				if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`=$id_ecriveur")))) {
					die('Erreur : ' . mysql_error());
				} 
				$prenom_ecriveur=stripslashes($recup[0]);		
				echo "<br><div class=\"corps\"><br><script type=\"text/javascript\" src=\"ckeditor/ckeditor.js\"></script>
				<form method='post' action='agenda.php?action=editer_article&amp;id=$id_agenda' enctype='multipart/form-data'>
				<input type='hidden' name='id_agenda' value=$id_agenda >
				<p class='titre'>Edition de l'évenement \" ".$titre." \" </p>
				
				<p>Evenement proposé par: <a href='profil.php?id=".$id_ecriveur."'>".$prenom_ecriveur."(".$id_ecriveur.")</a><br>
				
				<br>Type d'évenement:
				<select name='rubrique'>
				<option value='$id_rub' >$id_rub</option>
				<option value='AUB' >Auberges et Soirées (AUB)</option>
				<option value='INS' >Réunion d'inscription (INS)</option>
				<option value='REU' >Réunion de ".$nom." (REU)</option>
				<option value='RAN' >Randonnées et sorties nature (RAN)</option>
				<option value='SOR' >Toutes les autres sorties (SOR)</option>
				</select>
								
				<br>Date évenement:<input type='text' name='jour_evt' size='2' maxlength='2' value=\"".$jour_evt."\" />/<input type='text' name='mois_evt' size='2' maxlength='2' value=\"".$mois_evt."\" />/<input type='text' name='annee_evt' size='2' maxlength='2' value=\"".$annee_evt."\" /> à <input type='text' name='heure_evt' size='2' maxlength='2' value=\"".$heure_evt."\" />:<input type='text' name='minute_evt' size='2' maxlength='2' value=\"".$minute_evt."\" />
				
				<br>Titre:<input type='text' name='titre' size='50' maxlength='50' value=\"".$titre."\" />
				<br>Evenement:<br><center><textarea class=\"ckeditor\" id=\"editeur\" name='article' cols='80' rows='30'>".$article."</textarea></center><br>
				<input type='submit' value=' Enregistrer les modifications '><br>
				<br><a href='bureau.php' title='bureau'>Retour bureau</a>&nbsp;&nbsp;
				<a href='agenda.php?action=lire'>Revoir les évenements de l'agenda</a></p><br></form></div>";
			}
			else
			{
				// enregistre l'edition
				$id_agenda=$_GET['id'];
				$id_agenda_post=$_POST['id_agenda'];
				if($id_agenda==$id_agenda_post)
				{
					$message=$_POST['article'];
					$article=$message;
					$titre=nl2br(htmlentities($_POST['titre'], ENT_QUOTES, "UTF-8"));
					$id_rub=htmlentities($_POST['rubrique'], ENT_QUOTES, "UTF-8");
					// transformation de la date en timestamp
					$jour_evt=htmlentities($_POST['jour_evt'], ENT_QUOTES, "UTF-8");
					$mois_evt=htmlentities($_POST['mois_evt'], ENT_QUOTES, "UTF-8");
					$annee_evt=htmlentities($_POST['annee_evt'], ENT_QUOTES, "UTF-8");
					$heure_evt=htmlentities($_POST['heure_evt'], ENT_QUOTES, "UTF-8");
					$minute_evt=htmlentities($_POST['minute_evt'], ENT_QUOTES, "UTF-8");
					$date_evt= mktime($heure_evt, $minute_evt,0,$mois_evt,$jour_evt,$annee_evt); 
					// enregistrement					
					if (!($requete=mysql_query("UPDATE `agenda` SET `detail` = '$article', `date_evenement` = '$date_evt', `rubrique` = '$id_rub', `titre` = '$titre' WHERE `id_agenda` ='$id_agenda' LIMIT 1") )) {
						die('Erreur : ' . mysql_error());
					}
					echo "<br><div class=\"corps\"><br>
						<p class='titre'>Votre édition est enregistrée!</p><br>		
					<br><a href='bureau.php'>Retour bureau</a>&nbsp;&nbsp;
				<a href='agenda.php?action=lire'>Revoir les évenements de l'agenda</a></p><br></div>";
				}
			}
		}					
		//rien dans l'action
		else
		{
			header ("location:404.php");
			session_destroy();
		}
	}
	else
	{ 	 //délai dépassé
		header ("location:troptard.php");
		session_destroy();
	}
 }
else
{ 	 // pas de session
	header ("location:404.php");
	session_destroy();
}
mysql_close($connexion); 
include ("fin.php");	
?>
