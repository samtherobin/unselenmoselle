<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/

if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
{								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade`, `prenom`, `email`, `grains` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		$prenom=$ligne[1];
		$email=$ligne[2];
		$grains_seliste=$ligne[3];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
		switch ($grade)
		{
			case 'SELISTE' : header("location:404.php");break;				
			case 'MODERATEUR' :header("location:404.php");break;	
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page

		

		echo "<br><br><div class='corps'><br>
		<table width=90% class='tablevu' border=1>
		<tr>
		<td colspan=3 class='teinte1'><p class='t1'>Analyse entre la table SELISTES et ECHANGES.</p></td>
		</tr>
		<tr><td class='teinte2'><p class='t1'>Numéro Séliste</p></td><td class='teinte2'><p class='t1'>Selon la table SELISTES</p></td><td class='teinte2'><p class='t1'>Selon la table Echange (Cumul)</p></td></tr>";
		$solde1=0;
		$solde2=0;
		$i==0;
		/*calcul maximum de selistes*/
		if (!($requetemaxi=mysql_query("SELECT MAX(  `id_seliste` ) FROM selistes"))) {
			die('Erreur : ' . mysql_error());
		} 
		if($ligne=mysql_fetch_row($requetemaxi))
		{
			$maxi=$ligne[0];
		}
		/*calcul si des échanges avec numero de seliste > au maxi*/
		if (!($requete2=mysql_query("SELECT * FROM `echanges` where `id_seliste_dem`>'$maxi' AND `id_seliste_fou`>'$maxi'"))) {
			die('Erreur : ' . mysql_error());
		}
		if($ligne=mysql_fetch_row($requete2))
		{
			echo "<p>L'échange numéro ".$ligne[0]." est impossible! L'id du seliste est trop élevé. </p>";
		}
		/*calcul si des échanges avec numero 0*/
		if (!($requete2=mysql_query("SELECT * FROM `echanges` where `id_seliste_dem`>'0' AND `id_seliste_fou`='0' AND `etat`='OKI'"))) {
			die('Erreur : ' . mysql_error());
		}
		if($ligne=mysql_fetch_row($requete2))
		{
			echo "<p>L'échange numéro ".$ligne[0]." est impossible ! Le séliste numéro 0 n'existe pas! </p>";
		}
			
			
		while($i!=$maxi)
		{
			$i++;
			$id_sel=$i;
			$id_sel_grain=0;
			if (!($requete2=mysql_query("SELECT `id_seliste`, `grains`, `valide` FROM `selistes` WHERE `id_seliste`='$id_sel' "))) {
				die('Erreur : ' . mysql_error());
			}
			if($ligne=mysql_fetch_row($requete2))
			{
				$id_sel_grain=$ligne[1];
				$valide=$ligne[2];
			}
			// affichage des échanges oki
			$grains=0;
			if (!($requete2=mysql_query("SELECT sum(`grains`) FROM `echanges` where `etat`='OKI' AND `id_seliste_fou`='$id_sel'"))) {
				die('Erreur : ' . mysql_error());
			}
			if($ligne=mysql_fetch_row($requete2))
			{
				$grains=$ligne[0];
			}

			if (!($requete3=mysql_query("SELECT sum(`grains`) FROM `echanges` where `etat`='OKI' AND `id_seliste_dem`='$id_sel'"))) {
				die('Erreur : ' . mysql_error());
			}
			if($ligne=mysql_fetch_row($requete3))
			{
				$grains=$grains-$ligne[0];
			}
			if (!($requete5=mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`='$id_sel'") )) {
				die('Erreur : ' . mysql_error());
			}
			$ligne5=mysql_fetch_row($requete5) ;
			$prenom_sel= $ligne5[0];	
		  if ($id_sel_grain==$grains)
		  {
			if($id_sel_grain!=0)
			{	
				if($valide=='OUI')
				{
					echo "<tr><td class='teinte2'><p class=t4><a href=\"profil.php?id=".$id_sel."\">".$prenom_sel."(".$id_sel.")</a></p></td><td><p class=t4>$id_sel_grain $monnaie</p></td><td><p class=t4>$grains $monnaie</p></td></tr>";
				}
				else
				{
					echo "<tr><td class='teinte3'><p class=t4><a href=\"profil.php?id=".$id_sel."\">".$prenom_sel."(".$id_sel.")</a></p></td><td><p class=t4>$id_sel_grain $monnaie</p></td><td><p class=t4>$grains $monnaie</p></td></tr>";
				}
			}
		  }
		  else
		  {
			echo "<tr><td class='teinte3'><p class='t4'><a href=\"profil.php?id=".$id_sel."\">".$prenom_sel."(".$id_sel.")</a></td><td><p class='t4'>$id_sel_grain $monnaie</p></td><td><p class='t4'>$grains $monnaie</p></td></tr>";
			// LANCEMENT REQUETE MISE A JOUR //
			if (!($MaJ=mysql_query("UPDATE  `selistes` SET  `grains` =  '$grains' WHERE  `id_seliste` =$id_sel LIMIT 1 "))) {
				die('Erreur : ' . mysql_error());
			}
			echo "<tr><td class='teinte2' colspan=3> Compte mis à jour ! </p></td></tr>";
			//////////////////////////////////
		  }
			$solde1=$solde1+($id_sel_grain);
			$solde2=$solde2+($grains);
		}
		echo "<tr><td class='teinte1'><p class='t1'>Solde</p></td><td class='teinte1'><p class='t1'>$solde1 $monnaie</p></td><td class='teinte1'><p class='t1'>$solde2 $monnaie</p></td></tr></table><br></div>";
			
			
			
	}
	else
	{ 	 // délai dépassé
		1;
	}
 }
else
{ 	 // pas de session
	2;
}
mysql_close($connexion);
include ("fin.php");	
?>
