<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/

if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
 {								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
		switch ($grade)
		{
			case 'SELISTE' : include("lien/seliste.php");break;
			case 'MODERATEUR' :include("lien/seliste.php"); include("lien/moderateur.php");break;		
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page
		echo "<br><br><div class='corps'><br><br>";
		if(($grade=='ADMIN')&&($_GET['id']!=NULL))//suppression
		{
			echo "<p class='titre'>Adresse supprimée</p><br><br>";		
			$id_message=$_GET['id'];
			if (!($requete=mysql_query("SELECT * FROM `ba` WHERE `id_message`='$id_message'"))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete);
			//$id_action=$ligne[1];
			//$time=$ligne[2];
			//$message=$ligne[3];
			if (!(mysql_query("DELETE FROM `ba` WHERE `id_message`=$id_message LIMIT 1"))) {
				die('Erreur : ' . mysql_error());
			}  
		}
		//debut du ba
		if (isset($_POST['nom'])) // Si les variables existent
		{
			if ($_POST['message']!= NULL && $_POST['nom']!= NULL) // Si on a quelque chose à enregistrer
				{
					$message= nl2br(htmlentities($_POST['message'], ENT_QUOTES, "UTF-8"));
					$addresse= nl2br(htmlentities($_POST['addresse'], ENT_QUOTES, "UTF-8"));
					$nom= nl2br(htmlentities($_POST['nom'], ENT_QUOTES, "UTF-8"));
					$courriel= nl2br(htmlentities($_POST['courriel'], ENT_QUOTES, "UTF-8"));
					$telephone= nl2br(htmlentities($_POST['telephone'], ENT_QUOTES, "UTF-8"));
					$url= nl2br(htmlentities($_POST['url'], ENT_QUOTES, "UTF-8"));
					if (!(mysql_query("INSERT INTO `ba` VALUES ('', '$id_seliste', '$time', '$message', '$addresse','$nom','$courriel','$url','$telephone')"))) {
						die('Erreur : ' . mysql_error());
					}	
				}
		}
		echo "<div class='message'>
		<SCRIPT type=\"text/javascript\">
		function reste()
		{
			document.getElementById('formu').message.value = document.getElementById('formu').message.value.substring(0,250);
			document.getElementById('formu').restant.value = 250 - document.getElementById('formu').message.value.length;
		}
		</SCRIPT>
		<form id=\"formu\" action=\"ba.php\" method=\"post\">
		<table summary=\"\" border=\"0\" width='90%'>
		<tr><td colspan='2'><p class='t1'>Le carnet des bonnes adresses.</p></td></tr>
		<tr>
			<td width='30%'><p class='t3'>Nom:</p></td>
			<td><p class='t2'><input type=\"text\" name=\"nom\" size=\"60\" /></p></td>
		</tr>
		<tr>
			<td><p class='t3'>Description:</p></td>
			<td><p class='t2'><textarea name=\"message\" cols=\"80\" rows=\"2\" onKeyUp=\"reste()\"></textarea><br />
				Nombre de caractères restants: <input readonly type=\"text\" name=\"restant\" onBlur=\"reste()\" onChange=\"reste()\" onFocus=\"reste()\" onSelect=\"reste()\" size=3 value=250></p>
			</td>
		</tr>
		<tr>
			<td><p class='t3'>T&eacute;l&eacute;phone :</p></td>
			<td><p class='t2'><input type=\"text\" name=\"telephone\" size=\"15\" /></p></td>
		</tr>
		<tr>
			<td class='t3'>Courriel :</td>
			<td class='t2'><input type=\"text\" name=\"courriel\" size=\"60\" /></td>
		</tr>
		<tr>
			<td class='t3'>Url de leur site :</td>
			<td class='t2'><input type=\"text\" name=\"url\" size=\"60\" /></td>
		</tr>
		<tr>
			<td colspan=2><p class=t1><input type='submit' value='Envoyer' /></td>
		</tr>
		</table></form></div><br>";
		//affichage des messages
		if (!($requete=mysql_query("SELECT * FROM `ba` ORDER BY id_message DESC LIMIT 0 , 40 "))) {
			die('Erreur : ' . mysql_error());
		}	
		while ($donnees = mysql_fetch_array($requete) )
		{
			$id_message=$donnees['id_message'];
			$id_sel=$donnees['id_seliste'];
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`=$id_sel")))) {
				die('Erreur : ' . mysql_error());
			} 
			$prenom_sel=$recup[0];		
			$date=date('d/m/y',$donnees['timestamp']);
			$message=stripslashes($donnees['message']);
			$addresse=stripslashes($donnees['addresse']);
			$nom=stripslashes($donnees['nom']);
			$telephone=stripslashes($donnees['telephone']);
			$courriel=stripslashes($donnees['courriel']);
			$url=stripslashes($donnees['url']);
			include("smileyscompagnie.php");
			echo "<div class='message'><table summary=\"\" border=\"0\" width='90%'>";
			if($nom!=null){ echo "<tr><td colspan=2><p class='t1'>".$nom."</p></td></tr>"; }
			if($message!=null){ echo "<tr><td><p class=t3>Description:</p></td><td><p class=t2>".$message."</p></td></tr>"; }
			if($addresse!=null){ echo "<tr><td><p class=t3>Addresse:</p></td><td><p class=t2>".$addresse."</p></td></tr>"; }
			if($telephone!=null){ echo "<tr><td><p class=t3>Téléphone:</p></td><td><p class=t2>".$telephone."</p></td></tr>"; }
			if($courriel!=null){ echo "<tr><td><p class=t3>Courriel:</p></td><td><p class=t2>".$courriel."</p></td></tr>"; }
			if($url!=null){ echo "<tr><td><p class=t3>Url</p></td><td><p class=t2><a href=\"".$url."\">".$url."</a></p></td></tr>"; }
			
			echo "<tr><td colspan=2><hr width='50%'><p class='t4 pasimportant'>Adresse proposée par <a href='profil.php?id=".$id_sel."'>".$prenom_sel."(".$id_sel.")</a>, le ".$date."";
			if($grade=='ADMIN'){echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<a class='aadmin' href='ba.php?id=".$id_message."'>Supprimer cette adresse</a>";}
			echo "</p></td></tr></table></div><br>";
		}
		echo "<br></div><br>";
	}
	else
	{ 	 // délai dépassé
		header ("location:troptard.php");
		session_destroy();
	}
 }
else
{ 	 // pas de session
	header ("location:404.php");
	session_destroy();
}
mysql_close($connexion); 
echo "</center>";
include ("fin.php");	
?>