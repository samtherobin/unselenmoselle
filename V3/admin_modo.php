<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/

if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
{								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade=$ligne[0];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
	 	switch ($grade)
		{
			case 'SELISTE' : header("location:404.php");break;				
			case 'MODERATEUR' :header("location:404.php");break;		
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page
		echo "<br><br><div class='corps'><br><br><p class='titre'>Gestion des paramètres modérateurs</p>";
		//debut de l'edition
		if($_GET['action']=='edition') //on enregistre les modifs
		{	
			
			// cherche txt  moderateurs
			if (!($requete=mysql_query("SELECT `texte` FROM  `variables` WHERE  `variable`='TMOD1' LIMIT 1"))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete);
			$TMOD1=stripslashes($ligne[0]);
			if (!($requete=mysql_query("SELECT `texte` FROM  `variables` WHERE  `variable`='TMOD2' LIMIT 1"))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete);
			$TMOD2=stripslashes($ligne[0]);
			if (!($requete=mysql_query("SELECT `texte` FROM  `variables` WHERE  `variable`='TMOD3' LIMIT 1"))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete);
			$TMOD3=stripslashes($ligne[0]);
			if (!($requete=mysql_query("SELECT `texte` FROM  `variables` WHERE  `variable`='TMOD4' LIMIT 1"))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete);
			$TMOD4=stripslashes($ligne[0]);
			if (!($requete=mysql_query("SELECT `texte` FROM  `variables` WHERE  `variable`='TMOD5' LIMIT 1"))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete);
			$TMOD5=stripslashes($ligne[0]);
			if (!($requete=mysql_query("SELECT `texte` FROM  `variables` WHERE  `variable`='TMOD6' LIMIT 1"))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete);
			$TMOD6=stripslashes($ligne[0]);
			// cherche num  moderateurs
			if (!($requete=mysql_query("SELECT `texte` FROM  `variables` WHERE  `variable`='MOD1' LIMIT 1"))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete);
			$MOD1=stripslashes($ligne[0]);
			if (!($requete=mysql_query("SELECT `texte` FROM  `variables` WHERE  `variable`='MOD2' LIMIT 1"))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete);
			$MOD2=stripslashes($ligne[0]);
			if (!($requete=mysql_query("SELECT `texte` FROM  `variables` WHERE  `variable`='MOD3' LIMIT 1"))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete);
			$MOD3=stripslashes($ligne[0]);
			if (!($requete=mysql_query("SELECT `texte` FROM  `variables` WHERE  `variable`='MOD4' LIMIT 1"))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete);
			$MOD4=stripslashes($ligne[0]);
			if (!($requete=mysql_query("SELECT `texte` FROM  `variables` WHERE  `variable`='MOD5' LIMIT 1"))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete);
			$MOD5=stripslashes($ligne[0]);
			if (!($requete=mysql_query("SELECT `texte` FROM  `variables` WHERE  `variable`='MOD6' LIMIT 1"))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete);
			$MOD6=stripslashes($ligne[0]);


			echo  "
			<form method=\"post\" action=\"admin_modo.php?action=enregistre\" enctype=\"multipart/form-data\">
			<table summary=\"\" border='1' class='tablevu' width='90%' cellpadding='5'>
			<tr>
			<td colspan='2'><p>Le résultat est visible sur la page <a href='selistes.php'>Selistes</a></p></td>
			</tr>
			<tr>
				<td><p class='t3'>Moderateur 1:<br><span class='pasimportant'>Comptable</span></p></td>
				<td><p class='t4'>Id du modérateur:<input name='MOD1' value='".$MOD1."' size=4 maxLength=5><br><textarea name='TMOD1' cols=70 rows=5 >".$TMOD1."</textarea></p></td>
			</tr>	
			<tr>
				<td><p class='t3'>Moderateur 2:<br><span class='pasimportant'>Inscription</span></p></td>
				<td><p class='t4'>Id du modérateur:<input name='MOD2' value='".$MOD2."' size=4 maxLength=5><br><textarea name='TMOD2' cols=70 rows=5 >".$TMOD2."</textarea></p></td>
			</tr>	
			<tr>
				<td><p class='t3'>Moderateur 3:<br><span class='pasimportant'>Annonces Agenda Actu..</span></p></td>
				<td><p class='t4'>Id du modérateur:<input name='MOD3' value='".$MOD3."' size=4 maxLength=5><br><textarea name='TMOD3' cols=70 rows=5 >".$TMOD3."</textarea></p></td>
			</tr>	
			<tr>
				<td><p class='t3'>Moderateur 4:<br><span class='pasimportant'>Catalogue</span></p></td>
				<td><p class='t4'>Id du modérateur:<input name='MOD4' value='".$MOD4."' size=4 maxLength=5><br><textarea name='TMOD4' cols=70 rows=5 >".$TMOD4."</textarea></p></td>
			</tr>	
			<tr>
				<td><p class='t3'>Moderateur 5:<br><span class='pasimportant'>Intersel</span></p></td>
				<td><p class='t4'>Id du modérateur:<input name='MOD5' value='".$MOD5."' size=4 maxLength=5><br><textarea name='TMOD5' cols=70 rows=5 >".$TMOD5."</textarea></p></td>
			</tr>	
			<tr>
				<td><p class='t3'>Moderateur 6:<br><span class='pasimportant'>Super-Moderateur</span></p></td>
				<td><p class='t4'>Id du modérateur:<input name='MOD6' value='".$MOD6."' size=4 maxLength=5><br><textarea name='TMOD6' cols=70 rows=5 >".$TMOD6."</textarea></p></td>
			</tr>				
			<tr>
				<td colspan='3'><p class='t1'><input type='submit' value=' Sauvegarder ' ></p></td>
			</tr>
			</table>			
			</form>";
		}
		else //on enregistre
		{
			if($_GET['action']=='enregistre')
			{ //ok pour enregistre
				// on encode tout et on ajoute des /
				
				// moderateur et texte des modo
				$MOD1=$_POST['MOD1'];$MOD2=$_POST['MOD2'];$MOD3=$_POST['MOD3'];$MOD4=$_POST['MOD4'];$MOD5=$_POST['MOD5'];$MOD6=$_POST['MOD6'];
				$TMOD1=nl2br(htmlentities($_POST['TMOD1'], ENT_QUOTES, "UTF-8"));
				$TMOD2=nl2br(htmlentities($_POST['TMOD2'], ENT_QUOTES, "UTF-8"));
				$TMOD3=nl2br(htmlentities($_POST['TMOD3'], ENT_QUOTES, "UTF-8"));
				$TMOD4=nl2br(htmlentities($_POST['TMOD4'], ENT_QUOTES, "UTF-8"));
				$TMOD5=nl2br(htmlentities($_POST['TMOD5'], ENT_QUOTES, "UTF-8"));
				$TMOD6=nl2br(htmlentities($_POST['TMOD6'], ENT_QUOTES, "UTF-8"));
				
				// on enregistre
				if (!($requete=mysql_query("UPDATE `variables` SET `texte` =  '$MOD1' WHERE `variable`= 'MOD1' LIMIT 1 ;"))) {
					die('Erreur : ' . mysql_error());
				}
				if (!($requete=mysql_query("UPDATE `variables` SET `texte` =  '$MOD2' WHERE `variable`= 'MOD2' LIMIT 1 ;"))) {
					die('Erreur : ' . mysql_error());
				}
				if (!($requete=mysql_query("UPDATE `variables` SET `texte` =  '$MOD3' WHERE `variable`= 'MOD3' LIMIT 1 ;"))) {
					die('Erreur : ' . mysql_error());
				}
				if (!($requete=mysql_query("UPDATE `variables` SET `texte` =  '$MOD4' WHERE `variable`= 'MOD4' LIMIT 1 ;"))) {
					die('Erreur : ' . mysql_error());
				}
				if (!($requete=mysql_query("UPDATE `variables` SET `texte` =  '$MOD5' WHERE `variable`= 'MOD5' LIMIT 1 ;"))) {
					die('Erreur : ' . mysql_error());
				}
				if (!($requete=mysql_query("UPDATE `variables` SET `texte` =  '$MOD6' WHERE `variable`= 'MOD6' LIMIT 1 ;"))) {
					die('Erreur : ' . mysql_error());
				}
				if (!($requete=mysql_query("UPDATE `variables` SET `texte` =  '$TMOD1' WHERE `variable`= 'TMOD1' LIMIT 1 ;"))) {
					die('Erreur : ' . mysql_error());
				}
				if (!($requete=mysql_query("UPDATE `variables` SET `texte` =  '$TMOD2' WHERE `variable`= 'TMOD2' LIMIT 1 ;"))) {
					die('Erreur : ' . mysql_error());
				}
				if (!($requete=mysql_query("UPDATE `variables` SET `texte` =  '$TMOD3' WHERE `variable`= 'TMOD3' LIMIT 1 ;"))) {
					die('Erreur : ' . mysql_error());
				}
				if (!($requete=mysql_query("UPDATE `variables` SET `texte` =  '$TMOD4' WHERE `variable`= 'TMOD4' LIMIT 1 ;"))) {
					die('Erreur : ' . mysql_error());
				}
				if (!($requete=mysql_query("UPDATE `variables` SET `texte` =  '$TMOD5' WHERE `variable`= 'TMOD5' LIMIT 1 ;"))) {
					die('Erreur : ' . mysql_error());
				}
				if (!($requete=mysql_query("UPDATE `variables` SET `texte` =  '$TMOD6' WHERE `variable`= 'TMOD6' LIMIT 1 ;"))) {
					die('Erreur : ' . mysql_error());
				}
				echo "<p  class='titre'>Les nouveaux paramètres sont mis à jour merci.</p>";
			}
			else
			{ 	//pirate
				header ("location:404.php");
				session_destroy();			
			}
		}
		//fin edition
		echo "<br></div>";
	} 
	//delai depassé
	else
	{
		header ("location:troptard.php");
		session_destroy();
	}
}
// pas de sesion
else
{
	header ("location:404.php");
	session_destroy();
}
mysql_close($connexion); 
include ("fin.php");	
?>
