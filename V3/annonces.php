<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/
 
if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
 {								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade`,`time_multi`, `email` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		$time_multi= $ligne[1];
		$email_exp= $ligne[2];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
	 	switch ($grade)
		{
			case 'SELISTE' : include("lien/seliste.php");break;	
			case 'MODERATEUR' :include("lien/seliste.php"); include("lien/moderateur.php");break;
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page
		echo "<br><div class=\"corps\"><br>";
		if($_GET['id']!=NULL)
		{ // supprimé une annonce
			$id_message=$_GET['id'];
			if (!($requete=mysql_query("SELECT * FROM `petite_annonce` WHERE `id_message`='$id_message'"))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete);
			$id_sel=$ligne[1];
			if(($id_sel==$id_seliste)||($grade=='MODERATEUR')||($grade=='ADMIN'))
			{
				echo "<p class='titre'>Votre annonce est supprimée</th></tr></table>";		
				if (!(mysql_query("DELETE FROM `petite_annonce` WHERE `id_message`=$id_message LIMIT 1"))) {
					die('Erreur : ' . mysql_error());
				}  
				if (!(mysql_query("INSERT INTO `log` VALUES ('', '$id_seliste', '$id_message', '$time', 'annonce_EFFACE', 'Annonce de $ligne[1]<br>$ligne[4]')"))) {
					die('Erreur : ' . mysql_error());
				}
			}
			else
			{
				echo "<p>Ce n'est pas ton annonce!</p>";
			}
		}
		if ($_GET['id_modif']!=null) // Si les variables existent
		{ // modification d'une annonce
			if($_POST['annonce']!=null)
			{ // en enregistre
				$id_modif=$_GET['id_modif'];
				$message=nl2br(htmlentities($_POST['annonce'], ENT_QUOTES, "UTF-8"));
				if (!($requete=mysql_query("UPDATE `petite_annonce` SET `message` = '$message' WHERE `id_message` ='$id_modif' LIMIT 1") )) {
					die('Erreur : ' . mysql_error());
				}
				echo "<p class='titre'>Modification enregistrée!</p>";
			}
			else
			{ // formulaire de modification
				$id_annonce=$_GET['id_modif'];
				if (!($requete=mysql_fetch_row(mysql_query("SELECT * FROM `petite_annonce` WHERE `id_message`=$id_annonce")))) {
					die('Erreur : ' . mysql_error());
				}
				$id_annonce=$requete[0];
				$id_sel=$requete[1];
				$time_annonce=$requete[2];
				$rubrique=$requete[3];
				$message=stripslashes($requete[4]);
				$message=str_replace("<br />","",$message);
				if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`=$id_sel")))) {
					die('Erreur : ' . mysql_error());
				} 
				$prenom_sel=stripslashes($recup[0]);
				if(($id_seliste==$id_sel)||($grade=='MODERATEUR')||($grade=='ADMIN'))
				{
					echo "<form method='post' action='annonces.php?id_modif=$id_annonce' enctype='multipart/form-data'>
					<br><p class='titre'>Edition de l'annonce de <a href='profil.php?id=".$id_sel."'>".$prenom_sel."(".$id_sel.")</a>.</p><br>
					<p>Annonce:<br /><textarea name='annonce' cols='80' rows='5'>".$message."</textarea>
					<br><input type='submit' value=' Enregistrer les modifications '><p><br>
					</form>";
				}
				else
				{
					echo "<p>Ce n'est pas votre annonce !</p>";
				}
			}
		}
		if (isset($_POST['message'])) // Si les variables existent
		{
			if ($_POST['message']!= NULL) // Si on a quelque chose à enre
			{
				// calcul temporisation entre 2 demandes urgentes variable $tempo2
				$time=time();
				$timelimite=$time-$tempo2;
				if($time_multi<$timelimite)
				{ 
					if (!($requete2=mysql_query("UPDATE `selistes` SET `time_multi` = '$time' WHERE `id_seliste`='$id_seliste' LIMIT 1") )) {
						die('Erreur : ' . mysql_error());
					}
					$message= nl2br(htmlentities($_POST['message'], ENT_QUOTES, "UTF-8"));
					$message2= stripslashes(nl2br($_POST['message']));
					$rubrique= nl2br(htmlentities($_POST['rubrique'], ENT_QUOTES, "UTF-8"));
					if (!(mysql_query("INSERT INTO `petite_annonce` VALUES ('', '$id_seliste', '$time', '$rubrique', '$message')"))) {
						die('Erreur : ' . mysql_error());
					}	
					$time3mois=$time-7889231;
					if (!(mysql_query("DELETE FROM `petite_annonce` WHERE `timestamp`<$time3mois"))) {
						die('Erreur : ' . mysql_error());
					}
					// Gestion du mail 
					if (!($requete=mysql_query("SELECT `prenom`, `email` FROM `selistes` WHERE `valide`='OUI' AND `mail_pet`='OUI'"))) {
						die('Erreur : ' . mysql_error());
					}
						while($ligne=mysql_fetch_row($requete))
						{
							$prenom_dest=stripslashes($ligne[0]);
							$email_dest=$ligne[1];
							// envoi du mail 
							$message3=stripslashes("[".$nom."] Nouvelle petite annonce de ".$prenom."(".$id_seliste.")");
							$headers = "From: $nom <$email>\n";
							$headers .= "Reply-To: $prenom <$email_exp>\n";
							$headers .= "X-Sender: $nom <$email>\n";
							$headers .= "X-Author: $prenom <$email_exp>\n";
							$headers .= "X-Priority:1\n";
							$headers .= "X-Mailer: PHP\n";
							$headers .= "Return_Path: $email\n";
							$headers .='Content-Type: text/html; charset="UTF-8"'."\n";
							$mel ="<html><head><title></title></head><body>
							<p>Bonjour ".$prenom_dest.", une nouvelle petite annonce vient d'être publiée par <a href=http://".$site."/index.php?vers=profil.php?id=".$id_seliste.">".$prenom."(".$id_seliste.")</a>.
							<br><hr width=90%><br>".$message2."
							<br><hr width=90%>
							<br>Pour la lire sur le site <a href=http://".$site."/index.php?vers=annonces.php> cliquez-ici </a>
							<br><u>Pour répondre:</u>
							<br>Par messagerie privée sur le site: <a href=\"http://".$site."/index.php?vers=messagerie.php?action=ecrire&id=".$id_seliste."&ecri=\"> Cliquez ici pour aller sur votre messagerie.</a>
							<br>Soit directement sur son courriel: ".$email_exp."
							<br>Cordialement, <br> ".$nom.".
							<br><br><i>Si vous ne souhaitez plus avoir ces alertes:<br>
							<a href=http://".$site."/index.php?vers=compte.php> Cliquez ici </a> ou via la page compte de l'onglet outils pour desactiver ces alertes.</i></p></body></html>";
							sw_mail($email_dest,$message3,$mel,$headers);
						}
				// fin de gestion du mail
				}
				else
				{
					// message deja envoyé y'a moin de 2tempo2!
					echo "<p class='titre'>Bonjour, vous avez déjà envoyé une annonce récemment!<br>
						Vous ne pouvez envoyer une demande qu'une fois toutes les ".$tempo2." secondes.</p>";	
				}
			}
		}
		echo "
		<form action=\"annonces.php\" method=\"post\">
		<p class='titre'>Les petites annonces</p><br>
		<p><a href='export_annonces.php'>Télécharger les petites annonces</a></p><br>
		<div class='message'>
		<p class='titre'>Ajouter ma petite annonce:</p><br>
		<p>Rubrique:<select name='rubrique'><option value='DEM' >Demande</option><option value='PRO' >Propose</option></select><br>
		<p>Message :<br>
		<textarea name=\"message\" cols=\"80\" rows=\"2\"></textarea><br><br>
		<input type='submit' value='Envoyer' /></form><br></div><br>";
		// formulaire de recherche
		echo "<div class='message'>
		<form method=\"post\" action=\"recherche.php\" enctype=\"multipart/form-data\">
		<p class='titre'>Recherche dans les annonces:</p><br>
		<p><input type='text' name='annonce' size='50' maxlength='50' /><br>
		<input type=\"submit\" value=\"Lancer les recherches \"><br>
		</form></p><br></div><br>		
		<p class='rouge'>Les annonces de plus de 3 mois sont automatiquement effacées.</p>
		<br>
		<table summary=\"\" border=\"1\" width='90%'>
		<tr><td width='50%' class='teinte1'><p class='titre'>Les demandes:</p></td>
			<td width='50%' class='teinte1'><p class='titre'>Les propositions:</p></td></tr></table>";
		//affichage des messages
		if (!($requete1=mysql_query("SELECT * FROM `petite_annonce` WHERE `rubrique`='DEM' ORDER BY id_message DESC LIMIT 0 , 40 "))) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete2=mysql_query("SELECT * FROM `petite_annonce` WHERE `rubrique`='PRO' ORDER BY id_message DESC LIMIT 0 , 40 "))) {
			die('Erreur : ' . mysql_error());
		}		
		echo "
		<table summary=\"\" border=\"1\" width='90%'>
		<tr><td class='teinte2' valign=\"top\" width='50%'>";
		while($donnees = mysql_fetch_array($requete1))
			{
				$id_message=$donnees['id_message'];
				$id_sel=$donnees['id_seliste'];
				if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`=$id_sel")))) {
					die('Erreur : ' . mysql_error());
				} 
				$prenom_sel=$recup[0];		
				$date=date('d/m/y à H\h i\m\i\n',$donnees['timestamp']);
				$message=stripslashes($donnees['message']);
				include("smileyscompagnie.php");
				echo "<br>
					<div style='background-color:#ffffff' class='message'><p>Annonce de <a href='profil.php?id=".$id_sel."'>".$prenom_sel."(".$id_sel.")</a><br>
					<span class='pasimportant'>Le ".$date." </span></p>
					<p class=center>
					<hr width=50%>			   
					<p>".$message."</p>";
					echo "<p><br><a href='messagerie.php?action=ecrire&amp;id=".$id_sel."&amp;ecri='><img src='images/lettreNormal.jpg' width='20'> Répondre</a>";
					if(($id_sel==$id_seliste)||($grade=='ADMIN')||($grade=='MODERATEUR'))
					{
						echo "&nbsp;&nbsp;
						<a class='amodo' href='annonces.php?id=".$id_message."'><img src=\"images/croix.jpg\"> Supprimer</a>&nbsp;&nbsp;
						<a class='amodo' href='annonces.php?id_modif=".$id_message."'><img src=\"images/editer.gif\"> Modifier</a>";
					}									
					echo "</p></div>";
			}
			echo "</td><td class='teinte2' valign=\"top\"width='50%'>";
			while ($donnees2 = mysql_fetch_array($requete2))
			{ 
				$id_message2=$donnees2['id_message'];
				$id_sel2=$donnees2['id_seliste'];
				if (!($recup2=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`=$id_sel2")))) {
					die('Erreur : ' . mysql_error());
				} 
				$prenom_sel2=$recup2[0];		
				$date2=date('d/m/y à H\h i\m\i\n',$donnees2['timestamp']);
				$message=stripslashes($donnees2['message']);
				include("smileyscompagnie.php");
				echo "<br>
					<div style='background-color:#ffffff' class='message'><p>Annonce de <a href='profil.php?id=".$id_sel2."'>".$prenom_sel2."(".$id_sel2.")</a><br>
					<span class='pasimportant'>Le ".$date2." </span></p>
					<p class=center>
					<hr width=50%>	   
					<p>".$message."</p>";
					echo "<p><br><a href='messagerie.php?action=ecrire&amp;id=".$id_sel2."&amp;ecri='><img src='images/lettreNormal.jpg' width='20'> Répondre</a>";
					if(($id_sel2==$id_seliste)||($grade=='MODERATEUR')||($grade=='ADMIN'))
					{
						echo "&nbsp;&nbsp;
						<a class='amodo' href='annonces.php?id=".$id_message2."'><img src=\"images/croix.jpg\"> Supprimer</a>&nbsp;&nbsp;
						<a class='amodo' href='annonces.php?id_modif=".$id_message2."'><img src=\"images/editer.gif\"> Modifier</a>";
					}
					echo "</p></div>";				
			}
		echo "</td></table></div>";
	}
	else
	{ 	 // délai dépassé
		header ("location:troptard.php");
		session_destroy();
	}
 }
else
{ 	 // pas de session
	header ("location:404.php");
	session_destroy();
}
mysql_close($connexion); 
echo "</center>";
include ("fin.php");	
?>
