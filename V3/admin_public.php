<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/

if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
{								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade=$ligne[0];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
	 	switch ($grade)
		{
			case 'SELISTE' : header("location:404.php");break;				
			case 'MODERATEUR' :header("location:404.php");break;		
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page
		echo "<br><br><div class='corps'><br><br><p class='titre'>Gestion de la partie public du site</p>";
		//debut de l'edition
		if($_GET['action']=='edition') //on enregistre les modifs
		{	
				// rapatriement parametre12
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'site'")))) {
				die('Erreur : ' . mysql_error());
			}
			$site=stripslashes($recup[0]);
			// rapatriement parametre20
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'index'")))) {
				die('Erreur : ' . mysql_error());
			}
			$index=stripslashes($recup[0]);
			$index=str_replace('<br />', '', $index);
			// rapatriement parametre5
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'charte'")))) {
				die('Erreur : ' . mysql_error());
			}
			$charte=stripslashes($recup[0]);
			$charte=str_replace('<br />', '', $charte);
			// rapatriement parametre9
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'description'")))) {
				die('Erreur : ' . mysql_error());
			}
			$description=stripslashes($recup[0]);
			$description=str_replace('<br />', '', $description);
			// rapatriement parametre10
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'lieu'")))) {
				die('Erreur : ' . mysql_error());
			}
			$lieu=stripslashes($recup[0]);
			$lieu=str_replace('<br />', '', $lieu);
			// rapatriement parametre11
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'presentation'")))) {
				die('Erreur : ' . mysql_error());
			}
			$presentation=stripslashes($recup[0]);
			$presentation=str_replace('<br />', '', $presentation);
			// rapatriement parametre6
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'ville'")))) {
				die('Erreur : ' . mysql_error());
			}
			$ville=stripslashes($recup[0]);
			// rapatriement parametre
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'titresup'")))) {
				die('Erreur : ' . mysql_error());
			}
			$titresup=stripslashes($recup[0]);
			// rapatriement parametre
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'pagesup'")))) {
				die('Erreur : ' . mysql_error());
			}
			$pagesup=stripslashes($recup[0]);
			echo  "
			<form method=\"post\" action=\"admin_public.php?action=enregistre\" enctype=\"multipart/form-data\">
			<table summary=\"\" border='1' class='tablevu' width='90%' cellpadding='5'>
			<tr>
			<td colspan='2' bgcolor='#FF0000'><p>/!\ Attention aux modifications, elles influent sur les pages de la partie PUBLIC du site.<br> Les balises HTML sont autorisés. Vous pouvez donc inclure vidéos photos etc..</p></td>
			</tr>
			<tr>
				<td width='20%'><p class='t1'>Page index de ".$nom.":</p><p class='t4'><a target=\"_blank\" href='http://$site'>Voir la page actuelle</a></p></td>
				<td><p class='t4'><textarea name='index' cols=80 rows=15 >".$index."</textarea></p></td>
			</tr>
			<tr>
				<td><p class='t1'>Charte de ".$nom.":</p><p class='t4'><a target=\"_blank\" href='http://$site/charte.php'>Voir la page actuelle</a></p></td>
				<td><p class='t4'><textarea name='charte' cols=80 rows=15 >".$charte."</textarea></p></td>
			</tr>
			<tr>
				<td><p class='t1'>Description de ".$nom.":</p><p class='t4'>(phrase pour le référencement google):</p></td>
				<td><p class='t4'><textarea name='description' cols=60 rows=3 >".$description."</textarea></p></td>
			</tr>
			<tr>
				<td><p class='t1'>Lieu de rencontre pour les inscriptions:</p><p class='t4'><a target=\"_blank\" href='http://$site/inscription.php'>Voir la page actuelle</a><br>La date se met automatiquement à jour selon l'agenda du site.</p></td>
				<td><p class='t4'><textarea name='lieu' cols=70 rows=15 >".$lieu."</textarea></p></td>
			</tr>
			<tr>
				<td><p class='t1'>Présentation du SEL de ".$ville.":</p><p class='t4'><a target=\"_blank\" href='http://$site/presentation.php'>Voir la page actuelle</a></p></td>
				<td><p class='t4'><textarea name='presentation' cols=70 rows=15 >".$presentation."</textarea></p></td>
			</tr>
			<tr>
				<td><p class='t1'>Page supplémentaire</p><p class='t4'>(Réglement/Statut..):</p><p class='t4'><a target=\"_blank\" href='http://$site/pagesup.php'>Voir la page actuelle</a></p></td>
				<td><p class='t4'><input name=\"titresup\" size=\"30\" value=\"".$titresup."\" maxlength=\"100\"><br>
								<textarea name='pagesup' cols=80 rows=15 >".$pagesup."</textarea></p></td>
			</tr>			
			<tr>
				<td colspan='3'><p class='t1'><input type='submit' value=' Sauvegarder ' ></p></td>
			</tr>
			</table>			
			</form>";
		}
		else //on enregistre
		{
			if($_GET['action']=='enregistre')
			{ //ok pour enregistre
				// ici on encode tout (accent et code html) puis on decode le html pour pouvoir l'afficher
				$charte=nl2br(htmlspecialchars_decode(htmlentities($_POST['charte'], ENT_QUOTES, 'UTF-8')));
				$index=nl2br(htmlspecialchars_decode(htmlentities($_POST['index'], ENT_QUOTES, 'UTF-8')));
				$description=nl2br(htmlspecialchars_decode(htmlentities($_POST['description'], ENT_QUOTES, 'UTF-8')));
				$lieu=nl2br(htmlspecialchars_decode(htmlentities($_POST['lieu'], ENT_QUOTES, 'UTF-8')));
				$presentation=nl2br(htmlspecialchars_decode(htmlentities($_POST['presentation'], ENT_QUOTES, 'UTF-8')));
				$titresup=nl2br(htmlspecialchars_decode(htmlentities($_POST['titresup'], ENT_QUOTES, 'UTF-8')));
				$pagesup=nl2br(htmlspecialchars_decode(htmlentities($_POST['pagesup'], ENT_QUOTES, 'UTF-8')));

				
				// on enregistre
				if (!($requete=mysql_query("UPDATE `variables` SET `texte` =  '$charte' WHERE `variable`= 'charte' LIMIT 1 ;"))) {
					die('Erreur : ' . mysql_error());
				}
				if (!($requete=mysql_query("UPDATE `variables` SET `texte` =  '$description' WHERE `variable`= 'description' LIMIT 1 ;"))) {
					die('Erreur : ' . mysql_error());
				}
				if (!($requete=mysql_query("UPDATE `variables` SET `texte` =  '$lieu' WHERE `variable`= 'lieu' LIMIT 1 ;"))) {
					die('Erreur : ' . mysql_error());
				}
				if (!($requete=mysql_query("UPDATE `variables` SET `texte` =  '$presentation' WHERE `variable`= 'presentation' LIMIT 1 ;"))) {
					die('Erreur : ' . mysql_error());
				}
				if (!($requete=mysql_query("UPDATE `variables` SET `texte` =  '$index' WHERE `variable`= 'index' LIMIT 1 ;"))) {
					die('Erreur : ' . mysql_error());
				}
				if (!($requete=mysql_query("UPDATE `variables` SET `texte` =  '$titresup' WHERE `variable`= 'titresup' LIMIT 1 ;"))) {
					die('Erreur : ' . mysql_error());
				}
				if (!($requete=mysql_query("UPDATE `variables` SET `texte` =  '$pagesup' WHERE `variable`= 'pagesup' LIMIT 1 ;"))) {
					die('Erreur : ' . mysql_error());
				}
				
				echo "<p  class='titre'>Les nouveaux paramètres sont mis à jour merci.</p>";
			}
			else
			{ 	//pirate
				header ("location:404.php");
				session_destroy();			
			}
		}
		//fin edition
		echo "<br></div>";
	} 
	//delai depassé
	else
	{
		header ("location:troptard.php");
		session_destroy();
	}
}
// pas de sesion
else
{
	header ("location:404.php");
	session_destroy();
}
mysql_close($connexion); 
include ("fin.php");	
?>
