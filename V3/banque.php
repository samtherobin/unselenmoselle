<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/
if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
{								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade`, `nbr_art`, `prenom` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		$nbr_art=$ligne[1];
		$prenom=$ligne[2];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
		switch ($grade)
		{
			case 'SELISTE' : header("location:404.php");break;
			case 'MODERATEUR' :include("lien/seliste.php"); include("lien/moderateur.php");break;
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page
		
		echo "<br><div class=\"corps\"><br>
			<p class='titre'>Banque $nom</p><br>";
		if ($_POST['libelle']!= NULL) // Si on a quelque chose à enregistrer
		{
			$libelle= nl2br(htmlentities($_POST['libelle'], ENT_QUOTES, "UTF-8"));
			$montant= nl2br(htmlentities($_POST['montant']));
			if (!(mysql_query("INSERT INTO `banque` VALUES ('', '$time', '$montant', '$libelle', '$id_seliste')"))) {
				die('Erreur : ' . mysql_error());
			}	
			echo "<div class='message'><p>Entrée enregistrée</p></div>";
		}
		if ($_GET['id_banque']!= NULL) // Si on a quelque chose à effacer
		{
			$id_banque= $_GET['id_banque'];
			if (!(mysql_query("DELETE FROM `banque` WHERE `id`=$id_banque"))) {
				die('Erreur : ' . mysql_error());
			}	
			echo "<div class='message teinte3'><p class='titre'>Entrée éffacée</p></div>";
		}
		echo "<br><div class='message'>
			<form id=\"formu\" action=\"banque.php\" method=\"post\">
			<p class=\"titre\">Ajouter une entrée:</p>
			<p>Libellé: <input type='text' size='80' maxlength='120' name='libelle' value=''>
			<br>Montant: <input type='text' size='8' maxlength='8' name='montant' value=''> €uros
			<br><input type='submit' value='Ajouter cette entrée' /><br></p></div>";
		
		// calcul du total
		if (!($requete1=mysql_fetch_row(mysql_query("SELECT SUM(`montant`) FROM `banque`")))) {
			die('Erreur : ' . mysql_error());
		}
		$total=$requete1[0];
		// affichage livre
		echo "<br><div class='message'><br>
			<p class='titre'>Livre des comptes de ".$nom.":</p><br>
			<br><div class='message teinte1'><p class='titre'>$total €uros sur le compte.</p></br></div><br>
			<table summary=\"\" border=\"1\" width=\"100%\">
			<tr>
				<td class='t1 teinte1'><p>Date:</p></td>
				<td class='t1 teinte1'><p>Libellé:</p></td>
				<td class='t1 teinte1'><p>Montant:</p></td>
				<td class='t1 teinte1'><p>Enregistré par:</p></td>
			</tr>";

		
		if (!($requete1=mysql_query("SELECT * FROM `banque` ORDER BY `date` DESC") )) {
			die('Erreur : ' . mysql_error());
		}
		while($ligne=mysql_fetch_row($requete1))
			{
				$id_banque=$ligne[0];
				$date=date('d/m/y à H\h i\m\i\n',$ligne[1]);
				$montant=$ligne[2];
				$libelle=stripslashes($ligne[3]);
				$id_sel=$ligne[4];
				if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`=$id_sel")))) {
					die('Erreur : ' . mysql_error());
				} 
				$prenom_sel=$recup[0];	
				echo "
				<tr>
				<td class='t1 teinte2'><p>";if($grade=='ADMIN'){echo "&nbsp;<a class='aadmin' href='banque.php?id_banque=".$id_banque."'><img src='images/croix.jpg'></a>";}echo "$date</p></td>
				<td class='t1 teinte2'><p>$libelle</p></td>
				<td class='t1 teinte3'><p>$montant</p></td>
				<td class='t1 teinte2'><p><a href='profil.php?id=".$id_sel."'>".$prenom_sel."(".$id_sel.")</a></p></td>
				</tr>";
			}
		echo "</table><br></div><br></div><br>";
		
	}
	else
	{ 	 // délai dépassé
		header ("location:troptard.php");
		session_destroy();
	}
 }
else
{ 	 // pas de session
	header ("location:404.php");
	session_destroy();
}
mysql_close($connexion); 
echo "</center>";
include ("fin.php");	
?>
