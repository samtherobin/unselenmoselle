<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/
if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
{								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade`, `grains`, `prenom`, `nbr_mp`, `signature`, `email` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		$grains= $ligne[1];	
		$prenom= $ligne[2];			
		$nbr_mp= $ligne[3];
		$signature=$ligne[4];
		$email=$ligne[5];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
		switch ($grade)
		{
			case 'SELISTE' : include("lien/seliste.php");break;	
			case 'MODERATEUR' :include("lien/seliste.php"); include("lien/moderateur.php");break;
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page
		$action=$_GET['action'];
		//afficher les article
		if($action=='lire')
		{
			echo "<br><div class=\"corps\"><br>
			<p class='titre'>Les 5 derniers compte-rendu publiés:</p><br><p>";
			if($grade==ADMIN){ echo "
			<a class='aadmin' href=\"compte_rendu.php?action=proposer\"><img src=\"images/editer.gif\"> Publier un compte-rendu</a>&nbsp;&nbsp;&nbsp; ";
			}
			echo "
			<a href=\"compte_rendu.php?action=ancien\">Voir les anciens comptes-rendus</a></p><br>";
			if (!($requete=mysql_query("SELECT * FROM `compte_rendu` ORDER BY `timestamp` DESC LIMIT 0,5"))) {
				die('Erreur : ' . mysql_error());
			}	
			while($ligne=mysql_fetch_row($requete))
				{
					$id_article= $ligne[0];
					$id_ecriveur= $ligne[1];
					$timestamp= $ligne[2];
					$date=date('d/m/y à H\h',$timestamp);	
					$titre= stripslashes($ligne[3]);
					$article= stripslashes($ligne[4]);
					$message=$article;
					include("smileyscompagnie.php" );
					$article=$message;
					if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`='$id_ecriveur'")))) {
						die('Erreur : ' . mysql_error());
					} 
					$prenom_ecriveur=stripslashes($recup[0]);		
					if (!($nb_replique=mysql_num_rows(mysql_query("SELECT * FROM `compte_rendu_repliques` WHERE `id_com_ren`=$id_article")))) {
						die('Erreur : ' . mysql_error());
					}
					echo "<div class='message'><p class='titre'>".$titre."</p><br>
					<p>Compte-rendu écrit par <a href=\"profil.php?id=".$id_ecriveur."\">".$prenom_ecriveur."(".$id_ecriveur.")</a> le ".$date."</p><br>
					<hr width=70%><p class='t2'>".$article."</p><hr width=70%><br><p>
					<a href=\"compte_rendu.php?action=reponses&amp;id=".$id_article."\"><img src=\"images/lettres.jpg\"> Voir les ".$nb_replique." Réponse";if($nb_replique>1){echo "s";}echo "</a>
					<a href=\"compte_rendu.php?action=repondre&amp;id=".$id_article."\" title=\"Repondre\"><img src=\"images/lettre.jpg\"> Répondre</a>&nbsp; ";
					if(($grade=='MODERATEUR')||($grade=='ADMIN')){echo "
					<a class='amodo' href=\"compte_rendu.php?action=editer_article&amp;id=".$id_article."\" title=\"Editerarticle\"><img src=\"images/editer.gif\"> Editer le compte-rendu</a>";} echo "					
					</p></div><br>"; 	
				}
				echo"</div>";
			}
			elseif($action=='ancien')
			{
				echo "<br><div class=\"corps\"><br>
				<p class='titre'>Les anciens comptes-rendus :</p><br>
				<p><a href=\"compte_rendu.php?action=lire\">Voir les derniers comptes-rendus</a></p><br>";
				if (!($requete=mysql_query("SELECT * FROM `compte_rendu` ORDER BY `timestamp` DESC LIMIT 5,100"))) {
					die('Erreur : ' . mysql_error());
				}	
				while($ligne=mysql_fetch_row($requete))
				{
					$id_article= $ligne[0];
					$id_ecriveur= $ligne[1];
					$timestamp= $ligne[2];
					$date=date('d/m/y à H\h',$timestamp);	
					$titre= stripslashes($ligne[3]);					
					if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`=$id_ecriveur")))) {
						die('Erreur : ' . mysql_error());
					} 
					$prenom_ecriveur=stripslashes($recup[0]);					
					if (!($nb_replique=mysql_num_rows(mysql_query("SELECT * FROM `compte_rendu_repliques` WHERE `id_article`=$id_article")))) {
						die('Erreur : ' . mysql_error());
					}
					echo "<div class='message'><p class='titre'>".$titre."</p><br>
					<p>Compte-rendu publier par <a href=\"profil.php?id=".$id_ecriveur."\">".$prenom_ecriveur."(".$id_ecriveur.")</a> le ".$date."</p><br>
					<p>
					<a href=\"compte_rendu.php?action=reponses&amp;id=".$id_article."\"><img src=\"images/lettres.jpg\"> Voir le compte-rendu ";if($nb_replique>1){echo "et ses ".$nb_replique." réponses";}echo "</a>
					<a href=\"compte_rendu.php?action=repondre&amp;id=".$id_article."\" title=\"Repondre\"><img src=\"images/lettre.jpg\"> Répondre</a>&nbsp; ";
					if(($grade=='MODERATEUR')||($grade=='ADMIN')){echo "
					<a class='amodo' href=\"compte_rendu.php?action=editer_article&amp;id=".$id_article."\" title=\"Editerarticle\"><img src=\"images/editer.gif\"> Editer</a>";}echo "
					</p></div><br>"; 
				}
				echo"</div>";
			}
			//repondre a une article
			elseif($action=='repondre')
			{
				$id_article=nl2br(htmlentities($_GET['id'], ENT_QUOTES, "UTF-8"));
				if (!($recup=mysql_fetch_row(mysql_query("SELECT `titre` FROM `compte_rendu` WHERE `id_com_ren`=$id_article")))) {
					die('Erreur : ' . mysql_error());
				}
				$titre=stripslashes($recup[0]);
					echo "<br><div class=\"corps\"><br><form method='post' action='compte_rendu.php?action=envoye&amp;id=$id_article' enctype='multipart/form-data'>
			   	<input type='hidden' name='id_article' value=$id_article >
					<p class='titre'>Répondre au compte-rendu : \" ".$titre." \"</p>
					<br><p><textarea name='message' cols='80' rows='10'></textarea></p><br>
					<input type='submit' value=' Répondre '>
					</form><br>
					<table summary=\"\" border=\"1\" width=\"90%\">
					<tr class='t1'>
					<td><img alt=\"smile\" src='smiles/smile1.gif'></td><td><img alt=\"smile\" src='smiles/smile2.gif'></td><td><img alt=\"smile\" src='smiles/smile3.gif'></td>
		   		<td><img alt=\"smile\" src='smiles/smile4.gif'></td><td><img alt=\"smile\" src='smiles/smile5.gif'></td><td><img alt=\"smile\" src='smiles/smile6.gif'></td>
		   		<td><img alt=\"smile\" src='smiles/smile7.gif'></td><td><img alt=\"smile\" src='smiles/smile8.gif'></td><td><img alt=\"smile\" src='smiles/smile9.gif'></td>
		  			<td><img alt=\"smile\" src='smiles/smile10.gif'></td><td><img alt=\"smile\" src='smiles/smile11.gif'></td><td><img alt=\"smile\" src='smiles/smile12.gif'></td>
		   		<td><img alt=\"smile\" src='smiles/smile13.gif'></td><td><img alt=\"smile\" src='smiles/smile27.gif'></td><td><img alt=\"smile\" src='smiles/smile28.gif'></td>
		   		<td><img alt=\"smile\" src='smiles/smile36.gif'></td><td><img alt=\"smile\" src='smiles/smile25.gif'></td>
					</tr>	
					<tr class='t2'>
					<td>:)</td><td>:(</td><td>;)</td><td>:D</td><td>:*</td><td>B)</td><td>:o</td><td>:$</td>
					<td>:p</td><td>:x</td><td>(k)</td><td>:-/</td><td>Zzz</td><td>O:]</td><td>(bain)</td><td>(sante)</td>
					<td>(love)</td>
					</tr>
					<tr class='t1'>
		   		<td><img alt=\"smile\" src='smiles/smile19.gif'></td><td><img alt=\"smile\" src='smiles/smile20.gif'></td><td><img alt=\"smile\" src='smiles/smile18.gif'></td>
		   		<td><img alt=\"smile\" src='smiles/smile22.gif'></td><td><img alt=\"smile\" src='smiles/smile23.gif'></td><td><img alt=\"smile\" src='smiles/smile24.gif'></td>
		   		<td><img alt=\"smile\" src='smiles/smile17.gif'></td><td><img alt=\"smile\" src='smiles/smile16.gif'></td><td><img alt=\"smile\" src='smiles/smile14.gif'></td>
		   		<td><img alt=\"smile\" src='smiles/smile15.gif'></td><td><img alt=\"smile\" src='smiles/smile29.gif'></td><td><img alt=\"smile\" src='smiles/smile30.gif'></td>
		   		<td><img alt=\"smile\" src='smiles/smile31.gif'></td><td><img alt=\"smile\" src='smiles/smile34.gif'></td><td><img alt=\"smile\" src='smiles/smile32.gif'></td>
					<td><img alt=\"smile\" src='smiles/smile26.gif'></td><td><img alt=\"smile\" src='smiles/smile21.gif'>
					</tr>
					<tr class='t2'>
					<td>:F</td><td>:X</td><td>(l)</td><td>:P</td><td>:!</td><td>:i</td>
					<td>;x</td><td>:-*</td><td>8)</td><td>:#</td><td>;(</td><td>:@</td><td>(mur)</td>
					<td>O:}</td><td>(jtm)</td><td>(merci)</td><td>(gne)</td>	
					</tr>										
					</table></div>";
			}
			//envoyé la reponse
			elseif($action=='envoye')
			{
				$id_article_url=nl2br(htmlentities($_GET['id'], ENT_QUOTES, "UTF-8"));
				$id_article=nl2br(htmlentities($_POST['id_article'], ENT_QUOTES, "UTF-8"));
				$message=nl2br(htmlentities($_POST['message'], ENT_QUOTES, "UTF-8"));
				if($id_article_url==$id_article)
				{
					if( strlen( $message ) > 10 ) 
					{
						if (!(mysql_query("INSERT INTO `compte_rendu_repliques` VALUES ('', '$id_article', '$id_seliste', '$time', '$message')"))) {
							die('Erreur : ' . mysql_error());
						}
						echo "<br><div class=\"corps\"><br>
						<p>Votre réponse à bien été envoyée.<br><br>
						<a href='bureau.php' title='bureau'>Retour bureau</a>&nbsp;&nbsp;&nbsp;&nbsp;
						<a href='compte_rendu.php?action=lire'>Revoir les comptes-rendus</a></p><br>
						</div>";
					}
					// Message trop court
					else 
					{
						echo "<br><div class=\"corps\"><br>
						<p>Votre message est trop court..<br><br>
						<a href='compte_rendu.php?action=lire'>Revoir les comptes-rendus</a>&nbsp;&nbsp;&nbsp;&nbsp;
						<a href='bureau.php' title='bureau'>Retour bureau</a></p><br>
						</div>";
					}				
				}
				else
				{
					header ("location:404.php");
					session_destroy();
				}
			}
			//voir les reponses
			elseif($action=='reponses')
			{
				$id_article=nl2br(htmlentities($_GET['id'], ENT_QUOTES, "UTF-8"));
				if (!($requete=mysql_query("SELECT * FROM `compte_rendu` WHERE `id_com_ren`='$id_article'"))) {
					die('Erreur : ' . mysql_error());
				}	
				$ligne=mysql_fetch_row($requete);
				$id_article= $ligne[0];
				$id_ecriveur= $ligne[1];
				$timestamp= $ligne[2];
				$date=date('d/m/y à H\h',$timestamp);	
				$titre=stripslashes($ligne[3]);
				$article=stripslashes($ligne[4]);
				$message=$article;
				include("smileyscompagnie.php" );
				$article=$message;
				if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`=$id_ecriveur")))) {
					die('Erreur : ' . mysql_error());
				} 
				$prenom_ecriveur=stripslashes($recup[0]);		
					echo "<br><div class=\"corps\"><br><div class=\"message\"><br><p class='titre'>".$titre."</p><br>
					<p>Compte-rendu écrit par <a href=\"profil.php?id=".$id_ecriveur."\">".$prenom_ecriveur."(".$id_ecriveur.")</a> le ".$date."</p><br>
					<hr width=70%><p class='t2'>".$article."</p><hr width=70%><br><p>
					<a href=\"compte_rendu.php?action=repondre&amp;id=".$id_article."\" title=\"Repondre\"><img src=\"images/lettre.jpg\"> Répondre</a>&nbsp; ";
					if(($grade=='MODERATEUR')||($grade=='ADMIN')){echo "
					<a class='amodo' href=\"compte_rendu.php?action=editer_article&amp;id=".$id_article."\" title=\"Editerarticle\"><img src=\"images/editer.gif\"> Editer le compte-rendu</a>
					";} echo "					
					</p><br></div><br>"; 
					// recup des repliques
					if (!($requete=mysql_query("SELECT * FROM `compte_rendu_repliques` WHERE `id_com_ren`=$id_article ORDER BY `timestamp` ASC"))) {
						die('Erreur : ' . mysql_error());
					}	
					while($ligne=mysql_fetch_row($requete))
					{
						$id_replique= $ligne[0];
						$id_ecriveur= $ligne[2];
						$timestamp= $ligne[3];
						$date=date('d/m/y à H\h i\m\i\n',$timestamp);	
						$message=stripslashes($ligne[4]);
						include("smileyscompagnie.php" );
						if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`=$id_ecriveur")))) {
							die('Erreur : ' . mysql_error());
						} 
						$prenom_ecriveur=stripslashes($recup[0]);		
						$grade_ecriveur=$recup[1];
						echo "
						<div class='message'><p class=\"pasimportant\">Réponse de <a href='profil.php?id=".$id_ecriveur."'>".$prenom_ecriveur."(".$id_ecriveur.")</a> le ".$date."</p><br>
						<p>".$message."</p>";
						if(($grade=='MODERATEUR')||($grade=='ADMIN')){echo " 
						<br><a  class='amodo' href='compte_rendu.php?action=sup_rep&amp;id=".$id_replique."'><img src=\"images/croix.jpg\"> Supprimer cette réponse</a>
						&nbsp;&nbsp;&nbsp;<a class='amodo' href='compte_rendu.php?action=edit_rep&amp;id=".$id_replique."'><img src=\"images/editer.gif\"> Editer cette réponse</a><br>";}
						echo "<br></div><br>";
					}
					echo"<br></div>";
			}
			//envoyé la reponse
			elseif(($action=='proposer')&&($grade=='ADMIN'))
			{
				if($_GET['action2']=='envoye')
				{
					$titre=nl2br($_POST['titre']);
					$message=nl2br($_POST['article']);
					$titrebase=nl2br(htmlentities($_POST['titre'], ENT_QUOTES, "UTF-8"));
					$messagebase=nl2br(htmlentities($_POST['article'], ENT_QUOTES, "UTF-8"));
					if( strlen( $message ) > 50 ) 
					{
						if (!(mysql_query("INSERT INTO `compte_rendu` VALUES ('', '$id_seliste', '$time', '$titrebase', '$messagebase')"))) {
							die('Erreur : ' . mysql_error());
						}
						if (!(mysql_query("INSERT INTO `log` VALUES ('', '$id_seliste', '$id_com_ren', '$time', 'cr_PUB_IN', '$titre')"))) {
							die('Erreur : ' . mysql_error());
						}
						// rapatriement parametre
						if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'annee'")))) {
							die('Erreur : ' . mysql_error());
						}
						$annee=stripslashes($recup[0]);
						// envoi du mail:
						if (!($requete=mysql_query("SELECT `prenom`, `email` FROM `selistes` WHERE `valide`='OUI' AND `mail_actu`='OUI'"))) {
							die('Erreur : ' . mysql_error());
						}
						while($ligne=mysql_fetch_row($requete))
						{
							$prenom_dest=stripslashes($ligne[0]);
							$email_dest=$ligne[1];
							// envoi du mail 
							$message2=stripslashes("[".$nom."]Compte-rendu: \" ".$titre." \" ");
							$headers = "From: Compte rendu $nom <$email>\n";
							$headers .= "Reply-To: $nom\n";
							$headers .= "X-Sender: $nom\n";
							$headers .= "X-Author: Compte-rendu $site\n";
							$headers .= "X-Priority:1\n";
							$headers .= "X-Mailer: PHP\n";
							$headers .= "Return_Path: <$email>\n";
							$headers .='Content-Type: text/html; charset="UTF-8"'."\n";
							$mel ="
	<html><head><title></title></head><body>
	<p>Bonjour ".$prenom_dest.", un nouveau compte-rendu vient d'être publié par <a href=http://".$site."/index.php?vers=profil.php?id=".$id_seliste.">".$prenom."(".$id_seliste.")</a>.
	<br><br><b>".$titre."</b>
	<br><hr width=90%><br>".$message."
	<br><hr width=90%>
	<br>Pour le lire sur le site <a href=http://".$site."/index.php?vers=compte_rendu.php?action=lire> cliquez-ici </a>
	<br>Cordialement, <br> ".$nom.".
	<br><br><i>Si vous ne souhaitez plus avoir ces alertes:<br>
	<a href=http://".$site."/index.php?vers=compte.php> Cliquez ici </a> ou via la page compte de l'onglet outils pour desactiver ces alertes.</i></p></body></html>";
								sw_mail($email_dest,$message2,$mel,$headers);
							}
						echo "<br><div class=\"corps\"><br>
						<p>Votre compte-rendu a bien été publié.<br><br>
						<a href='compte_rendu.php?action=lire'>Revoir les derniers comptes-rendus</a>&nbsp;&nbsp;&nbsp;&nbsp;
						<a href='bureau.php' title='bureau'>Retour bureau</a></p><br>
						</div>";
					}
					// Message trop court
					else 
					{
						echo "
						<br><div class=\"corps\"><br>
						<p class='titre'>Votre compte-rendu est trop court!<br><br><br>
						<a href='compte_rendu.php?action=lire'>Revoir les derniers comptes-rendus</a>&nbsp;&nbsp;&nbsp;&nbsp;
						<a href='bureau.php' title='bureau'>Retour bureau</a></p><br>
						</div>";
					}	
				}
				else
				{
					echo "<br><div class=\"corps\"><br>
					<form method='post' action='compte_rendu.php?action=proposer&amp;action2=envoye' enctype='multipart/form-data'>
					<p class='titre'>Publication de compte-rendu:</p>
					<p>Titre:<input type='text' name='titre' size='80' maxlength='80' /></p>
					<p><textarea name='article' cols='80' rows='30'>Ecrivez votre compte-rendu ici.</textarea></p>
					<hr width='70%'>
					<p>Publication instantanée, si vous êtes sûre de vous, vérifiez que le titre est mis, les fautes d'orthographes sont corrigés,<br>
					la mise en forme de votre compte-rendu, n'hésitez pas à l'égayer avec des styles (Gras, couleurs..):<br>
					<input type='submit' value=' Publication immédiate et envoi des mails'></p>
					<p class='rouge'><b>Attention, l'envoi peut mettre quelques minutes, patientez une fois que vous avez cliquez.</b></p><hr width='70%'></p></form>
					<table summary=\"\" border=\"1\" width=\"90%\">
					<tr class='t1'>
					<td><img alt=\"smile\" src='smiles/smile1.gif'></td><td><img alt=\"smile\" src='smiles/smile2.gif'></td><td><img alt=\"smile\" src='smiles/smile3.gif'></td>
		   		<td><img alt=\"smile\" src='smiles/smile4.gif'></td><td><img alt=\"smile\" src='smiles/smile5.gif'></td><td><img alt=\"smile\" src='smiles/smile6.gif'></td>
		   		<td><img alt=\"smile\" src='smiles/smile7.gif'></td><td><img alt=\"smile\" src='smiles/smile8.gif'></td><td><img alt=\"smile\" src='smiles/smile9.gif'></td>
		  			<td><img alt=\"smile\" src='smiles/smile10.gif'></td><td><img alt=\"smile\" src='smiles/smile11.gif'></td><td><img alt=\"smile\" src='smiles/smile12.gif'></td>
		   		<td><img alt=\"smile\" src='smiles/smile13.gif'></td><td><img alt=\"smile\" src='smiles/smile27.gif'></td><td><img alt=\"smile\" src='smiles/smile28.gif'></td>
		   		<td><img alt=\"smile\" src='smiles/smile36.gif'></td><td><img alt=\"smile\" src='smiles/smile25.gif'></td>
					</tr>	
					<tr class='t2'>
					<td>:)</td><td>:(</td><td>;)</td><td>:D</td><td>:*</td><td>B)</td><td>:o</td><td>:$</td>
					<td>:p</td><td>:x</td><td>(k)</td><td>:-/</td><td>Zzz</td><td>O:]</td><td>(bain)</td><td>(sante)</td>
					<td>(love)</td>
					</tr>
					<tr class='t1'>
		   		<td><img alt=\"smile\" src='smiles/smile19.gif'></td><td><img alt=\"smile\" src='smiles/smile20.gif'></td><td><img alt=\"smile\" src='smiles/smile18.gif'></td>
		   		<td><img alt=\"smile\" src='smiles/smile22.gif'></td><td><img alt=\"smile\" src='smiles/smile23.gif'></td><td><img alt=\"smile\" src='smiles/smile24.gif'></td>
		   		<td><img alt=\"smile\" src='smiles/smile17.gif'></td><td><img alt=\"smile\" src='smiles/smile16.gif'></td><td><img alt=\"smile\" src='smiles/smile14.gif'></td>
		   		<td><img alt=\"smile\" src='smiles/smile15.gif'></td><td><img alt=\"smile\" src='smiles/smile29.gif'></td><td><img alt=\"smile\" src='smiles/smile30.gif'></td>
		   		<td><img alt=\"smile\" src='smiles/smile31.gif'></td><td><img alt=\"smile\" src='smiles/smile34.gif'></td><td><img alt=\"smile\" src='smiles/smile32.gif'></td>
					<td><img alt=\"smile\" src='smiles/smile26.gif'></td><td><img alt=\"smile\" src='smiles/smile21.gif'>
					</tr>
					<tr class='t2'>
					<td>:F</td><td>:X</td><td>(l)</td><td>:P</td><td>:!</td><td>:i</td>
					<td>;x</td><td>:-*</td><td>8)</td><td>:#</td><td>;(</td><td>:@</td><td>(mur)</td>
					<td>O:}</td><td>(jtm)</td><td>(merci)</td><td>(gne)</td>	
					</tr>										
					</table><br></div>";
				}
			}
			//editer une replique
			elseif(($action=='edit_rep')&&(($grade=='MODERATEUR')||($grade=='ADMIN')))
			{
				if($_POST['id_rep']==NULL)
				{
					// formulaire
					$id_replique=$_GET['id'];
					if (!($requete=mysql_fetch_row(mysql_query("SELECT * FROM `compte_rendu_repliques` WHERE `id_replique`=$id_replique")))) {
						die('Erreur : ' . mysql_error());
					}
					$id_sel=$requete[2];
					$timestamp=$requete[3];
					$message=stripslashes($requete[4]);
					$message=str_replace("<br />","",$message);
					if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`=$id_sel")))) {
						die('Erreur : ' . mysql_error());
					} 
					$prenom_sel=stripslashes($recup[0]);		
					echo "<br><div class=\"corps\"><br>
					<form method='post' action='compte_rendu.php?action=edit_rep&amp;id=$id_replique' enctype='multipart/form-data'>
					<input type='hidden' name='id_rep' value=$id_replique >
					<p class='titre'>Edition de la réponse de <a href='profil.php?id=".$id_sel."'>".$prenom_sel."(".$id_sel.")</a>.</p><br>
					<p>Réponse:<br><textarea name='replique' cols='80' rows='10'>".$message."</textarea><br>
					<input type='submit' value=' Enregistrer les modifications '></form><br>
					<a href='compte_rendu.php?action=lire'>Revoir les derniers comptes-rendus</a>&nbsp;&nbsp;&nbsp;&nbsp;
					<a href='bureau.php' title='bureau'>Retour bureau</a></p><br></div>";
				}
				else
				{
					// enregistre l'edition
					$id_rep=$_GET['id'];
					$id_rep_post=$_POST['id_rep'];
					if($id_rep==$id_rep_post)
					{
						// transformation des bbcodes et smileys
						$message=nl2br(htmlentities($_POST['replique'], ENT_QUOTES, "UTF-8"));
						if (!($requete=mysql_query("UPDATE `compte_rendu_repliques` SET `replique` = '$message' WHERE `id_replique` ='$id_rep' LIMIT 1") )) {
							die('Erreur : ' . mysql_error());
						}
						echo "<br><div class=\"corps\"><br>		
						<p class='titre'>Edition enregistrée.</p><br>
						<p><a href='compte_rendu.php?action=lire'>Revoir les derniers comptes-rendus</a>&nbsp;&nbsp;&nbsp;&nbsp;
					<a href='bureau.php' title='bureau'>Retour bureau</a></p><br></div>";
					}
				}			
			}
			//supprimer d'une replique
			elseif(($action=='sup_rep')&&(($grade=='MODERATEUR')||($grade=='ADMIN')))
			{
				$id_rep=$_GET['id'];
				if (!($requete=mysql_fetch_row(mysql_query("SELECT * FROM `compte_rendu_repliques` WHERE `id_seliste`=$id_rep")))) {
					die('Erreur : ' . mysql_error());
				}
				$id_article=$requete[1];
				// suppression de la replique
				if (!(mysql_query("DELETE FROM `compte_rendu_repliques` WHERE `id_replique`=$id_rep LIMIT 1"))) {
					die('Erreur : ' . mysql_error());
				}
				echo "<br><div class=\"corps\"><br>
				<p class='titre'>La réponse est supprimée.</p>	<br>
				<p><a href='compte_rendu.php?action=lire'>Revoir les derniers comptes-rendus</a>&nbsp;&nbsp;&nbsp;&nbsp;
					<a href='bureau.php' title='bureau'>Retour bureau</a></p><br></div>";				
			}
			//edition de l'article
			elseif(($action=='editer_article')&&(($grade=='MODERATEUR')||($grade=='ADMIN')))
			{
				if($_POST['id_article']==NULL)
				{
					// formulaire
					$id_article=$_GET['id'];
					if (!($requete=mysql_fetch_row(mysql_query("SELECT * FROM `compte_rendu` WHERE `id_com_ren`=$id_article")))) {
						die('Erreur : ' . mysql_error());
					}
					$id_ecriveur=$requete[1];
					$timestamp=$requete[2];
					$titre=$requete[3];
					$article=stripslashes($requete[4]);
					$article=str_replace("<br />","",$article);
					if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`=$id_ecriveur")))) {
						die('Erreur : ' . mysql_error());
					} 
					$prenom_ecriveur=stripslashes($recup[0]);		
					echo "<br><div class=\"corps\"><br>
					<form method='post' action='compte_rendu.php?action=editer_article&amp;id=$id_article' enctype='multipart/form-data'>
					<input type='hidden' name='id_article' value=$id_article >
					<p class='titre'>Edition du compte-rendu \"".$titre."\"</p>	
					<br><p>Compte-rendu proposé par: <a href='profil.php?id=".$id_ecriveur."'>".$prenom_ecriveur."(".$id_ecriveur.")</a></p><br>
					<p>Titre:<br><input type='text' name='titre' size='50' maxlength='50' value=\"".$titre."\" /></p>
					<p>Compte-rendu:<br><textarea name='article' cols='80' rows='30'>".$article."</textarea></p><br>
					<p><input type='submit' value=' Enregistrer les modifications '></p><br>
					<p><a href='compte_rendu.php?action=lire'>Revoir les derniers comptes-rendus</a>&nbsp;&nbsp;&nbsp;&nbsp;
					<a href='bureau.php' title='bureau'>Retour bureau</a></p><br>
					</form></div>";
				}
				else
				{
					// enregistre l'edition
					$id_article=$_GET['id'];
					$id_article_post=$_POST['id_article'];
					if($id_article==$id_article_post)
					{
						// transformation des bbcodes et smileys
						$message=nl2br(htmlentities($_POST['article'], ENT_QUOTES, "UTF-8"));
						$article=$message;
						$titre=nl2br(htmlentities($_POST['titre'], ENT_QUOTES, "UTF-8"));
						if (!($requete=mysql_query("UPDATE  `compte_rendu` SET  `compte_rendu` =  '$article', `titre` = '$titre' WHERE `id_com_ren` ='$id_article' LIMIT 1") )) {
							die('Erreur : ' . mysql_error());
						}
						echo "<br><br><div class=\"corps\"><br>
						<p class='titre'>Votre édition est enregistrée!</p><br>	
						<p><a href='compte_rendu.php?action=lire'>Revoir les derniers comptes-rendus</a>&nbsp;&nbsp;&nbsp;&nbsp;
					<a href='bureau.php' title='bureau'>Retour bureau</a></p><br>
					</div>";
					}
				}
			}					
			//rien dans l'action
			else
			{
				header ("location:404.php");
				session_destroy();
			}
	}
	else
	{ 	 //délai dépassé
		header ("location:troptard.php");
		session_destroy();
	}
 }
else
{ 	 // pas de session
	header ("location:404.php");
	session_destroy();
}
mysql_close($connexion); 
include ("fin.php");	
?>
