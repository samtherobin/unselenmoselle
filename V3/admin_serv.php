<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/

if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
{								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id     
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade`, `nbr_art` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		$nbr_art=$ligne[1];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
	 	switch ($grade)
		{
			case 'SELISTE' : header("location:404.php");break;				
			case 'MODERATEUR' :include("lien/seliste.php"); include("lien/moderateur.php");break;
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page
		//  gere les demandes de services a ajouter dans le catalogue admin et moderateurs
		echo "<br><br><div class='corps'><br>";
		if($_GET['action']=='supprimer')
		{
			$id_dem=$_GET['id'];
			if (!($requete=mysql_query("DELETE FROM `demande_services` WHERE `id_demande`='$id_dem' "))) {
				die('Erreur : ' . mysql_error());
			}
			echo "<div class='message'><br><p class='titre'>Demande supprimé.</p><br></div>";		
		}
		if (!($requete=mysql_query("SELECT * FROM `demande_services` order by `time` ASC "))) {
			die('Erreur : ' . mysql_error());
		}
		echo "<p class='titre'>Liste des demandes d'ajouts dans le catalogue:</p><br>
		<p class='t1'><a href='liste_serv.php'>Voir la liste détaillé des services</a></p><br>
		<table class='tablevu' border=\"1\" width=\"100%\" cellpadding=\"2\">
		<tr>
			<td><p class='t1'>Suprimer:</td>
			<td><p class='t1'>Message privé:</td>			
			<td><p class='t1'><p>Séliste:</p></td>
			<td><p class='t1'><p>Date:</p></td>
			<td><p class='t1'><p>Description:</p></td>
			<td><p class='t1'><p>Recherche par mot clé dans le catalogue existant: 
			<span class='pasimportant'>(Attention c'est un guide cela ne dispense pas d'une vrai recherche dans les rubriques, les mots comme \"apprendre\" ou \"proposer\" ou \"conseil\" ou \"faire\" sont exclus)</span></p></td>
		</tr>";
		while($ligne=mysql_fetch_row($requete))
		{
			$id_dem=$ligne[0];
			$id_sel=$ligne[1];
			$time_dem=$ligne[2];
			$date=date('d/m/y à H\h i\m\i\n',$time_dem);	
			$description=$ligne[3];
			// recup info seliste
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`='$id_sel'")))) {
				die('Erreur : ' . mysql_error());
			}
			$prenom_sel=stripslashes($recup[0]);
			// recherche dans les sous rubrique des mot clés
			$listemots=explode(" ",$description);
			$service='';
			$i==0;
			// boucle de mot
			foreach($listemots as $mot) 
			{
				if(($mot!='déjà')&&($mot!='ailleur')&&($mot!='faire')&&($mot!='apprendre')&&($mot!='bonjour')&&($mot!='Bonjour')&&($mot!='proposer')&&($mot!='conseil')&&($mot!='Bonjour,'))
				{
					$nbrcaractere=strlen($mot);
					if($nbrcaractere>4)
					{
						// recherche dans les sous rubrique
						$mot=addslashes($mot);
						if (!($requetemo=mysql_query("SELECT * FROM  `sous_rubrique` WHERE  `designation` LIKE  '%$mot%'"))) {
							die('Erreur : ' . mysql_error());
						}
						$test='non';
						while($lignemo=mysql_fetch_row($requetemo))
						{			
							if($test=='non') {$service=$service."<p class='t1'>\" ".$mot." \"</p>";}
							$test='OUI';
							$id_ss_rub=$lignemo[0];
							$id_rubrique=$lignemo[1];
							$designation_ss_rub=stripslashes($lignemo[2]);						
							// recherche des catalogue et rubrique
							if (!($requete3=mysql_query("SELECT `designation`, `id_cat` FROM `rubrique` WHERE `id_rubrique`='$id_rubrique'") )) {
								die('Erreur : ' . mysql_error());
							}
							$ligne3=mysql_fetch_row($requete3);
							$designation_rub=stripslashes($ligne3[0]);
							$id_cat=$ligne3[1];
							if (!($requete4=mysql_query("SELECT `designation` FROM `catalogue` WHERE `id_cat`='$id_cat'") )) {
								die('Erreur : ' . mysql_error());
							}
							$ligne4=mysql_fetch_row($requete4);
							$designation_cat=stripslashes($ligne4[0]);
							$service=$service."<p class='t2'><b>".$designation_cat."(".$id_cat.")</b>-".$designation_rub."(".$id_rubrique.")-<i>".$designation_ss_rub."(".$id_ss_rub.")</i></p>";
							$i++;
						}
					}
				}
			}
			if($i>20){ $service=$i." sous-rubriques trouvés..";}
			if($i==0){ $service="Aucun mot n'a été trouvé dans les sous-rubriques..";}
			echo "<tr>
			<td><p class='t3'><p><a href='admin_serv.php?action=supprimer&amp;id=$id_dem'><img src='images/croix.jpg'></a></p></td>			
			<td><p class='t3'><p><a class='aphoto' href='messagerie.php?action=ecrire&amp;id=$id_sel&amp;ecri=Bonjour, suite à votre demande la sous-rubrique suivante à été ajouté au catalogue:'><img src=\"images/lettreNormal.jpg\" alt=\"repondre\"></a></p></td>			
			<td><p class='t4'><p><a href='profil.php?id=$id_sel'>".$prenom_sel."(".$id_sel.")</a></p></td>
			<td><p class='t2'><p>".$date."</p></td>
			<td><p class='t2'><p>".$description."</p></td>
			<td><p class='t4'><p>".$service."</p></td>
			</tr>";
			$i=0;
		}
		echo "</table><br></div>";
		
	}
	else
	{ 	 //délai dépassé
		header ("location:troptard.php");
		session_destroy();
	}
 }
else
{ 	 // pas de session
	header ("location:404.php");
	session_destroy();
}
mysql_close($connexion); 
include ("fin.php");	
?>
