<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/

if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
{								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	if(($tpsattente<900)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
		switch ($grade)
		{
			case 'SELISTE' : include("lien/seliste.php");break;	
			case 'MODERATEUR' :include("lien/seliste.php"); include("lien/moderateur.php");break;
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page
		echo "<br><br><div class='corps'><br><br>";
		$action=$_GET['action'];
		if(($action=='SUPPRIMER')&&($grade=='ADMIN'))
		{
			$id_fichier=$_GET['id'];
			if (!($requete1=mysql_query("SELECT * FROM `serveur` WHERE `id_fichier`='$id_fichier'"))) {
				die('Erreur : ' . mysql_error());
			}
			
			$ligne=mysql_fetch_row($requete1);
			$id_fichier=$ligne[0];
			$nom_fichier=stripslashes($ligne[1]);
			unlink ("serveur/$nom_fichier");
			if (!($requete1=mysql_query("DELETE FROM `serveur` WHERE `id_fichier`='$id_fichier'"))) {
				die('Erreur : ' . mysql_error());
			}
			
			echo "<p class='t1'>Fichier \"$nom_fichier\" supprimé et éffacé et disparu et a pu là ! </p>";
		}
		elseif($action=='enregistre')
		{
			if( isset($_POST['envoi']) ) // si formulaire soumis
			{
			    $content_dir = 'serveur/'; // dossier où sera déplacé le fichier
			    $tmp_file = $_FILES['fichier']['tmp_name'];
			   	if( !is_uploaded_file($tmp_file) )
			    {
			        echo "Le fichier est introuvable";
			    }
			    // on vérifie maintenant l'extension
			    $type_file = $_FILES['fichier']['type'];
			    if( !strstr($type_file, 'jpg') && !strstr($type_file, 'jpeg')&& !strstr($type_file, 'rtf')&& !strstr($type_file, 'doc')&& !strstr($type_file, 'pdf') && !strstr($type_file, 'bmp') && !strstr($type_file, 'docx')&& !strstr($type_file, 'odt')&& !strstr($type_file, 'pps')&& !strstr($type_file, 'ppsx') && !strstr($type_file, 'gif') )
			    {
			        echo"Le fichier n'est pas une image un document ou un pdf. (Extention: jpg, doc, docx, pps, ppsx, txt, odt, bmp, doc, gif, rtf, pdf)";
			    }
			    // on copie le fichier dans le dossier de destination
			    $name_file = $_FILES['fichier']['name'];
				$smile1=array(" ","'","é","è","à","ç");
				$smile2=array("_","_","e","e","a","c"); 
				$name_file=str_replace($smile1,$smile2,$name_file);
			    if( !move_uploaded_file($tmp_file, $content_dir . $name_file) )
			    {
			        echo "Impossible de copier le fichier dans $content_dir";
			    }
			}
			$commentaire=htmlentities($_POST['comm'], ENT_QUOTES, "UTF-8");
			// enregistrement dans la base
			if (!(mysql_query("INSERT INTO `serveur` VALUES ( '' , '$name_file' , '$id_seliste' , '$time' , 'AFF' , '$commentaire')"))) {
				die('Erreur : ' . mysql_error());
			}
			echo "<p class='titre'>Fichier envoyé.</p>";
		}
			//lire les fichiers
			echo "<table summary=\"\" class='tablevu' border=\"1\" width=\"90%\">
			<tr><td colspan=5><p class='titre'>Liste des fichiers disponibles</p></td></tr>
			<tr><td><p class='t1'>Séliste:</p></td>
			<td><p class='t1'>Balise:</p></td>
			<td><p class='t1'>Fichier:</p></td>
			<td><p class='t1'>Date:</p></td>
			<td><p class='t1'>Commentaire:</p></td></tr>";
			
			if (!($requete1=mysql_query("SELECT * FROM `serveur` WHERE `etat`='AFF' ORDER BY  `id_fichier` DESC"))) {
				die('Erreur : ' . mysql_error());
			}
			
			while($ligne=mysql_fetch_row($requete1))
			{
				$id_fichier=$ligne[0];
				$nom_fichier=stripslashes($ligne[1]);
				$id_sel=$ligne[2];
				$time_fichier=$ligne[3];
				$time_fichier=date('d/m/y',$time_fichier);	
				// etat ( en ligne4)
				$commentaire=stripslashes($ligne[5]);	
				if (!($requete2=mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`='$id_sel'") )) {
					die('Erreur : ' . mysql_error());
				}
				$ligne2=mysql_fetch_row($requete2) ;
				$prenom_sel= $ligne2[0];	
				echo "<tr>
				<td><p class='t4'><a href=\"profil.php?id=".$id_sel."\">".$prenom_sel."(".$id_sel.")</a>	</p></td>
				<td><p>[fichier=".$id_fichier."]</p></td>
				<td><p>";
				if($grade=="ADMIN"){echo "<a href='serveur.php?action=SUPPRIMER&amp;id=$id_fichier'><img src='images/croix.jpg'></a>";}
				echo "<a href=\"serveur/".$nom_fichier."\">";
				$extention = explode('.',$nom_fichier);
				$type_file = array_pop($extention);
				if(($type_file=='JPG')||($type_file=='jpg')||($type_file=='gif')||($type_file=='GIF')||($type_file=='JPEG')||($type_file=='jpeg'))
			    {
			        echo"<img src='serveur/".$nom_fichier."' width=\"100\">";
			    }		
				else
				{
				 echo $nom_fichier;
				}
				echo "</a></p></td>
				<td><p>".$time_fichier."</p></td>
				<td><p>".$commentaire."</p></td></tr>";
			}
			echo "</table>
			<table summary=\"\" border=\"0\" width=\"90%\">
			<tr>
			<td><p>Ajouter un fichier:</p><p class=pasimportant>Attention les fichiers doivent être de moins de 10Mo,<br>Merci</p></td>
			<td class='t1'><form method=\"post\" action=\"serveur.php?action=enregistre\" enctype=\"multipart/form-data\"><br>
			Commentaire: <input type=\"text\" name=\"comm\" size=100><br>
			Fichier:<input type=\"file\" name=\"fichier\" size=60><input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"10485760\" /><br>
			<input type=\"submit\" name=\"envoi\" value=' Envoyer ' ></form></td></tr></table>";
		
		echo "<br></div><br><br>";
	}
	else
	{ 	 // délai dépassé
		header ("location:troptard.php");
		session_destroy();
	}
 }
else
{ 	 // pas de session
	header ("location:404.php");
	session_destroy();
}
mysql_close($connexion); 
include ("fin.php");	
?>
