<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/
 
if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
{								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
		switch ($grade)
		{
			case 'SELISTE' : include("lien/seliste.php");break;	
			case 'MODERATEUR' :include("lien/seliste.php"); include("lien/moderateur.php");break;
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page
		//debut du scan
		if (!($requete=mysql_query("SELECT `id_seliste`, `connection`, `prenom`  FROM `selistes` ORDER BY `connection` DESC LIMIT 0,5"))) {
			die('Erreur : ' . mysql_error());
		}	
		if (!($requete2=mysql_query("SELECT `id_seliste`, `connection`, `prenom`  FROM `selistes`  ORDER BY `connection` DESC LIMIT 5,5"))) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete3=mysql_query("SELECT `id_seliste`, `connection`, `prenom`  FROM `selistes`  ORDER BY `connection` DESC LIMIT 10,5"))) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete4=mysql_query("SELECT `id_seliste`, `connection`, `prenom`  FROM `selistes`  ORDER BY `connection` DESC LIMIT 15,5"))) {
			die('Erreur : ' . mysql_error());
		}		
		if (!($requete5=mysql_query("SELECT `id_seliste`, `connection`, `prenom`  FROM `selistes`  ORDER BY `connection` DESC LIMIT 20,5"))) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete6=mysql_query("SELECT `id_cat`, `designation` FROM `catalogue` ORDER BY `designation` ASC"))) {
			die('Erreur : ' . mysql_error());
		}
		echo "<br><div class=\"corps\"><br>
		<p class='titre'>Les sélistes:</p><br>
		<p class='t4'><a href=\"liste_selistes.php\">Voir la liste complète</a> <a href='carte.php'>Voir la carte</a></p><br>
		<div class='message'>
		<table summary=\"\" border='0' width='100%' cellpadding='5'>
		<tr><td colspan='5'><p>Faire un tri selon le catalogue :
		<select name=\"catalogue\" onChange=\"document.location='selistes.php?catalogue='+this.value\">
		<option value=\"\"> --- Catalogue ---</option>";
		// menu déroulant catalogue	
		while($ligne=mysql_fetch_row($requete6))
		  	{
				echo "<option value='".$ligne[0]."' ";
				if($_GET['catalogue']==$ligne[0])
				{
					echo "selected";
				}
				echo " >".$ligne[1]."</option>";
			}
		echo "</select>";
		
		// menu déroulant rubrique
		if( $_GET['catalogue']!=null)
		{
			$id_cat=$_GET['catalogue'];
			echo "<select name='rubrique' 
			onChange=\"document.location='selistes.php?type=menu&catalogue=$id_cat&amp;rubrique='+this.value\">
			<option value=\"\"> -- Rubrique --</option> ";
			if (!($requete6=mysql_query("SELECT `id_rubrique`, `designation` FROM `rubrique` where `id_cat`='$id_cat' ORDER BY `designation` ASC"))) {
				die('Erreur : ' . mysql_error());
			}
			while($ligne=mysql_fetch_row($requete6))
		  	{
				echo "<option value='".$ligne[0]."'";
				if($_GET['rubrique']==$ligne[0])
				{
					echo "selected";
				}
					echo ">".$ligne[1]."</option>";
			}
		echo "</select>";
		}
		
		echo "</p></td></tr>";
		if($_GET['type']=='')
		{
			echo "<tr><td colspan='5'><p class=\"titre\">Les 30 derniers Sélistes connectés :</p></td></tr>
			<tr>";  
			while($ligne=mysql_fetch_row($requete))
		  	{
				$id_sel= $ligne[0];
		  		$connection= $ligne[1];
				$prenom_sel=stripslashes($ligne[2]);
				//recup photo
				if (!($requete_photo=mysql_query("SELECT `nom` FROM `photos` WHERE id_seliste=$id_sel AND ordre=0"))) {
					die('Erreur : ' . mysql_error());
				}	
					$ligne_photo=mysql_fetch_row($requete_photo) ;
					$nom_photo=$ligne_photo[0];
					if($nom_photo==""){$nom_photo="membre";}
				// recup tps de connection
				$seconde_connect=$time-$connection;
				if ($seconde_connect<180) //3minutes = online 
				{
					$seconde_connect='Connecté';
				}
				elseif($seconde_connect<3600)
				{
					$seconde_connect=floor($seconde_connect/60)."Min";
				}
				elseif($seconde_connect<86400)
				{
					$seconde_connect=floor($seconde_connect/60/60)."H";
				}
				else
				{
					$seconde_connect=floor($seconde_connect/60/60/24);
					if($seconde_connect==1)
					{
						$pluriel="Jour"; 
					} 
					else 
					{
						$pluriel="Jours";
					}
					$seconde_connect=$seconde_connect." ".$pluriel;
				}	
				echo "<td class=\"t4\" width=\"20%\"><p><a href=\"profil.php?id=".$id_sel."\">".$prenom_sel."(".$id_sel.")</a></p><p class='pasimportant gris'>".$seconde_connect."<br><img  height=\"60\" src=\"photos/".$nom_photo.".jpg\" alt=\"Photo du seliste\"></p></td>";
			}
			echo "</tr><tr>";
			while($ligne=mysql_fetch_row($requete2))
			{
				$id_sel= $ligne[0];
		  		$connection= $ligne[1];
				$prenom_sel=stripslashes($ligne[2]);
				// recup tps de connection
				//recup photo
				if (!($requete_photo=mysql_query("SELECT `nom` FROM `photos` WHERE id_seliste=$id_sel AND ordre=0"))) {
					die('Erreur : ' . mysql_error());
				}
					$ligne_photo=mysql_fetch_row($requete_photo) ;
					$nom_photo=$ligne_photo[0];
					if($nom_photo==""){$nom_photo="membre";}
				$seconde_connect=$time-$connection;
				if ($seconde_connect<180) //3minutes = online 
				{
					$seconde_connect='Connecté';
				}
				elseif($seconde_connect<3600)
				{
					$seconde_connect=floor($seconde_connect/60)."Min";
				}
				elseif($seconde_connect<86400)
				{
					$seconde_connect=floor($seconde_connect/60/60)."H";
				}
				else
				{
					$seconde_connect=floor($seconde_connect/60/60/24);
					if($seconde_connect==1)
					{
						$pluriel="Jour"; 
					} 
					else 
					{
						$pluriel="Jours";
					}
					$seconde_connect=$seconde_connect." ".$pluriel;
				}	
				echo "<td class=\"t4\" width=\"20%\"><p><a href=\"profil.php?id=".$id_sel."\">".$prenom_sel."(".$id_sel.")</a></p><p class='pasimportant gris'>".$seconde_connect."<br><img  height=\"60\" src=\"photos/".$nom_photo.".jpg\" alt=\"Photo du seliste\"></p></td>";
			}
			echo "</tr><tr>";
			while($ligne=mysql_fetch_row($requete3))
		   	{
				$id_sel= $ligne[0];
				$connection= $ligne[1];
				$prenom_sel=stripslashes($ligne[2]);
				//recup photo
				if (!($requete_photo=mysql_query("SELECT `nom` FROM `photos` WHERE id_seliste=$id_sel AND ordre=0"))) {
					die('Erreur : ' . mysql_error());
				}
					$ligne_photo=mysql_fetch_row($requete_photo) ;
					$nom_photo=$ligne_photo[0];
					if($nom_photo==""){$nom_photo="membre";}
				// recup tps de connection
				$seconde_connect=$time-$connection;
				if ($seconde_connect<180) //3minutes = online 
				{
					$seconde_connect='Connecté';
				}
				elseif($seconde_connect<3600)
				{
					$seconde_connect=floor($seconde_connect/60)."Min";
				}
				elseif($seconde_connect<86400)
				{
					$seconde_connect=floor($seconde_connect/60/60)."H";
				}
				else
				{
					$seconde_connect=floor($seconde_connect/60/60/24);
					if($seconde_connect==1)
					{
						$pluriel="Jour"; 
					} 
					else 
					{
						$pluriel="Jours";
					}
					$seconde_connect=$seconde_connect." ".$pluriel;
				}	
				echo "<td class=\"t4\" width=\"20%\"><p><a href=\"profil.php?id=".$id_sel."\">".$prenom_sel."(".$id_sel.")</a></p><p class='pasimportant gris'>".$seconde_connect."<br><img  height=\"60\" src=\"photos/".$nom_photo.".jpg\" alt=\"Photo du seliste\"></p></td>";
			}
			echo "</tr><tr>";
			while($ligne=mysql_fetch_row($requete4))
		   	{
				$id_sel= $ligne[0];
				$connection= $ligne[1];
				$prenom_sel=stripslashes($ligne[2]);
				//recup photo
				if (!($requete_photo=mysql_query("SELECT `nom` FROM `photos` WHERE id_seliste=$id_sel AND ordre=0"))) {
					die('Erreur : ' . mysql_error());
				}
					$ligne_photo=mysql_fetch_row($requete_photo) ;
					$nom_photo=$ligne_photo[0];
					if($nom_photo==""){$nom_photo="membre";}
				// recup tps de connection
				$seconde_connect=$time-$connection;
				if ($seconde_connect<180) //3minutes = online 
				{
					$seconde_connect='Connecté';
				}
				elseif($seconde_connect<3600)
				{
					$seconde_connect=floor($seconde_connect/60)."Min";
				}
				elseif($seconde_connect<86400)
				{
					$seconde_connect=floor($seconde_connect/60/60)."H";
				}
				else
				{
					$seconde_connect=floor($seconde_connect/60/60/24);
					if($seconde_connect==1)
					{
						$pluriel="Jour"; 
					} 
					else 
					{
						$pluriel="Jours";
					}
					$seconde_connect=$seconde_connect." ".$pluriel;
				}	
				echo "<td class=\"t4\" width=\"20%\"><p><a href=\"profil.php?id=".$id_sel."\">".$prenom_sel."(".$id_sel.")</a></p><p class='pasimportant gris'>".$seconde_connect."<br><img  height=\"60\" src=\"photos/".$nom_photo.".jpg\" alt=\"Photo du seliste\"></p></td>";
			}
			echo "</tr><tr>";
			while($ligne=mysql_fetch_row($requete5))
		   	{
				$id_sel= $ligne[0];
				$connection= $ligne[1];
				$prenom_sel=stripslashes($ligne[2]);
				//recup photo
				if (!($requete_photo=mysql_query("SELECT `nom` FROM `photos` WHERE id_seliste=$id_sel AND ordre=0"))) {
					die('Erreur : ' . mysql_error());
				}
					$ligne_photo=mysql_fetch_row($requete_photo) ;
					$nom_photo=$ligne_photo[0];
					if($nom_photo==""){$nom_photo="membre";}
				// recup tps de connection
				$seconde_connect=$time-$connection;
				if ($seconde_connect<180) //3minutes = online 
				{
					$seconde_connect='Connecté';
				}
				elseif($seconde_connect<3600)
				{
					$seconde_connect=floor($seconde_connect/60)."Min";
				}
				elseif($seconde_connect<86400)
				{
					$seconde_connect=floor($seconde_connect/60/60)."H";
				}
				else
				{
					$seconde_connect=floor($seconde_connect/60/60/24);
					if($seconde_connect==1)
					{
						$pluriel="Jour"; 
					} 
					else 
					{
						$pluriel="Jours";
					}
					$seconde_connect=$seconde_connect." ".$pluriel;
				}	
				echo "<td class=\"t4\" width=\"20%\"><p><a href=\"profil.php?id=".$id_sel."\">".$prenom_sel."(".$id_sel.")</a></p><p class='pasimportant gris'>".$seconde_connect."<br><img  height=\"60\" src=\"photos/".$nom_photo.".jpg\" alt=\"Photo du seliste\"></p></td>";
			}
			echo "</tr></table><br>
			</div><br>";	
			
			echo "<br></div><br>";
		}
		elseif(($_GET['type']=='menu')||($_GET['rubrique']!=null))
		{
			$id_rub=$_GET['rubrique'];
			// proposé
			if (!($requete1=mysql_query("SELECT `id_seliste` FROM `services` where `etat`='PRO' AND `id_rubrique`='$id_rub' ORDER BY `time` DESC LIMIT 0,5"))) {
				die('Erreur : ' . mysql_error());
			}
			if (!($requete2=mysql_query("SELECT `id_seliste` FROM `services` where `etat`='PRO' AND `id_rubrique`='$id_rub' ORDER BY `time` DESC LIMIT 5,5"))) {
				die('Erreur : ' . mysql_error());
			}
			// demandé
			if (!($requete3=mysql_query("SELECT `id_seliste` FROM `services` where `etat`='DEM' AND `id_rubrique`='$id_rub' ORDER BY `time` DESC LIMIT 0,5"))) {
				die('Erreur : ' . mysql_error());
			}
			if (!($requete4=mysql_query("SELECT `id_seliste` FROM `services` where `etat`='DEM' AND `id_rubrique`='$id_rub' ORDER BY `time` DESC LIMIT 5,5"))) {
				die('Erreur : ' . mysql_error());
			}
			if (!($requete5=mysql_query("SELECT `designation`, `id_cat` FROM `rubrique` where `id_rubrique`='$id_rub'"))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete5) ;
			$designation=stripslashes($ligne[0]);
			$id_cat=$ligne[1];
			if (!($requete6=mysql_query("SELECT `designation` FROM `catalogue` where `id_cat`='$id_cat'"))) {
			    die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete6);
			$categorie=stripslashes($ligne[0]);
			echo "<tr><td colspan='5' class=\"t1\">Les Sélistes qui proposent des service dans la catégorie $categorie - $designation :</tr>
			<tr>";  
			while($ligne=mysql_fetch_row($requete1))
			{
				$id_sel= $ligne[0];
				if (!($requete6=mysql_query("SELECT `connection`, `prenom` FROM `selistes` where `id_seliste`='$id_sel' LIMIT 1"))) {
					die('Erreur : ' . mysql_error());
				}
				$ligne=mysql_fetch_row($requete6) ;
		   		$connection= $ligne[0];
				$prenom_sel=stripslashes($ligne[1]);
				//recup photo
				if (!($requete_photo=mysql_query("SELECT `nom` FROM `photos` WHERE id_seliste=$id_sel AND ordre=0"))) {
					die('Erreur : ' . mysql_error());
				}
					$ligne_photo=mysql_fetch_row($requete_photo) ;
					$nom_photo=$ligne_photo[0];
					if($nom_photo==""){$nom_photo="membre";}
			
				// recup tps de connection
				$seconde_connect=$time-$connection;
				if ($seconde_connect<180) //3minutes = online 
				{
					$seconde_connect='Connecté';
				}
				elseif($seconde_connect<3600)
				{
					$seconde_connect=floor($seconde_connect/60)."Min";
				}
				elseif($seconde_connect<86400)
				{
					$seconde_connect=floor($seconde_connect/60/60)."H";
				}
				else
				{
					$seconde_connect=floor($seconde_connect/60/60/24);
					if($seconde_connect==1)
					{
						$pluriel="Jour"; 
					} 
					else 
					{
						$pluriel="Jours";
					}
					$seconde_connect=$seconde_connect." ".$pluriel;
				}	
				echo "<td width='20%' class=\"t4\"><p><a href=\"profil.php?id=".$id_sel."\">".$prenom_sel."(".$id_sel.")</a></p><p class='gris'>".$seconde_connect."<br><img height=\"60\" src=\"photos/".$nom_photo.".jpg\" alt=\"Photo du seliste\"></p></td>";
			}
			echo "</tr><tr>";  
			while($ligne=mysql_fetch_row($requete2))
			{
				$id_sel= $ligne[0];
				if (!($requete6=mysql_query("SELECT `connection`, `prenom` FROM `selistes` where `id_seliste`='$id_sel' LIMIT 1"))) {
					die('Erreur : ' . mysql_error());
				}
				$ligne=mysql_fetch_row($requete6) ;
		   		$connection= $ligne[0];
				$prenom_sel=stripslashes($ligne[1]);
				//recup photo
				if (!($requete_photo=mysql_query("SELECT `nom` FROM `photos` WHERE id_seliste=$id_sel AND ordre=0"))) {
					die('Erreur : ' . mysql_error());
				}
					$ligne_photo=mysql_fetch_row($requete_photo) ;
					$nom_photo=$ligne_photo[0];
					if($nom_photo==""){$nom_photo="membre";}
			
				// recup tps de connection
				$seconde_connect=$time-$connection;
				if ($seconde_connect<180) //3minutes = online 
				{
					$seconde_connect='Connecté';
				}
				elseif($seconde_connect<3600)
				{
					$seconde_connect=floor($seconde_connect/60)."Min";
				}
				elseif($seconde_connect<86400)
				{
					$seconde_connect=floor($seconde_connect/60/60)."H";
				}
				else
				{
					$seconde_connect=floor($seconde_connect/60/60/24);
					if($seconde_connect==1)
					{
						$pluriel="Jour"; 
					} 
					else 
					{
						$pluriel="Jours";
					}
					$seconde_connect=$seconde_connect." ".$pluriel;
				}	
				echo "<td width='20%' class=\"t4\"><p><a href=\"profil.php?id=".$id_sel."\">".$prenom_sel."(".$id_sel.")</a></p><p class='gris'>".$seconde_connect."<br><img height=\"60\" src=\"photos/".$nom_photo.".jpg\" alt=\"Photo du seliste\"></p></td>";
			}
			echo "</tr>
			<tr><td colspan='6' class=\"t1\">Les Sélistes qui demandent des services dans la catégorie $categorie - $designation :</tr>
			<tr>";  
			while($ligne=mysql_fetch_row($requete3))
			{
				$id_sel= $ligne[0];
				if (!($requete6=mysql_query("SELECT `connection`, `prenom` FROM `selistes` where `id_seliste`='$id_sel' LIMIT 1"))) {
					die('Erreur : ' . mysql_error());
				}
				$ligne=mysql_fetch_row($requete6) ;
		   		$connection= $ligne[0];
				$prenom_sel=stripslashes($ligne[1]);
				//recup photo
				if (!($requete_photo=mysql_query("SELECT `nom` FROM `photos` WHERE id_seliste=$id_sel AND ordre=0"))) {
					die('Erreur : ' . mysql_error());
				}
					$ligne_photo=mysql_fetch_row($requete_photo) ;
					$nom_photo=$ligne_photo[0];
					if($nom_photo==""){$nom_photo="membre";}
			
				// recup tps de connection
				$seconde_connect=$time-$connection;
				if ($seconde_connect<180) //3minutes = online 
				{
					$seconde_connect='Connecté';
				}
				elseif($seconde_connect<3600)
				{
					$seconde_connect=floor($seconde_connect/60)."Min";
				}
				elseif($seconde_connect<86400)
				{
					$seconde_connect=floor($seconde_connect/60/60)."H";
				}
				else
				{
					$seconde_connect=floor($seconde_connect/60/60/24);
					if($seconde_connect==1)
					{
						$pluriel="Jour"; 
					} 
					else 
					{
						$pluriel="Jours";
					}
					$seconde_connect=$seconde_connect." ".$pluriel;
				}	
				echo "<td width='20%' class=\"t4\"><p><a href=\"profil.php?id=".$id_sel."\">".$prenom_sel."(".$id_sel.")</a></p><p class='gris'>".$seconde_connect."<br><img height=\"60\" src=\"photos/".$nom_photo.".jpg\" alt=\"Photo du seliste\"></p></td>";
			}
			echo "</tr><tr>";
			while($ligne=mysql_fetch_row($requete4))
			{
				$id_sel= $ligne[0];
				if (!($requete6=mysql_query("SELECT `connection`, `prenom` FROM `selistes` where `id_seliste`='$id_sel' LIMIT 1"))) {
					die('Erreur : ' . mysql_error());
				}
				$ligne=mysql_fetch_row($requete6) ;
		   		$connection= $ligne[0];
				$prenom_sel=stripslashes($ligne[1]);
				//recup photo
				if (!($requete_photo=mysql_query("SELECT `nom` FROM `photos` WHERE id_seliste=$id_sel AND ordre=0"))) {
					die('Erreur : ' . mysql_error());
				}
					$ligne_photo=mysql_fetch_row($requete_photo) ;
					$nom_photo=$ligne_photo[0];
					if($nom_photo==""){$nom_photo="membre";}
			
				// recup tps de connection
				$seconde_connect=$time-$connection;
				if ($seconde_connect<180) //3minutes = online 
				{
					$seconde_connect='Connecté';
				}
				elseif($seconde_connect<3600)
				{
					$seconde_connect=floor($seconde_connect/60)."Min";
				}
				elseif($seconde_connect<86400)
				{
					$seconde_connect=floor($seconde_connect/60/60)."H";
				}
				else
				{
					$seconde_connect=floor($seconde_connect/60/60/24);
					if($seconde_connect==1)
					{
						$pluriel="Jour"; 
					} 
					else 
					{
						$pluriel="Jours";
					}
					$seconde_connect=$seconde_connect." ".$pluriel;
				}	
				echo "<td width='20%' class=\"t4\"><p><a href=\"profil.php?id=".$id_sel."\">".$prenom_sel."(".$id_sel.")</a></p><p class='gris'>".$seconde_connect."<br><img height=\"60\" src=\"photos/".$nom_photo.".jpg\" alt=\"Photo du seliste\"></p></td>";
			}
			echo "</tr></table></div><br></div><br>";
		}		
		else
		{
			header ("location:404.php");
			session_destroy();
		}
	}
	else
	{ 	 // délai dépassé
		header ("location:troptard.php");
		session_destroy();
	}
 }
else
{ 	 // pas de session
	header ("location:404.php");
	session_destroy();
}
mysql_close($connexion);
include ("fin.php");	
?>
