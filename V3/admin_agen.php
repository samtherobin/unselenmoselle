<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/
if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
{								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade`, `nbr_art`, `prenom` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		$nbr_art=$ligne[1];
		$prenom=$ligne[2];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
		switch ($grade)
		{
			case 'SELISTE' : header("location:404.php");break;
			case 'MODERATEUR' :include("lien/seliste.php"); include("lien/moderateur.php");break;
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page
		
		if ($_GET['action']=='voir') 
		{
			if (!($requete=mysql_query("SELECT COUNT(*) FROM selistes WHERE valide='OUI' AND `mail_age`='OUI'"))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete);
			$nbrmail=$ligne[0];
			echo "<br><div class=\"corps\"><br>
			<p class='titre'>Les évènements proposées à gérer:</p><br>";
			if (!($requete=mysql_query("SELECT * FROM `agenda` WHERE `publication`='NON' ORDER BY `date_evenement` ASC "))) {
				die('Erreur : ' . mysql_error());
			}
			while($ligne=mysql_fetch_row($requete))
			{
				$id_agenda=$ligne[0];
				$rub=$ligne[1];
				if($rub=="INS") {$rub='Prochaines réunion d\'inscription';}
				elseif($rub=='AUB'){$rub='Les auberges et soirées';} 
				elseif($rub=='RAN'){$rub='Les randonnées et sorties nature';} 
				elseif($rub=='REU'){$rub='Réunion de fonctionnement';} 
				elseif($rub=='SOR'){$rub='Toutes les autres sorties';} 
				$id_ecriveur=$ligne[4];
				$timestamp=$ligne[3];
				$date_cre=date('d/m/y à H\h',$timestamp);	
				$timestamp=$ligne[5];
				$date_eve=date('d/m/y à H\h',$timestamp);	
				$titre= stripslashes($ligne[6]);
				$article= stripslashes($ligne[7]);
				$message=$article;
				include("smileyscompagnie.php");
				$article=$message;
				if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`='$id_ecriveur'")))) {
					die('Erreur : ' . mysql_error());
				}
				$prenom_ecriveur=$recup[0];		
				echo "<div class='message'><p class=titre>".$titre." <br>
				<span class='pasimportant'>Evenement écrit par <a href=\"profil.php?id=".$id_ecriveur."\">".$prenom_ecriveur."(".$id_ecriveur.")</a> le ".$date_cre."</span></p>
				<p>Prévu le : ".$date_eve."</p>
				<p>Rubrique: <b>\"".$rub."\"</b></p>
				<hr width=70%>						
				<span class='t2'>".$article."</span>
				<hr width=70%><br>
				<p>
				<a class='amodo' href=\"admin_agen.php?action=edition&amp;id=".$id_agenda."\"><img src='images/editer.gif'> Edition</a>&nbsp;&nbsp;
				<a class='amodo' href=\"admin_agen.php?action=publication&amp;mail=non&amp;id=".$id_agenda."\"><img src='images/lettre.jpg'> Publication SANS email</a>&nbsp;&nbsp;
				<a class='amodo' href=\"admin_agen.php?action=publication&amp;id=".$id_agenda."\"><img src='images/lettre.jpg'> Publication et $nbrmail emails envoyés</a>&nbsp;&nbsp;
				<a class='amodo' href=\"admin_agen.php?action=supprimer&amp;id=".$id_agenda."\"><img src='images/croix.jpg'> Supprimer</a>
				</p><br></div><br>";	
			}
			echo"<br></div></br>";
		}
		// publication d'un article
		elseif ($_GET['action']=='publication') 
		{
			$id_agenda=$_GET['id'];
			if (!($requete=mysql_fetch_row(mysql_query("SELECT `publication`, `titre`, `detail`, `date_evenement` FROM `agenda` WHERE `id_agenda`='$id_agenda'")))) {
				die('Erreur : ' . mysql_error());
			}
			$publication=$requete[0];
			$titre=$requete[1];	
			$message=$requete[2];
			$date_evenement=date('d/m/y à H\h i',$requete[3]);
			if($publication=='NON')
			{
				if (!(mysql_query("UPDATE `agenda` SET `publication`='OUI' WHERE `id_agenda`=$id_agenda LIMIT 1") )) {
					die('Erreur : ' . mysql_error());
				}
				// l'enregistré dans le log 
				if (!mysql_query("INSERT INTO `log` VALUES ('', '$id_seliste', '$id_agenda', '$time', 'agenda_PUBLIE', '$titre')")) {
					die('Requête invalide : ' . mysql_error());
				}
				// selection de celui qui à écrit l'article
				if (!($requete=mysql_fetch_row(mysql_query("SELECT `id_seliste` FROM `agenda` WHERE `id_agenda`='$id_agenda'")))) {
					die('Erreur : ' . mysql_error());
				}
				$id_ecrit=$requete[0];	
				if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste` =$id_ecrit")))) {
					die('Erreur : ' . mysql_error());
				}
				$prenom_ecrit=stripslashes ($recup[0]);
				$titre=html_entity_decode($titre,ENT_QUOTES,"UTF-8");$i=0;
				$titre=stripslashes($titre);
				$message2="[".$nom."] Evènement: ".$titre."";				
				// envoi du mail:
				$headers = "From: Evenement $nom <$email>\n";
				$headers .= "Reply-To: $nom\n";
				$headers .= "X-Sender: $nom\n";
				$headers .= "X-Author: Evenement $site\n";
				$headers .= "X-Priority:1\n";
				$headers .= "X-Mailer: PHP\n";
				$headers .= "Return_Path: <$email>\n";
				$headers .='Content-Type: text/html; charset="UTF-8"'."\n";				
				if (!($requete=mysql_query("SELECT `prenom`, `email` FROM `selistes` WHERE `valide`='OUI' AND `mail_age`='OUI'"))) {
					die('Erreur : ' . mysql_error());
				}
				$i=0;
				if($_GET['mail']==null)
				{
					
					while($ligne=mysql_fetch_row($requete))
					{
						$i++;
						$prenom_dest=stripslashes($ligne[0]);
						$email_dest=$ligne[1];
						// envoi du mail 								
						$mel ="
						<html><head><title></title></head><body>
						<p>Bonjour ".$prenom_dest.", un nouvel évenement écrit par <a href=http://".$site."/index.php?vers=profil.php?id=".$id_ecrit.">".$prenom_ecrit."(".$id_ecrit.")</a> qui aura lieu le ".$date_evenement.", vient d'être publié par <a href=http://".$site."/index.php?vers=profil.php?id=".$id_seliste.">".$prenom."(".$id_seliste.")</a>.
						<br><br><b>".$titre."</b>
						<br><hr width=90%><br>".$message."
						<br><hr width=90%>
						<br>Pour le lire sur le site <a href=http://".$site."/index.php?vers=agenda.php?action=lire> cliquez-ici </a>
						<br>Cordialement, <br> ".$nom.".
						<br><br><i>Si vous ne souhaitez plus avoir ces alertes:<br>
						<a href=http://".$site."/index.php?vers=compte.php> Cliquez ici </a> ou via la page notifications (en haut à gauche), pour désactiver ces alertes.</i></p></body></html>";
						sw_mail($email_dest,$message2,$mel,$headers);
					}
				}
				else
				{
					$i=0;
				}
			}
			echo "<br><div class=\"corps\"><br>
						<p>L'évenement a bien été publié il est en ligne, $i courriels ont été envoyés.</p><br>
						<p class='t4'><a href='bureau.php' title='bureau'>Retour bureau</a>&nbsp;&nbsp;
						<a class='amodo' href='admin_agen.php?action=voir'>Revoir les évenements à gérer</a><br>
						</p><br></div>";
		}
		// edition d'une article
		elseif ($_GET['action']=='edition') 
		{
			$id_agenda=$_GET['id'];
			if (!($ligne=mysql_fetch_row(mysql_query("SELECT * FROM `agenda` WHERE `id_agenda`=$id_agenda")))) {
				die('Erreur : ' . mysql_error());
			}
			$id_agenda=$ligne[0];
			$rubrique=$ligne[1];
			$id_ecriveur=$ligne[4];
			$timestamp=$ligne[3];
			$date_cre=date('d/m/y à H\h',$timestamp);	
			$date_evt=$ligne[5];
			$jour_evt=date('d',$date_evt);
			$mois_evt=date('m',$date_evt);
			$annee_evt=date('y',$date_evt);
			$heure_evt=date('H',$date_evt);
			$minute_evt=date('i',$date_evt);
			$titre= stripslashes($ligne[6]);
			$article= stripslashes($ligne[7]);
			$article=str_replace("<br />","",$article);	
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`=$id_ecriveur")))) {
				die('Erreur : ' . mysql_error());
			}
			$prenom_ecriveur=stripslashes($recup[0]);		
			echo "<br><div class=\"corps\"><br><script type=\"text/javascript\" src=\"ckeditor/ckeditor.js\"></script>
			<script type=\"text/javascript\" src=\"ckeditor/config.js\"></script>
			<form method='post' action='admin_agen.php?action=env_edition&amp;id=$id_agenda' enctype='multipart/form-data'>
			<input type='hidden' name='id' value='$id_agenda'>
			<p class='titre'>Edition de l'évenement \" ".$titre." \" </p>
			<br><p>Evenement proposé par: <a href='profil.php?id=".$id_ecriveur."'>".$prenom_ecriveur."(".$id_ecriveur.")</a><br>
			<br>Type d'évenement:
				<select name='rubrique'>
				<option value='$rubrique' >$rubrique</option>
				<option value='AUB' >Auberges et Soirées (AUB)</option>
				<option value='INS' >Réunion d'inscription (INS)</option>
				<option value='REU' >Réunion de ".$nom." (REU)</option>
				<option value='RAN' >Randonnées et sorties nature (RAN)</option>
				<option value='SOR' >Toutes les autres sorties (SOR)</option>
				</select>
			<br>Date évenement:<input type='text' name='jour_evt' size='2' maxlength='2' value=\"".$jour_evt."\" />/<input type='text' name='mois_evt' size='2' maxlength='2' value=\"".$mois_evt."\" />/<input type='text' name='annee_evt' size='2' maxlength='2' value=\"".$annee_evt."\" /> à <input type='text' name='heure_evt' size='2' maxlength='2' value=\"".$heure_evt."\" />h<input type='text' name='minute_evt' size='2' maxlength='2' value=\"".$minute_evt."\" />
				
				<br>Titre:<input type='text' name='titre' size='50' maxlength='50' value=\"".$titre."\" />
				<br>Evenement:<br><center><textarea name='article' class=\"ckeditor\" id=\"editeur\" cols='80' rows='30'>".$article."</textarea></center><br>
				<input type='submit' value=' Enregistrer les modifications '><br>
				<br><a href='bureau.php' title='bureau'>Retour bureau</a>&nbsp;&nbsp;
				<a class='amodo' href='admin_agen.php?action=voir'>Revoir les évenements à gérer</a></p><br></form></div>";
		}
		//envoi de l'edition d'un article
		elseif ($_GET['action']=='env_edition') 
		{
			$id_agenda=$_GET['id'];
			$id_agenda_post=$_POST['id'];
			if($id_article==$id_article_post)
			{
				// transformation des bbcodes et smileys
					$message=$_POST['article'];
					$article=$message;
					$titre=nl2br(htmlentities($_POST['titre'], ENT_QUOTES, "UTF-8"));
					$id_rub=htmlentities($_POST['rubrique'], ENT_QUOTES, "UTF-8");
					// transformation de la date en timestamp
					$jour_evt=htmlentities($_POST['jour_evt'], ENT_QUOTES, "UTF-8");
					$mois_evt=htmlentities($_POST['mois_evt'], ENT_QUOTES, "UTF-8");
					$annee_evt=htmlentities($_POST['annee_evt'], ENT_QUOTES, "UTF-8");
					$heure_evt=htmlentities($_POST['heure_evt'], ENT_QUOTES, "UTF-8");
					$minute_evt=htmlentities($_POST['minute_evt'], ENT_QUOTES, "UTF-8");
					$date_evt= mktime($heure_evt, $minute_evt,0,$mois_evt,$jour_evt,$annee_evt); 
				if (!($requete=mysql_query("UPDATE `agenda` SET `detail` = '$article', `date_evenement` = '$date_evt', `rubrique` = '$id_rub', `titre` = '$titre' WHERE `id_agenda` ='$id_agenda' LIMIT 1") )) {
					die('Erreur : ' . mysql_error());
				}
					echo "<br><div class=\"corps\"><br>
						<p class='titre'>Votre édition est enregistrée!</p><br>		
					<br><a href='bureau.php' title='bureau'>Retour bureau</a>&nbsp;&nbsp;
				<a class='amodo' href='admin_agen.php?action=voir'>Revoir les évenements à gérer</a></p><br></div>";
			}
			// post correspond pas au get
			else
			{
				header ("location:404.php");
				session_destroy();
			}
		}
		//suppresion  d'un article
		elseif (($_GET['action']=='supprimer')&&(($grade=='MODERATEUR')||($grade=='ADMIN')))
		{
			$id_agenda=$_GET['id'];
			if($_POST['justificatif']==NULL)
			{
				echo "<br><br><div class='corps'><br>
				<form method='post' action='admin_agen.php?action=supprimer&amp;id=$id_agenda' enctype='multipart/form-data'>
				<input type='hidden' name='id_agenda' value=$id_agenda >
				<p class='titre'>Justifier la suppression de l'évenement:<br>
				<textarea name='justificatif' cols='80' rows='10'></textarea><br>
				<input type='submit' value=' Supprimer '>
				</form></p><br>
				<p><a href='bureau.php' title='bureau'>Retour bureau</a>&nbsp;&nbsp;
				<a class='amodo' href='admin_agen.php?action=voir'>Revoir les évenements à gérer</a></p><br></div>";
			}
			else
			{
				$justificatif=nl2br(htmlentities($_POST['justificatif'], ENT_QUOTES, "UTF-8"));
				$id_agenda=$_POST['id_agenda'];
				if($id_agenda==$_GET['id'])
				{
					// l'enregistré dans le log 
					if (!mysql_query("INSERT INTO `log` VALUES ('', '$id_seliste', '$id_agenda', '$time', 'agenda_SUP', '$justificatif')")) {
						die('Requête invalide : ' . mysql_error());
					}
					// supprimer de la base.
					if (!(mysql_query("UPDATE `agenda` SET `publication` = 'SUP' WHERE `id_agenda` =$id_agenda LIMIT 1") )) {
						die('Erreur : ' . mysql_error());
					}
					echo "<br><div class=\"corps\"><br>
						<p class='titre'>Evenement supprimé!</p><br>		
					<br><a href='bureau.php' title='bureau'>Retour bureau</a>&nbsp;&nbsp;
				<a class='amodo' href='admin_agen.php?action=voir'>Revoir les évenements à gérer</a></p><br></div>";
				}
			}
		}					
		//rien dans l'action
		else
		{
			header ("location:404.php");
			session_destroy();
		}
	}
	else
	{ 	 //délai dépassé
		header ("location:troptard.php");
		session_destroy();
	}
 }
else
{ 	 // pas de session
	header ("location:404.php");
	session_destroy();
}
mysql_close($connexion); 
include ("fin.php");	
?>
