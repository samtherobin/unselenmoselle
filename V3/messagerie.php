<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/

if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
{								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade`, `grains`, `prenom`, `nbr_mp`, `signature`, `email` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		$grains= $ligne[1];	
		$prenom= $ligne[2];			
		$nbr_mp= $ligne[3];
		$signature=$ligne[4];
		//$email=$ligne[5];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
		switch ($grade)
		{
			case 'SELISTE' : include("lien/seliste.php");break;	
			case 'MODERATEUR' :include("lien/seliste.php"); include("lien/moderateur.php");break;
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page
		/*echo "<br/><div class=\"corps\"><br/>
		  <p>Message général</p>
		  <p>Avant d'envoyer un message général, n'hésitez pas à consulter <a href='#'>le catalogue</a> afin de faire des demandes plus ciblées. Vous trouverez peut-être directement votre bonheur !</p><br/>
		<p><a href='mess_generale.php?action=ecrire'>Envoyer un message général</a></p><br/></div>";*/
		$action=$_GET['action'];
		if($action=='lire')
		{// lire ses messages
			echo "<br/><div class=\"corps\"><br/>
			    <p>
				<a href='messagerie.php?action=lire' title='Message recu'><img src='images/lettres.jpg'><b> Tous</b></a>
				<a href='messagerie.php?action=lireprive' title='Message recu'><img src='images/lettre.jpg'> Perso uniquement</a>
				<a href='messagerie.php?action=envoyer' title='Message envoye'><img src='images/envoye.jpg'> Messages envoyés</a>";
			$npage=$_GET['npage'];
			if($npage==''){$npage=0;}
			$sqlrequete="SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF' ORDER BY `time` DESC LIMIT ".$npage.", 20";
			$limit=$npage+10;
			if (!($nbrmessrestant=mysql_num_rows(mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF' ORDER BY `time` DESC LIMIT ".$limit.", 20")))) {
				die('Erreur : ' . mysql_error());
			}
			$limit=$npage-10;
			if($limit>=0){
    			if (!($query=mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF' ORDER BY `time` DESC LIMIT ".$limit.", 20"))) {
    				die('Erreur : ' . mysql_error());
    			}
    			$nbrmessrestantavant=mysql_num_rows($query);
			}
			if (!($requete=mysql_query($sqlrequete))) {
				die('Erreur : ' . mysql_error());
			}
			$nextpage=$npage+10;
			$prevpage=$npage-10;
			if( $limit>=0) { echo "<a title='Page précédente' href=\"messagerie.php?action=lire&npage=".$prevpage."\"><img src='images/flechegauche.png' width='15px'></a>";}
			if($nbrmessrestant!=0){ echo "<a title='Page suivante' href=\"messagerie.php?action=lire&npage=".$nextpage."\"><img src='images/flechedroite.png' width='15px'></a>"; }
			echo "<br/>";
			$nb=0;
			$nb= mysql_num_rows($requete);	  
			if($nb!=0) //Si une ligne alors il a un message mini
			{	
			    echo "<table style='border-collapse:collapse'>";
			    echo "<tbody>";
				while($ligne=mysql_fetch_row($requete))
		      {
					$id_mess= $ligne[0];
					$id_expe= $ligne[2];	   
					if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`='$id_expe'")))) {
						die('Erreur : ' . mysql_error());
					} 
					$prenom_expe=stripslashes($recup[0]);		
					$time_mess= $ligne[4];
					$message=stripslashes($ligne[6]);	
					include("smileyscompagnie.php" );
					$date_mess=date('d/m/y', $time_mess);
					$date_jour=date('d/m/y');
					if ($date_jour == $date_mess) {
					   $date=date('H\:i',$time_mess);
					}
					else {
					   $date=strftime('%e %b.',$time_mess);
					}		
					echo "<tr style='background:#F1F3F4;' class='message'><td align='left' style='border-top:1px solid #e5e5e5;border-bottom:1px solid #e5e5e5;padding-right:20px''><input name=\"idmess[]\" value=\"".$id_mess."\" type=\"checkbox\">
					<a style='color:black' href=\"profil.php?id=".$id_expe."\">".$prenom_expe."(".$id_expe.")</a></td>
					<td align='left', style='white-space:nowrap;border-top:1px solid #e5e5e5;border-bottom:1px solid #e5e5e5'><p style='text-overflow:ellipsis;width:600px;overflow:hidden'><a style='color:#777' href='messagerie.php?action=repondre&amp;id_mess=".$id_mess."'>".strip_tags(str_replace("Demande urgente générale:", "", $message))."</a></p></td>
					<td align='right', style='border-top:1px solid #e5e5e5;border-bottom:1px solid #e5e5e5;padding-left:20px'>".$date."</td><td></tr>";	
				}
					echo "</tbody>";
					echo "</table>";
					echo "<br/>";
					echo "<form method=\"post\" action=\"messagerie.php?action=supprimer\" enctype=\"multipart/form-data\">";
					echo "<input type='submit' value='Effacer les messages selectionnés'><br><br>";
					echo "</form>";
			}  
			else
			{
				echo "<br><p>Vous n'avez pas de message à lire.</p><br>";
			}
			echo "</form>";		
		}
		elseif($action=='lireprive')
		{// lire ses messages sans Multi
			echo "<br><div class=\"corps\"><br>
			    <p>
				<a href='messagerie.php?action=lire' title='Message recu'><img src='images/lettres.jpg'> Tous</a>
				<a href='messagerie.php?action=lireprive' title='Message recu'><img src='images/lettre.jpg'> <b>Perso uniquement</b></a>
				<a href='messagerie.php?action=envoyer' title='Message envoye'><img src='images/envoye.jpg'> Messages envoyés</a>
				<a href='messagerie.php?action=sauvegarder' title='Message sauvegarder'><img src='images/disquette.jpg'> Messages sauvegardés</a>";
			$npage=$_GET['npage'];
			if($npage==''){$npage=0;}
			$sqlrequete="SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF' AND NOT `message` LIKE  '%[ROUGE][ITALIQUE]Demande%' ORDER BY `time` DESC LIMIT ".$npage.", 10";
			$limit=$npage+10;
			if (!($nbrmessrestant=mysql_num_rows(mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF' AND NOT `message` LIKE  '%[ROUGE][ITALIQUE]Demande%' ORDER BY `time` DESC LIMIT ".$limit.", 10")))) {
				die('Erreur : ' . mysql_error());
			}
			$limit=$npage-10;
			if($limit>=0){
    			if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF' AND NOT `message` LIKE  '%[ROUGE][ITALIQUE]Demande%' ORDER BY `time` DESC LIMIT ".$limit.", 10"))) {
    				die('Erreur : ' . mysql_error());
    			}
    			$nbrmessrestantavant = mysql_num_rows($query);
			}
			if (!($requete=mysql_query($sqlrequete))) {
				die('Erreur : ' . mysql_error());
			}
			echo "<form method=\"post\" action=\"messagerie.php?action=supprimer\" enctype=\"multipart/form-data\">";
			$nb=0;
			$nb= mysql_num_rows($requete);	  
			if($nb!=0) //Si une ligne alors il a un message mini
			{	   
				while($ligne=mysql_fetch_row($requete))
		      {
					$id_mess= $ligne[0];
					$id_expe= $ligne[2];	   
					if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`='$id_expe'")))) {
						die('Erreur : ' . mysql_error());
					} 
					$prenom_expe=stripslashes($recup[0]);					
					$time_mess= $ligne[4];
					$message=stripslashes($ligne[6]);	
					include("smileyscompagnie.php" )	;
					$date=date('d/m/y à H\h i\m\i\n',$time_mess);		
					echo "<div class='message'><p><input name=\"idmess[]\" value=\"".$id_mess."\" type=\"checkbox\">
					<b>Message de <a href=\"profil.php?id=".$id_expe."\">".$prenom_expe."(".$id_expe.")</a> le ".$date."</b>&nbsp
					<a href='messagerie.php?action=repondre&amp;id_mess=".$id_mess."'><img src=\"images/lettreNormal.jpg\" alt='repondre'> Répondre</a>&nbsp;
					<a href='messagerie.php?action=supprimer&amp;id_mess=".$id_mess."'><img src=\"images/croix.jpg\" alt='supprimer'> Supprimer</a>&nbsp;
					<a href='messagerie.php?action=sauvegarder&amp;id_mess=".$id_mess."'><img src=\"images/disquette.jpg\" alt='sauvegarder'> Sauvegarder</a>&nbsp;
					<a href='messagerie.php?action=conversation&amp;id_mess=".$id_mess."'><img src=\"images/lettres.jpg\" > Historique</a></p>
					<hr width=50%><p>".$message."</p></div><br>";	
				}
					echo "<input type='submit' value='Effacer les messages selectionnés'><br><br>";
					$nextpage=$npage+10;
					$prevpage=$npage-10;
					if( $limit>=0) { echo "<a href=\"messagerie.php?action=lireprive&npage=".$prevpage."\"><img src='images/flechegauche.png' width='15px'>Page précédente</a>";}
					if($nbrmessrestant!=0){ echo "<a href=\"messagerie.php?action=lireprive&npage=".$nextpage."\">Page suivante <img src='images/flechedroite.png' width='15px'></a>"; }
					echo "<br><br>";					
			}  
			else
			{
				echo "<br><p>Vous n'avez pas de message.</p><br>";
			}
			echo "</form> ";			
		}
		elseif($action=='repondre')
		{
				if((isset($_POST['message']))&&(isset($_GET['id_mess'])))
				{	//envoyé le message
					$message=nl2br(htmlentities($_POST['message'], ENT_QUOTES, "UTF-8")); 
					if( isset($_POST['signature'])){ $message=$message."<br /><span class=\'pasimportant\'>".$signature."</span>";}	
					$id_mess=nl2br(htmlentities($_GET['id_mess'], ENT_QUOTES, "UTF-8"));
					if (!($requete=mysql_query("SELECT `destinataire`, `expediteur` FROM `messagerie` WHERE `id_message`=$id_mess"))) {
						die('Erreur : ' . mysql_error());
					}
					$cpt_req++;
					$ligne=mysql_fetch_row($requete) ;
					//inversion
					$expe=$ligne[0];
					$dest=$ligne[1];
					if((strlen($message)>5)&&($expe==$id_seliste)) 
					{
						if (!(mysql_query("INSERT INTO `messagerie` VALUES ('', '$dest', '$id_seliste', '$id_mess', '$time', 'AFF', '$message')"))) {
							die('Erreur : ' . mysql_error());
						}
						$nbr_mp++;
						if (!(mysql_query("UPDATE `selistes` SET `nbr_mp`='$nbr_mp' WHERE `id_seliste`='$id_seliste' LIMIT 1"))) {
							die('Erreur : ' . mysql_error());
						}
						$message_mail=$message;
						// on transforme les bbcode
						include("smileysmail.php");
						if (!($requete=mysql_query("SELECT `prenom`, `mail_mp`, `email` FROM `selistes` WHERE `id_seliste`=$dest"))) {
							die('Erreur : ' . mysql_error());
						}
						$ligne=mysql_fetch_row($requete) ;
						$prenom_dest=stripslashes($ligne[0]);
						$mail_mp=$ligne[1];
						$email_dest=$ligne[2];
						$prenom=html_entity_decode($prenom);
						if($mail_mp=='OUI')
						{ // envoi du mail 
							if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom`, `email` FROM `selistes` WHERE `id_seliste`='$expe'")))) {
								die('Erreur : ' . mysql_error());
							} 
							$prenom_expe=stripslashes($recup[0]);
							$email_expe=stripslashes($recup[1]);
							$message_mail=stripslashes($message_mail);
							$titre="[".$nom."] Réponse à un message privé de $prenom_expe ($id_seliste)";
							$headers = "From: ".$nom." <".$email.">\n";
								$headers .= "Reply-To: ".$prenom_expe." <".$email_expe.">\n";
								$headers .= "X-Sender: ".$nom." <".$email.">\n";
								$headers .= "X-Author: ".$prenom_expe." <".$email_expe.">\n";
								$headers .= "X-Priority:1\n";
								$headers .= "X-Mailer: PHP\n";
								$headers .= "Return_Path: <".$email.">\n";
								$headers .='Content-Type: text/html; charset="iso-8859-1"'."\n";
							$mel ='<html><head><title></title></head><body><p>'.$message_mail.' <br>
							Message de <a href=http://'.$site.'/index.php?vers=profil.php?id='.$id_seliste.'>'.$prenom.'('.$id_seliste.').</a><br>
							<br>Cordialement, <br> '.$nom.'.
	<br><br><i>Si vous ne souhaitez plus avoir ces alertes:<br>
	<a href=http://'.$site.'/index.php?vers=compte.php> Cliquez ici </a> ou via la page compte de l\'onglet outils.</i></p></body></html>';
							sw_mail($email_dest,$titre,$mel,$headers);
						}
						echo "<br><div class=\"corps\"><br>
						<p>Votre message a bien été envoyé à <a href=\"profil.php?id=".$dest."\">".$prenom_dest."(".$dest." )</a></p><br><br>
						<a href='messagerie.php?action=lire' title='Messagerie'>Retour messagerie</a>&nbsp;&nbsp;
						<a href='bureau.php' title='Bureau'>Retour bureau</a><br><br>";
					}
					// Message trop court
					else 
					{
						echo "<br><div class=\"corps\"><br>
						<p>Votre message est trop court ! </p><br><br>
					<a href='messagerie.php?action=lire' title='Messagerie'>Retour messagerie</a>&nbsp;&nbsp;
					<a href='bureau.php' title='Bureau'>Retour bureau</a><br><br>";
					}			
				}
				else 
				{//formulaire pour repondre a un message
					$id_mess=nl2br(htmlentities($_GET['id_mess'], ENT_QUOTES, "UTF-8"));
					if (!($requete=mysql_query("SELECT `destinataire`,`expediteur`, `message` FROM `messagerie` WHERE `id_message`='$id_mess'"))) {
						die('Erreur : ' . mysql_error());
					}
					$ligne=mysql_fetch_row($requete) ;
					//inversion
					$id_expe=$ligne[0];
					$id_dest=$ligne[1];	
					$message=stripslashes($ligne[2]);	
					include("smileyscompagnie.php" )	;	
					$message_anc=$message;
					if(($id_mess!=NULL)&&($id_expe==$id_seliste))
					{	
						if (!($requete=mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`=$id_dest"))) {
							die('Erreur : ' . mysql_error());
						}
						$ligne=mysql_fetch_row($requete) ;
						$prenom_dest=$ligne[0];
						echo "<br><div class=\"corps\"><br>
						<form method='post' action='messagerie.php?action=repondre&amp;id_mess=$id_mess' enctype='multipart/form-data'>
				   	
						
						<span class='titre'>Message de </span><a href=\"profil.php?id=".$id_dest."\">".$prenom_dest."(".$id_dest.")</a>
                        <br/>
						<br/>
						<div class='important'>".$message_anc."</div>
						<br/>				
						<span class='titre'><b>Répondre :</b></span><br/>		
						<textarea  name='message' cols='60' rows='10' ></textarea><br />
			
						<input type='submit' value='Envoyer'>
						</form>
						<br/>
						<table border='1' width='90%' summary=''>
						<tr class='t1'>
						<td><img alt='smile' src='smiles/smile1.gif'></td><td><img alt='smile' src='smiles/smile2.gif'></td><td><img alt='smile' src='smiles/smile3.gif'></td>
			   		<td><img alt='smile' src='smiles/smile4.gif'></td><td><img alt='smile' src='smiles/smile5.gif'></td><td><img alt='smile' src='smiles/smile6.gif'></td>
			   		<td><img alt='smile' src='smiles/smile7.gif'></td><td><img alt='smile' src='smiles/smile8.gif'></td><td><img alt='smile' src='smiles/smile9.gif'></td>
			  			<td><img alt='smile' src='smiles/smile10.gif'></td><td><img alt='smile' src='smiles/smile11.gif'></td><td><img alt='smile' src='smiles/smile12.gif'></td>
			   		<td><img alt='smile' src='smiles/smile13.gif'></td><td><img alt='smile' src='smiles/smile27.gif'></td><td><img alt='smile' src='smiles/smile28.gif'></td>
			   		<td><img alt='smile' src='smiles/smile36.gif'></td><td><img alt='smile' src='smiles/smile25.gif'></td>
						</tr>	
						<tr class='t2'>
						<td>:)</td><td>:(</td><td>;)</td><td>:D</td><td>:*</td><td>B)</td><td>:o</td><td>:$</td>
						<td>:p</td><td>:x</td><td>(k)</td><td>:-/</td><td>Zzz</td><td>O:]</td><td>(bain)</td><td>(sante)</td>
						<td>(love)</td>
						</tr>
						<tr class='t1'>
			   		<td><img alt='smile' src='smiles/smile19.gif'></td><td><img alt='smile' src='smiles/smile20.gif'></td><td><img alt='smile' src='smiles/smile18.gif'></td>
			   		<td><img alt='smile' src='smiles/smile22.gif'></td><td><img alt='smile' src='smiles/smile23.gif'></td><td><img alt='smile' src='smiles/smile24.gif'></td>
			   		<td><img alt='smile' src='smiles/smile17.gif'></td><td><img alt='smile' src='smiles/smile16.gif'></td><td><img alt='smile' src='smiles/smile14.gif'></td>
			   		<td><img alt='smile' src='smiles/smile15.gif'></td><td><img alt='smile' src='smiles/smile29.gif'></td><td><img alt='smile' src='smiles/smile30.gif'></td>
			   		<td><img alt='smile' src='smiles/smile31.gif'></td><td><img alt='smile' src='smiles/smile34.gif'></td><td><img alt='smile' src='smiles/smile32.gif'></td>
						<td><img alt='smile' src='smiles/smile26.gif'></td><td><img alt='smile' src='smiles/smile21.gif'>
						</tr>
						<tr class='t2'>
						<td>:F</td><td>:X</td><td>(l)</td><td>:P</td><td>:!</td><td>:i</td>
						<td>;x</td><td>:-*</td><td>8)</td><td>:#</td><td>;(</td><td>:@</td><td>(mur)</td>
						<td>O:}</td><td>(jtm)</td><td>(merci)</td><td>(gne)</td>	
						</tr>	
						</table>
						<table border='1' width='90%' summary=''>
						<tr class='t3'>
			   		<td class='pasimportant'><a href=\"profil.php?id=".$id."\">[id=".$id."][/id]</a></td>
						<td class='pasimportant'><b>[GRAS][/GRAS]</b></td>
						<td class='pasimportant'><i>[ITALIQUE][/ITALIQUE]</i></td>
						<td class='pasimportant'><span class='noir'>[NOIR][/COULEUR]</span></td>
						<td class='pasimportant'><span class='gris'>[GRIS][/COULEUR]</span></td></tr><tr class='t3'>
						<td class='pasimportant'><span class='rouge'>[ROUGE][/COULEUR]</span></td>
						<td class='pasimportant'><span class='vert'>[VERT][/COULEUR]</span></td>
						<td class='pasimportant'><span class='jaune'>[JAUNE][/COULEUR]</span></td>
						<td class='pasimportant'><span class='blanc'>[BLANC][/COULEUR]</span></td>
						<td class='pasimportant'><span class='bleu'>[BLEU][/COULEUR]</span></td>
						</tr>
						</table>";					
					}
				else
				{
					echo "<p>Occupez vous de vos messages!</p><br><p>
					<a href='messagerie.php?action=lire' title='Messagerie'>Retour messagerie</a>&nbsp;&nbsp;
						<a href='bureau.php' title='Bureau'>Retour bureau</a><br><br>";
				}
			}
		}
		elseif($action=='ecrire')
		{
			if((isset($_POST['message']))&&(isset($_POST['id_dest']))&&(isset($_GET['dest'])))
			{// ecriture d'un message ( envoi)
				$id_dest=$_POST['id_dest'];
				$id_dest_get=$_GET['dest'];
				$message=nl2br(htmlentities($_POST['message'], ENT_QUOTES, "UTF-8"));
				if( isset($_POST['signature'])){ $message=$message."<br /><span class=\'pasimportant\'>".$signature."</span>";}	
				if((strlen($message)>5)&&($id_dest==$id_dest_get))
				{//envoi le message
					if (!(mysql_query("INSERT INTO `messagerie` VALUES ('', '$id_dest', '$id_seliste', '0', '$time', 'AFF', '$message')"))) {
						die('Erreur : ' . mysql_error());
					}
					$nbr_mp++;
					if (!(mysql_query("UPDATE `selistes` SET `nbr_mp`='$nbr_mp' WHERE `id_seliste`='$id_seliste' LIMIT 1"))) {
						die('Erreur : ' . mysql_error());
					}
					$message_mail=$message;
					// on transforme les bbcode
					include("smileysmail.php");
					if (!($requete=mysql_query("SELECT `prenom`, `mail_mp`, `email` FROM `selistes` WHERE `id_seliste`=$id_dest"))) {
						die('Erreur : ' . mysql_error());
					}	
					$ligne=mysql_fetch_row($requete) ;
					$prenom_dest=stripslashes($ligne[0]);
					$mail_mp=$ligne[1];
					$email_dest=$ligne[2];
					$prenom=html_entity_decode($prenom);
					if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom`, `email` FROM `selistes` WHERE `id_seliste`='$id_seliste'")))) {
						die('Erreur : ' . mysql_error());
					} 
					$prenom_expe=stripslashes($recup[0]);
					$email_expe=stripslashes($recup[1]);
						if($mail_mp=='OUI')
						{ // envoi du mail 
							$message_mail=stripslashes($message_mail);
							$titre="[".$nom."] Nouveau message privé de $prenom ($id_seliste)";
							$headers = "From: ".$nom." <".$email.">\n";
							$headers .= "Reply-To: ".$prenom_expe." <".$email_expe.">\n";
							$headers .= "X-Sender: ".$email."\n";
							$headers .= "X-Author: ".$prenom_expe." <".$email_expe.">\n";
							$headers .= "X-Priority:1\n";
							$headers .= "X-Mailer: PHP\n";
							$headers .= "Return_Path: <".$email.">\n";
							$headers .='Content-Type: text/html; charset="UTF-8"'."\n";
						$mel ='<html><head><title></title></head><body><p>'.$message_mail.' <br> Message de <a href=http://'.$site.'/index.php?vers=profil.php?id='.$id_seliste.'> '.$prenom.'('.$id_seliste.')</a><br><br><i>Si vous ne souhaitez plus avoir ces alertes:<br>
	<a href=http://'.$site.'/index.php?vers=compte.php> Cliquez ici </a> ou via la page compte de l\'onglet outils.</i></p></body></html>';
						sw_mail($email_dest,$titre,$mel,$headers);
						}
					echo "<br><div class=\"corps\"><br>
						<p>Votre message a bien été envoyé à <a href=\"profil.php?id=".$id_dest."\">".$prenom_dest."(".$id_dest." )</a></p><br><br>
						<a href='messagerie.php?action=lire' title='Messagerie'>Retour messagerie</a>&nbsp;&nbsp;
						<a href='bureau.php' title='Bureau'>Retour bureau</a><br><br>";
				}
				else
				{// post != get  ou message trop court
					echo "<br><div class=\"corps\"><br>
						<p>Votre message est trop court ! </p><br><br>
					<a href='messagerie.php?action=lire' title='Messagerie'>Retour messagerie</a>&nbsp;&nbsp;
					<a href='bureau.php' title='Bureau'>Retour bureau</a><br><br>";
				}
			}
			else
			{//écriture d'un message (formulaire)
				$id_dest=$_GET['id'];				
				if($id_dest!=NULL)
				{	
					if (!($requete=mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`=$id_dest"))) {
						die('Erreur : ' . mysql_error());
					}
					$ligne=mysql_fetch_row($requete) ;
					$prenom_dest=stripslashes($ligne[0]);
					// si message pré écrit
					if($_GET['ecri']!=null)
					{
						$ecri=nl2br(htmlentities($_GET['ecri'], ENT_QUOTES, "UTF-8"));
					}
					else
					{
					 $ecri='';
					}
					echo "<br><div class=\"corps\"><br>
					<form method='post' action='messagerie.php?action=ecrire&amp;dest=$id_dest' enctype='multipart/form-data'>
			   	<input type='hidden' name='id_dest' value=$id_dest >
					<table summary=\"\" border='0' width='90%'>
					<tr>
					<th>Message pour <a href=\"profil.php?id=".$id_dest."\">".$prenom_dest."(".$id_dest.")</a></th>
					</tr>
					<tr>
					<td class='t1'><center><textarea name='message' cols='80' rows='10'>".$ecri."</textarea><br />Ajouter ma signature :<input type=\"checkbox\" name=\"signature\" value=\"1\" ";if($signature!=null){ echo "checked=\"checked\"";} echo "/>
					<br><input type='submit' value='Envoyer au facteur'></center></td>
					</tr>
					</table>
					</form>
					<table border='1' width='90%' summary=''>
					<tr class='t1'>
					<td><img alt='smile' src='smiles/smile1.gif'></td><td><img alt='smile' src='smiles/smile2.gif'></td><td><img alt='smile' src='smiles/smile3.gif'></td>
		   		<td><img alt='smile' src='smiles/smile4.gif'></td><td><img alt='smile' src='smiles/smile5.gif'></td><td><img alt='smile' src='smiles/smile6.gif'></td>
		   		<td><img alt='smile' src='smiles/smile7.gif'></td><td><img alt='smile' src='smiles/smile8.gif'></td><td><img alt='smile' src='smiles/smile9.gif'></td>
		  			<td><img alt='smile' src='smiles/smile10.gif'></td><td><img alt='smile' src='smiles/smile11.gif'></td><td><img alt='smile' src='smiles/smile12.gif'></td>
		   		<td><img alt='smile' src='smiles/smile13.gif'></td><td><img alt='smile' src='smiles/smile27.gif'></td><td><img alt='smile' src='smiles/smile28.gif'></td>
		   		<td><img alt='smile' src='smiles/smile36.gif'></td><td><img alt='smile' src='smiles/smile25.gif'></td>
					</tr>	
					<tr class='t2'>
					<td>:)</td><td>:(</td><td>;)</td><td>:D</td><td>:*</td><td>B)</td><td>:o</td><td>:$</td>
					<td>:p</td><td>:x</td><td>(k)</td><td>:-/</td><td>Zzz</td><td>O:]</td><td>(bain)</td><td>(sante)</td>
					<td>(love)</td>
					</tr>
					<tr class='t1'>
		   		<td><img alt='smile' src='smiles/smile19.gif'></td><td><img alt='smile' src='smiles/smile20.gif'></td><td><img alt='smile' src='smiles/smile18.gif'></td>
		   		<td><img alt='smile' src='smiles/smile22.gif'></td><td><img alt='smile' src='smiles/smile23.gif'></td><td><img alt='smile' src='smiles/smile24.gif'></td>
		   		<td><img alt='smile' src='smiles/smile17.gif'></td><td><img alt='smile' src='smiles/smile16.gif'></td><td><img alt='smile' src='smiles/smile14.gif'></td>
		   		<td><img alt='smile' src='smiles/smile15.gif'></td><td><img alt='smile' src='smiles/smile29.gif'></td><td><img alt='smile' src='smiles/smile30.gif'></td>
		   		<td><img alt='smile' src='smiles/smile31.gif'></td><td><img alt='smile' src='smiles/smile34.gif'></td><td><img alt='smile' src='smiles/smile32.gif'></td>
					<td><img alt='smile' src='smiles/smile26.gif'></td><td><img alt='smile' src='smiles/smile21.gif'>
					</tr>
					<tr class='t2'>
					<td>:F</td><td>:X</td><td>(l)</td><td>:P</td><td>:!</td><td>:i</td>
					<td>;x</td><td>:-*</td><td>8)</td><td>:#</td><td>;(</td><td>:@</td><td>(mur)</td>
					<td>O:}</td><td>(jtm)</td><td>(merci)</td><td>(gne)</td>	
					</tr>	
					</table>
					<table border='1' width='90%' summary=''>
					<tr class='t3'>
		   		<td class='pasimportant'><a href=\"profil.php?id=".$id."\">[id=".$id."][/id]</a></td>
					<td class='pasimportant'><b>[GRAS][/GRAS]</b></td>
					<td class='pasimportant'><i>[ITALIQUE][/ITALIQUE]</i></td>
					<td class='pasimportant'><span class='noir'>[NOIR][/COULEUR]</span></td>
					<td class='pasimportant'><span class='gris'>[GRIS][/COULEUR]</span'></td></tr><tr class='t3'>
					<td class='pasimportant'><span class='rouge'>[ROUGE][/COULEUR]</span></td>
					<td class='pasimportant'><span class='vert'>[VERT][/COULEUR]</span></td>
					<td class='pasimportant'><span class='jaune'>[JAUNE][/COULEUR]</span></td>
					<td class='pasimportant'><span class='blanc'>[BLANC][/COULEUR]</span></td>
					<td class='pasimportant'><span class='bleu'>[BLEU][/COULEUR]</span></td>
					</tr>					
					</table>		   ";
					//<td>(bienvenue)</td><td><img alt='smile' src='smiles/smile33.gif'><td>(anniv)</td><td><img alt='smile' src='smiles/smile35.gif'>
				}
				else
				{
					header ("location:404.php");
					session_destroy();
				}
			}
		}
		elseif($action=='supprimer')
		{//supprimer un ou des  messages 
			if($_GET['id_mess']!=null)
			{ // un message dans l'url		
				$id_mess=$_GET['id_mess'];
				if (!($requete=mysql_query("SELECT `destinataire` FROM `messagerie` WHERE `id_message`='$id_mess'"))) {
					die('Erreur : ' . mysql_error());
				}
				$cpt_req++;
				$ligne=mysql_fetch_row($requete) ;
				$id_dest=$ligne[0];
				if($id_seliste==$id_dest)
				{
					if (!(mysql_query("UPDATE `messagerie` SET `rubrique`='EFF' WHERE `id_message`='$id_mess' LIMIT 1"))) {
						die('Erreur : ' . mysql_error());
					}
					echo "<br><div class=\"corps\"><br>
					<p class='titre'>Message effacé.</p><br>";
				}
				else
				{
					echo "<br><div class=\"corps\"><br>
					<p class='titre'>Occupé vous de vos messages !</p><br><p>
					<a href='messagerie.php?action=lire' title='Messagerie'>Retour messagerie</a>&nbsp;&nbsp;
						<a href='bureau.php' title='Bureau'>Retour bureau</a><br><br>";
				}				
			}
			elseif($_GET['id_mess']==null)
			{
				foreach ($_POST['idmess'] as $value)
				{// coché plusieur messages
					$id_mess=$value;
					if (!($requete=mysql_query("SELECT `destinataire` FROM `messagerie` WHERE `id_message`='$id_mess'"))) {
						die('Erreur : ' . mysql_error());
					}
					$ligne=mysql_fetch_row($requete) ;
					$id_dest=$ligne[0];
					if($id_seliste==$id_dest)
					{
						if (!(mysql_query("UPDATE `messagerie` SET `rubrique`='EFF' WHERE `id_message`='$id_mess' LIMIT 1"))) {
							die('Erreur : ' . mysql_error());
						}
						$nb++;
					}
					else
					{
						echo "<br><div class=\"corps\"><br>
						<p class='titre'>Occupé vous de vos messages !</p><br>";
					}
				}
				echo "<br><div class=\"corps\"><br><p class='titre'>".$nb." messages effacés.</p><br>";
			}
			// lire les messages :
			echo "<br><p>
				<a href='messagerie.php?action=lire' title='Message recu'><img src='images/lettres.jpg'><b> Tous</b></a>
				<a href='messagerie.php?action=lireprive' title='Message recu'><img src='images/lettre.jpg'> Perso uniquement</a>
				<a href='messagerie.php?action=envoyer' title='Message envoye'><img src='images/envoye.jpg'> Messages envoyés</a>
				<a href='messagerie.php?action=sauvegarder' title='Message sauvegarder'><img src='images/disquette.jpg'> Messages sauvegardés</a></p><br>";
			$npage=$_GET['npage'];
			if($npage==''){$npage=0;}
			$sqlrequete="SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF' ORDER BY `time` DESC LIMIT ".$npage.", 10";
			$limit=$npage+10;
			if (!($nbrmessrestant=mysql_num_rows(mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF' ORDER BY `time` DESC LIMIT ".$limit.", 10")))) {
				die('Erreur : ' . mysql_error());
			}
			$limit=$npage-10;
			if($limit>=0) {
    			if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF' ORDER BY `time` DESC LIMIT ".$limit.", 10"))) {
    				die('Erreur : ' . mysql_error());
    			}
    			$nbrmessrestantavant = mysql_num_rows($query);
			}
			if (!($requete=mysql_query($sqlrequete))) {
				die('Erreur : ' . mysql_error());
			}
			echo "<form method=\"post\" action=\"messagerie.php?action=supprimer\" enctype=\"multipart/form-data\">
			<p class='titre' align='center'>Tout les messages que j'ai reçus:</p><br> ";  
			$nb=0;
			$nb= mysql_num_rows($requete);	  
			if($nb!=0) //Si une ligne alors il a un message mini
			{	   
				while($ligne=mysql_fetch_row($requete))
		      {
					$id_mess= $ligne[0];
					$id_expe= $ligne[2];	   
					if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`='$id_expe'")))) {
						die('Erreur : ' . mysql_error());
					} 
					$prenom_expe=stripslashes($recup[0]);		
					$time_mess= $ligne[4];
					$message=stripslashes($ligne[6]);	
					include("smileyscompagnie.php" )	;
					$date=date('d/m/y à H\h i\m\i\n',$time_mess);		
					echo "<div class='message'><p><input name=\"idmess[]\" value=\"".$id_mess."\" type=\"checkbox\">
					<b>Message de <a href=\"profil.php?id=".$id_expe."\">".$prenom_expe."(".$id_expe.")</a> le ".$date."</b>&nbsp
					<a href='messagerie.php?action=repondre&amp;id_mess=".$id_mess."'><img src=\"images/lettreNormal.jpg\" alt='repondre'> Répondre</a>&nbsp;
					<a href='messagerie.php?action=supprimer&amp;id_mess=".$id_mess."'><img src=\"images/croix.jpg\" alt='supprimer'> Supprimer</a>&nbsp;
					<a href='messagerie.php?action=sauvegarder&amp;id_mess=".$id_mess."'><img src=\"images/disquette.jpg\" alt='sauvegarder'> Sauvegarder</a>&nbsp;
					<a href='messagerie.php?action=conversation&amp;id_mess=".$id_mess."'><img src=\"images/lettres.jpg\" > Historique</a></p>
					<hr width=50%><p>".$message."</p></div><br>";	
				}
					echo "<input type='submit' value='Effacer les messages selectionnés'><br><br>";
					$nextpage=$npage+10;
					$prevpage=$npage-10;
					if( $limit>=0) { echo "<a href=\"messagerie.php?action=lire&npage=".$prevpage."\"><img src='images/flechegauche.png' width='15px'>Page précédente</a>";}
					if($nbrmessrestant!=0){ echo "<a href=\"messagerie.php?action=lire&npage=".$nextpage."\">Page suivante <img src='images/flechedroite.png' width='15px'></a>"; }
					echo "<br><br>";					
			}  
			else
			{
				echo "<br><p>Vous n'avez pas de message à lire.</p><br>";
			}
			echo "</form>";					
		}
		elseif($action=='sauvegarder')
		{
			if(isset($_GET['id_mess']))
			{//sauvegarder un message
				$id_mess=$_GET['id_mess'];
				if (!($requete=mysql_query("SELECT `destinataire` FROM `messagerie` WHERE `id_message`='$id_mess'"))) {
					die('Erreur : ' . mysql_error());
				}
				$cpt_req++;
				$ligne=mysql_fetch_row($requete) ;
				$id_dest=$ligne[0];
				if($id_seliste==$id_dest)
				{
					if (!(mysql_query("UPDATE `messagerie` SET `rubrique`='SAU' WHERE `id_message`='$id_mess' LIMIT 1"))) {
						die('Erreur : ' . mysql_error());
					}
					echo "<br><div class=\"corps\"><br>
					<p class='titre'>Message sauvegardé.</p><br><br><p>
					<a href='messagerie.php?action=lire' title='Messagerie'>Retour messagerie</a>&nbsp;&nbsp;
						<a href='bureau.php' title='Bureau'>Retour bureau</a><br><br>";
				}
				else
				{
					echo "<br><div class=\"corps\"><br>
					<p class='titre'>Occupez vous de vos messages.</p><br><br><p>
					<a href='messagerie.php?action=lire' title='Messagerie'>Retour messagerie</a>&nbsp;&nbsp;
						<a href='bureau.php' title='Bureau'>Retour bureau</a><br><br>";
				}
			}
			else
			{// lire les messages sauvegardés
				if (!($requete=mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='SAU' ORDER BY `id_message` DESC"))) {
					die('Erreur : ' . mysql_error());
				}
				echo "<br><div class=\"corps\"><br><p>
				<a href='messagerie.php?action=lire' title='Message recu'><img src='images/lettres.jpg'> Tous</a>
				<a href='messagerie.php?action=lireprive' title='Message recu'><img src='images/lettre.jpg'> Perso uniquement</a>
				<a href='messagerie.php?action=envoyer' title='Message envoye'><img src='images/envoye.jpg'> Messages envoyés</a>
				<a href='messagerie.php?action=sauvegarder' title='Message sauvegarder'><img src='images/disquette.jpg'><b> Messages sauvegardés</b></a></p><br>";
				$nb=0;
				$nb= mysql_num_rows($requete);	  
				if($nb!=0) //Si une ligne alors il a un message mini
				{	   
					while($ligne=mysql_fetch_row($requete))
					{
						$id_mess= $ligne[0];
						$id_expe= $ligne[2];	   
						if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`='$id_expe'")))) {
							die('Erreur : ' . mysql_error());
						} 
						$prenom_expe=stripslashes($recup[0]);		
						$time_mess= $ligne[4];
						$message=stripslashes($ligne[6]);	  
						include("smileyscompagnie.php" )	;
						$date=date('d/m/y à H\h i\m\i\n',$time_mess);	
						echo "<div class='message'><p>
						<b>Message de <a href=\"profil.php?id=".$id_expe."\">".$prenom_expe."(".$id_expe.")</a> le ".$date."</b>&nbsp
						<a href='messagerie.php?action=repondre&amp;id_mess=".$id_mess."'><img src=\"images/lettreNormal.jpg\" alt='repondre'> Répondre</a>&nbsp;
						<a href='messagerie.php?action=supprimer&amp;id_mess=".$id_mess."'><img src=\"images/croix.jpg\" alt='supprimer'> Supprimer</a>&nbsp;
						<a href='messagerie.php?action=conversation&amp;id_mess=".$id_mess."'><img src=\"images/lettres.jpg\" > Historique</a></p>
						<hr width=50%><p>".$message."</p></div><br>";	  
					}	  
				}  
				else
				{
					echo "<p>Vous n'avez pas de message sauvegardé.</p>";
				}
				
			}
		}
		elseif($action=='envoyer')
		{//affichage des messages envoyés
			if (!($requete=mysql_query("SELECT * FROM `messagerie` WHERE `expediteur`='$id_seliste' AND `rubrique`='AFF' ORDER BY `id_message` DESC"))) {
				die('Erreur : ' . mysql_error());
			}
			echo "<br><div class=\"corps\"><br><p>
				<a href='messagerie.php?action=lire' title='Message recu'><img src='images/lettres.jpg'> Tous</a>
				<a href='messagerie.php?action=lireprive' title='Message recu'><img src='images/lettre.jpg'> Perso uniquement</a>
				<a href='messagerie.php?action=envoyer' title='Message envoye'><img src='images/envoye.jpg'> <b>Messages envoyés</b></a>
				<a href='messagerie.php?action=sauvegarder' title='Message sauvegarder'><img src='images/disquette.jpg'> Messages sauvegardés</a></p><br>
			<p class='pasimportant'>Un message déjà effacé par son destinataire n'apparaît plus, vous ne pouvez pas effacer un message envoyé.</p><br>";  
			$nb=0;
			$nb= mysql_num_rows($requete);	  
			if($nb!=0) //Si une ligne alors il a un message mini
			{	   
				while($ligne=mysql_fetch_row($requete))
				{
					$id_mess= $ligne[0];
					$id_dest= $ligne[1];	   
					if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`='$id_dest'")))) {
						die('Erreur : ' . mysql_error());
					}
					$cpt_req++;
					$prenom_dest=stripslashes($recup[0]);		
					$time_mess= $ligne[4];
					$message=stripslashes($ligne[6]);	 
					include("smileyscompagnie.php" )	;
					$date=date('d/m/y à H\h i\m\i\n',$time_mess);		
					echo "<div class='message'><p>
					<b>Message envoyé à <a href=\"profil.php?id=".$id_dest."\">".$prenom_dest."(".$id_dest.")</a> le ".$date."</b>&nbsp
					<hr width=50%><p>".$message."</p></div><br>";	  
				}	  
			}  
			else
			{
				echo "<br><p>Vous n'avez pas envoyé de message ou il est déjà supprimé.</p><br>";
			}	
		}
		elseif(($action=='conversation')&&(isset($_GET['id_mess'])))
		{//affichage des messages envoyés
			$id_mess=$_GET['id_mess'];
			echo "<br><div class=\"corps\"><br><p>
				<a href='messagerie.php?action=lire' title='Message recu'><img src='images/lettres.jpg'> Tous</a>
				<a href='messagerie.php?action=lireprive' title='Message recu'><img src='images/lettre.jpg'> Perso uniquement</a>
				<a href='messagerie.php?action=envoyer' title='Message envoye'><img src='images/envoye.jpg'> Messages envoyés</a>
				<a href='messagerie.php?action=sauvegarder' title='Message sauvegarder'><img src='images/disquette.jpg'> Messages sauvegardés</a></p><br>
				<p>Attention les messages éffacés ou qui ne sont pas passés par la fonction \"répondre\" n'apparaissent pas.</p><br>"; 
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `destinataire`, `expediteur`, `id_ancien` FROM `messagerie` WHERE `id_message`='$id_mess' LIMIT 1")))) {
				die('Erreur : ' . mysql_error());
			}
			$id_dest=$recup[0];
			$id_expe=$recup[1];
			$id_ancien=$recup[2];
			if($id_ancien=='0')
			{// nouvel convers
				echo "<p>C'était le 1er message de la conversation :)</p>";
			}
			elseif(($id_dest==$id_seliste)||($id_expe=$id_seliste))
			{//affichage de la convers
				if($id_ancien!=0)
				{
					while($id_ancien!=NULL)
					{
						if($id_ancien!=0)
						{
							if (!($recup=mysql_fetch_row(mysql_query("SELECT * FROM `messagerie` WHERE `id_message`='$id_ancien' LIMIT 1")))) {
								die('Erreur : ' . mysql_error());
							}
							$cpt_req++;
							$id_expe=$recup[1];
							$id_dest=$recup[2];
							$id_ancien=$recup[3];
							$time_mess=$recup[4];
							$message=stripslashes($recup[6]);
							include("smileyscompagnie.php" )	;
							if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`='$id_dest'")))) {
								die('Erreur : ' . mysql_error());
							}
							$cpt_req++;
							$prenom_dest=stripslashes($recup[0]);		
							if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`='$id_expe'")))) {
								die('Erreur : ' . mysql_error());
							}
							$cpt_req++;
							$prenom_expe=stripslashes($recup[0]);		
							$date=date('d/m/y à H\h i\m\i\n',$time_mess);
							if($id_ancien!='')
							{
								echo "<div class='message'><p>
								<b>Message envoyé à <a href=\"profil.php?id=".$id_dest."\">".$prenom_dest."(".$id_dest.")</a>
								vers <a href=\"profil.php?id=".$id_expe."\">".$prenom_expe."(".$id_expe.")</a> le ".$date."</b>&nbsp
								<hr width=50%><p>".$message."</p></div><br>";
							}
						}
						else
						{
							$id_ancien='';
							echo "<br><p>Fin..</p><br>";
						}
					}
				}
				else
				{
					echo "<br><p>Fin..</p><br>";
					$id_ancien='';
				}	
			}
			else
			{//trafiquage
				echo "<br><p>Euh.. C'est celà oui !</p><br>";
				session_destroy();
			}
		}
		else
		{ //rien dans le get 'action'
			header ("location:404.php");
			session_destroy();
		}
		//fin du bureau
		echo "</div>";
		mysql_close($connexion); 
		include ("fin.php");
	}
	else
	{ 	// trop tard
		header ("location:404.php");
		session_destroy();
	}
}
else
{ 	// voleur
	header ("location:404.php");
	session_destroy();
}
?>
