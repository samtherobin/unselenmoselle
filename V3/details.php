<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/
 
if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
{								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
		switch ($grade)
		{
			case 'SELISTE' : include("lien/seliste.php");break;	
			case 'MODERATEUR' :include("lien/seliste.php"); include("lien/moderateur.php");break;
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page
		echo "<br><div class=\"corps\"><br>";
		//debut de l'edition
		if(($_GET['action']=='editer')&&($_GET['id_service']!=null))
			{ //edition detail service
				$id_service=htmlentities($_GET['id_service'], ENT_QUOTES, "UTF-8");	
				if (!($requete2=mysql_query("SELECT `detail`, `description`, `id_rubrique`  FROM `services` WHERE `id_service` ='$id_service'"))) {
					die('Erreur : ' . mysql_error());
				}
				$ligne2=mysql_fetch_row($requete2);
				$detail=stripslashes($ligne2[0]);
				$description=stripslashes($ligne2[1]);
				$id_rubrique = $ligne2[2];
				if (!($requete3=mysql_query("SELECT `id_cat` FROM `rubrique` WHERE `id_rubrique` ='$id_rubrique'"))) {
					die('Erreur : ' . mysql_error());
				}
				$ligne3=mysql_fetch_row($requete3);
				$id_cat = $ligne3[0];
				if (!($rubriques = mysql_query("SELECT `id_rubrique`, `designation` FROM `rubrique` WHERE `id_cat`='$id_cat'"))) {
					die('Erreur : ' . mysql_error());
				}
				if (!($catalogues = mysql_query("SELECT `id_cat`, `designation` FROM `catalogue`"))) {
					die('Erreur : ' . mysql_error());
				}
				echo "
				<p class='titre'>Modification d'un service</p><br/>
				<form method=\"post\" action=\"details.php?action=enregistre&amp;id_service=$id_service\" enctype=\"multipart/form-data\">";
				echo "
				<p class='important'>Catégorie : 
				<select><option value='0'>-----</option>";
				while ($ligne_catalogue = mysql_fetch_row($catalogues)) {
				    echo "<option";
				    if ($id_cat == $ligne_catalogue[0]) {
				        echo " selected";
				    }
				    echo " value='$ligne_catalogue[0]'>$ligne_catalogue[1]</option>";
				}
				echo "</select>";
				echo "
				&nbsp;&nbsp;&nbsp;&nbsp;Rubrique : 
				<select name='id_rubrique'><option value='0'>-----</option>";
				while ($ligne_rubrique = mysql_fetch_row($rubriques)) {
				    echo "<option";
				    if ($id_rubrique == $ligne_rubrique[0]) {
				        echo " selected";
				    }
				    echo " value='$ligne_rubrique[0]'>$ligne_rubrique[1]</option>";
				}
				echo "</select></p><br/>";
				echo "
				<p class='important'>Description du service : <input style='width:50%;' name='description' type='text' value='$description'/><br/><br/>
				<p class='important'>Détails :</p>
				<p class='important'><textarea name='detail' cols=70 rows=10 >".$detail."</textarea>
				<br><input type='submit' value=' Sauvegarder ' ></p>			
				</form>
				<p align='left'><a href='bbcode.php'>BBCODE Activé.</a></p><br>";
			}
			elseif($_GET['action']=='enregistre')
			{ //ok pour enregistre
				$detail=htmlentities($_POST['detail'], ENT_QUOTES, "UTF-8");
				$description=htmlentities($_POST['description'], ENT_QUOTES, "UTF-8");
				$id_rubrique=htmlentities($_POST['id_rubrique'], ENT_QUOTES, "UTF-8");
				$id_service=htmlentities($_GET['id_service'], ENT_QUOTES, "UTF-8");	
				if (!mysql_query("UPDATE `services` SET `detail` = '$detail', `description` = '$description', `id_rubrique` = '$id_rubrique' WHERE `id_service` ='$id_service'")) {
				    die('Erreur : ' . mysql_error());
				}
				echo "
				<p class='titre'>Le détail du service est mis à jour: <a href='profil.php?id=".$id_seliste."'>Voir le profil</a></p><br>	
				<br><a href='services.php?action=edition' title='SERVICES'>Retour services</a></p><br>";
			}
			else
			{ 	//pirate
				header ("location:404.php");
				session_destroy();			
			}
		//fin edition
		echo "<br></div><br>";
	} 
	//delai depassé
	else
	{
		header ("location:troptard.php");
		session_destroy();
	}
}
// pas de sesion
else
{
	header ("location:404.php");
	session_destroy();
}
mysql_close($connexion); 
include ("fin.php");	
?>
