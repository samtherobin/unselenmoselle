<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/

if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
{								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
		switch ($grade)
		{
			case 'SELISTE' : include("lien/seliste.php");break;	
			case 'MODERATEUR' :include("lien/seliste.php"); include("lien/moderateur.php");break;
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page
		echo "<br><div class=\"corps\"><br>";
		echo "<table class='tablevu' border='1' width='90%' cellpadding='5'>
			<tr>
			<td colspan='5'><p><b>Les services:</b><br><span class='pasimportant'> (Référence  C=Catalogue R=rubrique SR=Sous-rubrique, suivi de son numéro unique)</span></p></td>
			</tr>
			<tr><td><p class=t1><b>Catalogue:</b></p></td><td><p class=t1><b>Rubrique:</b></p></td><td><p class=t1><b>Sous rubrique:</b></p></td></tr>";
			if (!($requete1=mysql_query("SELECT * FROM `catalogue` order by `designation` asc") )) {
				die('Erreur : ' . mysql_error());
			}
			while($ligne1=mysql_fetch_row($requete1))
			{
				$id_cat= $ligne1[0];
				$designation=stripslashes($ligne1[1]);
				if (!($requete2=mysql_query("SELECT * FROM `rubrique` where `id_cat`='$id_cat' order by `designation` asc") )) {
					die('Erreur : ' . mysql_error());
				}
				$nbr_rub=mysql_num_rows($requete2);
				echo "<tr><td rowspan='$nbr_rub'><p class='t1'><b>".$designation."</b> <span class='pasimportant'>(C".$id_cat.")</span></p></td>";
				$nb=0;
				while($ligne2=mysql_fetch_row($requete2))
				{
					$id_rub= $ligne2[0];
					$designation_rub=stripslashes($ligne2[2]);
					$nb++;
					if ($nb==1){echo "<td><p class='t4'>".$designation_rub." <span class='pasimportant'>(R".$id_rub.")</span></p></td>";}
					else{echo "<tr><td><p class='t4'>".$designation_rub." <span class='pasimportant'>(R".$id_rub.")</span></p></td>";}
					// sous rubrique
					if (!($requete3=mysql_query("SELECT * FROM `sous_rubrique` where `id_rubrique`='$id_rub' order by `designation` asc") )) {
						die('Erreur : ' . mysql_error());
					}
					echo "<td><table width='100%'>";
					while($ligne3=mysql_fetch_row($requete3))
					{
						$id_ss=$ligne3[0];
						$designation_ss=stripslashes($ligne3[2]);
						$detail_ss=stripslashes($ligne3[3]);
						$nb++;
						echo "<tr><td><p class='t2'><i>".$designation_ss." </i><span class='pasimportant'>(SR".$id_ss.")</span><br></td></tr>";
					}
					echo "</table></td></tr>";
				}
			}
			echo "</table><br></div>";
	} 
	//delai depassé
	else
	{
		header ("location:troptard.php");
		session_destroy();
	}
}
// pas de sesion
else
{
	header ("location:404.php");
	session_destroy();
}
mysql_close($connexion); 
include ("fin.php");	
?>
