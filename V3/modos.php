<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/
 
if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
{								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
		switch ($grade)
		{
			case 'SELISTE' : include("lien/seliste.php");break;	
			case 'MODERATEUR' :include("lien/seliste.php"); include("lien/moderateur.php");break;
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page
		echo "<br><div class=\"corps\"><br>
		<p class='titre'>Conseil d'animation d':</p><br>
		<p class='t4'><a href='selistes.php'>Voir tous les selistes</a></p><br>";

		//chercher les modos
			if (!($requete=mysql_query("SELECT `texte` FROM  `variables` WHERE  `variable`='webmaster' LIMIT 1"))) {
			    die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete);
			$webmasters_id=explode(',', stripslashes($ligne[0]));
			$webmasters = array();
			foreach ($webmasters_id as $webmaster_id) {
			     if (!($requete=mysql_query("SELECT `prenom` FROM  `selistes` WHERE  `id_seliste`='$webmaster_id' LIMIT 1"))) {
			         die('Erreur : ' . mysql_error());
			     }
			     $ligne=mysql_fetch_row($requete);
			     array_push($webmasters, array($webmaster_id, stripslashes($ligne[0])));
			}
			if (!($requete=mysql_query("SELECT `texte` FROM  `variables` WHERE  `variable`='MOD2' LIMIT 1"))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete);
			$MOD2=stripslashes($ligne[0]);
			if (!($requete=mysql_query("SELECT `prenom` FROM  `selistes` WHERE  `id_seliste`='$MOD2' LIMIT 1"))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete);
			$PMOD2=stripslashes($ligne[0]);
			if (!($requete=mysql_query("SELECT `texte` FROM  `variables` WHERE  `variable`='MOD3' LIMIT 1"))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete);
			$MOD3=stripslashes($ligne[0]);
			if (!($requete=mysql_query("SELECT `prenom` FROM  `selistes` WHERE  `id_seliste`='$MOD3' LIMIT 1"))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete);
			$PMOD3=stripslashes($ligne[0]);
			if (!($requete=mysql_query("SELECT `texte` FROM  `variables` WHERE  `variable`='MOD4' LIMIT 1"))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete);
			$MOD4=stripslashes($ligne[0]);
			if (!($requete=mysql_query("SELECT `prenom` FROM  `selistes` WHERE  `id_seliste`='$MOD4' LIMIT 1"))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete);
			$PMOD4=stripslashes($ligne[0]);
			if (!($requete=mysql_query("SELECT `texte` FROM  `variables` WHERE  `variable`='MOD5' LIMIT 1"))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete);
			$MOD5=stripslashes($ligne[0]);
			if (!($requete=mysql_query("SELECT `prenom` FROM  `selistes` WHERE  `id_seliste`='$MOD5' LIMIT 1"))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete);
			$PMOD5=stripslashes($ligne[0]);
			if (!($requete=mysql_query("SELECT `texte` FROM  `variables` WHERE  `variable`='MOD6' LIMIT 1"))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete);
			$MOD6=stripslashes($ligne[0]);
			if (!($requete=mysql_query("SELECT `prenom` FROM  `selistes` WHERE  `id_seliste`='$MOD6' LIMIT 1"))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete);
			$PMOD6=stripslashes($ligne[0]);
			if (!($requete=mysql_query("SELECT `texte` FROM  `variables` WHERE  `variable`='TMOD1' LIMIT 1"))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete);
			$TMOD1=stripslashes($ligne[0]);
			if (!($requete=mysql_query("SELECT `texte` FROM  `variables` WHERE  `variable`='TMOD2' LIMIT 1"))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete);
			$TMOD2=stripslashes($ligne[0]);
			if (!($requete=mysql_query("SELECT `texte` FROM  `variables` WHERE  `variable`='TMOD3' LIMIT 1"))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete);
			$TMOD3=stripslashes($ligne[0]);
			if (!($requete=mysql_query("SELECT `texte` FROM  `variables` WHERE  `variable`='TMOD4' LIMIT 1"))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete);
			$TMOD4=stripslashes($ligne[0]);
			if (!($requete=mysql_query("SELECT `texte` FROM  `variables` WHERE  `variable`='TMOD5' LIMIT 1"))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete);
			$TMOD5=stripslashes($ligne[0]);
			if (!($requete=mysql_query("SELECT `texte` FROM  `variables` WHERE  `variable`='TMOD6' LIMIT 1"))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete);
			$TMOD6=stripslashes($ligne[0]);

			if (!($requete=mysql_query("SELECT `id_seliste`, `connection`, `prenom`  FROM `selistes` WHERE `grade`='MODERATEUR' ORDER BY `connection` DESC LIMIT 0,5"))) {
				die('Erreur : ' . mysql_error());
			}
			if (!($requete2=mysql_query("SELECT `id_seliste`, `connection`, `prenom`  FROM `selistes` WHERE `grade`='MODERATEUR' ORDER BY `connection` DESC LIMIT 5,5"))) {
				die('Erreur : ' . mysql_error());
			}
			echo "<div class='message'>
			<p class='titre'>Les membres du conseil d'animation :<p class='pasimportant'>Des questions ? Besoin d'aide ? Des doutes ? Ils sont là pour vous servir !<br></p>
			<table summary=\"\" border='0' width='100%' cellpadding='3'>
			<tr><td colspan=3 width='60%' class='t2'><p class='t2 pasimportant'>- Une question ? Consultez la FAQ la réponse y est peut-être...</p></td>
			<td colspan=2 width=40%><p><a href='help.php'>Foire aux questions</a></p></td></tr>
			<tr><td colspan=5><hr width=70%><td></tr>
			<tr><td colspan=3 class='t2'><p class='t2 pasimportant'>- URGENT ? Y a le feu ? Un grave problème ? Un pervers t'ennuie ?</p></td><td colspan=2><p><a href='mess_generale.php?action=ecrire'>Demande urgente aux modérateurs</a></p></td></tr>
			<tr><td colspan=5><hr width=70%><td></tr>
			        
			<tr><td colspan=3 class='t2'><p class='t2 pasimportant'>- "."Administrateur(s) du site"."</p></td><td colspan=2><p>";
			foreach ($webmasters as $webmaster) {
			 echo "<a href='messagerie.php?action=ecrire&id=".$webmaster[0]."&ecri=Explique&nbsp;Moi&nbsp;ton&nbsp;souci&nbsp;:)'>".$webmaster[1]."(".$webmaster[0].")</a>";
			}
			echo "</p></td></tr>
			<tr><td colspan=5><hr width=70%><td></tr>
			<tr><td colspan=3 class='t2'><p class='t2 pasimportant'>- ".$TMOD2."</p></td><td colspan=2><p><a href='messagerie.php?action=ecrire&id=".$MOD2."&ecri=Explique&nbsp;Moi&nbsp;ton&nbsp;souci&nbsp;:)'>Vois avec ".$PMOD2."(".$MOD2.")</a></p></td></tr>
			<tr><td colspan=5><hr width=70%><td></tr>
			<tr><td colspan=3 class='t2'><p class='t2 pasimportant'>- ".$TMOD3."</p></td><td colspan=2><p><a href='messagerie.php?action=ecrire&id=".$MOD3."&ecri=Explique&nbsp;Moi&nbsp;ton&nbsp;souci&nbsp;:)'> Vois avec ".$PMOD3."(".$MOD3.")</a></p></td></tr>
			<tr><td colspan=5><hr width=70%><td></tr>
			<tr><td colspan=3 class='t2'><p class='t2 pasimportant'>- ".$TMOD4."</p></td><td colspan=2><p><a href='messagerie.php?action=ecrire&id=".$MOD4."&ecri=Explique&nbsp;Moi&nbsp;ton&nbsp;souci&nbsp;:)'> Vois avec ".$PMOD4."(".$MOD4.")</a></p></td></tr>
			<tr><td colspan=5><hr width=70%><td></tr>
			<tr><td colspan=3 class='t2'><p class='t2 pasimportant'>- ".$TMOD5."</p></td><td colspan=2><p><a href='messagerie.php?action=ecrire&id=".$MOD5."&ecri=Explique&nbsp;Moi&nbsp;ton&nbsp;souci&nbsp;:)'> Vois avec ".$PMOD5."(".$MOD5.")</a></p></td></tr>
			<tr><td colspan=5><hr width=70%><td></tr>
			<tr><td colspan=3 class='t2'><p class='t2 pasimportant'>- ".$TMOD6."</p></td><td colspan=2></p><a href='messagerie.php?action=ecrire&id=".$MOD6."&ecri=Explique&nbsp;Moi&nbsp;ton&nbsp;souci&nbsp;:)'>Vois avec ".$PMOD6."(".$MOD6.")</a></p></td></tr><tr>
			<tr><td colspan=5><hr width=80%><td></tr></table>
			<table summary=\"\" border='0' width='100%' cellpadding='3'><tr>"; 
			while($ligne=mysql_fetch_row($requete))
			{
				$id_sel= $ligne[0];
		    	$connection= $ligne[1];
				$prenom_sel=stripslashes($ligne[2]);
				//recup photo
				if (!($requete_photo=mysql_query("SELECT `nom` FROM `photos` WHERE id_seliste=$id_sel AND ordre=0"))) {
					die('Erreur : ' . mysql_error());
				}
					$ligne_photo=mysql_fetch_row($requete_photo) ;
					$nom_photo=$ligne_photo[0];
					if($nom_photo==""){$nom_photo="membre";}
				// recup tps de connection
				$seconde_connect=$time-$connection;
				if ($seconde_connect<180) //3minutes = online 
				{
					$seconde_connect='Connecté';
				}
				elseif($seconde_connect<3600)
				{
					$seconde_connect=floor($seconde_connect/60)."Min";
				}
				elseif($seconde_connect<86400)
				{
					$seconde_connect=floor($seconde_connect/60/60)."H";
				}
				else
				{
					$seconde_connect=floor($seconde_connect/60/60/24);
					if($seconde_connect==1)
					{
						$pluriel="Jour"; 
					} 
					else 
					{
						$pluriel="Jours";
					}
					$seconde_connect=$seconde_connect." ".$pluriel;
				}		
				echo "<td width='20%' class=\"t4\"><p><a href=\"profil.php?id=".$id_sel."\">".$prenom_sel."(".$id_sel.")</a></p><p class='gris'>".$seconde_connect."<br><img height=\"60\" src=\"photos/".$nom_photo.".jpg\" alt=\"Photo du seliste\"></p></td>";
			}
			echo "</tr><tr>";
			// 2 eme ligne admin
			while($ligne=mysql_fetch_row($requete2))
			{
				$id_sel= $ligne[0];
		    	$connection= $ligne[1];
				$prenom_sel=stripslashes($ligne[2]);
				//recup photo
				if (!($requete_photo=mysql_query("SELECT `nom` FROM `photos` WHERE id_seliste=$id_sel AND ordre=0"))) {
					die('Erreur : ' . mysql_error());
				}
					$ligne_photo=mysql_fetch_row($requete_photo) ;
					$nom_photo=$ligne_photo[0];
					if($nom_photo==""){$nom_photo="membre";}
				// recup tps de connection
				$seconde_connect=$time-$connection;
				if ($seconde_connect<180) //3minutes = online 
				{
					$seconde_connect='Connecté';
				}
				elseif($seconde_connect<3600)
				{
					$seconde_connect=floor($seconde_connect/60)."Min";
				}
				elseif($seconde_connect<86400)
				{
					$seconde_connect=floor($seconde_connect/60/60)."H";
				}
				else
				{
					$seconde_connect=floor($seconde_connect/60/60/24);
					if($seconde_connect==1)
					{
						$pluriel="Jour"; 
					} 
					else 
					{
						$pluriel="Jours";
					}
					$seconde_connect=$seconde_connect." ".$pluriel;
				}		
				echo "<td width='20%' class=\"t4\"><p><a href=\"profil.php?id=".$id_sel."\">".$prenom_sel."(".$id_sel.")</a></p><p class='gris'>".$seconde_connect."<br><img height=\"60\" src=\"photos/".$nom_photo.".jpg\" alt=\"Photo du seliste\"></p></td>";
			}
			echo "</tr></table></div><br>";
			
			// les admins :
			if (!($requete=mysql_query("SELECT `id_seliste`, `connection`, `prenom`  FROM `selistes` WHERE `grade`='ADMIN' ORDER BY `connection` DESC LIMIT 0,5"))) {
				die('Erreur : ' . mysql_error());
			}
			$nb=mysql_num_rows($requete);
			echo "<div class='message'>
			
			<p class='titre'>Les administrateurs du site:</p><br>
			<table summary=\"\" border='0' width='100%' cellpadding='3'>
			<tr>"; 
			$nb=100/$nb;	
			$nb=round($nb, 0); 
			while($ligne=mysql_fetch_row($requete))
			{
				$id_sel= $ligne[0];
		    	$connection= $ligne[1];
				$prenom_sel=stripslashes($ligne[2]);
				//recup photo
				if (!($requete_photo=mysql_query("SELECT `nom` FROM `photos` WHERE id_seliste=$id_sel AND ordre=0"))) {
					die('Erreur : ' . mysql_error());
				}
					$ligne_photo=mysql_fetch_row($requete_photo) ;
					$nom_photo=$ligne_photo[0];
					if($nom_photo==""){$nom_photo="membre";}
				// recup tps de connection
				$seconde_connect=$time-$connection;
				if ($seconde_connect<180) //3minutes = online 
				{
					$seconde_connect='Connecté';
				}
				elseif($seconde_connect<3600)
				{
					$seconde_connect=floor($seconde_connect/60)."Min";
				}
				elseif($seconde_connect<86400)
				{
					$seconde_connect=floor($seconde_connect/60/60)."H";
				}
				else
				{
					$seconde_connect=floor($seconde_connect/60/60/24);
					if($seconde_connect==1)
					{
						$pluriel="Jour"; 
					} 
					else 
					{
						$pluriel="Jours";
					}
					$seconde_connect=$seconde_connect." ".$pluriel;
				}		
				echo "<td width='20%' class=\"t4\"><p><a href=\"profil.php?id=".$id_sel."\">".$prenom_sel."(".$id_sel.")</a></p><p class='gris'>".$seconde_connect."<br><img height=\"60\" src=\"photos/".$nom_photo.".jpg\" alt=\"Photo du seliste\"></p></td>";
			}
			echo "</tr><tr>";
			// 2 eme ligne admin
			while($ligne=mysql_fetch_row($requete2))
			{
				$id_sel= $ligne[0];
		    	$connection= $ligne[1];
				$prenom_sel=stripslashes($ligne[2]);
				//recup photo
				if (!($requete_photo=mysql_query("SELECT `nom` FROM `photos` WHERE id_seliste=$id_sel AND ordre=0"))) {
					die('Erreur : ' . mysql_error());
				}
					$ligne_photo=mysql_fetch_row($requete_photo) ;
					$nom_photo=$ligne_photo[0];
					if($nom_photo==""){$nom_photo="membre";}
				// recup tps de connection
				$seconde_connect=$time-$connection;
				if ($seconde_connect<180) //3minutes = online 
				{
					$seconde_connect='Connecté';
				}
				elseif($seconde_connect<3600)
				{
					$seconde_connect=floor($seconde_connect/60)."Min";
				}
				elseif($seconde_connect<86400)
				{
					$seconde_connect=floor($seconde_connect/60/60)."H";
				}
				else
				{
					$seconde_connect=floor($seconde_connect/60/60/24);
					if($seconde_connect==1)
					{
						$pluriel="Jour"; 
					} 
					else 
					{
						$pluriel="Jours";
					}
					$seconde_connect=$seconde_connect." ".$pluriel;
				}		
				echo "<td width='20%' class=\"t4\"><p><a href=\"profil.php?id=".$id_sel."\">".$prenom_sel."(".$id_sel.")</a></p><p class='gris'>".$seconde_connect."<br><img height=\"60\" src=\"photos/".$nom_photo.".jpg\" alt=\"Photo du seliste\"></p></td>";
			}
			echo "</tr></table></div><br></div><br>";
		
	}
	else
	{ 	 // délai dépassé
		header ("location:troptard.php");
		session_destroy();
	}
 }
else
{ 	 // pas de session
	header ("location:404.php");
	session_destroy();
}
mysql_close($connexion);
include ("fin.php");	
?>
