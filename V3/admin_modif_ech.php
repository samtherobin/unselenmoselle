<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/

if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
{								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade`, `nbr_art` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		$nbr_art=$ligne[1];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
	 	switch ($grade)
		{
			case 'SELISTE' : header("location:404.php");break;	
			case 'MODERATEUR' :include("lien/seliste.php"); include("lien/moderateur.php");break;
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;				
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page
				
		if ($_GET['action']=='valider')
		{ // enregistrement des modif
			
			$id_sel_fou=htmlentities($_POST['id_sel_fou'], ENT_QUOTES, "UTF-8");
			$id_sel_dem=htmlentities($_POST['id_sel_dem'], ENT_QUOTES, "UTF-8");
			$id_ss_rub=htmlentities($_POST['id_ss_rub'], ENT_QUOTES, "UTF-8");
			$grains=htmlentities($_POST['grains'], ENT_QUOTES, "UTF-8");
			$etat=htmlentities($_POST['etat'], ENT_QUOTES, "UTF-8");
			$id_ech=htmlentities($_POST['id_ech'], ENT_QUOTES, "UTF-8");
			if (!($requete=mysql_query("UPDATE `echanges` SET `id_seliste_fou` = '$id_sel_fou', `id_seliste_dem` = '$id_sel_dem', `id_ss_rub` = '$id_ss_rub',`grains` = '$grains', `etat` = '$etat' WHERE `id_echange` ='$id_ech' LIMIT 1"))) {
				die('Erreur : ' . mysql_error());
			}
			
			echo "<br><br><div class='corps'><br>
			<p class='titre'>Echange mis à jour.</p>	
			<br><br>
			<p class='t1'><a href='bureau.php'>Retour bureau</a>&nbsp;&nbsp;
			<a href='admin_comp.php' class='amodo'>Retour validaton des échanges</a></p>
			<br></div>";			
		}
		elseif($_GET['id']!=null)
		{ // formulaire de modification d'échange
			$id_ech=htmlentities($_GET['id'], ENT_QUOTES, "UTF-8");
			if (!($requete=mysql_query("SELECT *  FROM `echanges` WHERE `id_echange`='$id_ech'"))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete) ;
			$id_sel_fou=$ligne[1];
			if (!($requete2=mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`='$id_sel_fou'"))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne2=mysql_fetch_row($requete2);
			$prenom_fou=stripslashes($ligne2[0]);
			$id_sel_dem=$ligne[2];
			if (!($requete3=mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`='$id_sel_dem'"))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne3=mysql_fetch_row($requete3) ;
			$prenom_dem=stripslashes($ligne3[0]);			
			$time_ech=$ligne[3];
			$date=date('d/m/y à H\h i\m\i\n',$time_ech);	
			$grains=$ligne[4];
			$id_ss_rub=$ligne[5];
			$etat=$ligne[6];
			$detail=stripslashes($ligne[7]);
			$date_ech=$ligne[8];
			$date_ech=date('d/m/y à H\h i\m\i\n',$date_ech);
			echo "<br><br><div class='corps'><br>
			<p class='titre'>Edition d'un échange.</p>
			<form method=\"post\" action=\"admin_modif_ech.php?action=valider\" enctype=\"multipart/form-data\">
			<table summary=\"\" border='0' width='90%' cellpadding='3'>
			<tr>
			<td><p class='t3'><b>Echange N°:</b></p></td>
			<td><p class='t2'><input readonly name='id_ech' value='".$id_ech."' size='6'></p></td>
			</tr>
			<tr>
			<td><p class='t3'><b>Fournit le service:</b></p></td>
			<td><p class='t2'><input name='id_sel_fou' value='".$id_sel_fou."' size=5 maxLength=5> ".$prenom_fou."</p></td>
			</tr>
			<td><p class='t3'><b>Demande le service:</b></p></td>
			<td><p class='t2'><input name='id_sel_dem' value='".$id_sel_dem."' size=5 maxLength=5> ".$prenom_dem."</p></td>
			</tr>
			<tr>
			<td><p class='t3'><b>Date:</b></p></td>
			<td><p class='t2'>".$date."</p></td>
			</tr>
			<tr>
			<td><p class='t3'><b>$monnaie:</b></p></td>
			<td><p class='t2'><input name='grains' value='".$grains."' size=4 maxLength=4></p></td>
			</tr>
			<tr>
			<td><p class='t3'><b>Id_sous_rubrique:</b></p></td>
			<td><p class='t2'><input name='id_ss_rub' value='".$id_ss_rub."' size=5 maxLength=5></p></td>
			</tr>
			<tr>
			<td><p class='t3'><b>Détail:</b></p></td>
			<td><p class='t2'>$detail</p></td>
			</tr>
			<tr>
			<td><p class='t3'><b>Etat:</b></p></td>
			<td><p class='t2'><select name='etat'>
			<option value='".$etat."' >".$etat."</option>
			<option value='DEM' >Demande en cours (DEM)</option>
			<option value='VAL' >Validé par le 2eme séliste (VAL)</option>
			<option value='REF' >Refusé (REF)</option>
			</select></p></td>		
			</tr>
			<tr>
			<td><p class='t3'><b>Date_ech:</b></p></td>
			<td><p class='t2'>".$date_ech."</p></td>
			</tr>
			<tr>
			<td colspan='3'><p class='t1'><input type='submit' value=' Sauvegarder ' ></p></td>
			</tr>
			</table>			
			</form><br></div>";			
		}
		else
		{ // na rien a faire sur cette page
			header ("location:troptard.php");
			session_destroy();
		}
	}
	else
	{ 	 //délai dépassé
		header ("location:troptard.php");
		session_destroy();
	}
 }
else
{ 	 // pas de session
	header ("location:404.php");
	session_destroy();
}
mysql_close($connexion); 
include ("fin.php");	
?>
