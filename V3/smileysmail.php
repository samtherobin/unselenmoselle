<!--   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->

<?php  

$balise1=array("[GRAS]","[/GRAS]","[ITALIQUE]","[/ITALIQUE]","[NOIR]","[BLANC]","[JAUNE]","[VERT]","[ROUGE]","[GRIS]","[BLEU]","[/COULEUR]");
$balise2=array("<b>","</b>","<i>","</i>","<span class=\"noir\">","<span class=\"blanc\">","<span class=\"jaune\">","<span class=\"vert\">","<span class=\"rouge\">","<span class=\"gris\">","<span class=\"bleu\">","</span>");
$message_mail=str_replace($balise1,$balise2,$message_mail);

// scan pour les profils
$message_mail=ereg_replace("\\[id=([^\[]*)]([^]]*)?\[/id\]", "<a href='".$site."/V3/profil.php?id=\\1'>\\2</a>", $message_mail);

// scan pour les photos
$message_mail=ereg_replace("\\[image=([^\[]*)]", "<a href='".$site."/V3/photos/\\1.jpg'><img alt='image' width='150' src='".$site."/V3/photos/\\1.jpg'></a>", $message_mail);

// scan pour serveur
$message_mail=ereg_replace("\\[fichier=([^\[]*)]", "<a target='_blank' href='".$site."/fichier.php?id=\\1'>Cliquez-ici</a>", $message_mail);
 


?>


