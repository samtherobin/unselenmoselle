<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/ 
if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
{								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade`, `email`, `prenom` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		$email_exp= $ligne[1];
		$prenom_exp= $ligne[2];	
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
		switch ($grade)
		{
			case 'SELISTE' : include("lien/seliste.php");break;	
			case 'MODERATEUR' :include("lien/seliste.php"); include("lien/moderateur.php");break;
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page
		echo "<br><div class=\"corps\"><br>";
		if($_GET['sousrubrique']==null)
		{
			// liste catalogue
			echo "
			<p class='titre'>Envoi d'un multi-messages aux sélistes en fonction du catalogue:</p><br><p>
			<select name=\"catalogue\" onChange=\"document.location='mailcata.php?catalogue='+this.value\">
			<option value=\"\"> --- Catalogue ---</option>";
			// menu déroulant catalogue	
			if (!($requete5=mysql_query("SELECT `id_cat`, `designation` FROM `catalogue` ORDER BY `designation` ASC"))) {
				die('Erreur : ' . mysql_error());
			}
			$cpt_req++;
			while($ligne=mysql_fetch_row($requete5))
				{
					echo "<option value='".$ligne[0]."' ";
					if($_GET['catalogue']==$ligne[0])
					{
						echo "selected";
					}
					echo " >".$ligne[1]."</option>";
				}
			echo "</select>";
			
			// menu déroulant rubrique
			if( $_GET['catalogue']!=null)
			{
				$id_cat=$_GET['catalogue'];
				echo "<select name='rubrique' 
				onChange=\"document.location='mailcata.php?catalogue=$id_cat&amp;rubrique='+this.value\">
				<option value=\"\"> -- Rubrique --</option> ";
				if (!($requete6=mysql_query("SELECT `id_rubrique`, `designation` FROM `rubrique` where `id_cat`='$id_cat' ORDER BY `designation` ASC"))) {
					die('Erreur : ' . mysql_error());
				}
				$cpt_req++;
				while($ligne=mysql_fetch_row($requete6))
				{
					echo "<option value='".$ligne[0]."'";
					if($_GET['rubrique']==$ligne[0])
					{
						echo "selected";
					}
						echo ">".$ligne[1]."</option>";
				}
			echo "</select>";
			}
			
			// menu déroulant sous  rubrique
			if( $_GET['rubrique']!=null)
			{
				$id_rub=$_GET['rubrique'];
				if (!($requete7=mysql_query("SELECT `id_cat` FROM `rubrique` where `id_rubrique`='$id_rub'"))) {
					die('Erreur : ' . mysql_error());
				}
				$cpt_req++;
				$ligne=mysql_fetch_row($requete7) ;
				$id_cat= $ligne[0];
				echo "<select name='ssrubrique' 
				onChange=\"document.location='mailcata.php?type=menu&amp;catalogue=$id_cat&amp;rubrique=$id_rub&amp;ssrubrique='+this.value\">
				<option value=\"\"> -- Sous Rubrique --</option> ";
				if (!($requete9=mysql_query("SELECT `id_ss_rub`, `designation` FROM `sous_rubrique` where `id_rubrique`='$id_rub' ORDER BY `designation` ASC"))) {
					die('Erreur : ' . mysql_error());
				}
				$cpt_req++;
				while($ligne=mysql_fetch_row($requete9))
				{
					echo "<option value='".$ligne[0]."' >".$ligne[1]."</option>";
				}
			echo "</select>";
			}
		}
		// si ya un id ss rubrique formulaire: 
		$sousrubrique=htmlentities($_GET['ssrubrique'], ENT_QUOTES, "UTF-8");
		if($sousrubrique!=null)
		{
			echo "<center><form method=\"post\" action=\"mailcata.php?sousrubrique=$sousrubrique\" enctype=\"multipart/form-data\"><table summary=\"\" border='1' width='90%'>
				
				
				<p><select name='sens'>	<option value='pro'>Je l'envoi à ceux qui le proposent:</option>
										<option value='dem'>Je l'envoi à ceux qui le demandent:</option></select></p>
				<p bgcolor='#FF0000' class='blanc'>N'oubliez pas qu'utiliser cette fonction envois des emails en masse. Vous avez Agenda et Actualités pour les événements datés.</p>
				
				<p>Message: <textarea name='message' cols='80' rows='10'></textarea></p>
				<input type='submit' value=' Envoyer ' >
				
				
				<table border='1' width='90%' summary=''>
				<tr class='t1'>
				<td><img src='smiles/smile1.gif'></td><td><img src='smiles/smile2.gif'></td><td><img src='smiles/smile3.gif'></td>
		   	<td><img src='smiles/smile4.gif'></td><td><img src='smiles/smile5.gif'></td><td><img src='smiles/smile6.gif'></td>
		   	<td><img src='smiles/smile7.gif'></td><td><img src='smiles/smile8.gif'></td><td><img src='smiles/smile9.gif'></td>
		  		<td><img src='smiles/smile10.gif'></td><td><img src='smiles/smile11.gif'></td><td><img src='smiles/smile12.gif'></td>
		   	<td><img src='smiles/smile13.gif'></td><td><img src='smiles/smile27.gif'></td><td><img src='smiles/smile28.gif'></td>
		   	<td><img src='smiles/smile36.gif'></td><td><img src='smiles/smile25.gif'></td>
				</tr>	
				<tr class='t2'>
				<td>:)</td><td>:(</td><td>;)</td><td>:D</td><td>:*</td><td>B)</td><td>:o</td><td>:$</td>
				<td>:p</td><td>:x</td><td>(k)</td><td>:/</td><td>Zzz</td><td>O:]</td><td>(bain)</td><td>(sante)</td>
				<td>(love)</td>
				</tr>
				<tr class='t1'>
		   	<td><img src='smiles/smile19.gif'></td><td><img src='smiles/smile20.gif'></td><td><img src='smiles/smile18.gif'></td>
		   	<td><img src='smiles/smile22.gif'></td><td><img src='smiles/smile23.gif'></td><td><img src='smiles/smile24.gif'></td>
		   	<td><img src='smiles/smile17.gif'></td><td><img src='smiles/smile16.gif'></td><td><img src='smiles/smile14.gif'></td>
		   	<td><img src='smiles/smile15.gif'></td><td><img src='smiles/smile29.gif'></td><td><img src='smiles/smile30.gif'></td>
		   	<td><img src='smiles/smile31.gif'></td><td><img src='smiles/smile34.gif'></td><td><img src='smiles/smile32.gif'></td>
				<td><img src='smiles/smile26.gif'></td><td><img src='smiles/smile21.gif'>
				</tr>
				<tr class='t2'>
				<td>:F</td><td>:X</td><td>(l)</td><td>:P</td><td>:!</td><td>:i</td>
				<td>;x</td><td>:§</td><td>8)</td><td>:#</td><td>;(</td><td>:@</td><td>(mur)</td>
				<td>O:}</td><td>(jtm)</td><td>(merci)</td><td>(gne)</td>	
				</tr>										
				</table>"; 
		}
		// preparation + envoi du message
		$sousrubrique=htmlentities($_GET['sousrubrique'], ENT_QUOTES, "UTF-8");
		$sens=htmlentities($_POST['sens'], ENT_QUOTES, "UTF-8");
		$message=nl2br(htmlentities($_POST['message'], ENT_QUOTES, "UTF-8"));
		$message_mail=$message;
		$time=time();
		// traitement
		// test si ok pour multi
		if(($sousrubrique!=null)&&($sens!=null)&&($message!=null))
		{
			// on met à jour la tempo car il envoi un message de masse..
			if($grade_mess!='TEST')
			{
				if (!($requete2=mysql_query("UPDATE `selistes` SET `time_multi` = '$time' WHERE `id_seliste`='$id_seliste' LIMIT 1") )) {
					die('Erreur : ' . mysql_error());
				}
			}
			if($sens=='dem')
			{// liste id des demande
				if (!($requete=mysql_query("SELECT `id_seliste` FROM `services` where `etat`='DEM' AND `id_ss_rub`='$sousrubrique' ORDER BY `time` DESC"))) {
					die('Erreur : ' . mysql_error());
				}
			}
			elseif($sens=='pro')
			{ // liste des id propose
				if (!($requete=mysql_query("SELECT `id_seliste` FROM `services` where `etat`='PRO' AND `id_ss_rub`='$sousrubrique' ORDER BY `time` DESC"))) {
					die('Erreur : ' . mysql_error());
				}
			}
			else
			{
				echo "<p>Erreur interne contacter le webmaster</p>";
			}
			echo "
			<p>Les Sélistes suivant on reçu votre message:</p><br>";
			while($ligne=mysql_fetch_row($requete))
			{
				$id_dest= $ligne[0];
				// recup info seliste
				if (!($requete1=mysql_query("SELECT `prenom`, `mail_mp`, `email`, `valide` FROM `selistes` WHERE `id_seliste`='$id_dest' AND `valide`='OUI'"))) {
					die('Erreur : ' . mysql_error());
				}
				$ligne1=mysql_fetch_row($requete1);
				$prenom_dest=stripslashes($ligne1[0]);
				$mail_mp=stripslashes($ligne1[1]);
				$email_dest=stripslashes($ligne1[2]);
				$valide=stripslashes($ligne1[3]);
				// envoi du mail :
				if($id_dest==2)
				{
				}
				else
				{
					if($valide=='OUI')
					{ 
						// envoi du message privé 
						if (!(mysql_query("INSERT INTO `messagerie` VALUES ('', '$id_dest', '$id_seliste', '0', '$time', 'AFF', 'Multi-messages catalogue:<br>$message')"))) {
							die('Erreur : ' . mysql_error());
						}
						echo "<tr>					
						<td class='t1'><p>Message envoyé à <a href='profil.php?id=".$id_dest."'>".$prenom_dest."(".$id_dest." )</a>";
						$prenom_exp=html_entity_decode($prenom_exp);
						$prenom_dest=html_entity_decode($prenom_dest);
						// test si envoi du mail?
						if($mail_mp=='OUI')
						{ // envoi du mail 
								$headers = "From: ".$nom." <".$email.">\n";
								$headers .= "Reply-To: ".$email_exp."\n";
								$headers .= "X-Author: ".$prenom_exp."\n";
								$headers .= "X-Priority:1\n";
								$headers .= "X-Mailer: PHP\n";
								$headers .='Content-Type: text/html; charset="iso-8859-1"'."\n";
							$mel ='<html><head><title></title></head><body>
							<p>Bonjour '.$prenom_dest.', vous avez recu un message Multi-catalogue de <a href="http://'.$site.'/index.php?vers=profil.php?id='.$id_seliste.'">'.$prenom_exp.' ('.$id_seliste.')</a>. voici son message:
							<br><br><b>'.$message_mail.'</b>
							<br>
							<br><u>Pour répondre:</u>
							<br>Par messagerie privée sur le site: <a href="http://'.$site.'/index.php?vers=messagerie.php?action=lire"> Cliquez ici pour aller sur votre messagerie.</a>
							<br>Soit directement sur son courriel: '.$email_exp.'
							<br>Cordialement, <br> '.$nom.'.
							<br><br><i>Si vous ne souhaitez plus avoir ces alertes:
							<br><a href="http://'.$site.'/index.php?vers=compte.php"> Cliquez ici </a> ou via la page compte de l\'onglet outils.</i></p>
							</body></html>';
							sw_mail($email_dest,'['.$nom.'] Email général catalogue.',$mel,$headers);
							echo " + email ! ";
						}
						echo "</p><br>";
					}
				}		
			}
		}
		else
		{
			
		}
		echo "</div>";
	}
	else
	{ 	 // délai dépassé
		header ("location:troptard.php");
		session_destroy();
	}
 }
else
{ 	 // pas de session
	header ("location:404.php");
	session_destroy();
}
mysql_close($connexion);
include ("fin.php");	
?>
