<!--   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->

<?php 

if ( isset($_SESSION['session']))
	{								  
		if (($_SESSION['grade']=='SELISTE')||($_SESSION['grade']=='MODERATEUR')||($_SESSION['grade']=='ADMIN'))
		{
			/*recup de la page actuel*/
			$page=htmlentities(addslashes($_SERVER['PHP_SELF']));
			$page=substr($page,9) ;
			$page=substr($page,0,-4);
			
			// messagerie avec ou sans le nombre?
			if($nbrmess!=0){$mess="Messagerie<b>(".$nbrmess.")</b>";}
			else{$mess="Messagerie";}
			// recuperation des modules actifs
			// rapatriement parametre1
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'multicatalogue'")))) {
			    die('Erreur : ' . mysql_error());
			}
			$var1=stripslashes($recup[0]);
			// rapatriement parametre2
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'multimessage'")))) {
			    die('Erreur : ' . mysql_error());
			}
			$var2=stripslashes($recup[0]);
			// rapatriement parametre3
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'seliste'")))) {
			    die('Erreur : ' . mysql_error());
			}
			$var3=stripslashes($recup[0]);
			// rapatriement parametre4
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'chat'")))) {
			    die('Erreur : ' . mysql_error());
			}
			$var4=stripslashes($recup[0]);
			// rapatriement parametre5
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'actu'")))) {
			    die('Erreur : ' . mysql_error());
			}
			$var5=stripslashes($recup[0]);
			// rapatriement parametre6
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'agenda'")))) {
			    die('Erreur : ' . mysql_error());
			}
			$var6=stripslashes($recup[0]);
			// rapatriement parametre7
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'cr'")))) {
			    die('Erreur : ' . mysql_error());
			}
			$var7=stripslashes($recup[0]);
			// rapatriement parametre8
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'boiteaidee'")))) {
			    die('Erreur : ' . mysql_error());
			}
			$var8=stripslashes($recup[0]);
			// rapatriement parametre9
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'petiteannonce'")))) {
			    die('Erreur : ' . mysql_error());
			}
			$var9=stripslashes($recup[0]);
			// rapatriement parametre10
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'autresel'")))) {
			    die('Erreur : ' . mysql_error());
			}
			$var10=stripslashes($recup[0]);
			// rapatriement parametre blog
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'blog'")))) {
			    die('Erreur : ' . mysql_error());
			}
			$var11=stripslashes($recup[0]);
			// rapatriement parametre url blog
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'urlblog'")))) {
			    die('Erreur : ' . mysql_error());
			}
			$var12=stripslashes($recup[0]);			
			/*echo "
		
	
			
				<ul id=\"menuDeroulant\">
					<li";if(($page=='bureau')||($page=='services')||($page=='echanges')||($page=='robots')){echo " class=\"menuactif\"";}echo "><a href=\"#\">Mon Bureau</a>
						<ul class=\"sousMenu\">
						   <li";if($page=='bureau'){echo " class=\"menuactif\"";}echo "><a href=\"bureau.php\">Accueil</a></li>
						   <li";if($page=='services'){echo " class=\"menuactif\"";}echo "><a href=\"services.php?action=edition\">Mes services</a></li>
						   <li";if($page=='echanges'){echo " class=\"menuactif\"";}echo "><a href=\"echanges.php\">Ma compta</a></li>
						   <li";if($page=='robots'){echo " class=\"menuactif\"";}echo "><a href=\"robots.php\">Mes outils</a></li>
						</ul>
					</li>
					<li";if(($page=='messagerie')||($page=='mailcata')||($page=='mess_generale')||($page=='chat')){echo " class=\"menuactif\"";}echo "><a href=\"#\">Contacts(".$nbrmess.")</a>
						<ul class=\"sousMenu\">
						   <li";if($page=='messagerie'){echo " class=\"menuactif\"";}echo "><a href=\"messagerie.php?action=lire\">".$mess."</a></li>
";if($var1=='OUI'){echo"   <li";if($page=='mailcata'){echo " class=\"menuactif\"";}echo "><a href=\"mailcata.php\">Multi message catalogue</a></li>   ";}echo "
";if($var2=='OUI'){echo"   <li";if($page=='mess_generale'){echo " class=\"menuactif\"";}echo "><a href=\"mess_generale.php?action=ecrire\">Demande urgente</a></li>";}echo "
";if($var4=='OUI'){echo "  <li";if($page=='chat'){echo " class=\"menuactif\"";}echo "><a href=\"chat.php\">Chat</a></li> ";}echo "
						</ul>
					</li>
					<li";if(($page=='actualites')||($page=='agenda')||($page=='agendav3')){echo " class=\"menuactif\"";}echo "><a href=\"#\">Evenements</a>
						<ul class=\"sousMenu\">
";if($var5=='OUI'){echo "  <li";if($page=='actualites'){echo " class=\"menuactif\"";}echo "><a href=\"actualites.php?action=lire\">Actualités</a></li>";}echo "
";if($var6=='OUI'){echo "  <li";if($page=='agendav3'){echo " class=\"menuactif\"";}echo "><a href=\"agendav3.php\">Agenda</a></li>";}echo "
						</ul>
					</li>
					<li";if(($page=='catalogue')||($page=='annonces')){echo " class=\"menuactif\"";}echo "><a href=\"#\">Echanges</a>
						<ul class=\"sousMenu\">
						   <li";if($page=='catalogue'){echo " class=\"menuactif\"";}echo "><a href=\"catalogue.php\">Catalogue</a></li>
";if($var9=='OUI'){echo "  <li";if($page=='annonces'){echo " class=\"menuactif\"";}echo "><a href=\"annonces.php\">Petites annonces</a></li>
                           <li><a href=\"serveur/livret_d_echange_SEL_2014.pdf\">Livret d'échanges</a></li>";}echo "
						</ul>
					</li>
					<li";if(($page=='selistes')||($page=='compte_rendu')||($page=='forum')){echo " class=\"menuactif\"";}echo "><a href=\"#\">".$nom."</a>
						<ul class=\"sousMenu\">
";if($var3=='OUI'){echo "  <li";if($page=='selistes'){echo " class=\"menuactif\"";}echo "><a href=\"selistes.php\">Selistes</a></li> ";}echo "
";if($var3=='OUI'){echo "  <li";if($page=='modos'){echo " class=\"menuactif\"";}echo "><a href=\"modos.php\">Modérateurs</a></li> ";}echo "
";if($var11=='OUI'){echo "  <li><a target=\"_about\" href=\"http://".$var12."/\">Blog</a></li> ";}echo "
";if($var7=='OUI'){echo "  <li";if($page=='compte_rendu'){echo " class=\"menuactif\"";}echo "><a href=\"compte_rendu.php?action=lire\">Comptes-rendus</a></li> ";}echo "
";if($var8=='OUI'){echo "  <li";if($page=='forum'){echo " class=\"menuactif\"";}echo "><a href=\"forum.php?action=lire\">Boite à idée</a></li>";}echo "
						</ul>
					</li>
					";if($var10=='OUI'){echo "
					<li";if($page=='sel'){echo " class=\"menuactif\"";}echo "><a href=\"#\">Les autres SEL</a>
						<ul class=\"sousMenu\">
							<li";if($page=='selannonces'){echo " class=\"menuactif\"";}echo "><a href=\"selannonces.php\">Petites annonces</a></li>
						   <li><a target=\"_about\" href=\"http://route-des-sel.org/\">Route des SEL</a></li>
						   <li";if($page=='sel'){echo " class=\"menuactif\"";}echo "><a href=\"sel.php\">Autre Sel</a></li>
						</ul>
					</li>";}echo "
				</ul>	
			";*/
			echo "<div style='border-bottom:1px solid #e5e5e5;border-top:1px solid #e5e5e5'>
        <ul id=\"menuDeroulant\">
            <li><a href='services.php?action=edition'><img style='vertical-align: middle' src='images/agt-services-publics-icone-4819-32.png'/><br/><span style='vertical-align: middle'>Mes services</span></a></li>
            <li><a href='echanges.php'><img style='vertical-align: middle' src='images/fleches-interagir-recharger-mise-a-jour-icone-5225-32.png'/><br/><span style='vertical-align: middle'>Mes échanges</span></a></li>
            <li><a href='messagerie.php?action=lire'><img style='vertical-align: middle' src='images/mail-message-icone-5822-32.png'><br/><span style='vertical-align: middle'>Mes messages</span></a></li>
            <li><a href='selistes.php'><img style='vertical-align: middle' src='images/forum-groupe-utilisateurs-icone-4597-32.png'/><br/><span style='vertical-align: middle'>Les sélistes</span></a></li>
            <li><a href='#'><img style='vertical-align: middle' src='images/forum-groupe-utilisateurs-icone-4597-32.png'/><br/><span style='vertical-align: middle'>Le SEL</span></a></li>
        </ul></div>";
		}
		else
			{
				header ("location:../404.php");
			}
	}
	else  
		{
			header ("location:../404.php");  
		}
?>
