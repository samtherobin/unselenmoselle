<!--   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->

<?php  
if ( isset($_SESSION['session']))
	{								  
		if (($_SESSION['grade']=='MODERATEUR')||($_SESSION['grade']=='ADMIN'))
		{
			echo "<br>
				<ul id=\"menuDeroulantModo\">
					<li";if(($page=='admin_actu')||($page=='admin_agen')){echo " class=\"menuactifModo\"";}echo "><a href=\"#\">Evenements</a>
						<ul class=\"sousMenuModo\">
						   <li";if($page=='admin_actu'){echo " class=\"menuactifModo\"";}echo "><a href=\"admin_actu.php?action=voir\">Moderation des Actualités</a></li>
						   <li";if($page=='admin_agen'){echo " class=\"menuactifModo\"";}echo "><a href=\"admin_agen.php?action=voir\">Modération de l'agenda</a></li>
						</ul>
					</li>
					<li";if(($page=='admin_comp')||($page=='cotisations')){echo " class=\"menuactifModo\"";}echo "><a href=\"#\">Comptabilité</a>
						<ul class=\"sousMenuModo\">
						   <li";if($page=='admin_comp'){echo " class=\"menuactifModo\"";}echo "><a href=\"admin_comp.php\">Validation des échanges</a></li>
						   <li><a href=\"statistique.php?action=echange\">Statistique des échanges</a></li>
						   <li";if($page=='cotisations'){echo " class=\"menuactifModo\"";}echo "><a href=\"cotisations.php?action=cotisation\">Retirer les cotisations</a></li>
						</ul>
					</li>
					<li";if(($page=='admin_serv')||($page=='admin_cata')||($page=='liste_serv')){echo " class=\"menuactifModo\"";}echo "><a href=\"#\">Gestion catalogue</a>
						<ul class=\"sousMenuModo\">
						   <li";if($page=='admin_serv'){echo " class=\"menuactifModo\"";}echo "><a href=\"admin_serv.php\">Demande en cours</a></li>
						   <li";if($page=='admin_cata'){echo " class=\"menuactifModo\"";}echo "><a href=\"admin_cata.php\">Modification du catalogue</a></li>
						   <li";if($page=='liste_serv'){echo " class=\"menuactifModo\"";}echo "><a href=\"liste_serv.php\">Liste des services</a></li>
						</ul>
					</li>
					<li";if(($page=='inscription')||($page=='admin_liste_selistes')){echo " class=\"menuactifModo\"";}echo "><a href=\"#\">Inscription</a>
						<ul class=\"sousMenuModo\">
							<li";if($page=='inscription'){echo " class=\"menuactifModo\"";}echo "><a href=\"inscription.php\">Inscription</a></li>
							<li><a href=\"statistique.php?action=inscrit\">Statistique des inscrit</a></li>
							<li";if($page=='admin_liste_selistes'){echo " class=\"menuactifModo\"";}echo "><a href=\"admin_liste_selistes.php\">Liste des membres</a></li>
						</ul>
					</li>
					<li";if($page=='admin_multi'){echo " class=\"menuactifModo\"";}echo "><a href=\"#\">Outils</a>
						<ul class=\"sousMenuModo\">
						    <li";if($page=='admin_multi'){echo " class=\"menuactifModo\"";}echo "><a href=\"admin_multi.php\">Messages multiples</a></li>
							<li";if($page=='serveurprive'){echo " class=\"menuactifModo\"";}echo "><a href=\"serveurprive.php\">Serveur Privé</a></li>
							<li";if($page=='banque'){echo " class=\"menuactifModo\"";}echo "><a href=\"banque.php\">Banque</a></li>
						</ul>
					</li>
				</ul>
			";
		}
		else
			{
				header ("location:../404.php");
			}
	}
	else  
	{
		header ("location:../404.php");  
	}
?>
