<!--   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->

<?php  

if ( isset($_SESSION['session']))
	{								  
		if ($_SESSION['grade']=='ADMIN')
		{
			include "lien/moderateur.php";
			echo "<br>
				<ul id=\"menuDeroulantAdmin\">
					<li";if(($page=='admin_BDD')||($page=='admin_log')||($page=='analyse3')){echo " class=\"menuactifAdmin\"";}echo "><a href=\"#\">Gestion de la base de donnée</a>
						<ul class=\"sousMenuAdmin\">
							<li><a target=\"_blank\" href=\"http://".$site."/Backup/Backup.php\">Sauvegarde</a></li>
						   <li";if($page=='admin_BDD'){echo " class=\"menuactifAdmin\"";}echo "><a href=\"admin_BDD.php\">Purge BDD</a></li>
						   <li";if($page=='admin_log'){echo " class=\"menuactifAdmin\"";}echo "><a href=\"admin_log.php\">Logs Modérateurs</a></li>
						   <li";if($page=='analyse3'){echo " class=\"menuactifAdmin\"";}echo "><a href=\"analyse3.php\">Analyse comptable</a></li>
						</ul>
					</li>
					<li";if(($page=='admin_param')||($page=='admin_modules')){echo " class=\"menuactifAdmin\"";}echo "><a href=\"#\">Paramètres du site</a>
						<ul class=\"sousMenuAdmin\">
						   <li";if($page=='admin_param'){echo " class=\"menuactifAdmin\"";}echo "><a href=\"admin_param.php?action=edition\">Paramètres globaux</a></li>
						   <li";if($page=='admin_public'){echo " class=\"menuactifAdmin\"";}echo "><a href=\"admin_public.php?action=edition\">Paramètres site public</a></li>
						   <li";if($page=='admin_modo'){echo " class=\"menuactifAdmin\"";}echo "><a href=\"admin_modo.php?action=edition\">Paramètres modérateurs</a></li>
						   <li";if($page=='admin_modules'){echo " class=\"menuactifAdmin\"";}echo "><a href=\"admin_modules.php?action=edition\">Gestion des modules</a></li>
						</ul>
					</li>
				</ul>
			";
			
			
			
		}
		else
			{
				header ("location:../404.php");
			}
	}
	else  
	{
		header ("location:../404.php");  
	}
?>
