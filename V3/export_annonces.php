<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/

if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
 {								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade`, `grains`, `prenom`, `nbr_mp`, `signature` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		$grains= $ligne[1];	
		$prenom= $ligne[2];			
		$nbr_mp= $ligne[3];
		$signature=$ligne[4];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
		switch ($grade)
		{
			case 'SELISTE' : include("lien/seliste.php");break;	
			case 'MODERATEUR' :include("lien/seliste.php"); include("lien/moderateur.php");break;
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page
		// le fichier doit deja exister
		$time=time();
		$date=date('_M',$time);
		$date2=date('j/m/y',$time);
		$file = ("PetitesAnnonces$date.xls"); 
		//on ouvre le fichier 
		if(!$myfile = fopen($file, "w"))     
		{
			 print("erreur: ");
			 print("'$filename' n'existe pas!\n");
			 exit;
		}
		fputs($myfile,"<table summary=\"\" border=\"1\" >
		<tr><td colspan=2 style='background-color:#008202;text-align=center'><p style='text-align=center; color:#ffffff; font-size:18pt'><b>Liste des petites annonces de ".$nom." du ".$date2."</b></p></td></tr>");
		if (!($requete_dem=mysql_query("SELECT * FROM `petite_annonce` WHERE `rubrique`='DEM' ORDER BY id_message DESC LIMIT 0 , 40 "))) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete_pro=mysql_query("SELECT * FROM `petite_annonce` WHERE `rubrique`='PRO' ORDER BY id_message DESC LIMIT 0 , 40 "))) {
			die('Erreur : ' . mysql_error());
		}
		$nbr_dem=mysql_num_rows($requete_dem);		
		$nbr_pro=mysql_num_rows($requete_pro);
		
		fputs($myfile,"<tr><td style='background-color:#73b8ff'><p align=center style='font-size:14pt'><b>Liste des demandes:</b></p></td>
		<td style='background-color:#73b8ff'><p align=center style='font-size:14pt'><b>Liste des propositions:</b></p></tr>");
		// si nombre de demande superieur au propositions alors on termine par des demandes
		$i=0;
		if($nbr_dem>$nbr_pro)
		{
			while($donnees = mysql_fetch_array($requete_dem))
			{
				// gestion de la demande
				$id_sel_dem=$donnees['id_seliste'];
				$date_dem=date('d/m/y',$donnees['timestamp']);
				$message_dem=utf8_decode(stripslashes($donnees['message']));
				if (!($requete6=mysql_query("SELECT `prenom`,`tel` FROM `selistes` WHERE `id_seliste`='$id_sel_dem'"))) {
					die('Erreur : ' . mysql_error());
				}
					$ligne6=mysql_fetch_row($requete6);
					$prenom_dem=utf8_decode(stripslashes($ligne6[0]));
					$tel_dem=$ligne6[1];
				// gestion des propositions
				if (!($requete_pro=mysql_fetch_array(mysql_query("SELECT * FROM `petite_annonce` WHERE `rubrique`='PRO' ORDER BY id_message DESC LIMIT $i,1")))) {
					die('Erreur : ' . mysql_error());
				}
				$id_sel_pro=$requete_pro['id_seliste'];
				$date_pro=date('d/m/y',$requete_pro['timestamp']);
				$message_pro=utf8_decode(stripslashes($requete_pro['message']));
				if (!($requete6=mysql_query("SELECT `prenom`,`tel` FROM `selistes` WHERE `id_seliste`='$id_sel_pro'"))) {
					die('Erreur : ' . mysql_error());
				}
					$ligne6=mysql_fetch_row($requete6);
					$prenom_pro=utf8_decode(stripslashes($ligne6[0]));
					$tel_pro=$ligne6[1];
				// ajout de la ligne dans le tableau
				fputs($myfile,"<tr>");
				fputs($myfile,"<td style='width:400px'><p align=center><b>$prenom_dem($id_sel_dem)-<i>$tel_dem</i> - $date_dem<br>$message_dem<br><br></p></td>");
				if($message_pro==null)
				{
					fputs($myfile,"<td style='width:400px'><p align=center>&nbsp;</p></td>");
				}
				else
				{
					fputs($myfile,"<td style='width:400px'><p align=center><b>$prenom_pro($id_sel_pro)-<i>$tel_pro</i> - $date_pro<br>$message_pro<br><br></p></td>");
				}
				fputs($myfile,"</tr>");
				$i++;
			}
		}
		else
		{
			while($donnees = mysql_fetch_array($requete_pro))
			{
				// gestion des propositions
				$id_sel_pro=$donnees['id_seliste'];
				$date_pro=date('d/m/y',$donnees['timestamp']);
				$message_pro=utf8_decode(stripslashes($donnees['message']));
				if (!($requete6=mysql_query("SELECT `prenom`,`tel` FROM `selistes` WHERE `id_seliste`='$id_sel_pro'"))) {
					die('Erreur : ' . mysql_error());
				}
					$ligne6=mysql_fetch_row($requete6);
					$prenom_pro=utf8_decode(stripslashes($ligne6[0]));
					$tel_pro=$ligne6[1];
				// gestion des demandes
				if (!($requete_dem=mysql_fetch_array(mysql_query("SELECT * FROM `petite_annonce` WHERE `rubrique`='DEM' ORDER BY id_message DESC LIMIT $i,1")))) {
					die('Erreur : ' . mysql_error());
				}
				$id_sel_dem=$requete_dem['id_seliste'];
				$date_dem=date('d/m/y',$requete_dem['timestamp']);
				$message_dem=utf8_decode(stripslashes($requete_dem['message']));
				if (!($requete6=mysql_query("SELECT `prenom`,`tel` FROM `selistes` WHERE `id_seliste`='$id_sel_dem'"))) {
					die('Erreur : ' . mysql_error());
				}
					$ligne6=mysql_fetch_row($requete6);
					$prenom_dem=utf8_decode(stripslashes($ligne6[0]));
					$tel_dem=$ligne6[1];
				// ajout de la ligne dans le tableau
				fputs($myfile,"<tr>");
				if($message_dem==null)
				{
					fputs($myfile,"<td style='width:400px'><p align=center>&nbsp;</p></td>");
				}
				else
				{
					fputs($myfile,"<td style='width:400px'><p align=center><b>$prenom_dem($id_sel_dem)-<i>$tel_dem</i> - $date_dem<br>$message_dem<br><br></p></td>");
				}
				fputs($myfile,"<td style='width:400px'><p align=center><b>$prenom_pro($id_sel_pro)-<i>$tel_pro</i> - $date_pro<br>$message_pro<br><br></p></td>");
				fputs($myfile,"</tr>");
				$i++;
			}
		}
		fputs($myfile,"</table>");
		//fermeture fichier
		fclose($myfile);   //on ferme le fichier
		mysql_close();  // on ferme la connexion
		echo "<br><br><div class='corps'><br><br>
		<p class='titre'>Exportation des petites annonces.</p><br>
		<p class='t4'><a href='PetitesAnnonces$date.xls' class='aphoto'>Cliquez ici pour l'ouvrir & le télécharger sur votre ordinateur.<br><img src='images/excel.gif' width='50'></a></p>
		<br></div><br><br>";
		include ("fin.php");
		
	}
	else
	{ 	// trop tard
		header ("location:404.php");
		session_destroy();
	}
}
else
{ 	// voleur
	header ("location:404.php");
	session_destroy();
}
?>