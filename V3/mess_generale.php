<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/
if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
{								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	// test si session corespond a l'id    
	include("debut.php");
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade`, `email`, `prenom`,`time_multi` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		$email_exp= $ligne[1];
		$prenom_exp= $ligne[2];
		$time_multi= $ligne[3];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
		switch ($grade)
		{
			case 'SELISTE' : include("lien/seliste.php");break;	
			case 'MODERATEUR' :include("lien/seliste.php"); include("lien/moderateur.php");break;
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page
		echo "<br><div class=\"corps\"><br>";
		$action=$_GET['action'];
		if($action=='ecrire')
		{
			if(isset($_POST['message']))
			{// écriture d'un message ( envoi)
				$message=nl2br(htmlentities($_POST['message'], ENT_QUOTES, "UTF-8"));
				$message_mail=stripslashes($message);
				$grade_mess=$_POST['grade'];
				if($grade_mess=='TOUS')
				{				
					if (!($requete=mysql_query("SELECT `id_seliste`, `prenom`, `mail_multi`, `email` FROM `selistes` WHERE `valide`='OUI' "))) {
						die('Erreur : ' . mysql_error());
					}
					$grade_messtext='générale';					
				}
				elseif($grade_mess=='ADMIN')
				{
					if (!($requete=mysql_query("SELECT `id_seliste`, `prenom`, `mail_multi`, `email` FROM `selistes` WHERE `grade`='MODERATEUR' OR `grade`='ADMIN'"))) {
						die('Erreur : ' . mysql_error());
					}
					$grade_messtext='modérateurs et administrateurs';
				}
				elseif($grade_mess=='TEST')
				{
					if (!($requete=mysql_query("SELECT `id_seliste`, `prenom`, `mail_multi`, `email` FROM `selistes` WHERE `id_seliste`='$id_seliste'"))) {
						die('Erreur : ' . mysql_error());
					}
					$grade_messtext='Test envoyé à moi uniquement';
				}
				// calcul temporisation entre 2 demandes urgentes variable $tempo2
				$time=time();
				$timelimite=$time-$tempo2;
				// traitement
				echo "<center>";
				// test si ok pour multi
				if($time_multi<$timelimite)
				{ 
					// on met a jour la tempo si c'est pas un test ;)
					if($grade_mess!='TEST')
					{
						if (!($requete2=mysql_query("UPDATE `selistes` SET `time_multi` = '$time' WHERE `id_seliste`='$id_seliste' LIMIT 1") )) {
							die('Erreur : ' . mysql_error());
						}
					}
					while($ligne=mysql_fetch_row($requete))
					{
						$id_dest=$ligne[0];
						// on l'envoi pas a l'id 1 2 3 4 5 puisque c des comptes réservés !
						if($id_dest<5)
						{
						}
						else
						{
							// on envoi en mp
							if (!(mysql_query("INSERT INTO `messagerie` VALUES ('', '$id_dest', '$id_seliste', '0', '$time', 'AFF', '[ROUGE][ITALIQUE]Demande urgente $grade_messtext:[/ITALIQUE][/COULEUR]<br>$message')"))) {
								die('Erreur : ' . mysql_error());
							}
							
							$prenom_dest=stripslashes($ligne[1]);
							echo "<p>Message envoyé à <a href='profil.php?id=".$id_dest."'>".$prenom_dest."(".$id_dest." )</a>";	
							// on transforme les bbcode
							include("smileysmail.php");
							// on envoi le mail (on test avant)
							$mail_multi=$ligne[2];
							$email_dest=$ligne[3];
							$prenom_exp=html_entity_decode($prenom_exp);
							$prenom_dest=html_entity_decode($prenom_dest);
							$headers = "From:$nom <$email>\n";
							$headers .= "Reply-To: $prenom_exp <$email_exp>\n";
							$headers .= "X-Sender: $email\n";
							$headers .= "X-Author: $email_exp\n";
							$headers .= "X-Priority:1\n";
							$headers .= "X-Mailer: PHP\n";
							$headers .= "Return_Path: <$email>\n";
							$headers .='Content-Type: text/html; charset="UTF-8"'."\n";
							if($mail_multi=='OUI')
							{ // envoi du mail 
								
								$mel ='<html><head><title></title></head><body>
								<p>Bonjour '.$prenom_dest.', <a href="http://'.$site.'/index.php?vers=profil.php?id='.$id_seliste.'">'.$prenom_exp.' ('.$id_seliste.')</a> a formulé une Demande Urgente '.$grade_messtext.', voici son message:
								<br><br><b>'.$message_mail.'</b>
								<br>
								<br><u>Pour répondre:</u>
								<br>Par messagerie privée sur le site: <a href="http://'.$site.'/index.php?vers=messagerie.php?action=lire"> Cliquez ici pour aller sur votre messagerie.</a>
								<br>Soit directement sur son courriel: '.$email_exp.'
								<br>Cordialement, <br> '.$nom.'.
								<br><br><i>Si vous ne souhaitez plus avoir ces alertes:
								<br><a href="http://'.$site.'/index.php?vers=compte.php"> Cliquez ici </a> ou via la page compte de l\'onglet outils.</i></p>
								</body></html>';
								sw_mail($email_dest,'['.$nom.'] Demande urgente '.$grade_messtext.' de '.$prenom_exp.' ('.$id_seliste.')',$mel,$headers);
								echo " + email ! ";
							}
							echo "</p><br>";
						}
					}
				}
				else
				{
					// multimessage deja envoyé y'a moin de 2h!
					echo "<p class='titre'>Bonjour, vous avez déjà envoyé une demande urgente récemment!<br>
						Vous ne pouvez envoyer une demande qu'une fois toutes les ".$tempo2." secondes.</p>";	
				}
			}
			else
			{//écriture d'un message (formulaire)
			if (!($requete=mysql_query("SELECT COUNT(*) FROM selistes WHERE valide='OUI' AND `mail_multi`='OUI'"))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete);
			$nbrmail=$ligne[0];
			if (!($requete=mysql_query("SELECT COUNT(*) FROM selistes WHERE valide='OUI'"))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete);
			$nbrseliste=$ligne[0];
		 		echo "<center><form method='post' action='mess_generale.php?action=ecrire' enctype='multipart/form-data'>
					<p>Message pour les sélistes :
					<select name='grade'>
					<option value='TEST' >Envoi à moi même pour tester mon message</option>
					<option value='ADMIN' >Envoi aux modérateurs et administrateurs</option>
					<option value='TOUS' >Tous les sélistes ($nbrseliste membres + $nbrmail emails)</option>
					</select>
					</p><br>
					<p class='rouge'><b>N'oubliez pas qu'utiliser cette fonction envoie des emails en masse. Vous avez Agenda et Actualités pour les événements datés. <br>
					Une fois que vous cliquez sur \"envoyer\" patientez quelques secondes sans quitter la page, l'envoi des courriels prend du temps.</b></p>
					
					<br><textarea name='message' cols='80' rows='10'></textarea>
					
					<br><input type='submit' value='Envoyer'><br>
					</form>
					
					
				<table border='1' width='90%' summary=''>
				<tr class='t1'>
				<td><img src='smiles/smile1.gif'></td><td><img src='smiles/smile2.gif'></td><td><img src='smiles/smile3.gif'></td>
		   	<td><img src='smiles/smile4.gif'></td><td><img src='smiles/smile5.gif'></td><td><img src='smiles/smile6.gif'></td>
		   	<td><img src='smiles/smile7.gif'></td><td><img src='smiles/smile8.gif'></td><td><img src='smiles/smile9.gif'></td>
		  		<td><img src='smiles/smile10.gif'></td><td><img src='smiles/smile11.gif'></td><td><img src='smiles/smile12.gif'></td>
		   	<td><img src='smiles/smile13.gif'></td><td><img src='smiles/smile27.gif'></td><td><img src='smiles/smile28.gif'></td>
		   	<td><img src='smiles/smile36.gif'></td><td><img src='smiles/smile25.gif'></td>
				</tr>	
				<tr class='t2'>
				<td>:)</td><td>:(</td><td>;)</td><td>:D</td><td>:*</td><td>B)</td><td>:o</td><td>:$</td>
				<td>:p</td><td>:x</td><td>(k)</td><td>:/</td><td>Zzz</td><td>O:]</td><td>(bain)</td><td>(sante)</td>
				<td>(love)</td>
				</tr>
				<tr class='t1'>
		   	<td><img src='smiles/smile19.gif'></td><td><img src='smiles/smile20.gif'></td><td><img src='smiles/smile18.gif'></td>
		   	<td><img src='smiles/smile22.gif'></td><td><img src='smiles/smile23.gif'></td><td><img src='smiles/smile24.gif'></td>
		   	<td><img src='smiles/smile17.gif'></td><td><img src='smiles/smile16.gif'></td><td><img src='smiles/smile14.gif'></td>
		   	<td><img src='smiles/smile15.gif'></td><td><img src='smiles/smile29.gif'></td><td><img src='smiles/smile30.gif'></td>
		   	<td><img src='smiles/smile31.gif'></td><td><img src='smiles/smile34.gif'></td><td><img src='smiles/smile32.gif'></td>
				<td><img src='smiles/smile26.gif'></td><td><img src='smiles/smile21.gif'>
				</tr>
				<tr class='t2'>
				<td>:F</td><td>:X</td><td>(l)</td><td>:P</td><td>:!</td><td>:i</td>
				<td>;x</td><td>:§</td><td>8)</td><td>:#</td><td>;(</td><td>:@</td><td>(mur)</td>
				<td>O:}</td><td>(jtm)</td><td>(merci)</td><td>(gne)</td>	
				</tr>										
				</table>		   ";
				//<td>(bienvenue)</td><td><img src='smiles/smile33.gif'><td>(anniv)</td><td><img src='smiles/smile35.gif'>
			}
		echo "</div>";
		}
		else
		{ //rien dans le get 'action'
			header ("location:404.php");
			session_destroy();
		}
	} 
	else
	{ 	//délai dépassé
		header ("location:troptard.php");
		session_destroy();
	}
}
else
{ 	// voleur
	header ("location:404.php");
	session_destroy();
}
mysql_close($connexion);
include ("fin.php");	
?>

