<!--   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->

<?php  
$smile1=array(":)",":(",";)",":D",":*","B)",":o",":$",":p",":x","(k)",":-/","Zzz","8-)",":##",":-*",";x","(l)",":F",":X","(gne)",":P",":!",":i","(love)","(merci)","O:]","(bain)",";(",":@","(mur)","(jtm)","(anniv)","O:}","(bienvenue)","(sante)","(demon)","(gay)","(glande)","(vacance)","(toil)");
$smile2=array("<img alt=smile src=smiles/smile1.gif>","<img alt=smile src=smiles/smile2.gif>","<img alt=smile src=smiles/smile3.gif>","<img alt=smile src=smiles/smile4.gif>","<img alt=smile src=smiles/smile5.gif>","<img alt=smile src=smiles/smile6.gif>","<img alt=smile src=smiles/smile7.gif>","<img alt=smile src=smiles/smile8.gif>","<img alt=smile src=smiles/smile9.gif>","<img alt=smile src=smiles/smile10.gif>","<img alt=smile src=smiles/smile11.gif>","<img alt=smile src=smiles/smile12.gif>","<img alt=smile src=smiles/smile13.gif>","<img alt=smile src=smiles/smile14.gif>","<img alt=smile src=smiles/smile15.gif>","<img alt=smile src=smiles/smile16.gif>","<img alt=smile src=smiles/smile17.gif>","<img alt=smile src=smiles/smile18.gif>","<img alt=smile src=smiles/smile19.gif>","<img alt=smile src=smiles/smile20.gif>","<img alt=smile src=smiles/smile21.gif>","<img alt=smile src=smiles/smile22.gif>","<img alt=smile src=smiles/smile23.gif>","<img alt=smile src=smiles/smile24.gif>","<img alt=smile src=smiles/smile25.gif>","<img alt=smile src=smiles/smile26.gif>","<img alt=smile src=smiles/smile27.gif>","<img alt=smile src=smiles/smile28.gif>","<img alt=smile src=smiles/smile29.gif>","<img alt=smile src=smiles/smile30.gif>","<img alt=smile src=smiles/smile31.gif>","<img alt=smile src=smiles/smile32.gif>","<img alt=smile src=smiles/smile33.gif>","<img alt=smile src=smiles/smile34.gif>","<img alt=smile src=smiles/smile35.gif>","<img alt=smile src=smiles/smile36.gif>","<img alt=smile src=smiles/smile37.gif>","<img alt=smile src=smiles/smile38.gif>","<img alt=smile src=smiles/smile39.gif>","<img alt=smile src=smiles/smile40.gif>","<img alt=smile src=smiles/smile41.gif>"); 
$message=str_replace($smile1,$smile2,$message);

$balise1=array("[GRAS]","[/GRAS]","[ITALIQUE]","[/ITALIQUE]","[NOIR]","[BLANC]","[JAUNE]","[VERT]","[ROUGE]","[GRIS]","[BLEU]","[/COULEUR]");
$balise2=array("<b>","</b>","<i>","</i>","<span class=\"noir\">","<span class=\"blanc\">","<span class=\"jaune\">","<span class=\"vert\">","<span class=\"rouge\">","<span class=\"gris\">","<span class=\"bleu\">","</span>");
$message=str_replace($balise1,$balise2,$message);

// scan pour les profils
$message=ereg_replace("\\[id=([^\[]*)]([^]]*)?\[/id\]", "<a href='profil.php?id=\\1'>\\2</a>", $message);

// scan pour les photos
$message=ereg_replace("\\[image=([^\[]*)]", "<a href='photos/\\1.jpg'><img alt='image' width='150' src='photos/\\1.jpg'></a>", $message);

// scan pour serveur
$message=ereg_replace("\\[fichier=([^\[]*)]", "<a target='_blank' href='../fichier.php?id=\\1'>Cliquez-ici</a>", $message);
 


?>


