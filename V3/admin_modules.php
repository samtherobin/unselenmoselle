<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/

if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
{								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
	 	switch ($grade)
		{
			case 'SELISTE' : header("location:404.php");break;				
			case 'MODERATEUR' :header("location:404.php");break;		
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page
		//debut de l'edition
		if($_GET['action']=='edition') //on enregistre les modifs
		{	
			// rapatriement parametre1
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'multicatalogue'")))) {
				die('Erreur : ' . mysql_error());
			}
			$var1=stripslashes($recup[0]);
			// rapatriement parametre2
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'multimessage'")))) {
				die('Erreur : ' . mysql_error());
			}
			$var2=stripslashes($recup[0]);
			// rapatriement parametre3
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'seliste'")))) {
				die('Erreur : ' . mysql_error());
			}
			$var3=stripslashes($recup[0]);
			// rapatriement parametre4
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'chat'")))) {
				die('Erreur : ' . mysql_error());
			}
			$var4=stripslashes($recup[0]);
			// rapatriement parametre5
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'actu'")))) {
				die('Erreur : ' . mysql_error());
			}
			$var5=stripslashes($recup[0]);
			// rapatriement parametre6
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'agenda'")))) {
				die('Erreur : ' . mysql_error());
			}
			$var6=stripslashes($recup[0]);
			// rapatriement parametre7
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'cr'")))) {
				die('Erreur : ' . mysql_error());
			}
			$var7=stripslashes($recup[0]);
			// rapatriement parametre8
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'boiteaidee'")))) {
				die('Erreur : ' . mysql_error());
			}
			$var8=stripslashes($recup[0]);
			// rapatriement parametre9
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'petiteannonce'")))) {
				die('Erreur : ' . mysql_error());
			}
			$var9=stripslashes($recup[0]);
			// rapatriement parametre10
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'autresel'")))) {
				die('Erreur : ' . mysql_error());
			}
			$var10=stripslashes($recup[0]);	
			// rapatriement parametre blog
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'blog'")))) {
				die('Erreur : ' . mysql_error());
			}
			$varblog=stripslashes($recup[0]);
			// rapatriement parametre urlblog
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'urlblog'")))) {
				die('Erreur : ' . mysql_error());
			}
			$varurlblog=stripslashes($recup[0]);			


			echo  "<br><br><div class='corps'><br>
			<form method=\"post\" action=\"admin_modules.php?action=enregistre\" enctype=\"multipart/form-data\">
			<table summary=\"\" border='1' class='tablevu' width='90%' cellpadding='5'>
			<tr>
			<td colspan='2' bgcolor='#FF0000'><p>/!\ Les modifications desactivent les liens sans supprimer les infos.</p></td>
			</tr>
			<tr>
			<th width='40%' colspan='2'>Activation et désactivation des modules:<br>
			</p></td>
			</tr>
			<tr>
				<td><p class='t1'>Multi catalogue:</p></td>
				<td><p class='t2'>
				OUI : <INPUT type=radio name='multicatalogue' value='OUI' ";if($var1=='OUI'){echo "checked";}echo "><br>
				NON : <INPUT type=radio name='multicatalogue' value='NON' ";if($var1=='NON'){echo "checked";}echo "></p></td>
			</tr>
			<tr>
				<td><p class='t1'>Multi message:</p></td>
				<td><p class='t2'>
				OUI : <INPUT type=radio name='multimessage' value='OUI' ";if($var2=='OUI'){echo "checked";}echo "><br>
				NON : <INPUT type=radio name='multimessage' value='NON' ";if($var2=='NON'){echo "checked";}echo "></p></td>
			</tr>
			<tr>
				<td><p class='t1'>Selistes/Modos:</p></td>
				<td><p class='t2'>
				OUI : <INPUT type=radio name='seliste' value='OUI' ";if($var3=='OUI'){echo "checked";}echo "><br>
				NON : <INPUT type=radio name='seliste' value='NON' ";if($var3=='NON'){echo "checked";}echo "></p></td>
			</tr>
			<tr>
				<td><p class='t1'>Chat:</p></td>
				<td><p class='t2'>
				OUI : <INPUT type=radio name='chat' value='OUI' ";if($var4=='OUI'){echo "checked";}echo "><br>
				NON : <INPUT type=radio name='chat' value='NON' ";if($var4=='NON'){echo "checked";}echo "></p></td>
			</tr>
			<tr>
				<td><p class='t1'>Actualités:</p></td>
				<td><p class='t2'>
				OUI : <INPUT type=radio name='actu' value='OUI' ";if($var5=='OUI'){echo "checked";}echo "><br>
				NON : <INPUT type=radio name='actu' value='NON' ";if($var5=='NON'){echo "checked";}echo "></p></td>
			</tr>
			<tr>
				<td><p class='t1'>Agenda:</p></td>
				<td><p class='t2'>
				OUI : <INPUT type=radio name='agenda' value='OUI' ";if($var6=='OUI'){echo "checked";}echo "><br>
				NON : <INPUT type=radio name='agenda' value='NON' ";if($var6=='NON'){echo "checked";}echo "></p></td>
			</tr>
			<tr>
				<td><p class='t1'>Blog du site:</p></td>
				<td><p class='t2'>
				OUI : <INPUT type=radio name='blog' value='OUI' ";if($varblog=='OUI'){echo "checked";}echo "><br>
				NON : <INPUT type=radio name='blog' value='NON' ";if($varblog=='NON'){echo "checked";}echo "></p></td>
			</tr>
			<tr>
				<td><p class='t3'>URL du blog: (www.monblog.fr)</p></td>
				<td><p class='t2'><input name='urlblog' value='".$varurlblog."' size=30 maxLength=40></p></td>
			</tr>
			<tr>
				<td><p class='t1'>Compte rendu:</p></td>
				<td><p class='t2'>
				OUI : <INPUT type=radio name='cr' value='OUI' ";if($var7=='OUI'){echo "checked";}echo "><br>
				NON : <INPUT type=radio name='cr' value='NON' ";if($var7=='NON'){echo "checked";}echo "></p></td>
			</tr>
			<tr>
				<td><p class='t1'>Boite à idées:</p></td>
				<td><p class='t2'>
				OUI : <INPUT type=radio name='boiteaidee' value='OUI' ";if($var8=='OUI'){echo "checked";}echo "><br>
				NON : <INPUT type=radio name='boiteaidee' value='NON' ";if($var8=='NON'){echo "checked";}echo "></p></td>
			</tr>
			<tr>
				<td><p class='t1'>Petites annonces:</p></td>
				<td><p class='t2'>
				OUI : <INPUT type=radio name='petiteannonce' value='OUI' ";if($var9=='OUI'){echo "checked";}echo "><br>
				NON : <INPUT type=radio name='petiteannonce' value='NON' ";if($var9=='NON'){echo "checked";}echo "></p></td>
			</tr>
			<tr>
				<td><p class='t1'>Autres SEL:</p></td>
				<td><p class='t2'>
				OUI : <INPUT type=radio name='autresel' value='OUI' ";if($var10=='OUI'){echo "checked";}echo "><br>
				NON : <INPUT type=radio name='autresel' value='NON' ";if($var10=='NON'){echo "checked";}echo "></p></td>
			</tr>
			<tr>
				<td colspan='3'><p class='t1'><input type='submit' value=' Sauvegarder ' ></p></td>
			</tr>
			</table>			
			</form><br><br></div><br>";
		}
		else //on enregistre
		{
			if($_GET['action']=='enregistre')
			{ //ok pour enregistre
				$var=nl2br($_POST['multicatalogue']);
				if (!($requete=mysql_query("UPDATE `variables` SET `texte` =  '$var' WHERE `variable`= 'multicatalogue' LIMIT 1 ;"))) {
					die('Erreur : ' . mysql_error());
				}
				$var=nl2br($_POST['multimessage']);
				if (!($requete=mysql_query("UPDATE `variables` SET `texte` =  '$var' WHERE `variable`= 'multimessage' LIMIT 1 ;"))) {
					die('Erreur : ' . mysql_error());
				}
				$var=nl2br($_POST['seliste']);
				if (!($requete=mysql_query("UPDATE `variables` SET `texte` =  '$var' WHERE `variable`= 'seliste' LIMIT 1 ;"))) {
					die('Erreur : ' . mysql_error());
				}
				$var=nl2br($_POST['chat']);
				if (!($requete=mysql_query("UPDATE `variables` SET `texte` =  '$var' WHERE `variable`= 'chat' LIMIT 1 ;"))) {
					die('Erreur : ' . mysql_error());
				}
				$var=nl2br($_POST['actu']);
				if (!($requete=mysql_query("UPDATE `variables` SET `texte` =  '$var' WHERE `variable`= 'actu' LIMIT 1 ;"))) {
					die('Erreur : ' . mysql_error());
				}
				$var=nl2br($_POST['agenda']);
				if (!($requete=mysql_query("UPDATE `variables` SET `texte` =  '$var' WHERE `variable`= 'agenda' LIMIT 1 ;"))) {
					die('Erreur : ' . mysql_error());
				}
				$var=nl2br($_POST['cr']);
				if (!($requete=mysql_query("UPDATE `variables` SET `texte` =  '$var' WHERE `variable`= 'cr' LIMIT 1 ;"))) {
					die('Erreur : ' . mysql_error());
				}
				$var=nl2br($_POST['boiteaidee']);
				if (!($requete=mysql_query("UPDATE `variables` SET `texte` =  '$var' WHERE `variable`= 'boiteaidee' LIMIT 1 ;"))) {
					die('Erreur : ' . mysql_error());
				}
				$var=nl2br($_POST['petiteannonce']);
				if (!($requete=mysql_query("UPDATE `variables` SET `texte` =  '$var' WHERE `variable`= 'petiteannonce' LIMIT 1 ;"))) {
					die('Erreur : ' . mysql_error());
				}
				$var=nl2br($_POST['autresel']);
				if (!($requete=mysql_query("UPDATE `variables` SET `texte` =  '$var' WHERE `variable`= 'autresel' LIMIT 1 ;"))) {
					die('Erreur : ' . mysql_error());
				}
				$var=nl2br($_POST['blog']);
				if (!($requete=mysql_query("UPDATE `variables` SET `texte` =  '$var' WHERE `variable`= 'blog' LIMIT 1 ;"))) {
					die('Erreur : ' . mysql_error());
				}
				$urlblog=htmlentities($_POST['urlblog'], ENT_QUOTES, "UTF-8");
				if (!($requete=mysql_query("UPDATE `variables` SET `texte` =  '$urlblog' WHERE `variable`= 'urlblog' LIMIT 1 ;"))) {
					die('Erreur : ' . mysql_error());
				}
				echo "<br><br><div class='corps'><br>
				<p class='titre'>Les nouveaux modules sont mis à jour merci.</p><br>
				<p><a href='bureau.php' title='Bureau'>Retour bureau</a></p></br><br></div><br><br>";
			}
			else
			{ 	//pirate
				header ("location:404.php");
				session_destroy();			
			}
		}
		//fin edition
	} 
	//delai depassé
	else
	{
		header ("location:troptard.php");
		session_destroy();
	}
}
// pas de sesion
else
{
	header ("location:404.php");
	session_destroy();
}
mysql_close($connexion); 
include ("fin.php");	
?>
