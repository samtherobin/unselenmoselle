<?php  
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  */

if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
 {								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade`, `nbr_cha` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		$nbr_cha=$ligne[1];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
		switch ($grade)
		{
			case 'SELISTE' : include("lien/seliste.php");break;
			case 'MODERATEUR' :include("lien/seliste.php"); include("lien/moderateur.php");break;
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page
		echo "<br><div class=\"corps\"><br>";
		
		// selection que les valides avec un tri des 15 plus importants
			if (!($requete4=mysql_query("SELECT COUNT(`id_seliste`), `ville` FROM `selistes` WHERE `valide`='OUI' AND `ville`!='' GROUP BY `ville` Order by COUNT(`id_seliste`) DESC LIMIT 15 ") )) {
				die('Erreur : ' . mysql_error());
			}
			$i==0;
			while($ligne=mysql_fetch_row($requete4))
				{
					$i++;
					if($i==1)
					{
						$nbre1=$ligne[0];
						$ville1=stripslashes($ligne[1]);
						$ville1url=rawurlencode($ville1);
					}
					elseif($i==2)
					{
						$nbre2=$ligne[0];
						$ville2=stripslashes($ligne[1]);
						$ville2url=rawurlencode($ville2);
					}
					elseif($i==3)
					{
						$nbre3=$ligne[0];
						$ville3=stripslashes($ligne[1]);
						$ville3url=rawurlencode($ville3);
					}
					elseif($i==4)
					{
						$nbre4=$ligne[0];
						$ville4=stripslashes($ligne[1]);
						$ville4url=rawurlencode($ville4);
					}
					elseif($i==5)
					{
						$nbre5=$ligne[0];
						$ville5=stripslashes($ligne[1]);
						$ville5url=rawurlencode($ville5);
					}	
					elseif($i==6)
					{
						$nbre6=$ligne[0];
						$ville6=stripslashes($ligne[1]);
						$ville6url=rawurlencode($ville6);
						
					}	
					elseif($i==7)
					{
						$nbre7=$ligne[0];
						$ville7=stripslashes($ligne[1]);
						$ville7url=rawurlencode($ville7);
						
					}						
				}
 echo '
 
  <script src="http://maps.google.com/maps/api/js?sensor=false "
          type="text/javascript"></script>
<center><br><p class=\'titre\'>Carte des membres :</p>
  <div id="map" style="width: 600px; height: 550px;"></div>
 </center><br>
  <script type="text/javascript">
<!--

  var address1 = "centre ville '.$ville1.'";
  var address2 = "centre ville '.$ville2.'";
  var address3 = "centre ville '.$ville3.'";
  var address4 = "centre ville '.$ville4.'";
  var address5 = "centre ville '.$ville5.'";
  var address6 = "centre ville '.$ville6.'";
  var address7 = "centre ville '.$ville7.'";
  //--------------------------------------//
  // creation de la MAP avant utilisation //
  //--------------------------------------//
  var myOptions = {
    zoom: 11,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  var oMap = new google.maps.Map(document.getElementById("map"), myOptions);
 
  // creation objet Geocoder
  var geocoder1 = new google.maps.Geocoder();
  // appel methode geocode
  geocoder1.geocode( { "address": address1}, function( data, status) {
    // reponse OK
    if( status == google.maps.GeocoderStatus.OK){
      // recup. position
      var pos = data[0].geometry.location;
      // creation d un marker
      var marker = new google.maps.Marker({
        map : oMap,
        position : pos
      } );
	  // centrage de la carte
      oMap.setCenter( pos);
     var message = "<p><a href=recherche.php?ville='.$ville1url.'>'.$ville1.': '.$nbre1.' membres.</a></p>";
   var infowindow = new google.maps.InfoWindow({
       content: message,
       size: new google.maps.Size(10,30)
   });
   google.maps.event.addListener(marker, "click", function() {
       infowindow.open(oMap,marker);
   });
	}
    else {
      alert("Geocode was not successful for the following reason: " + status);
    }
  })


  
  
    // creation objet Geocoder
  var geocoder2 = new google.maps.Geocoder();
  // fonction geocode 2
    geocoder2.geocode( { "address": address2}, function( data, status) {
    // reponse OK
    if( status == google.maps.GeocoderStatus.OK){
      // recup. position
      var pos = data[0].geometry.location;
      // creation d un marker
      var marker = new google.maps.Marker({
        map : oMap,
        position : pos
      });
	   var message = "<p><a href=recherche.php?ville='.$ville2url.'>'.$ville2.': '.$nbre2.' membres.</a></p>";
   var infowindow = new google.maps.InfoWindow({
       content: message,
       size: new google.maps.Size(10,30)
   });
   google.maps.event.addListener(marker, "click", function() {
       infowindow.open(oMap,marker);
   });
    }
    else {
      alert("Geocode was not successful for the following reason: " + status);
    }
  });
  
  
    
    // creation objet Geocoder
  var geocoder3 = new google.maps.Geocoder();
  // fonction geocode 3
    geocoder3.geocode( { "address": address3}, function( data, status) {
    // reponse OK
    if( status == google.maps.GeocoderStatus.OK){
      // recup. position
      var pos = data[0].geometry.location;
      // creation d un marker
      var marker = new google.maps.Marker({
        map : oMap,
        position : pos
      });
	   var message = "<p><a href=recherche.php?ville='.$ville3url.'>'.$ville3.': '.$nbre3.' membres.</a></p>";
   var infowindow = new google.maps.InfoWindow({
       content: message,
       size: new google.maps.Size(10,30)
   });
   google.maps.event.addListener(marker, "click", function() {
       infowindow.open(oMap,marker);
   });
    }
    else {
      alert("Geocode was not successful for the following reason: " + status);
    }
  });
  
  
      

    // creation objet Geocoder
  var geocoder4 = new google.maps.Geocoder();
  // fonction geocode 4
    geocoder4.geocode( { "address": address4}, function( data, status) {
    // reponse OK
    if( status == google.maps.GeocoderStatus.OK){
      // recup. position
      var pos = data[0].geometry.location;
      // creation d un marker
      var marker = new google.maps.Marker({
        map : oMap,
        position : pos
      });
	   var message = "<p><a href=recherche.php?ville='.$ville4url.'>'.$ville4.': '.$nbre4.' membres.</a></p>";
   var infowindow = new google.maps.InfoWindow({
       content: message,
       size: new google.maps.Size(10,30)
   });
   google.maps.event.addListener(marker, "click", function() {
       infowindow.open(oMap,marker);
   });
    }
    else {
      alert("Geocode was not successful for the following reason: " + status);
    }
  });
  
  
      
    // creation objet Geocoder
  var geocoder5 = new google.maps.Geocoder();
  // fonction geocode 5
    geocoder5.geocode( { "address": address5}, function( data, status) {
    // reponse OK
    if( status == google.maps.GeocoderStatus.OK){
      // recup. position
      var pos = data[0].geometry.location;
      // creation d un marker
      var marker = new google.maps.Marker({
        map : oMap,
        position : pos
      });
	       var message = "<p><a href=recherche.php?ville='.$ville5url.'>'.$ville5.': '.$nbre5.' membres.</a></p>";
   var infowindow = new google.maps.InfoWindow({
       content: message,
       size: new google.maps.Size(10,30)
   });
   google.maps.event.addListener(marker, "click", function() {
       infowindow.open(oMap,marker);
   });
    }
    else {
      alert("Geocode was not successful for the following reason: " + status);
    }
  });
  
  
      
    // creation objet Geocoder
  var geocoder6 = new google.maps.Geocoder();
  // fonction geocode 6
    geocoder6.geocode( { "address": address6}, function( data, status) {
    // reponse OK
    if( status == google.maps.GeocoderStatus.OK){
      // recup. position
      var pos = data[0].geometry.location;
      // creation d un marker
      var marker = new google.maps.Marker({
        map : oMap,
        position : pos
      });
	       var message = "<p><a href=recherche.php?ville='.$ville6url.'>'.$ville6.': '.$nbre6.' membres.</a></p>";
   var infowindow = new google.maps.InfoWindow({
       content: message,
       size: new google.maps.Size(10,30)
   });
   google.maps.event.addListener(marker, "click", function() {
       infowindow.open(oMap,marker);
   });
    }
    else {
      alert("Geocode was not successful for the following reason: " + status);
    }
  });


      // creation objet Geocoder
  var geocoder7 = new google.maps.Geocoder();
  // fonction geocode 7
    geocoder7.geocode( { "address": address7}, function( data, status) {
    // reponse OK
    if( status == google.maps.GeocoderStatus.OK){
      // recup. position
      var pos = data[0].geometry.location;
      // creation d un marker
      var marker = new google.maps.Marker({
        map : oMap,
        position : pos
      });
	       var message = "<p><a href=recherche.php?ville='.$ville7url.'>'.$ville7.': '.$nbre7.' membres.</a></p>";
   var infowindow = new google.maps.InfoWindow({
       content: message,
       size: new google.maps.Size(10,30)
   });
   google.maps.event.addListener(marker, "click", function() {
       infowindow.open(oMap,marker);
   });
    }
    else {
      alert("Geocode was not successful for the following reason: " + status);
    }
  });
-->
  </script> <br>';
  // affichage des villes 
			// selection que les valides avec un tri des 15 plus importants
			if (!($requete4=mysql_query("SELECT COUNT(`id_seliste`), `ville` FROM `selistes` WHERE `valide`='OUI' AND `ville`!='' GROUP BY `ville` Order by COUNT(`id_seliste`) DESC LIMIT 15 ") )) {
				die('Erreur : ' . mysql_error());
			}
			echo "<br><div class='message'><br>
			<p class='titre'>Les 15 villes les plus représentées:</p><br>
			<table class='tablevu' border='1' width='90%' cellpadding='5'>
			<tr>
				<td><p class='t1'>Nombre de selistes:</p></th>
				<th><p class='t1'>Ville:</p></th>
			</tr>";
			while($ligne=mysql_fetch_row($requete4))
				{
					$nbre=$ligne[0];
					$Ville=stripslashes($ligne[1]);
				echo
				"<tr>
					<td><p class=t1>$nbre</p></td>
					<td><p class=t2>$Ville</p></td>
				</tr>";
			}		
			echo "</table><br></div><br>";
		echo "</div>";
	}
	else
	{ 	 // délai dépassé
		header ("location:troptard.php");
		session_destroy();
	}
 }
else
{ 	 // pas de session
	header ("location:404.php");
	session_destroy();
}
mysql_close($connexion); 
echo "</center>";
include ("fin.php");	
?>
