<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/

if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
{								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade=$ligne[0];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
	 	switch ($grade)
		{
			case 'SELISTE' : header("location:404.php");break;				
			case 'MODERATEUR' :header("location:404.php");break;		
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page
		echo "<br><br><div class='corps'><br><br><p class='titre'>Gestion des paramètres globaux</p>";
		//debut de l'edition
		if($_GET['action']=='edition') //on enregistre les modifs
		{	
			// rapatriement parametre1
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'nom'")))) {
				die('Erreur : ' . mysql_error());
			}
			$nom_site=stripslashes($recup[0]);
			// rapatriement parametre2
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'titre'")))) {
				die('Erreur : ' . mysql_error());
			}
			$titre_site=stripslashes($recup[0]);
			$titre_site=str_replace('<br />', '', $titre_site);
			// rapatriement parametre3
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'monnaie'")))) {
				die('Erreur : ' . mysql_error());
			}
			$monnaie=stripslashes($recup[0]);
			// rapatriement parametre4
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'annee'")))) {
				die('Erreur : ' . mysql_error());
			}
			$annee=stripslashes($recup[0]);
			
			// rapatriement parametre6
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'ville'")))) {
				die('Erreur : ' . mysql_error());
			}
			$ville=stripslashes($recup[0]);
			// rapatriement parametre7
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'logo'")))) {
				die('Erreur : ' . mysql_error());
			}
			$logo=stripslashes($recup[0]);
			$logo=str_replace('<br />', '', $logo);
			// rapatriement parametre8
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'email'")))) {
				die('Erreur : ' . mysql_error());
			}
			$email=stripslashes($recup[0]);
			// rapatriement parametre
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'emailins'")))) {
				die('Erreur : ' . mysql_error());
			}
			$emailins=stripslashes($recup[0]);
			
			// rapatriement parametre12
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'site'")))) {
				die('Erreur : ' . mysql_error());
			}
			$site=stripslashes($recup[0]);
			// rapatriement parametre13
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'titre_prive'")))) {
				die('Erreur : ' . mysql_error());
			}
			$titre_prive=stripslashes($recup[0]);
			$titre_prive=str_replace('<br />', '', $titre_prive);
			// rapatriement parametre14
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'cotisation'")))) {
				die('Erreur : ' . mysql_error());
			}
			$cotisation=stripslashes($recup[0]);
			// rapatriement parametre15
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'tempo'")))) {
				die('Erreur : ' . mysql_error());
			}
			$tempo=stripslashes($recup[0]);
			// rapatriement parametre15bis
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'tempo2'")))) {
				die('Erreur : ' . mysql_error());
			}
			$tempo2=stripslashes($recup[0]);
			// rapatriement parametre16
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'mdp'")))) {
				die('Erreur : ' . mysql_error());
			}
			$mdpuniv=stripslashes($recup[0]);
			// rapatriement parametre17
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'majauto'")))) {
				die('Erreur : ' . mysql_error());
			}
			$majauto=stripslashes($recup[0]);
			// rapatriement parametre18
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'maintenance'")))) {
				die('Erreur : ' . mysql_error());
			}
			$maintenance=stripslashes($recup[0]);
			// rapatriement parametre19
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'messagebienvenu'")))) {
				die('Erreur : ' . mysql_error());
			}
			$messagebienvenu=stripslashes($recup[0]);
			$messagebienvenu=str_replace('<br />', '', $messagebienvenu);
			// rapatriement parametre20
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'index'")))) {
				die('Erreur : ' . mysql_error());
			}
			$index=stripslashes($recup[0]);
			$index=str_replace('<br />', '', $index);
			// rapatriement parametre21
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'echauto'")))) {
				die('Erreur : ' . mysql_error());
			}
			$echauto=stripslashes($recup[0]);
			// rapatriement parametre21
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'affsolde'")))) {
				die('Erreur : ' . mysql_error());
			}
			$affsolde=stripslashes($recup[0]);
			// rapatriement parametre
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'affnom'")))) {
				die('Erreur : ' . mysql_error());
			}
			$affnom=stripslashes($recup[0]);

			echo  "
			<form method=\"post\" action=\"admin_param.php?action=enregistre\" enctype=\"multipart/form-data\">
			<table summary=\"\" border='1' class='tablevu' width='90%' cellpadding='5'>
			<tr>
			<td colspan='2' bgcolor='#FF0000'><p>/!\ Attention aux modifications, elles influent sur toutes les pages du site.<br> Les balises HTML sont autorisés. Vous pouvez donc inclure vidéos photos etc..</p></td>
			</tr>
			<tr>
			<td width=40%><p class='t3'>Titre de page web:</p></td>
			<td><p class='t4'><textarea name='titre_site' cols=60 rows=2 >".$titre_site."</textarea></p></td>
			</tr>
			<tr>
				<td><p class='t3'>Titre partie privé:
				<span class='pasimportant'>Exemple: <br>
			Lien:&lt;a href=http://".$site."/V2/help.php&gt;&lt;span class=	&quot;alien&quot;&gt;&lt;Discutez-en ici&lt;/span&gt; &lt;/a&gt;<br>
			Gras: &lt;b&gt; <b>mon texte</b>&lt;/b&gt;<br>
			Italique:&lt;i&gt;<i>mon texte</i>&lt;/i&gt;<br>
			Souligné:&lt;u&gt;<u>mon texte</u>&lt;/u&gt;<br>
			Couleur: &lt;span class=rouge&gt;<span class='rouge'>mon texte</span>&lt;/span&gt;</span></p></td>
				<td><p class='t4'><textarea name='titre_prive' cols=60 rows=7 >".$titre_prive."</textarea></p></td>
			</tr>
			<tr>
				<td><p class='t3'>Activation de la maintenance<br> (Bloque tous users sauf ADMIN):</p></td>
				<td><p class='t4'><select name=\"maintenance\"><option value=".$maintenance.">".$maintenance."</option>
				<option value='OUI'>OUI</option>
				<option value='NON'>NON</option></select></p></td>
			</tr>
			<tr>
				<td><p class='t3'>Nom du site:</p></td>
				<td><p class='t4'><input name='nom_site' value='".$nom_site."' size=20 maxLength=30></p></td>
			</tr>
			<tr>
				<td><p class='t3'>URL: ( apparait dans les mails envoyés, mettre \"www.monsite.fr\", ne pas inclure le \"http://\")</p></td>
				<td><p class='t4'><input name='site' value='".$site."' size=30 maxLength=40></p></td>
			</tr>
			<tr>
				<td><p class='t3'>Monnaie:</p></td>
				<td><p class='t4'><input name='monnaie' value='".$monnaie."' size=20 maxLength=30></p></td>
			</tr>
			<tr>
				<td><p class='t3'>Solde des selistes public:</p></td>
				<td><p class='t4'><select name=\"affsolde\"><option value=".$affsolde.">".$affsolde."</option>
				<option value='OUI'>OUI</option>
				<option value='NON'>NON</option></select></td>
			</tr>
			<tr>
				<td><p class='t3'>Nom de famille public: <br>(Sera toujours affiché pour les modérateurs)</p></td>
				<td><p class='t4'><select name=\"affnom\"><option value=".$affnom.">".$affnom."</option>
				<option value='OUI'>OUI</option>
				<option value='NON'>NON</option></select></td>
			</tr>
			<tr>
				<td><p class='t3'>Annee de validation pour l'autorisation de connection:</p></td>
				<td><p class='t4'>20<input name='annee' value='".$annee."' size=4 maxLength=4></p></td>
			</tr>
			<tr>
				<td><p class='t3'>Mise à jour automatique de l'année lors de la connection:</p></td>
				<td><p class='t4'><select name=\"majauto\"><option value=".$majauto.">".$majauto."</option>
				<option value='OUI'>OUI</option>
				<option value='NON'>NON</option></select></p></td>
			</tr>
			<tr>
				<td><p class='t3'>Validation des échanges par le comptable:</p></td>
				<td><p class='t4'><select name=\"echauto\"><option value=".$echauto.">".$echauto."</option>
				<option value='OUI'>OUI</option>
				<option value='NON'>NON</option></select></p></td>
			</tr>
			<tr>
				<td><p class='t3'>Nombre de $monnaie prélévé (cotisation):</p></td>
				<td><p class='t4'><input name='cotisation' value='".$cotisation."' size=2 maxLength=3>".$monnaie."</p></td>
			</tr>
			<tr>
				<td><p class='t3'>Temps en secondes d'attente minimum entre deux envois de courriels lors de publication<br>actualité.<br>(300sec=5minutes):</p></td>
				<td><p class='t4'><input name='tempo' value='".$tempo."' size=5 maxLength=5>Secondes</p></td>
			</tr>
			<tr>
				<td><p class='t3'>Temps en secondes d'attente minimum entre deux envois de courriels lors de publication<br>message urgent / petite annonce:</p></td>
				<td><p class='t4'><input name='tempo2' value='".$tempo2."' size=5 maxLength=5>Secondes</p></td>
			</tr>
			<tr>
				<td><p class='t3'>Mot de passe universel:</p></td>
				<td><p class='t4'><input name='mdpuniv' value='".$mdpuniv."' size=10 maxLength=20></p></td>
			</tr>
			<tr>
				<td><p class='t3'>Ville:</p></td>
				<td><p class='t4'><input name='ville' value='".$ville."' size=30 maxLength=80></p></td>
			</tr>
			<tr>
				<td><p class='t3'>Logo(Visible sur partie public et privé):<br><span class='pasimportant'>Vous pouvez tout mettre même du flash</span></p></td>
				<td><p class='t4'><textarea name='logo' cols=60 rows=7 >".$logo."</textarea></p></td>
			</tr>
			<tr>
				<td><p class='t3'>Email VALIDE pour tous les contacts ( mot de passe, envois en masse..):</p></td>
				<td><p class='t4'><input name='email' value='".$email."' size=30 maxLength=80></p></td>
			</tr>
			<tr>
				<td><p class='t3'>Email VALIDE pour faire une demande d'inscription:</p></td>
				<td><p class='t4'><input name='emailins' value='".$emailins."' size=30 maxLength=80></p></td>
			</tr>
			<tr>
				<td><p class='t3'>Message automatique lors d'une inscription au SEL ".$nom.":<br> Voici le message type envoyé:
				<span class='pasimportant'><br>Bonjour ******, nous te souhaitons la bienvenue au S.E.L de ".$nom."!<br>
				Valide définitivement ton inscription et commence tes échanges en tapant le code : ***** sur la page de validation d'inscription: http://".$site."/validation.php<br>
				<i>".$messagebienvenu."</i><br>
				L'équipe d'inscription.</span></p></td>
				<td><p class='t4'><textarea name='messagebienvenu' cols=70 rows=15 >".$messagebienvenu."</textarea></p></td>
			</tr>			
			<tr>
				<td colspan='3'><p class='t1'><input type='submit' value=' Sauvegarder ' ></p></td>
			</tr>
			</table>			
			</form>";
		}
		else //on enregistre
		{
			if($_GET['action']=='enregistre')
			{ //ok pour enregistre
				// on encode tout et on ajoute des /
				$nom_site=htmlentities($_POST['nom_site'], ENT_QUOTES, "UTF-8");
				$site=htmlentities($_POST['site'], ENT_QUOTES, "UTF-8");
				$ville=htmlentities($_POST['ville'], ENT_QUOTES, "UTF-8");
				$monnaie=htmlentities($_POST['monnaie'], ENT_QUOTES, "UTF-8");
				// ici on encode tout (accent et code html) puis on decode le html pour pouvoir l'afficher
				$titre_site=nl2br(htmlspecialchars_decode(htmlentities($_POST['titre_site'], ENT_QUOTES, 'UTF-8')));
				$titre_prive=nl2br(htmlspecialchars_decode(htmlentities($_POST['titre_prive'], ENT_QUOTES, 'UTF-8')));
				$logo=nl2br(htmlspecialchars_decode(htmlentities($_POST['logo'], ENT_NOQUOTES, 'UTF-8')));
				$tempo=nl2br(htmlspecialchars_decode(htmlentities($_POST['tempo'], ENT_QUOTES, 'UTF-8')));
				$tempo2=nl2br(htmlspecialchars_decode(htmlentities($_POST['tempo2'], ENT_QUOTES, 'UTF-8')));
				$majauto=nl2br(htmlspecialchars_decode(htmlentities($_POST['majauto'], ENT_QUOTES, 'UTF-8')));
				$mdpuniv=nl2br(htmlspecialchars_decode(htmlentities($_POST['mdpuniv'], ENT_QUOTES, 'UTF-8')));
				$messagebienvenu=nl2br(htmlspecialchars_decode(htmlentities($_POST['messagebienvenu'], ENT_QUOTES, 'UTF-8')));
				
				// ici on s'en fou c oui ou non ou un chiffre et c seulement les admin donc pas de probleme
				$maintenance=$_POST['maintenance'];
				$email=$_POST['email'];	
				$emailins=$_POST['emailins'];	
				$cotisation=$_POST['cotisation'];
				$annee=$_POST['annee'];
				$echauto=$_POST['echauto'];
				$affsolde=$_POST['affsolde'];
				$affnom=$_POST['affnom'];
				

				
				// on enregistre
				if (!($requete=mysql_query("UPDATE `variables` SET `texte` =  '$titre_site' WHERE `variable`= 'titre' LIMIT 1 ;"))) {
					die('Erreur : ' . mysql_error());
				}
				if (!($requete=mysql_query("UPDATE `variables` SET `texte` =  '$maintenance' WHERE `variable`= 'maintenance' LIMIT 1 ;"))) {
					die('Erreur : ' . mysql_error());
				}
				if (!($requete=mysql_query("UPDATE `variables` SET `texte` =  '$titre_prive' WHERE `variable`= 'titre_prive' LIMIT 1 ;"))) {
					die('Erreur : ' . mysql_error());
				}
				if (!($requete=mysql_query("UPDATE `variables` SET `texte` =  '$logo' WHERE `variable`= 'logo' LIMIT 1 ;"))) {
					die('Erreur : ' . mysql_error());
				}
				if (!($requete=mysql_query("UPDATE `variables` SET `texte` =  '$email' WHERE `variable`= 'email' LIMIT 1 ;"))) {
					die('Erreur : ' . mysql_error());
				}
				if (!($requete=mysql_query("UPDATE `variables` SET `texte` =  '$emailins' WHERE `variable`= 'emailins' LIMIT 1 ;"))) {
					die('Erreur : ' . mysql_error());
				}
				if (!($requete=mysql_query("UPDATE `variables` SET `texte` =  '$nom_site' WHERE `variable`= 'nom' LIMIT 1 ;"))) {
					die('Erreur : ' . mysql_error());
				}
				if (!($requete=mysql_query("UPDATE `variables` SET `texte` =  '$site' WHERE `variable`= 'site' LIMIT 1 ;"))) {
					die('Erreur : ' . mysql_error());
				}
				if (!($requete=mysql_query("UPDATE `variables` SET `texte` =  '$monnaie' WHERE `variable`= 'monnaie' LIMIT 1 ;"))) {
					die('Erreur : ' . mysql_error());
				}
				if (!($requete=mysql_query("UPDATE `variables` SET `texte` =  '$annee' WHERE `variable`= 'annee' LIMIT 1 ;"))) {
					die('Erreur : ' . mysql_error());
				}
				if (!($requete=mysql_query("UPDATE `variables` SET `texte` =  '$ville' WHERE `variable`= 'ville' LIMIT 1 ;"))) {
					die('Erreur : ' . mysql_error());
				}
				if (!($requete=mysql_query("UPDATE `variables` SET `texte` =  '$cotisation' WHERE `variable`= 'cotisation' LIMIT 1 ;"))) {
					die('Erreur : ' . mysql_error());
				}
				if (!($requete=mysql_query("UPDATE `variables` SET `texte` =  '$tempo' WHERE `variable`= 'tempo' LIMIT 1 ;"))) {
					die('Erreur : ' . mysql_error());
				}
				if (!($requete=mysql_query("UPDATE `variables` SET `texte` =  '$tempo2' WHERE `variable`= 'tempo2' LIMIT 1 ;"))) {
					die('Erreur : ' . mysql_error());
				}
				if (!($requete=mysql_query("UPDATE `variables` SET `texte` =  '$mdpuniv' WHERE `variable`= 'mdp' LIMIT 1 ;"))) {
					die('Erreur : ' . mysql_error());
				}
				if (!($requete=mysql_query("UPDATE `variables` SET `texte` =  '$majauto' WHERE `variable`= 'majauto' LIMIT 1 ;"))) {
					die('Erreur : ' . mysql_error());
				}
				if (!($requete=mysql_query("UPDATE `variables` SET `texte` =  '$echauto' WHERE `variable`= 'echauto' LIMIT 1 ;"))) {
					die('Erreur : ' . mysql_error());
				}
				if (!($requete=mysql_query("UPDATE `variables` SET `texte` =  '$messagebienvenu' WHERE `variable`= 'messagebienvenu' LIMIT 1 ;"))) {
					die('Erreur : ' . mysql_error());
				}
				if (!($requete=mysql_query("UPDATE `variables` SET `texte` =  '$affsolde' WHERE `variable`= 'affsolde' LIMIT 1 ;"))) {
					die('Erreur : ' . mysql_error());
				}
				if (!($requete=mysql_query("UPDATE `variables` SET `texte` =  '$affnom' WHERE `variable`= 'affnom' LIMIT 1 ;"))) {
					die('Erreur : ' . mysql_error());
				}
				
				echo "<p  class='titre'>Les nouveaux paramètres sont mis à jour merci.</p>";
			}
			else
			{ 	//pirate
				header ("location:404.php");
				session_destroy();			
			}
		}
		//fin edition
		echo "<br></div>";
	} 
	//delai depassé
	else
	{
		header ("location:troptard.php");
		session_destroy();
	}
}
// pas de sesion
else
{
	header ("location:404.php");
	session_destroy();
}
mysql_close($connexion); 
include ("fin.php");	
?>
