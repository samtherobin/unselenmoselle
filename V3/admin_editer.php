<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/

if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
{								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
	 	switch ($grade)
		{
			case 'SELISTE' : header("location:404.php");break;				
			case 'MODERATEUR' :header("location:404.php");break;		
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		echo "<br><div class='corps'><br><p class='titre'>Page réservé aux administrateurs.</p>";
		// Debut de la page
		// interdit au moderateur permet d'editer les parametres du profil
		//debut de l'edition
		$id_sel=nl2br(htmlentities($_GET['id'], ENT_QUOTES, "UTF-8"));
		if($_GET['action']=='editer') //on enregistre les modifs
		{	
			if (!($requete=mysql_query("SELECT * FROM `selistes` WHERE `id_seliste`='$id_sel'"))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete) ;
			$email=stripslashes($ligne[1]);
			$grade=$ligne[2];
			$prenom=stripslashes($ligne[5]);
			$nom=stripslashes($ligne[6]);
			$sexe=$ligne[7];
			$reinscription=stripslashes($ligne[14]);
			$mdp=stripslashes($ligne[15]);
			$valide=stripslashes($ligne[16]);
			$mail_multi=stripslashes($ligne[21]);
			$mail_dem=stripslashes($ligne[22]);
			$mail_mp=stripslashes($ligne[23]);
			$mail_actu=stripslashes($ligne[24]);
			$mail_age=stripslashes($ligne[32]);
			$mail_pet=stripslashes($ligne[33]);
			$theme=stripslashes($ligne[29]);
			echo  "
			<form method=\"post\" action=\"admin_editer.php?action=enregistre&amp;id=$id_sel\" enctype=\"multipart/form-data\">
			<table border='0' width='100%' cellpadding='4'>
			<tr>
			<td colspan='2' bgcolor='#FF0000' class='t1'><p>Administration du profil. <br> Attention vos changements doivent être en accord avec la Base de données!<br>(La limite de caractère est la même sur la bdd)</p></td>
			</tr>
			<tr valign=top>
			<td><p class='t3'><b>Grade:</b></p></td>
			<td class='t2'>
				<SELECT name='grade'>
						<OPTION value='".$grade."'>".$grade."</option>
						<OPTION value='SELISTE'>SELISTE</option>
						<OPTION value='MODERATEUR'>MODERATEUR</option>
						<OPTION value='ADMIN'>ADMINISTRATEUR</option>
				</SELECT>
			</td>
			</tr>
			<tr>
			<td><p class='t3'><b>Cotisation ou validation (Année en cours d'après la base: ".$annee.":</b></p></td>
			<td><p class='t2'>Pour l'annee :  20<input name='reinscription' value='".$reinscription."' size=2 maxLength=2></td>
			</tr>
			<tr>
			<td><p class='t3'><b>Validation du profil:</b><br>Peut-il se connecter?</p></td>
			<td><p class='t2'><select name='valide'>
			<option value='".$valide."' >".$valide."</option>
			<option value='OUI' >OUI</option>
			<option value='NON' >NON</option>
			</select></td>			
			</tr>
			<tr>
			<td><p class='t3'><b>Courriel lors d'envoi de Multi message (Mail_multi):</b></p></td>
			<td class='t2'><select name='mail_multi'>
			<option value='".$mail_multi."' >".$mail_multi."</option>
			<option value='OUI' >OUI</option>
			<option value='NON' >NON</option>
			</select></td>			
			</tr>
			<tr>
			<td><p class='t3'><b>Courriel lors d'envoi de Message perso (Mail_mp):</b></p></td>
			<td class='t2'><select name='mail_mp'>
			<option value='".$mail_mp."' >".$mail_mp."</option>
			<option value='OUI' >OUI</option>
			<option value='NON' >NON</option>
			</select></td>			
			</tr>
			<tr>
			<td><p class='t3'><b>Courriel lors de demande/proposition de services (Mail_dem):</b></p></td>
			<td class='t2'><select name='mail_dem'>
			<option value='".$mail_dem."' >".$mail_dem."</option>
			<option value='OUI' >OUI</option>
			<option value='NON' >NON</option>
			</select></td>			
			</tr>
			<tr>
			<td><p class='t3'><b>Courriel lors d'Evenement agenda (Mail_age):</b></p></td>
			<td class='t2'><select name='mail_age'>
			<option value='".$mail_age."' >".$mail_age."</option>
			<option value='OUI' >OUI</option>
			<option value='NON' >NON</option>
			</select></td>			
			</tr>
			<tr>
			<td><p class='t3'><b>Courriel lors d'Actualité et CR (Mail_actu):</b></p></td>
			<td class='t2'><select name='mail_actu'>
			<option value='".$mail_actu."' >".$mail_actu."</option>
			<option value='OUI' >OUI</option>
			<option value='NON' >NON</option>
			</select></td>			
			</tr>
			<tr>
			<td><p class='t3'><b>Courriel lors de petite annonce (Mail_pet):</b></p></td>
			<td class='t2'><select name='mail_pet'>
			<option value='".$mail_pet."' >".$mail_pet."</option>
			<option value='OUI' >OUI</option>
			<option value='NON' >NON</option>
			</select></td>			
			</tr>
			<tr>
			<td><p class='t3'><b>Theme:</b></p></td>
			<td class='t2'><input name='theme' value='".$theme."' size=3 maxLength=3></td>
			</tr>
			<tr>
			<td><p class='t3'><b>Mdp codé en md5:</b></p></td>
			<td class='t2'><input name='mdp' value='".$mdp."' size=50 maxLength=50></td>
			</tr>
			<tr>
			<td><p class='t3'><b>Email:</b></p></td>
			<td class='t2'><input name='email' value='".$email."' size=30 maxLength=50></td>
			</tr>
			<tr>
			<td><p class='t3'><b>Prenom:</b></p></td>
			<td class='t2'><input name='prenom' value='".$prenom."' size=10 maxLength=40></td>
			</tr>
			<tr>
			<td><p class='t3'><b>Nom:</b></p></td>
			<td class='t2'><input name='nom' value='".$nom."' size=10 maxLength=40></td>
			</tr>
			<tr>
			<td><p class='t3'><b>Genre:</b></p></td>
			<td class='t2'><select name='sexe'>
			<option value='".$sexe."' >".$sexe."</option>
			<option value='Femme' >Femme</option>
			<option value='Homme' >Homme</option>
			<option value='Couple' >Couple</option>
			</select></td>			
			</tr>
			<tr>
			<td colspan='3' class='1'><p><input type='submit' value=' Sauvegarder ' ></p></td>
			</tr>
			</table>			
			</form><br></div><br>";
		}
		else //on enregistre
		{
			if($_GET['action']=='enregistre')
			{ //ok pour enregistre
				$grade=addslashes(htmlentities($_POST['grade'], ENT_QUOTES, "UTF-8"));
				$reinscription=addslashes(htmlentities($_POST['reinscription'], ENT_QUOTES, "UTF-8"));
				$valide=addslashes(htmlentities($_POST['valide'], ENT_QUOTES, "UTF-8"));
				$mail_mp=addslashes(htmlentities($_POST['mail_mp'], ENT_QUOTES, "UTF-8"));
				$mail_multi=addslashes(htmlentities($_POST['mail_multi'], ENT_QUOTES, "UTF-8"));
				$mail_age=addslashes(htmlentities($_POST['mail_age'], ENT_QUOTES, "UTF-8"));
				$mail_dem=addslashes(htmlentities($_POST['mail_dem'], ENT_QUOTES, "UTF-8"));
				$mail_actu=addslashes(htmlentities($_POST['mail_actu'], ENT_QUOTES, "UTF-8"));
				$mail_pet=addslashes(htmlentities($_POST['mail_pet'], ENT_QUOTES, "UTF-8"));
				$theme=addslashes(htmlentities($_POST['theme'], ENT_QUOTES, "UTF-8"));
				$mdp=addslashes(htmlentities($_POST['mdp'], ENT_QUOTES, "UTF-8"));
				$email=addslashes(htmlentities($_POST['email'], ENT_QUOTES, "UTF-8"));
				$prenom=addslashes(htmlentities($_POST['prenom'], ENT_QUOTES, "UTF-8"));
				$nom=addslashes(htmlentities($_POST['nom'], ENT_QUOTES, "UTF-8"));
				$sexe=addslashes(htmlentities($_POST['sexe'], ENT_QUOTES, "UTF-8"));
				if (!($requete=mysql_query("UPDATE `selistes` SET `grade` = '$grade', `reinscription` = '$reinscription', `valide` = '$valide', `mail_mp` = '$mail_mp', `mail_dem` = '$mail_dem', `mail_age` = '$mail_age', `mail_pet` = '$mail_pet', `mail_multi` = '$mail_multi', `mail_actu` = '$mail_actu', `theme` = '$theme', `mdp` = '$mdp',`email` = '$email',`prenom` = '$prenom', `nom` = '$nom', `sexe` = '$sexe' WHERE `id_seliste` ='$id_sel' LIMIT 1"))) {
					die('Erreur : ' . mysql_error());
				}
				echo "<p> Le profil : <a href='profil.php?id=".$id_sel."'>".$prenom."(".$id_sel.")</a> est mise à jour merci.</p>
						<br>
						<p><a href='bureau.php' title='Bureau'>Retour bureau</a></p></br></div><br>";
			}
			else
			{ 	//pirate
				header ("location:404.php");
				session_destroy();			
			}
		}
		//fin edition
	} 
	//delai depassé
	else
	{
		header ("location:troptard.php");
		session_destroy();
	}
}
// pas de sesion
else
{
	header ("location:404.php");
	session_destroy();
}
mysql_close($connexion); 
include ("fin.php");	
?>
