<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/
if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
{								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
	 	switch ($grade)
		{
			case 'SELISTE' : header("location:404.php");break;				
			case 'MODERATEUR' :header("location:404.php");break;		
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page
		// interdit meme moderateur
		// compte les sessions
		$time48h=$time-48*60*60;
		if (!($requete=mysql_query("SELECT COUNT(*) FROM `session` WHERE  `timestamp` <$time48h"))) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete);
		$nbrsession=$ligne[0];
		// compte des messages prives
		$time3m=$time-30*3*24*60*60;
		if (!($requete=mysql_query("SELECT COUNT(*) FROM `messagerie` WHERE  `rubrique`='EFF' and `time`<$time3m"))) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete);
		$nbrmessages=$ligne[0];
		// compte message de 1an+2mois
		$time1an=$time-30*14*24*60*60;
		if (!($requete=mysql_query("SELECT COUNT(*) FROM `messagerie` WHERE `time`<$time1an and  NOT `rubrique`='SAU'"))) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete);
		$nbrmpancien=$ligne[0];
		// compte mp de desactivé et services a desactivé
		$nbrmpdesactive=0;
		$nbrservices=0;
		if (!($requete=mysql_query("SELECT `id_seliste` FROM `selistes` WHERE `valide`='NON'"))) {
			die('Erreur : ' . mysql_error());
		}
		while($ligne=mysql_fetch_row($requete))
			{
				$id_sel=$ligne[0];
				if (!($requete1=mysql_query("SELECT COUNT(*) FROM `messagerie` WHERE `destinataire`='$id_sel' and  NOT `rubrique`='SAU'"))) {
					die('Erreur : ' . mysql_error());
				}
				$ligne1=mysql_fetch_row($requete1);
				$nbrmpdesactive=$nbrmpdesactive+$ligne1[0];
				if (!($requete1=mysql_query("SELECT COUNT(*) FROM `services` WHERE `id_seliste`='$id_sel'"))) {
					die('Erreur : ' . mysql_error());
				}
				$ligne1=mysql_fetch_row($requete1);
				$nbrservices=$nbrservices+$ligne1[0];
			}	
		echo "<br><br><div class='corps'><br>";
		if ($_GET['action']=='SESSION') 
		{
			if (!($requete=mysql_query("DELETE FROM `session` WHERE  `timestamp` < $time48h"))) {
				die('Erreur : ' . mysql_error());
			}
			if (!($requete=mysql_query("SELECT COUNT(*) FROM `session`"))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete);
			$nbrsessionrest=$ligne[0];
			echo "<div class='message'><br><p class='titre'>Vidage de $nbrsession sessions. Il en reste:$nbrsessionrest de moins de 48h.</p><br></div><br><br>";
		}
		elseif ($_GET['action']=='ANCIEN') 
		{ 
			$i=0;
			if (!($requete=mysql_query("SELECT * FROM `messagerie` WHERE `time`<$time1an and  NOT `rubrique`='SAU'"))) {
				die('Erreur : ' . mysql_error());
			}
			while($ligne=mysql_fetch_row($requete))
			{
				$id_message=$ligne[0];
				if (!($requete2=mysql_query("DELETE FROM `messagerie` WHERE `id_message`='$id_message'"))) {
					die('Erreur : ' . mysql_error());
				}
				$i++;
			}
			echo "<div class='message'><br><p class='titre'>".$i." messages éffacés.</p><br></div><br><br>";
		}
		elseif ($_GET['action']=='DESACTIVE') 
		{ 
			$i=0;
			if (!($requete=mysql_query("SELECT `id_seliste` FROM `selistes` WHERE `valide`='NON'"))) {
				die('Erreur : ' . mysql_error());
			}
			while($ligne=mysql_fetch_row($requete))
			{
				$id_sel=$ligne[0];
				if (!($requete1=mysql_query("SELECT `id_message` FROM `messagerie` WHERE `destinataire`='$id_sel' and  NOT `rubrique`='SAU'"))) {
					die('Erreur : ' . mysql_error());
				}
				while($ligne1=mysql_fetch_row($requete1))
				{
					$id_message=$ligne1[0];
					if (!($requete2=mysql_query("DELETE FROM `messagerie` WHERE `id_message`='$id_message'"))) {
						die('Erreur : ' . mysql_error());
					}
					$i++;
				}				
			}			
			echo "<div class='message'><br><p class='titre'>".$i." messages éffacés.</p><br></div><br><br>";
		}
		elseif ($_GET['action']=='SERVICES') 
		{ 
			if (!($requete=mysql_query("SELECT `id_seliste` FROM `selistes` WHERE `valide`='NON'"))) {
				die('Erreur : ' . mysql_error());
			}
			while($ligne=mysql_fetch_row($requete))
			{
				$id_sel=$ligne[0];
				if (!($requete2=mysql_query("DELETE FROM `services` WHERE `id_seliste`='$id_sel'"))) {
					die('Erreur : ' . mysql_error());
				}				
			}			
			echo "<div class='message'><br><p class='titre'>".$nbrservices."\" services éffacés.</p><br></div><br><br>";
		}
		elseif ($_GET['action']=='MESSAGES') 
		{ 
			if (!($requete=mysql_query("DELETE FROM `messagerie` WHERE  `rubrique`='EFF' and `time`<$time3m"))) {
				die('Erreur : ' . mysql_error());
			}
			if (!($requete=mysql_query("SELECT COUNT(*) FROM `messagerie` WHERE  `rubrique`='EFF'"))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete);
			$nbrmessagesrest=$ligne[0];
			echo "<div class='message'><br><p class='titre'>".$nbrmessages." messages éffacés. Il en reste:$nbrmessagesrest mais dans pas dans le délai!</p><br></div><br><br>";
		}
		elseif ($_GET['action']=='ANNIVERSAIRE') 
		{ // calcul des date anniversaire en liaison avec age
		
			echo "<table class='tablevu' width=100% border=1><tr><td colspan=5><p class='t1'>Analyse entre la date de naissance et l'age.</p></td></tr>
			<tr><th><p>Numéro Séliste</p></th><th><p>Age dans la table</p></th><th><p>Difference</p></th><th><p>Age calculé selon date</p></th><th><p>Date</p></th></tr>";
			if (!($requete=mysql_query("SELECT `id_seliste`, `prenom`, `valide`, `age`, `nee` FROM `selistes`"))) {
				die('Erreur : ' . mysql_error());
			}
			while($ligne=mysql_fetch_row($requete))
			{
				$id_sel=$ligne[0];
				$prenom=$ligne[1];
				$valide=$ligne[2];
				$age=$ligne[3];
				$nee=$ligne[4];
				
				if ($nee!=null)
				{
					// recup date mois jour de date de naissance
					$nee=stripslashes($nee);
					$jour=substr($nee,0,2) ;
					$mois=substr($nee,3,2) ;
					$annee=substr($nee,6,4) ;
					//$timenee = mktime(0, 0, 0, $mois, $jour, $annee);
					$anneeactuel= date("y",$time);
					$anneeactuel="20".$anneeactuel;
					$moisactuel= date("m",$time);
					if($mois<$moisactuel)
					{ // date d'anniversaire deja passé
						$agereel=$anneeactuel-$annee;
					}
					else
					{ // date d'anniv pas encore passé
						$agereel=$anneeactuel-$annee-1;
					}
					$agedifference=$agereel-$age;
					if(($agereel<"120")&&($agereel!=$age)&&($agedifference>0))
					{						
						echo "<tr><td class=t2><p><a href=\"profil.php?id=".$id_sel."\">";if($valide!='OUI'){echo "<span class='GRIS'>";}echo "".$prenom."(".$id_sel.")";if($valide!='OUI'){echo "</span>";}echo "</a></p></td><td class='t3'>".$age."</td><td class='t1'>".$agedifference."</td><td class='t3'>".$agereel."</td><td class='t4'>".$nee."</td></tr>";
						// LANCEMENT REQUETE MISE A JOUR //
						if (!($MaJ=mysql_query("UPDATE  `selistes` SET  `age` =  '$agereel' WHERE `id_seliste` =$id_sel LIMIT 1 "))) {
							die('Erreur : ' . mysql_error());
						}
						echo "<tr><td colspan=5> Mis à jour ! </td></tr>";
						//////////////////////////////////
					}
				}
				
			}
			echo "</table>";
			// FIN DU CALCUL date anniversaire
		}
				
			echo "<br>
			<div class='message'><br>
			<p class='titre'>Administration de la base de donnée:</p><br>
			<p class='t1'>Avant toutes manipulations veillez à faire un backup de la base.</p><br>
			<p class='t4'><a class='aadmin' href=\"../Backup/Backup.php\">Créer une sauvegarde SQL de la base (dump).</a></p>
			<br><hr width='70%'>
			<p class='t1'>Nettoyage de la base:</p><br>
			<p class='t4'><a class='aadmin' href=\"admin_BDD.php?action=SESSION\">Vider les $nbrsession sessions > 48h</a>&nbsp;&nbsp;
			<a class='aadmin' href=\"admin_BDD.php?action=SERVICES\">Supprimer les $nbrservices services des selistes non valide</a></p>
			<br><hr width='70%'><br>
			<p class='t1'>Gestion de la messagerie (ne concerne pas les messages sauvegardés)</p><br>
			<p class='t4'><a class='aadmin' href=\"admin_BDD.php?action=DESACTIVE\">Supprimer les $nbrmpdesactive messages de membres désactivés</a>&nbsp;&nbsp;
			<a class='aadmin' href=\"admin_BDD.php?action=MESSAGES\">Supprimer les $nbrmessages messages privés éffacés >3 mois</a>&nbsp;&nbsp;
			<a class='aadmin' href=\"admin_BDD.php?action=ANCIEN\">Supprimer les $nbrmpancien messages privés >1 an</a></p>
			<br><hr width='70%'><br>
			<p class='t1'>Mise à jour date de naissance :</p><br>
			<p class='t4'><a class='aadmin' href=\"admin_BDD.php?action=ANNIVERSAIRE\">Comparaison date de naissance et age + mise à jour des profils</a></p>
			<br></div><br>";
	}
	else
	{ 	 //délai dépassé
		header ("location:troptard.php");
		session_destroy();
	}
 }
else
{ 	 // pas de session
	header ("location:404.php");
	session_destroy();
}
mysql_close($connexion); 
include ("fin.php");	
?>
