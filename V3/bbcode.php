<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/

if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
{								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
	 	switch ($grade)
		{
			case 'SELISTE' : include("lien/seliste.php");break;
			case 'MODERATEUR' :include("lien/seliste.php"); include("lien/moderateur.php");break;		
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		echo "<br><div class=\"corps\"><br>";
		// Debut de la page
		echo "
		<p class='titre'>Les BbCodes:</p>
		<br>
		<table class='tablevu' border=\"1\" width=\"90%\">
			<tr>
				<td class=t1><p class=\"pasimportant\">[id=1] Texte [/id]</p></td>
				<td class=t2> Lien vers une fiche: <a href=profil.php?id=1> Texte </a><br></td>
			</tr>
			<tr>
				<td class=t1><p class=\"pasimportant\">[GRAS] Texte [/GRAS]</td>
				<td class=t2><p> Ecriture en gras: <b> Texte </b></p></td>
			<tr>
			<tr>
				<td class=t1><p class=\"pasimportant\">[ITALIQUE] Texte [/ITALIQUE]</td>
				<td class=t2><p> Ecriture en italique: <i> Texte </i></p></td>
			<tr>
			<tr>
				<td class=t1><p class=\"pasimportant\">[NOIR] Texte [/COULEUR]</td>
				<td class=t2><p> Ecriture en Noir: <span class=\"noir\"> Texte </span></p></td>
			<tr>
							<tr>
				<td class=t1><p class=\"pasimportant\">[GRIS] Texte [/COULEUR]</td>
				<td class=t2><p> Ecriture en Gris: <span class=\"gris\"> Texte </span></p></td>
			<tr>
							<tr>
				<td class=t1><p class=\"pasimportant\">[ROUGE] Texte [/COULEUR]</td>
				<td class=t2><p> Ecriture en Rouge: <span class=\"rouge\"> Texte </span></p></td>
			<tr>
							<tr>
				<td class=t1><p class=\"pasimportant\">[VERT] Texte [/COULEUR]</td>
				<td class=t2><p> Ecriture en Vert: <span class=\"vert\"> Texte </span></p></td>
			<tr>
							<tr>
				<td class=t1><p class=\"pasimportant\">[JAUNE] Texte [/COULEUR]</td>
				<td class=t2><p> Ecriture en Jaune: <span class=\"jaune\"> Texte </span></p></td>
			<tr>
			<tr>
				<td class=t1><p class=\"pasimportant\">[BLANC] Texte [/COULEUR]</td>
				<td class=t2><p> Ecriture en Blanc: <span class=\"blanc\"> Texte </span></p></td>
			<tr>
			<tr>
				<td class=t1><p class=\"pasimportant\">[BLEU] Texte [/COULEUR]</td>
				<td class=t2><p> Ecriture en Bleu: <span class=\"bleu\"> Texte </span></p></td>
			<tr>
			</table>
		
		<br><br>
		<p class='titre'>Les Smileys:</p>
		<br>
		
		<table class='tablevu' border=\"1\" width=\"90%\">
				<tbody><tr class=\"t1\">
				<td><img alt=\"smile\" src=\"smiles/smile1.gif\"></td><td><img alt=\"smile\" src=\"smiles/smile2.gif\"></td><td><img alt=\"smile\" src=\"smiles/smile3.gif\"></td>
			<td><img alt=\"smile\" src=\"smiles/smile4.gif\"></td><td><img alt=\"smile\" src=\"smiles/smile5.gif\"></td><td><img alt=\"smile\" src=\"smiles/smile6.gif\"></td>
			<td><img alt=\"smile\" src=\"smiles/smile7.gif\"></td><td><img alt=\"smile\" src=\"smiles/smile8.gif\"></td><td><img alt=\"smile\" src=\"smiles/smile9.gif\"></td>

				<td><img alt=\"smile\" src=\"smiles/smile10.gif\"></td><td><img alt=\"smile\" src=\"smiles/smile11.gif\"></td><td><img alt=\"smile\" src=\"smiles/smile12.gif\"></td>
			<td><img alt=\"smile\" src=\"smiles/smile13.gif\"></td><td><img alt=\"smile\" src=\"smiles/smile27.gif\"></td><td><img alt=\"smile\" src=\"smiles/smile28.gif\"></td>
			<td><img alt=\"smile\" src=\"smiles/smile36.gif\"></td><td><img alt=\"smile\" src=\"smiles/smile25.gif\"></td>
				</tr>	
				<tr class=\"t2\">
				<td>:)</td><td>:(</td><td>;)</td><td>:D</td><td>:*</td><td>B)</td><td>:o</td><td>:$</td>

				<td>:p</td><td>:x</td><td>(k)</td><td>:-/</td><td>Zzz</td><td>O:]</td><td>(bain)</td><td>(sante)</td>
				<td>(love)</td>
				</tr>
				<tr class=\"t1\">

			<td><img alt=\"smile\" src=\"smiles/smile19.gif\"></td><td><img alt=\"smile\" src=\"smiles/smile20.gif\"></td><td><img alt=\"smile\" src=\"smiles/smile18.gif\"></td>
			<td><img alt=\"smile\" src=\"smiles/smile22.gif\"></td><td><img alt=\"smile\" src=\"smiles/smile23.gif\"></td><td><img alt=\"smile\" src=\"smiles/smile24.gif\"></td>
			<td><img alt=\"smile\" src=\"smiles/smile17.gif\"></td><td><img alt=\"smile\" src=\"smiles/smile16.gif\"></td><td><img alt=\"smile\" src=\"smiles/smile14.gif\"></td>
			<td><img alt=\"smile\" src=\"smiles/smile15.gif\"></td><td><img alt=\"smile\" src=\"smiles/smile29.gif\"></td><td><img alt=\"smile\" src=\"smiles/smile30.gif\"></td>
			<td><img alt=\"smile\" src=\"smiles/smile31.gif\"></td><td><img alt=\"smile\" src=\"smiles/smile34.gif\"></td><td><img alt=\"smile\" src=\"smiles/smile32.gif\"></td>
				<td><img alt=\"smile\" src=\"smiles/smile26.gif\"></td><td><img alt=\"smile\" src=\"smiles/smile21.gif\">
				</td></tr>
				<tr class=\"t2\">
				<td>:F</td><td>:X</td><td>(l)</td><td>:P</td><td>:!</td><td>:i</td>
				<td>;x</td><td>:-*</td><td>8)</td><td>:##</td><td>;(</td><td>:@</td><td>(mur)</td>
				<td>O:}</td><td>(jtm)</td><td>(merci)</td><td>(gne)</td>	
				</tr>	
				</tbody></table><br></div>";
	}
	else
	{ 	 // délai dépassé
		header ("location:troptard.php");
		session_destroy();
	}
 }
else
{ 	 // pas de session
	header ("location:404.php");
	session_destroy();
}
mysql_close($connexion); 
include ("fin.php");	
?>
