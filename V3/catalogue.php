<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/
 
if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
 {								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	//Connexion a la base
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade`, `grains`, `prenom` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		$grains= $ligne[1];	
		$prenom= $ligne[2];		
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);		
	 	switch ($grade)
		{
			case 'SELISTE' : include("lien/seliste.php");break;
			case 'MODERATEUR' :include("lien/seliste.php"); include("lien/moderateur.php");break;
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page
		echo "<br><div class=\"corps\"><br>
		<p class='titre'><b>Catalogue des services disponibles.</b></p><br>
		<p><a href='export.php'>Télécharger le catalogue</a>&nbsp;&nbsp;
		<a href='catalogue2.php'>Catalogue complet</a></p><br>
		
		<p><form method=\"post\" action=\"recherche.php\" enctype=\"multipart/form-data\">
		Recherche dans le catalogue:<br><input type='text' name='catalogue' size='50' maxlength='50' /><br>
		<input type=\"submit\" value=\"Lancer les recherches\">
		</form></p><br>
		
		<table summary=\"\" border=\"1\" width=\"80%\" >
		";
		if (!($requete=mysql_query("SELECT * FROM `catalogue` ORDER BY `designation` ASC"))) {
			die('Erreur : ' . mysql_error());
		}	
		// test si selection de rubrique a derouler
		$id_catget=htmlentities($_GET['id_cat'], ENT_QUOTES, "UTF-8");
		while($ligne=mysql_fetch_row($requete))
		{
			$id_cat= $ligne[0];
			$designation_cat=stripslashes($ligne[1]);
			if($id_catget==$id_cat)
			{
			echo "
			<tr>
			<td colspan=3 class='t2 teinte1'><p class='titre'><a href='catalogue.php'><img src='images/flechebas.gif' width='15'>&nbsp;<span class='bleu'><b>".$designation_cat."</b></span></a></p></td>
			</tr>";			
			}
			else
			{
			echo "
			<tr>
			<td colspan=3 class='t2'><p><a href='catalogue.php?id_cat=$id_cat'><img src='images/flechedroite.gif' width='10'>&nbsp;<b><span class='vert'>".$designation_cat."</span></b></a></p></td>
			</tr>";			
			}			
			if($id_catget==$id_cat)
			{
				//chercher les rubriques
				if (!($requete2=mysql_query("SELECT * FROM `rubrique` WHERE `id_cat`='$id_cat' ORDER BY `designation` ASC"))) {
					die('Erreur : ' . mysql_error());
				}	
				while($ligne2=mysql_fetch_row($requete2))
				{
					$id_rubrique= $ligne2[0];
					$designation_rub=stripslashes($ligne2[2]);
					
					if (!($requete4=mysql_query("SELECT `id_seliste`, `description`, `detail` FROM `services` WHERE `id_rubrique`='$id_rubrique' AND `etat`='PRO' ORDER BY `time` ASC"))) {
					    die("Erreur : " . mysql_error());
					}
					if (!($requete5=mysql_query("SELECT `id_seliste`, `description`, `detail` FROM `services` WHERE `id_rubrique`='$id_rubrique' AND `etat`='DEM' ORDER BY `time` ASC"))) {
					    die("Erreur : " . mysql_error());
					}
					if (mysql_num_rows($requete4)> 0 || mysql_num_rows($requete5)> 0) {
    					echo "
    					<tr>
    					<td colspan=3><p class='t1 teinte2'><b>".$designation_rub." </b></p></td>
    					</tr>
    					<tr>
    					<td class='t1'><p>Offres:</p></td>
    					<td class='t1'><p>Demandes:</p></td>
    					</tr>";
    					
    					echo "
    					<tr>
    					<td class=t3 width=\"33%\">";
    					// affichage des selistes qui propose
    					echo "<table summary=\"\" border=\"0\" width=\"100%\" >";
    					while($ligne4=mysql_fetch_row($requete4))
    					{
    						$id_seliste_pro=$ligne4[0];
    						$description_pro=stripslashes($ligne4[1]);
    						$detail_pro=stripslashes($ligne4[2]);	
if (!(    						$requete6=mysql_query("SELECT `prenom`,`valide` FROM `selistes` WHERE `id_seliste`='$id_seliste_pro'"))) {
	die('Erreur : ' . mysql_error());
}
    						$ligne6=mysql_fetch_row($requete6);
    						$prenom_pro=$ligne6[0];
    						$selistevalide=$ligne6[1];
    						if($selistevalide=='OUI')
    						{
    							if($detail_pro==null)
    							{
    									echo "<tr><td><p><a href='profil.php?id=".$id_seliste_pro."'><span class=bleu>".$prenom_pro."(".$id_seliste_pro.")</span></a></p></td></tr>";
    					
    							}
    							else
    							{
    							echo "<tr><td><p class=important><span class=noir>".$description_pro."</span></p></td>
    							  <td align='right'><p><a href='profil.php?id=".$id_seliste_pro."'><span class=bleu>".$prenom_pro."(".$id_seliste_pro.")</span></a></p></td></tr>
    							<tr><td colspan='2' class='t2'><p class=pasimportant><span class=gris>".$detail_pro."</span></p><hr width=70%></td></tr>";
    							}
    						}
    						else
    						{
    							
    						}
    					}		
    					echo "</table>";
    					echo "</td>
    					<td class=t3 width=\"33%\"><p>";
    					// affichage des seliste qui demande
    					echo "<table summary=\"\" border=\"0\" width=\"100%\" >";
    					while($ligne5=mysql_fetch_row($requete5))
    					{
    						$id_seliste_dem=$ligne5[0];
    						$description_dem=stripslashes($ligne5[1]);
    						$detail_dem=stripslashes($ligne5[2]);
if (!(    						$requete7=mysql_query("SELECT `prenom`,`valide` FROM `selistes` WHERE `id_seliste`='$id_seliste_dem'"))) {
	die('Erreur : ' . mysql_error());
}
    						$ligne7=mysql_fetch_row($requete7);
    						$prenom_dem=$ligne7[0];
    						$selistevalide=$ligne7[1];
    						if($selistevalide=='OUI')
    						{
    							if($detail_pro==null)
    							{
    								echo "<tr><td><p><a href='profil.php?id=".$id_seliste_dem."'><span class=rouge>".$prenom_dem."(".$id_seliste_dem.")</span></a></p></td></tr>";
    							}
    							else
    							{
    								echo "<tr><td><p class=important><span class=noir>".$description_dem."</span></p></td>
    								<td align='right'><p><a href='profil.php?id=".$id_seliste_dem."'><span class=rouge>".$prenom_dem."(".$id_seliste_dem.")</span></a></p></td></tr>
    							<tr><td class='t2' colspan='2'><p class=pasimportant><span class=gris>".$detail_dem."</span></p><hr width=70%></td></tr>";
    							}
    						}
    						else
    						{
    						}
    					}		
    					echo "</table>";
    					echo "</td>
    					</tr>";
					}
				}
			}
		}
		//fin du bureau
		echo "</table><br></div>";
		mysql_close($connexion); 
		include ("fin.php");
		
	}
	else
	{ 	// trop tard
		header ("location:404.php");
		session_destroy();
	}
}
else
{ 	// voleur
	header ("location:404.php");
	session_destroy();
}
?>