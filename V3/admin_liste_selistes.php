<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/

if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
{								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	//Connexion a la base
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
	 	switch ($grade)
		{
			case 'SELISTE' : header("location:404.php");break;	
			case 'MODERATEUR' :include("lien/seliste.php"); include("lien/moderateur.php");break;
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page
		
		//debut du scan
		$scan=$_GET['scan'];
		if($scan=='inactif')
		{
			if (!($requete=mysql_query("SELECT `id_seliste`, `connection`, `prenom`, `tel`, `grains`, `age`, `ville` , `reinscription`, `valide` FROM `selistes` WHERE `valide`='NON' ORDER BY `id_seliste` ASC "))) {
				die('Erreur : ' . mysql_error());
			}
			$texte="Liste des sélistes inactif (#Valide=NON)";
			if (!($requete1=mysql_query("SELECT AVG(`grains`) FROM `selistes` WHERE `valide`='NON' AND `id_seliste`>'5' ") )) {
				die('Erreur : ' . mysql_error());
			}
			if (!($requete2=mysql_query("SELECT AVG(`age`) FROM `selistes` WHERE `valide`='NON' AND `id_seliste`>'5' AND NOT `age`='0' ") )) {
				die('Erreur : ' . mysql_error());
			}
		}
		elseif($scan=='actif')
		{
			if (!($requete=mysql_query("SELECT `id_seliste`, `connection`, `prenom`, `tel`, `grains`, `age`, `ville`, `reinscription`, `valide` FROM `selistes` WHERE `valide`='OUI' ORDER BY `id_seliste` ASC "))) {
				die('Erreur : ' . mysql_error());
			}
			$texte="Liste des sélistes actif (#Valide=OUI)";
			if (!($requete1=mysql_query("SELECT AVG(`grains`) FROM `selistes` WHERE `valide`='OUI' AND `id_seliste`>'5' ") )) {
				die('Erreur : ' . mysql_error());
			}
			if (!($requete2=mysql_query("SELECT AVG(`age`) FROM `selistes` WHERE `valide`='OUI' AND `id_seliste`>'5' AND NOT `age`='0' ") )) {
				die('Erreur : ' . mysql_error());
			}
		}
		elseif($scan=='adesactiver')
		{
			$time2=time();
			$time2=$time2-25920000;
			if (!($requete=mysql_query("SELECT `id_seliste`, `connection`, `prenom`, `tel`, `grains`, `age`, `ville`, `reinscription`, `valide` FROM `selistes` WHERE `valide`='OUI' AND `connection`<$time2 ORDER BY `id_seliste` ASC "))) {
				die('Erreur : ' . mysql_error());
			}
			$texte="Liste des sélistes Actif (#Valide=OUI) et qui ne se connecte plus :(";
			if (!($requete1=mysql_query("SELECT AVG(`grains`) FROM `selistes` WHERE `valide`='OUI' AND `id_seliste`>'5' AND `connection`<$time2 ") )) {
				die('Erreur : ' . mysql_error());
			}
			if (!($requete2=mysql_query("SELECT AVG(`age`) FROM `selistes` WHERE `valide`='OUI' AND `id_seliste`>'5' AND `connection`<$time2 AND NOT `age`='0' ") )) {
				die('Erreur : ' . mysql_error());
			}
		}
		elseif($scan=='cotispaye')
		{
			if (!($requete=mysql_query("SELECT `id_seliste`, `connection`, `prenom`, `tel`, `grains`, `age`, `ville`, `reinscription`, `valide` FROM `selistes` WHERE `reinscription`='$annee' AND `valide`='OUI' ORDER BY `id_seliste` ASC "))) {
				die('Erreur : ' . mysql_error());
			}
			$texte="Liste des sélistes Actif (#Valide=OUI) et Réinscrit/Cotisation payé (#Année=$annee)";
			if (!($requete1=mysql_query("SELECT AVG(`grains`) FROM `selistes` WHERE `valide`='OUI' AND `id_seliste`>'5' AND `reinscription`='$annee' ") )) {
				die('Erreur : ' . mysql_error());
			}
			if (!($requete2=mysql_query("SELECT AVG(`age`) FROM `selistes` WHERE `valide`='OUI' AND `id_seliste`>'5' AND `reinscription`='$annee' AND NOT `age`='0' ") )) {
				die('Erreur : ' . mysql_error());
			}
		}
		elseif($scan=='cotisnonpaye')
		{
			if (!($requete=mysql_query("SELECT `id_seliste`, `connection`, `prenom`, `tel`, `grains`, `age`, `ville`, `reinscription`, `valide`  FROM `selistes` WHERE NOT `reinscription`='$annee' AND `valide`='OUI' ORDER BY `id_seliste` ASC "))) {
				die('Erreur : ' . mysql_error());
			}
			$texte="Liste des sélistes actif (#Valide=OUI) et NON réinscrit/ cotisation non payé (#Année différent de $annee)";
			if (!($requete1=mysql_query("SELECT AVG(`grains`) FROM `selistes` WHERE `valide`='OUI' AND `id_seliste`>'5' AND NOT `reinscription`='$annee' ") )) {
				die('Erreur : ' . mysql_error());
			}
			if (!($requete2=mysql_query("SELECT AVG(`age`) FROM `selistes` WHERE `valide`='OUI' AND `id_seliste`>'5' AND NOT `reinscription`='$annee' AND NOT `age`='0' ") )) {
				die('Erreur : ' . mysql_error());
			}
		}
		else  // actif
		{
			if (!($requete=mysql_query("SELECT `id_seliste`, `connection`, `prenom`, `tel`, `grains`, `age`, `ville`, `reinscription`, `valide` FROM `selistes`  ORDER BY `id_seliste` ASC "))) {
				die('Erreur : ' . mysql_error());
			}
			$texte="Liste complète des sélistes";
			if (!($requete1=mysql_query("SELECT AVG(`grains`) FROM `selistes` WHERE `id_seliste`>'5'") )) {
				die('Erreur : ' . mysql_error());
			}
			if (!($requete2=mysql_query("SELECT AVG(`age`) FROM `selistes` WHERE `id_seliste`>'5' AND NOT `age`='0'") )) {
				die('Erreur : ' . mysql_error());
			}
		}
			
		echo "<br><br><div class='corps'><br><br>
		<div class='message'><br>
		<p class='t4'>
		<a class='amodo' href='admin_liste_selistes.php'>Voir la liste complète</a><br><br>
		<a class='amodo' href='admin_liste_selistes.php?scan=inactif'>Voir que les non actif</a>&nbsp;&nbsp;<a  class='amodo' href='admin_liste_selistes.php?scan=actif'>Voir que les actifs</a>&nbsp;&nbsp;
		<a class='amodo' href='admin_liste_selistes.php?scan=adesactiver'>Voir les non connectés de 300 jours et actif</a><br><br>
		<a class='amodo' href='admin_liste_selistes.php?scan=cotisnonpaye'>Voir les actifs non réinscrit/cotisation non payé #Année < $annee</a>&nbsp;&nbsp;
		<a class='amodo' href='admin_liste_selistes.php?scan=cotispaye'>Voir les actifs réinscrit/cotisation payé #Année=$annee</a></p><br>
		</div><br>
		<p class='titre'>".$texte."</p><br>
		<table summary=\"\" class='tablevu' border='1' width='90%' cellpadding='5'>
		<tr>
			<td class='teinte1'><p class='t1'>Nombre:</p></td>
			<td class='teinte1'><p class='t1'>Pseudo(numéro):</p></td>
			<td class='teinte1'><p class='t1'>Dernière connection:</p></td>
			<td class='teinte1'><p class='t1'>Contact:</p></td>
			<td class='teinte1'><p class='t1'>$monnaie:</p></td>
			<td class='teinte1'><p class='t1'>Numéro de Téléphone:</p></td>
			<td class='teinte1'><p class='t1'>Age:</p></td>
			<td class='teinte1'><p class='t1'>Ville:</p></td>
			<td class='teinte1'><p class='t1'>#annee:</p></td>
			<td class='teinte1'><p class='t1'>#Valide:</p></td>
		</tr>";$nb=0;
		while($ligne=mysql_fetch_row($requete))
		{
			$id_sel= $ligne[0];
			$connection= $ligne[1];
			$prenom_sel=stripslashes($ligne[2]);
			$tel=stripslashes($ligne[3]);
			$grains=stripslashes($ligne[4]);
			$age=stripslashes($ligne[5]);
			$ville=stripslashes($ligne[6]);
			$cotis=stripslashes($ligne[7]);
			$valide=stripslashes($ligne[8]);
			if(($grains<-1500)||($grains>1500))
			{
				$grains="<p class=rouge>".$grains."</p>";
			}
			elseif(($grains<-1000)||($grains>1000))
			{
			 $grains="<p class=bleu>".$grains."</p>";
			}
			elseif(($grains<-500)||($grains>500))
			{
			 $grains="<p class=vert>".$grains."</p>";
			}
			elseif($grains>0)
			{
			 $grains="<p class=jaune>".$grains."</p>";
			}
			elseif($grains<0)
			{
			 $grains="<p class=jaune>".$grains."</p>";
			}
			// recup tps de connection
			$seconde_connect=$time-$connection;
				if ($seconde_connect<180) //3minutes = online 
				{
					$seconde_connect='Connecté';
				}
				elseif($seconde_connect<3600)
				{
					$seconde_connect=floor($seconde_connect/60)."Min";
				}
				elseif($seconde_connect<86400)
				{
					$seconde_connect=floor($seconde_connect/60/60)."H";
				}
				else
				{
					$seconde_connect=floor($seconde_connect/60/60/24);
					if($seconde_connect==1)
					{
						$pluriel="Jour"; 
					} 
					else 
					{
						$pluriel="Jours";
					}
					$seconde_connect=$seconde_connect." ".$pluriel;
				}	
				$nb++;
			echo "<tr>
			<td class='teinte";if($valide=='OUI'){echo "2";}else{echo "3";}echo"'><p class='t4'>$nb</td>
			<td class='teinte";if($valide=='OUI'){echo "2";}else{echo "3";}echo"'><p class='t2'><a href=\"profil.php?id=".$id_sel."\">".$prenom_sel."(".$id_sel.")</a></p></td>
			<td class='teinte";if($valide=='OUI'){echo "2";}else{echo "3";}echo"'><p class='t4'>".$seconde_connect."</p></td>
			<td><p class='t4'><a class='aphoto' href=\"messagerie.php?action=ecrire&amp;id=".$id_sel."\"><img src=\"images/lettreNormal.jpg\" alt=\"lettre\"></a></p></td>
			<td><p class='t4'>".$grains."</td>
			<td><p class='t2'>".$tel."</p></td>
			<td><p class='t4'>";if($age!=0){ echo $age." ans";}echo "</p></td>
			<td><p class='t2'>".$ville."</p></td>
			<td class='teinte";if($valide=='OUI'){echo "2";}else{echo "3";}echo"'><p class='t1'>".$cotis."</p></td>
			<td class='teinte";if($valide=='OUI'){echo "2";}else{echo "3";}echo"'><p class='t1'>".$valide."</p></td>
			</tr>";
		}
		
		
		$ligne=mysql_fetch_row($requete1) ;
		$grainmoy= round($ligne[0],0);
		$ligne=mysql_fetch_row($requete2) ;
		$agemoy= round($ligne[0],0);
		echo "<tr><td class='teinte1'><p class=t1>$nb sélistes</p></td>
		<td class='teinte1' colspan='3'><p class=t1>$monnaie moyen de la liste (sans compte 1 à 5):</p></td>
		<td class='teinte1'><p class=t1>$grainmoy</p></td>
		<td class='teinte1' colspan='2'><p class=t1>Age Moyen de la liste:</p></td>
		<td class='teinte1'><p class=t1>$agemoy ans</p></td>
		<td colspan='2'><p>&nbsp;</p></td></tr></table></br></div>";
		
	}
	else
	{ 	 // délai dépassé
		header ("location:troptard.php");
		session_destroy();
	}
 }
else
{ 	 // pas de session
	header ("location:404.php");
	session_destroy();
}
mysql_close($connexion);
include ("fin.php");	
?>
