<?php  
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  */

if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
 {								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade`, `nbr_cha` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		$nbr_cha=$ligne[1];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
		switch ($grade)
		{
			case 'SELISTE' : include("lien/seliste.php");break;
			case 'MODERATEUR' :include("lien/seliste.php"); include("lien/moderateur.php");break;
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page
		echo "<br><div class=\"corps\"><br>";
		if(($grade=='ADMIN')&&($_GET['id']!=NULL))
		{
			echo "<br><p class=titre>Message supprimé</p><br>";		
			$id_message=$_GET['id'];
			if (!($requete=mysql_query("SELECT * FROM `chat` WHERE `id_message`='$id_message'"))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete);
			$id_action=$ligne[1];
			$time=$ligne[2];
			$message=$ligne[3];
			if (!(mysql_query("DELETE FROM `chat` WHERE `id_message`=$id_message LIMIT 1"))) {
				die('Erreur : ' . mysql_error());
			}  
		}
			//debut du chat
			if (isset($_POST['message'])) // Si les variables existent
			{
				if ($_POST['message']!= NULL) // Si on a quelque chose à enregistrer
					{
						$message= nl2br(htmlentities($_POST['message'], ENT_QUOTES, "UTF-8"));
						if (!(mysql_query("INSERT INTO `chat` VALUES ('', '$id_seliste', '$time', '$message')"))) {
							die('Erreur : ' . mysql_error());
						}	
						$nbr_cha++;
						if (!(mysql_query("UPDATE `selistes` SET `nbr_cha`='$nbr_cha' WHERE `id_seliste`='$id_seliste' LIMIT 1"))) {
							die('Erreur : ' . mysql_error());
						}
						if (!($requete=mysql_query("SELECT * FROM `chat` "))) {
							die('Erreur : ' . mysql_error());
						}	
						$nb2=mysql_num_rows($requete);
						$id_max=0;
						if($nb2>40)
						{
							if (!($ligne=mysql_fetch_row(mysql_query("SELECT MIN(id_message) FROM `chat`")))) {
								die('Erreur : ' . mysql_error());
							} 
							$id_max=$ligne[0];
							if (!(mysql_query("DELETE FROM `chat` WHERE `id_message`=$id_max LIMIT 1"))) {
								die('Erreur : ' . mysql_error());
							}  
						}
					}
			}
			echo "
			<center><SCRIPT type=\"text/javascript\">
			function reste()
			{
				document.getElementById('formu').message.value = document.getElementById('formu').message.value.substring(0,500);
				document.getElementById('formu').restant.value = 500 - document.getElementById('formu').message.value.length;
			}
			</SCRIPT>
			<form id=\"formu\" action=\"chat.php\" method=\"post\">
			<p class=\"titre\">Le chat:</p>
			<br><p class='pasimportant'><a href='bbcode.php'>BBcode activé!</a></p>			
			<p><textarea name=\"message\" cols=\"80\" rows=\"6\" onKeyUp=\"reste()\"></textarea></p>			
			<p>Limite: 500 caractères &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Attention à vos propos!</p>
			<p>Nombre de caractères restants: <input readonly type=\"text\" name=\"restant\" onBlur=\"reste()\" onChange=\"reste()\" onFocus=\"reste()\" onSelect=\"reste()\" size=3 value=500></p><br>
			<input type='submit' value='Envoyer mon message au chat' /><br><br><img src='images/chat.gif' width=80><br>";
			//affichage des messages
			if (!($requete=mysql_query("SELECT * FROM `chat` ORDER BY id_message DESC LIMIT 0 , 40 "))) {
				die('Erreur : ' . mysql_error());
			}	
			while ($donnees = mysql_fetch_array($requete) )
			{
				$id_message=$donnees['id_message'];
				$id_sel=$donnees['id_seliste'];
				if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`=$id_sel")))) {
					die('Erreur : ' . mysql_error());
				} 
				$prenom_sel=$recup[0];		
				$date=date('d/m/y à H\h i\m\i\n',$donnees['timestamp']);
				$message=stripslashes($donnees['message']);
				include("smileyscompagnie.php");
				echo "<div class='message'><p><b>Message de <a href=\"profil.php?id=".$id_sel."\">".$prenom_sel."(".$id_sel.")</a> le ".$date."</b>";if($grade=='ADMIN'){echo "&nbsp;<a class='aadmin' href='chat.php?id=".$id_message."'>Supprimer ce message </a>";}echo "</p><hr width=50%><p>".$message."</p></div><br>";
			}
			echo "</table></form><p>$nb2-$id_max</p></div>";
	}
	else
	{ 	 // délai dépassé
		header ("location:troptard.php");
		session_destroy();
	}
 }
else
{ 	 // pas de session
	header ("location:404.php");
	session_destroy();
}
mysql_close($connexion); 
echo "</center>";
include ("fin.php");	
?>
