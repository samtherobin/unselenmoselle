<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/
if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
{								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade`, `nbr_art` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		$nbr_art=$ligne[1];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
		switch ($grade)
		{
			case 'SELISTE' : header("location:404.php");break;
			case 'MODERATEUR' :include("lien/seliste.php"); include("lien/moderateur.php");break;
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page
		echo "<br><br><div class='corps'><br><p class='titre'>Inscripton au SEL de ".$nom.": </p><br><br>";
			//debut de la page d'invit
			$action=htmlentities($_GET['action'], ENT_QUOTES, "UTF-8");
			if($action=='envoye')
			{
				$email=htmlentities($_POST['email'], ENT_QUOTES, "UTF-8");
				$pseudo_inv=htmlentities($_POST['pseudo'], ENT_QUOTES, "UTF-8");
				$internaute=htmlentities($_POST['internaute'], ENT_QUOTES, "UTF-8");
				$codealea=rand(10000,30000);
				$sexe=htmlentities($_POST['sexe'], ENT_QUOTES, "UTF-8");				
				$mdp=md5($codealea);
				//Requete pour verifié si il est deja inscrit
				if (!($requete=mysql_query("SELECT `email` FROM `selistes` WHERE `email`='$email'"))) {
					die('Erreur : ' . mysql_error());
				}
				$nb=0;
				$nb=mysql_num_rows($requete);
				if($nb==1) //Si une ligne alors il est inscrit !!
				{
					if (!($requete=mysql_query("SELECT `id_seliste`, `prenom` FROM `selistes` WHERE `email`='$email'"))) {
						die('Erreur : ' . mysql_error());
					}
					$ligne=mysql_fetch_row($requete) ;
					$id_ins= $ligne[0];
					$pseudo_ins= stripslashes($ligne[1]);
					echo "<div class='message'><p class='t1'>Le séliste est déjà inscrit <a href=profil.php?id=".$id_ins.">".$pseudo_ins."(".$id_ins.")</a></p></div><br><br>" ;
				}	 
				else	  // Sinon c'est qu'il est pas inscrit :)
				{
					// rapatriement anneee de validation
					if (!($annee=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'annee'")))) {
						die('Erreur : ' . mysql_error());
					}
					$annee=stripslashes($annee[0]);
					if (!($messageBienvenue=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'messagebienvenu'")))) {
						die('Erreur : ' . mysql_error());
					}
					$messageBienvenue=stripslashes($messageBienvenue[0]);
					// Requete pour inserer le citoyen
					if($internaute=="oui")
					{
						if (!(mysql_query("INSERT INTO `selistes` VALUES ('', '$email', 'SELISTE', '$time', '0', '$pseudo_inv', '', '$sexe', '', '00000', '', 'France', '', '$time', '$annee', '$mdp', 'NON', '', '0', '0', '0' , 'OUI' , 'OUI' , 'OUI' , 'OUI' , '', '', '', '','','','$time','OUI','OUI','$time')"))) {
							die('Erreur : ' . mysql_error());
						}
						//envoi du mail
						$headers = "MIME-Version: 1.0\n";
						$headers .= "From: Inscription $nom <$email>\n";
						$headers .= "Reply-To: $nom <$email>\n";
						$headers .= "X-Sender: $nom <$email>\n";
						$headers .= "X-Author: Inscription $nom\n";
						$headers .= "X-Priority:1\n";
						$headers .= "X-Mailer: PHP\n";
						$headers .= "Return_Path: <$email>\n";
						$headers .= "Content-Type: text/html; charset='UTF-8'\n";				
						$message ='<html><head><title></title></head><body><p>Bonjour '.$pseudo_inv.', nous te souhaitons la bienvenue au S.E.L de '.$nom.'! <br/> Valide définitivement ton inscription et commence tes échanges en tapant le code : '.$codealea.'<br>sur la page de validation  d\'inscription: <a href=http://'.$site.'/validation.php?email='.$email.'&amp;code='.$codealea.'> http://'.$site.'/validation.php</a></p>
						<p>'.$messageBienvenue.'<br>L\'équipe d\'inscription.</p></body></html>';
						sw_mail($email,'['.$nom.'] Inscription sur '.$site.' Le sel de '.$ville.'',$message,$headers);
						if (!($recherche=mysql_query("SELECT  `id_seliste` ,  `email` ,  `prenom` FROM  `selistes` ORDER BY `id_seliste` DESC LIMIT 1"))) {
							die('Erreur : ' . mysql_error());
						}
						$ligne=mysql_fetch_row($recherche) ;
						$id_ins= $ligne[0];
						$email_ins= $ligne[1];
						$pseudo_ins= stripslashes($ligne[2]);
						echo "<div class='message'><p class='t1'>Bienvenue à  ".$pseudo_ins."(".$id_ins.")! </p><br><p class='t4'>
						Un courriel vient de lui être envoyé avec le code ".$codealea." pour la validation de son courriel (".$email_ins.").</p><br></div><br><br>" ;
					}
					else
					{
						if (!(mysql_query("INSERT INTO `selistes` VALUES ('', '$email', 'SELISTE', '$time', '0', '$pseudo_inv', '', '$sexe', '', '00000', '', 'France', '', '$time', '$annee', '$mdp', 'OUI', '', '0', '0', '0' , 'NON' , 'NON' , 'NON' , 'NON' , '', '', '', '','','','$time','NON','NON','$time')"))) {
							die('Erreur : ' . mysql_error());
						}
						if (!($recherche=mysql_query("SELECT  `id_seliste` ,  `email` ,  `prenom` FROM  `selistes` ORDER BY `id_seliste` DESC LIMIT 1"))) {
							die('Erreur : ' . mysql_error());
						}
						$ligne=mysql_fetch_row($recherche) ;
						$id_ins= $ligne[0];
						$email_ins= $ligne[1];
						$pseudo_ins= stripslashes($ligne[2]);
						echo "<div class='message'><p class='t1'>Bienvenue à  ".$pseudo_ins."(".$id_ins.")! </p><br><p class='t4'>
						voici son mot de passe ".$codealea."</p><br></div><br><br>" ;
					}
				}
			}
			
				echo "<div class='message'><br>
				<form method=\"post\" action=\"inscription.php?action=envoye\" enctype=\"multipart/form-data\">
				<p class='t1'>Création de compte:</p>
				<p class='t2'>Courriel: <input type='text' name='email' size='50' maxlength='100' />(ou pseudo pour les non internaute)<br>
				Prénom: <input type='text' name='pseudo' size='30' maxlength='50'/><br>
				Sexe: <input type=\"radio\" name=\"sexe\" value=\"H\" checked  />Masculin
				<input type=\"radio\" name=\"sexe\" value=\"F\"  />Féminin 
				<input type=\"radio\" name=\"sexe\" value=\"C\"  />Couple </p>
				<p class='t2'>Internaute: <input type=\"radio\" name=\"internaute\" value=\"oui\" checked  />Oui
				<input type=\"radio\" name=\"internaute\" value=\"non\"  />Non 
				<p class='t4'><input type=\"submit\" value=\"Pré-inscription\"></p></form><br></div><br></div>"; 
			
	}					
	else
	{ 	 //délai dépassé
		header ("location:troptard.php");
		session_destroy();
	}
}
else
{ 	 // pas de session
	header ("location:404.php");
	session_destroy();
}
mysql_close($connexion); 
include ("fin.php");	
?>
