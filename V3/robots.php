<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/

if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
{								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
		switch ($grade)
		{
			case 'SELISTE' : include("lien/seliste.php");break;	
			case 'MODERATEUR' :include("lien/seliste.php"); include("lien/moderateur.php");break;
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page
		echo "<br><div class=\"corps\"><br>";
		$action=$_GET['action'];
		// voir tout les anges
		echo "<table summary=\"\" width=\"90%\" border=\"0\">
			<tr>
			<td colspan='6'><p class='titre'>Les outils à votre service.</p></td>
			</tr>
			<tr>
				<td width=\"20%\" class='t1'>
					<br><a href='recherche.php'>Recherche</a><br><br>
					<img src=\"images/loupe.jpg\" alt=\"loupe\" height=50px><br>
				</td>
				<td width=\"20%\" class='t1'>
					<br><a href='signature.php'>Signature</a><br><br>
					<img src=\"images/signature.jpg\" alt=\"\" height=50px><br>
				</td>
				<td width=\"20%\" class='t1'>
					<br><a href='cadeau.php'>Cadeaux</a><br><br>
					<img src=\"images/cadeau.jpg\" alt=\"\" height=50px><br>
				</td>
				<td width=\"20%\" class='t1'>
					<br><a href='themes.php'>Thèmes</a><br><br>
						<img src=\"images/theme.jpg\"  alt=\"image\" height=50px><br>
				</td>
				<td width=\"20%\" class='t1'>
					<br><a href='bbcode.php'>Smileys/BBcodes</a><br><br>
					<img src=\"smiles/smile37.gif\" alt=\"\" height=50px><br>
				</td>
			</tr>
			<tr>
				<td width=\"20%\" class='t1'>
					<br><a href='changemdp.php'>Mot de passe</a><br><br>
					<img src=\"images/mdp.jpg\" alt=\"\" height=50px><br>
				</td>
				<td width=\"20%\" class='t1'>
					<br><a href='compte.php'>Notifications</a><br><br>
					<img src=\"images/compte.jpg\" height=50px alt=\"\"><br>
				</td>
				<td width=\"20%\" class='t1'>
					<br><a href=\"amis.php?action=lire\">Liste amis</a><br><br>
					<img src=\"images/amis.jpg\" height=50px alt=\"\"><br>
				</td>
				<td width=\"20%\" class='t1'>
					<br><a href=\"liste_serv.php\">Liste des services</a><br><br>
					<img src=\"images/liste.jpg\" height=50px alt=\"\"><br>
				</td>
				<td width=\"20%\" class='t1'>
					<br><a href=\"liste_selistes.php\">Liste des selistes</a><br><br>
					<img src=\"images/liste_sel.jpg\" height=50px alt=\"\"><br>
				</td>
				</tr>
				<tr>
				<td width=\"20%\" class='t1'>
					<br><a href='help.php'>Aide</a><br><br>
					<img src=\"images/faq.jpg\" height=50px alt=\"\"><br>
				</td>
				<td width=\"20%\" class='t1'>
					<br><a href='serveur.php'>Serveur</a><br><br>
					<img src=\"images/serveur.gif\" height=50px alt=\"\"><br>
				</td>
				<td width=\"20%\" class='t1'>
					<br><a href='ba.php'>Bonnes Adresses</a><br><br>
					<img src=\"images/adresse.jpg\" height=50px alt=\"\"><br>
					</td>
				<td width=\"20%\" class='t1'>
					<br><a href='listeechanges.php'>Liste des échanges</a><br><br>
					<img src=\"images/listeech.jpg\" height=50px alt=\"\"><br>
					</td>
				<td width=\"20%\" class='t1'>
					<br><a href=\"statistique.php\">Statistiques</a><br><br>
					<img src=\"images/stat.jpg\" height=50px alt=\"\"><br>
					</td>				
				</tr>
				</table></div>";	
	} 
	else
	{ 	//délai dépassé
		header ("location:troptard.php");
		session_destroy();
	}
}
else
{ 	// voleur
	header ("location:404.php");
	session_destroy();
}
mysql_close($connexion);
include ("fin.php");	
?>
