<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/
 
if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
{								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
	 	switch ($grade)
		{
			case 'SELISTE' : header("location:404.php");break;				
			case 'MODERATEUR' :header("location:404.php");break;		
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page
		echo "<br><br><div class='corps'><br>";
		// interdit meme moderateur
		if (($_GET['action2']=='sup')&&($_GET['id_log']!=null))
		{
			if($id_seliste=='1')
			{
				$id_log=$_GET['id_log'];
				if (!($requete=mysql_query("DELETE FROM `log` WHERE `id_log`='$id_log' LIMIT 1"))) {
					die('Erreur : ' . mysql_error());
				}
				echo "<p>Hop éffacé!</p>";
			}
			else
			{
				$id_log=$_GET['id_log'];
				echo "<p>Vous n'êtes pas Administrateur vous êtes modérateurs. (Message transmis pour la suppression au près du compte $nom(1) )</p>";
				if (!mysql_query("INSERT INTO `messagerie` VALUES ('', '1', '$id_seliste', '0', '$time', 'AFF', '<p>Message automatique: <br>le seliste $id_seliste a essayé deffacé une log: $id_log')")) {
					die('Requête invalide : ' . mysql_error());
				}
			}
		}
		if ($_GET['action']=='actu') 
		{
			echo "<div class='message'><br>
			<table summary=\"\" border=\"1\" width=\"90%\">
			<tr>
			<td colspan=5><p class='titre'>Liste des actualités publiés par modérateur</p></td>
			</tr>";
			echo "
			<tr>
			<td class=\"t1\">Suppresion</td><td class=\"t1\">Séliste</td><td class=\"t1\">Article N°</td><td class=\"t1\">Time</td><td class=\"t1\">Commentaire</td>
			</tr>";
			if (!($requete=mysql_query("SELECT * FROM `log` WHERE `action`='article_PU' ORDER BY `timestamp` DESC "))) {
				die('Erreur : ' . mysql_error());
			}
			while($ligne=mysql_fetch_row($requete))
			{
				$id_log=$ligne[0];
				$id_sel=$ligne[1];
				$id_article=$ligne[2];
				$time_pub=$ligne[3];
				$date=date('d/m/y à H\h i\m\i\n',$time_pub);
				$commentaire=stripslashes($ligne[5]);
				if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`='$id_sel'")))) {
					die('Erreur : ' . mysql_error());
				}
				$prenom_sel=stripslashes($recup[0]);	
				echo "
				<tr>
					<td class=\"t1\"><a href=\"admin_log.php?action=actu&amp;action2=sup&amp;id_log=".$id_log."\">Suppresion</a></td><td class=\"t1\"><a href='profil.php?id=".$id_sel."'>".$prenom_sel."(".$id_sel.")</a></td><td class=\"t1\">".$id_article."</td><td class=\"t1\">".$date."</td><td class=\"t1\"><a href=\"actualites.php?action=reponses&id=".$id_article."\">".$commentaire."</a></td>
				</tr>";
			}
			echo "</table><br></div><br>";
		}
		elseif ($_GET['action']=='agenda') 
		{
			echo "<div class='message'><br><table summary=\"\" border=\"1\" width=\"90%\">
			<tr>
			<td colspan=5><p class='titre'>Liste des évènements publiés par modérateur</p></td>
			</tr>";
			echo "
			<tr>
			<td class=\"t1\">Suppresion</td><td class=\"t1\">Séliste</td><td class=\"t1\">Agenda N°</td><td class=\"t1\">Date Pub</td><td class=\"t1\">Date agenda</td><td class=\"t1\">Commentaire</td>
			</tr>";
			if (!($requete=mysql_query("SELECT * FROM `log` WHERE `action`='agenda_PUB' ORDER BY `timestamp` DESC "))) {
				die('Erreur : ' . mysql_error());
			}
			while($ligne=mysql_fetch_row($requete))
			{
				$id_log=$ligne[0];
				$id_sel=$ligne[1];
				$id_article=$ligne[2];
				if (!($recup=mysql_fetch_row(mysql_query("SELECT `date_evenement` FROM `agenda` WHERE `id_agenda`='$id_article'")))) {
					die('Erreur : ' . mysql_error());
				}
				$date_agenda=date('d/m/y <br>à H\h i\m\i\n',$recup[0]);				
				$time_pub=$ligne[3];
				$date=date('d/m/y <br>à H\h i\m\i\n',$time_pub);
				$commentaire=stripslashes($ligne[5]);
				if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`='$id_sel'")))) {
					die('Erreur : ' . mysql_error());
				}
				$prenom_sel=stripslashes($recup[0]);	
				echo "
				<tr>
					<td class=\"t1\"><a href=\"admin_log.php?action=agenda&amp;action2=sup&amp;id_log=".$id_log."\">Suppresion</a></td><td class=\"t1\"><a href='profil.php?id=".$id_sel."'>".$prenom_sel."(".$id_sel.")</a></td><td class=\"t1\">".$id_article."</td><td class=\"t1\">".$date."</td><td class=\"t1\">".$date_agenda."</td><td class=\"t1\">".$commentaire."</td>
				</tr>";
			}
			echo "</table><br></div><br>";
		}
		elseif ($_GET['action']=='petiteannonce') 
		{
			echo "<div class='message'><br><table summary=\"\" border=\"1\" width=\"90%\">
			<tr>
			<td colspan=6><p class='titre'>Liste des petites annonces éffacés</p></td>
			</tr>";
			echo "
			<tr>
			<td class=\"t1\">Suppresion</td><td class=\"t1\">Séliste</td><td class=\"t1\">id</td><td class=\"t1\">Date</td><td class=\"t1\">Commentaire</td>
			</tr>";
			if (!($requete=mysql_query("SELECT * FROM `log` WHERE `action`='annonce_EF' ORDER BY `timestamp` DESC "))) {
				die('Erreur : ' . mysql_error());
			}
			while($ligne=mysql_fetch_row($requete))
			{
				$id_log=$ligne[0];
				$id_sel=$ligne[1];
				$id_article=$ligne[2];		
				$time_pub=$ligne[3];
				$date=date('d/m/y <br>à H\h i\m\i\n',$time_pub);
				$commentaire=stripslashes($ligne[5]);
				if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`='$id_sel'")))) {
					die('Erreur : ' . mysql_error());
				}
				$prenom_sel=stripslashes($recup[0]);	
				echo "
				<tr>
					<td class=\"t1\"><a href=\"admin_log.php?action=agenda&amp;action2=sup&amp;id_log=".$id_log."\">Suppresion</a></td><td class=\"t1\"><a href='profil.php?id=".$id_sel."'>".$prenom_sel."(".$id_sel.")</a></td><td class=\"t1\">".$id_article."</td><td class=\"t1\">".$date."</td><td class=\"t1\">".$commentaire."</td>
				</tr>";
			}
			echo "</table><br></div><br>";
		}
		elseif ($_GET['action']=='pubauto') 
		{
			echo "<div class='message'><br><table summary=\"\" border=\"1\" width=\"90%\">
			<tr>
			<td colspan=5><p class='titre'>Liste des évènements publiés par seliste</p></td>
			</tr>";
			echo "
			<tr>
			<td class=\"t1\">Suppresion</td><td class=\"t1\">Séliste</td><td class=\"t1\">Time</td><td class=\"t1\">Commentaire</td>
			</tr>";
			if (!($requete=mysql_query("SELECT * FROM `log` WHERE `action`='art_PUB_IN' ORDER BY `timestamp` DESC "))) {
				die('Erreur : ' . mysql_error());
			}
			while($ligne=mysql_fetch_row($requete))
			{
				$id_log=$ligne[0];
				$id_sel=$ligne[1];
				
				$time_pub=$ligne[3];
				$date=date('d/m/y à H\h i\m\i\n',$time_pub);
				$commentaire=stripslashes($ligne[5]);
				if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`='$id_sel'")))) {
					die('Erreur : ' . mysql_error());
				}
				$prenom_sel=stripslashes($recup[0]);	
				echo "
				<tr>
					<td class=\"t1\"><a href=\"admin_log.php?action=pubauto&amp;action2=sup&amp;id_log=".$id_log."\">Suppresion</a></td><td class=\"t1\"><a href='profil.php?id=".$id_sel."'>".$prenom_sel."(".$id_sel.")</a></td><td class=\"t1\">".$date."</td><td class=\"t1\">".$commentaire."</td>
				</tr>";
			}
			echo "</table><br></div><br>";
		}
		elseif ($_GET['action']=='agendaauto') 
		{
			echo "<div class='message'><br><table summary=\"\" border=\"1\" width=\"90%\">
			<tr>
			<td colspan=5><p class='titre'>Liste des articlés publiés par séliste</p></td>
			</tr>
			<tr>
			<td class=\"t1\">Suppresion</td><td class=\"t1\">Séliste</td><td class=\"t1\">Time</td><td class=\"t1\">Commentaire</td>
			</tr>";
			if (!($requete=mysql_query("SELECT * FROM `log` WHERE `action`='agen_PUBIN' ORDER BY `timestamp` DESC "))) {
				die('Erreur : ' . mysql_error());
			}
			while($ligne=mysql_fetch_row($requete))
			{
				$id_log=$ligne[0];
				$id_sel=$ligne[1];
				$id_article=$ligne[2];
				$time_pub=$ligne[3];
				$date=date('d/m/y à H\h i\m\i\n',$time_pub);
				$commentaire=stripslashes($ligne[5]);
				if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`='$id_sel'")))) {
					die('Erreur : ' . mysql_error());
				}
				$prenom_sel=stripslashes($recup[0]);	
				echo "
				<tr>
					<td class=\"t1\"><a href=\"admin_log.php?action=agendaauto&amp;action2=sup&amp;id_log=".$id_log."\">Suppresion</a></td><td class=\"t1\"><a href='profil.php?id=".$id_sel."'>".$prenom_sel."(".$id_sel.")</a></td><td class=\"t1\">".$date."</td><td class=\"t1\">".$commentaire."</td>
				</tr>";
			}
			echo "</table><br></div><br>";
		}
		elseif ($_GET['action']=='artsup') 
		{
			echo "<div class='message'><br><table summary=\"\" border=\"1\" width=\"90%\">
			<tr>
			<td colspan=5><p class='titre'>Liste des articles supprimés</p></td>
			</tr>
			<tr>
			<td class=\"t1\">Suppresion</td><td class=\"t1\">Séliste</td><td class=\"t1\">Article N°</td><td class=\"t1\">Time</td><td class=\"t1\">Commentaire</td>
			</tr>";
			if (!($requete=mysql_query("SELECT * FROM `log` WHERE `action`='article_SU' ORDER BY `timestamp` DESC "))) {
				die('Erreur : ' . mysql_error());
			}
			while($ligne=mysql_fetch_row($requete))
			{
				$id_log=$ligne[0];
				$id_sel=$ligne[1];
				$id_article=$ligne[2];
				$time_pub=$ligne[3];
				$date=date('d/m/y à H\h i\m\i\n',$time_pub);
				$commentaire=stripslashes($ligne[5]);
				if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`='$id_sel'")))) {
					die('Erreur : ' . mysql_error());
				}
				$prenom_sel=stripslashes($recup[0]);	
				echo "
				<tr>
					<td class=\"t1\"><a href=\"admin_log.php?action=actu&amp;action2=sup&amp;id_log=".$id_log."\">Suppresion</a></td><td class=\"t1\"><a href='profil.php?id=".$id_sel."'>".$prenom_sel."(".$id_sel.")</a></td><td class=\"t1\">".$id_article."</td><td class=\"t1\">".$date."</td><td class=\"t1\">".$commentaire."</td>
				</tr>";
			}
			echo "</table><br></div><br>";
		}
		elseif ($_GET['action']=='agendasup') 
		{
			echo "<div class='message'><br><table summary=\"\" border=\"1\" width=\"90%\">
			<tr>
			<td colspan=5><p class='titre'>Liste des évènements supprimés</p></td>
			</tr>
			<tr>
			<td class=\"t1\">Suppresion</td><td class=\"t1\">Séliste</td><td class=\"t1\">Article N°</td><td class=\"t1\">Time</td><td class=\"t1\">Commentaire</td>
			</tr>";
			if (!($requete=mysql_query("SELECT * FROM `log` WHERE `action`='agenda_SUP' ORDER BY `timestamp` DESC "))) {
				die('Erreur : ' . mysql_error());
			}
			while($ligne=mysql_fetch_row($requete))
			{
				$id_log=$ligne[0];
				$id_sel=$ligne[1];
				$id_article=$ligne[2];
				$time_pub=$ligne[3];
				$date=date('d/m/y à H\h i\m\i\n',$time_pub);
				$commentaire=stripslashes($ligne[5]);
				if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`='$id_sel'")))) {
					die('Erreur : ' . mysql_error());
				}
				$prenom_sel=stripslashes($recup[0]);	
				echo "
				<tr>
					<td class=\"t1\"><a href=\"admin_log.php?action=agendasup&amp;action2=sup&amp;id_log=".$id_log."\">Suppresion</a></td><td class=\"t1\"><a href='profil.php?id=".$id_sel."'>".$prenom_sel."(".$id_sel.")</a></td><td class=\"t1\">".$id_article."</td><td class=\"t1\">".$date."</td><td class=\"t1\">".$commentaire."</td>
				</tr>";
			}
			echo "</table><br></div><br>";
		}
		
			echo "<div class='message'><br>
			<p class='titre'>Administration des modérateurs:</p><br>
			<p class='t1'>Mouvement sur les Actualités:</p><br>
			<p class='t4'><a class='aadmin' href=\"admin_log.php?action=actu\">Voir les publications par les modérateurs</a>&nbsp;&nbsp;
			<a class='aadmin' href=\"admin_log.php?action=pubauto\">Voir les publications par les sélistes (sans modération)</a>&nbsp;&nbsp;
			<a class='aadmin' href=\"admin_log.php?action=artsup\">Actualités supprimés</a></p>
			<br><hr width='70%'><br>
			<p class='t1'>Mouvement des évènements dans l'Agenda:</p><br>
			<p class='t4'><a class='aadmin' href=\"admin_log.php?action=agenda\">Voir les publications par les modérateurs</a>&nbsp;&nbsp
			<a class='aadmin' href=\"admin_log.php?action=agendaauto\">Voir les publications par les sélistes (sans modération)</a>&nbsp;&nbsp;
			<a class='aadmin' href=\"admin_log.php?action=agendasup\">Evènements supprimés</a></p>
			<br><hr width='70%'><br>
			<p class='t1'>Mouvement des petites annonces:</p><br>
			<p class='t4'><a class='aadmin' href=\"admin_log.php?action=petiteannonce\">Voir les petites annonces éffacés</a></p><br>
			</div><br>";	
			
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'memo'")))) {
				die('Erreur : ' . mysql_error());
			}
			$memo=stripslashes($recup[0]);
			
			if($_POST['message']!=null)
			{
				$memo=nl2br(addslashes($_POST['message']));
				if (!($requete=mysql_query("UPDATE `variables` SET `texte` =  '$memo'  WHERE `variable`='memo'LIMIT 1") )) {
					die('Erreur : ' . mysql_error());
				}				
			}
			$memo=str_replace("<br />","",$memo);
			$memo=stripslashes($memo);
			echo "<form method='post' action='admin_log.php' enctype='multipart/form-data'>
		   	<div class='message'><br>
			<p class='titre'>Mémo des administrateurs:</p><br>
			<p class='t4'><textarea name='message' cols='80' rows='15'>".$memo."</textarea><br>
			<input type='submit' value=' Enregistrer le mémo '></p><br></div><br></form></div>";
			
	}
	else
	{ 	 //délai dépassé
		header ("location:troptard.php");
		session_destroy();
	}
 }
else
{ 	 // pas de session
	header ("location:404.php");
	session_destroy();
}
mysql_close($connexion); 
include ("fin.php");	
?>
