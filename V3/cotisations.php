<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/

if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
{								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
		switch ($grade)
		{
			case 'SELISTE' : include("lien/seliste.php");break;
			case 'MODERATEUR' :include("lien/seliste.php"); include("lien/moderateur.php");break;
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page
		//debut de l'edition
		echo "<br><br><div class='corps'><br>";
		if(($_GET['action']=='cotisation')&&(($grade=='MODERATEUR')||($grade=='ADMIN')))
		{
			if (!($requete2=mysql_query("SELECT COUNT(`id_seliste`) FROM `selistes` WHERE `valide`='OUI' AND `id_seliste`>'5'") )) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete2) ;
			$nbr_coti=$ligne[0];
			$total=$nbr_coti*$cotisation;
			if (!($requete1=mysql_query("SELECT `time` FROM `echanges` WHERE `id_ss_rub`='478' ORDER BY `time` desc LIMIT 1") )) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete1);
			$datederniere=date('d/m/y à H\h i\m\i\n',$ligne[0]);
			echo "<p class='t4'>La cotisation s'élève à ".$cotisation." ".$monnaie."<br>
			Elle sera prélevée sur ".$nbr_coti." membres.<br>
			Le compte de ".$nom." sera crédité de ".$total." ".$monnaie.".<br>
			Date du dernier prélévement des cotisations:".$datederniere."<br><br>";
			// on interdit les prelevement < 20j
			$time20j=$time-1728000;
			if($ligne[0]<$time20j)
			{
				echo "<a class='amodo' href='cotisations.php?action=cotisationGO'> Cliquez-ici pour les prélever</a></p><br><br></div>";					
			}
			else
			{
				echo "Vous ne pouvez pas prélever de cotisation car le dernier retrait est trop récent.</p><br><br></div>";
			}
		}
		elseif(($_GET['action']=='cotisationGO')&&(($grade=='MODERATEUR')||($grade=='ADMIN')))
		{
			//on les retires
			echo "<p class='titre'>Les sélistes débités de ".$cotisation." ".$monnaie.":</p><br>";
			$tableau="<table summary=\"\" class='tablevu' border='1' width='90%' cellpadding='5'>
			<tr><td><p class=t1>Séliste:</p></td><td><p class=t1>".$monnaie." actuel:</p></td><td><p class=t1>".$monnaie." après:</p></td><td><p class=t1>TOTAL</p></td>";
			if (!($requete1=mysql_query("SELECT `id_seliste`, `prenom`, `grains` FROM `selistes` WHERE `valide`='OUI' AND `id_seliste`>'5'") )) {
				die('Erreur : ' . mysql_error());
			}
			$total=0;
			while($ligne=mysql_fetch_row($requete1))
			{
				$id_sel= $ligne[0];
				$prenom_sel=stripslashes($ligne[1]);
				$grains_sel=$ligne[2];
				$grains_apres=$grains_sel-$cotisation;
				$total=$total+$cotisation;
				$tableau=$tableau."<tr><td><p class=t4>".$prenom_sel."(".$id_sel.")</p></td><td><p class=t4><p>".$grains_sel."</p></td><td><p class=t4>".$grains_apres."</p></td><td><p class=t1>".$total."</p></td>";
				if (!($requete=mysql_query("UPDATE `selistes` SET `grains` = '$grains_apres' WHERE `id_seliste` ='$id_sel' LIMIT 1") )) {
					die('Erreur : ' . mysql_error());
				}
				if (!(mysql_query("INSERT INTO `echanges` VALUES ('', '1', '$id_sel', '$time', '$cotisation', '478', 'OKI','','$time','$id_seliste')"))) {
					die('Erreur : ' . mysql_error());
				}
			}
			// pour compte 1
			if (!($requete1=mysql_query("SELECT `grains` FROM `selistes` WHERE `id_seliste`='1'") )) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete1) ;
			$grains_sel= $ligne[0];
			$grains_apres=$grains_sel+$total;
			echo "<p class='t1'>".$grains_sel." $monnaie sur le compte 1  + ".$total." ".$monnaie." = ".$grains_apres." sur le compte 1</p>
			".$tableau."</table><br></div>";
			if (!($requete=mysql_query("UPDATE `selistes` SET `grains` = '$grains_apres' WHERE `id_seliste` ='1' LIMIT 1") )) {
				die('Erreur : ' . mysql_error());
			}	
			
		}
		else
		{ 	//pirate
			echo "<p>Tu n'a rien a faire ici.</p>";	
		}
		//fin edition
	} 
	//delai depassé
	else
	{
		header ("location:troptard.php");
		session_destroy();
	}
}
// pas de sesion
else
{
	header ("location:404.php");
	session_destroy();
}
mysql_close($connexion); 
include ("fin.php");	
?>
