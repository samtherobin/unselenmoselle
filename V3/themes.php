<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/

if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
{								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moins de 15 minutes de connexion (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
		$postheme=htmlentities($_POST['theme'], ENT_QUOTES, "UTF-8");
		if($postheme!=null)
		{
			if(($_POST['theme']=='DEF'))
			{
				$theme="";
				$_SESSION['theme']=$theme;
				if (!($requete=mysql_query("UPDATE `selistes` SET `theme`='$theme' WHERE `id_seliste`='$id_seliste' LIMIT 1") )) {
					die('Erreur : ' . mysql_error());
				} 
			}
			else
			{
				$theme=$_POST['theme'];
				$_SESSION['theme']=$theme;
				if (!($requete=mysql_query("UPDATE `selistes` SET `theme`='$theme' WHERE `id_seliste`='$id_seliste' LIMIT 1") )) {
					die('Erreur : ' . mysql_error());
				} 
			}
			echo "<div class='corps'><p>Le thème est changé!</p><br></div><br>";
		}
	 	switch ($grade)
		{
			case 'SELISTE' : include("lien/seliste.php");break;	
			case 'MODERATEUR' :include("lien/seliste.php"); include("lien/moderateur.php");break;
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page
		
	
		echo  "<br><br><div class='corps'><br>
			<form method=\"post\" action=\"themes.php\" enctype=\"multipart/form-data\">
			<table summary=\"\" border='0' width='90%'>
			<tr valign=\"top\">
			<td colspan=\"3\"><p class='titre'>Changement du thème:</p></td>
			</tr>
			<tr>
			<td class='t1' colspan=3><select name='theme'>
			<option value='DEF' >Vert</option>
			<option value='BLE' >Bleu</option>
			<option value='BLA' >Blanc Gris</option>
			<option value='VIO' >Violet</option>
			<option value='RAM' >Coloré</option>
			<option value='NBL' >Mal Voyant</option>
			</select><br/><input type='submit' value=' Changer de thème ' ></td>
			</tr>
			<tr>
			<td><p class='t2'>Vert (par défaut):<br/><img src=\"images/themedef.jpg\" height='100'></p></td>
			<td><p class='t2'>Bleu:<br/><img src=\"images/themeble.jpg\" height='100' ></p></td>
			<td><p class='t2'>Blanc/Gris:<br/><img src=\"images/themebla.jpg\" height='100' ></p></td>
			</tr><tr>
			<td><p class='t2'>Violet:<br/><img src=\"images/themevio.jpg\" height='100' ></p></td>
			<td><p class='t2'>Coloré:<br/><img src=\"images/themeram.jpg\" height='100' ></p></td>
			<td><p class='t2'>Mal Voyant: (Couleur à haut contraste RGB)<br/><img src=\"images/themenbl.jpg\" height='100' ></p></td>
			</tr>
			</table>			
			</form>	<br></div><br>	
			";
		
		
		// fin de la page 
	}
	else
	{ 	 //délai dépassé
		header ("location:troptard.php");
		session_destroy();
	}
}
else
{ 	 // pas de session
	header ("location:404.php");
	session_destroy();
}
mysql_close($connexion); 
include ("fin.php");	
?>
