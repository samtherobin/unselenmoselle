<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/

if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
{								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade`, `prenom`, `email` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		$prenom=$ligne[1];
		$email=$ligne[2];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
		switch ($grade)
		{
			case 'SELISTE' : include("lien/seliste.php");break;	
			case 'MODERATEUR' :include("lien/seliste.php"); include("lien/moderateur.php");break;
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page
		if($_GET['action']=='enregistre')
		{
			$id_sel=$_GET['id_sel'];
			if (!($requete1=mysql_query("SELECT `prenom`, `email`, `mail_dem` FROM `selistes` WHERE `id_seliste`='$id_sel'") )) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete1) ;
			$prenom_sel= $ligne[0];
			$email_dest= $ligne[1];
			$mail_dem= $ligne[2];
			$message=nl2br(stripslashes($_POST['message'])); 
			if($mail_dem=='OUI')
			{
				$headers = "From: ".$prenom." <".$email.">\n";
					$headers .= "Reply-To: ".$email."\n";
					$headers .= "X-Sender: ".$email."\n";
					$headers .= "X-Author: ".$prenom."\n";
					$headers .= "X-Priority:1\n";
					$headers .= "X-Mailer: PHP\n";
					$headers .= "Return_Path: <".$email.">\n";
					$headers .='Content-Type: text/html; charset="UTF-8"'."\n";
				$mel ='<html><head><title></title></head><body><p>'.$message.'<br><br><i>Si vous ne souhaitez plus avoir ces alertes:<br>
	<a href=http://'.$site.'/index.php?vers=compte.php> Cliquez ici </a> ou via la page notifications (en haut à gauche).</i></p></body></html>';
				sw_mail($email_dest,'['.$nom.'] '.$prenom.'('.$id_seliste.') demande tes services.',$mel,$headers);
				echo "<br><div class='corps'><br><p>Votre demande lui à été transmise par mail et message privé.</p>
						<br><p><a href='bureau.php' title='Bureau'>Retour bureau</a></p></div><br>";
			}
			else
			{
					echo "<br><div class='corps'><br><p>Votre demande lui à été transmise par message privé.</p>	
					<a href='bureau.php' title='Bureau'>Retour bureau</a></p><br></div><br>";
			}
			$message=addslashes($message);
			if (!(mysql_query("INSERT INTO `messagerie` VALUES ('', '$id_sel', '$id_seliste', '0', '$time', 'AFF', '$message')"))) {
				die('Erreur : ' . mysql_error());
			}
				
		}
		elseif(($_GET['action']=='demande')&&($_GET['id_service']!=null))
		{
			$id_service=htmlentities($_GET['id_service'], ENT_QUOTES, "UTF-8");
			if (!($requete1=mysql_query("SELECT `description`, `detail`, `id_rubrique`, `id_seliste` FROM `services` WHERE `id_service`='$id_service'") )) {
				die('Erreur : ' . mysql_error());
			}
			$ligne1=mysql_fetch_row($requete1);
			$designation_ss_rub=stripslashes($ligne1[0]);
			$detail=stripslashes($ligne1[1]);
			$id_rub=$ligne1[2];
			$id_sel=$ligne1[3];
			if (!($requete2=mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`='$id_sel'") )) {
				die('Erreur : ' . mysql_error());
			}
			$ligne2=mysql_fetch_row($requete2);
			$prenom_sel=stripslashes($ligne2[0]);
			// recherche des catalogue et rubrique
			if (!($requete3=mysql_query("SELECT `designation`, `id_cat` FROM `rubrique` WHERE `id_rubrique`='$id_rub'") )) {
				die('Erreur : ' . mysql_error());
			}
			$ligne3=mysql_fetch_row($requete3);
			$designation_rub=stripslashes($ligne3[0]);
			$id_cat=$ligne3[1];
			if (!($requete4=mysql_query("SELECT `designation` FROM `catalogue` WHERE `id_cat`='$id_cat'") )) {
				die('Erreur : ' . mysql_error());
			}
			$ligne4=mysql_fetch_row($requete4);
			$designation_cat=stripslashes($ligne4[0]);
			//affichage
			echo  "<br><div class='corps'><br><form method=\"post\" action=\"proposer.php?action=enregistre&amp;id_sel=".$id_sel."\" enctype=\"multipart/form-data\">
			<table summary=\"\" border='0' width='100%' cellpadding='5'>
			<tr>
			<td colspan='2'><p class='titre'>Demander un service:</p></td>
			</tr>
			<tr valign=top>
			<th>Service demandé:</th>
			<td class='t1'>".$designation_cat." - ".$designation_rub." - ".$designation_ss_rub."</td>
			</tr>
			<tr>
			<th>Détail:</th>
			<td class='t1'>".$detail."</td>
			</tr>
			<tr>
			<th>Sens de la demande:</th>
			<td class='t1'>".$prenom." demande le service ".$designation_ss_rub." que ".$prenom_sel." propose.</td>
			</tr>
			<tr>
			<th>Message qui lui sera adressé:<br><span class='pasimportant'>(par mail ou message privé selon ça configuration.)</span></th>
			<td class='t1'><textarea  name='message' cols='80' rows='10' >Bonjour ".$prenom_sel."(".$id_sel."), je te contacte aujourd'hui afin de te demander tes services de ".$designation_ss_rub." (dans la rubrique ".$designation_rub." du catalogue ".$designation_cat.").\nEst-ce que cela t'intéresse?\nA bientôt.\n\n".$prenom."(".$id_seliste.")</textarea></td>
			</tr>
			<tr>
			<td colspan='3' class='t3'><input type='submit' value=' Envoyer ma demande ' ></td>
			</tr>
			</table></form><br></div><br>";
		}
		
	}
	else
	{ 	 // délai dépassé
		header ("location:troptard.php");
		session_destroy();
	}
 }
else
{ 	 // pas de session
	header ("location:404.php");
	session_destroy();
}
mysql_close($connexion);
include ("fin.php");	
?>
