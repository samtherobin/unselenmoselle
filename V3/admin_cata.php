<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/

if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
{								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
		switch ($grade)
		{
			case 'SELISTE' : header("location:404.php");break;
			case 'MODERATEUR' :include("lien/seliste.php"); include("lien/moderateur.php");break;
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page
		echo "<br><br><div class='corps'><br>";
		if($_GET['action']=='ajouter')
		{
			$id_rub=htmlentities($_POST['rubrique'], ENT_QUOTES, "UTF-8");
			$designation=htmlentities($_POST['ssrubrique'], ENT_QUOTES, "UTF-8");
			if (!($requete2=mysql_query("SELECT * FROM `rubrique` WHERE `id_rubrique`='$id_rub'") )) {
				die('Erreur : ' . mysql_error());
			}
			$ligne2=mysql_fetch_row($requete2);
			$designation_rub=stripslashes($ligne2[2]);
			$id_cat=$ligne2[1];
			if (!($requete3=mysql_query("SELECT `designation` FROM `catalogue` WHERE `id_cat`='$id_cat'") )) {
				die('Erreur : ' . mysql_error());
			}
			$ligne3=mysql_fetch_row($requete3);
			$designation_cat=stripslashes($ligne3[0]);
			if (!mysql_query("INSERT INTO `sous_rubrique` VALUES ('', '$id_rub', '$designation', '')")) {
				die('Requête invalide : ' . mysql_error());
			}
			echo "<div class='message'><br>
			<p	class='titre'>Le service suivant est créé : </p><br>
			<p class='t4'>\" <b>".$designation_cat."</b>-".$designation_rub."-<i>".$_POST['ssrubrique']."</i> \"</p>
			<br>
			<p class='t4'><a href='admin_cata.php' class='amodo'>Retour gestion catalogue</a>&nbsp;&nbsp;<a href='bureau.php'>Retour bureau</a></p></div><br><br>";
		}
		elseif($_GET['action']=='ajoutrub')
		{
			$id_cat=htmlentities($_POST['catalogue'], ENT_QUOTES, "UTF-8");
			$designation=htmlentities($_POST['rubrique'], ENT_QUOTES, "UTF-8");
			if (!($requete3=mysql_query("SELECT `designation` FROM `catalogue` WHERE `id_cat`='$id_cat'") )) {
				die('Erreur : ' . mysql_error());
			}
			$ligne3=mysql_fetch_row($requete3);
			$designation_cat=stripslashes($ligne3[0]);			
			if (!mysql_query("INSERT INTO `rubrique` VALUES ('', '$id_cat', '$designation')")) {
				die('Requête invalide : ' . mysql_error());
			}
			echo "<div class='message'><br>
			<p	class='titre'>La rubrique suivante est créé : </p><br>
			<p class='t4'>\" <b>".$designation_cat."</b>-".$_POST['rubrique']."\" (N'oubliez pas de lui créer des sous-rubriques !)</p>
			<br>
			<p class='t4'><a href='admin_cata.php' class='amodo'>Retour gestion catalogue</a>&nbsp;&nbsp;<a href='bureau.php'>Retour bureau</a></p></div><br><br>";
		}
		elseif($_GET['action']=='modifier')
		{ // modifier service (sous rubrique)
			if($_POST['designation_ss']!=null)
			{ // enregestrement
				$id_rub=htmlentities($_POST['rubrique'], ENT_QUOTES, "UTF-8");
				$designation_ss=htmlentities($_POST['designation_ss'], ENT_QUOTES, "UTF-8");
				$detail=htmlentities($_POST['detail'], ENT_QUOTES, "UTF-8");
				$id_ss_rub=htmlentities($_GET['id_ss_rub'], ENT_QUOTES, "UTF-8");
				if (!($requete=mysql_query("UPDATE `sous_rubrique` SET `designation` = '$designation_ss', `détail` ='$detail', `id_rubrique` = '$id_rub' WHERE `id_ss_rub` ='$id_ss_rub' LIMIT 1") )) {
					die('Erreur : ' . mysql_error());
				}
				if (!($requete2=mysql_query("SELECT * FROM `rubrique` WHERE `id_rubrique`='$id_rub'") )) {
					die('Erreur : ' . mysql_error());
				}
				$ligne2=mysql_fetch_row($requete2);
				$designation_rub=stripslashes($ligne2[2]);
				$id_cat=$ligne2[1];
				if (!($requete3=mysql_query("SELECT `designation` FROM `catalogue` WHERE `id_cat`='$id_cat'") )) {
					die('Erreur : ' . mysql_error());
				}
				$ligne3=mysql_fetch_row($requete3);
				$designation_cat=stripslashes($ligne3[0]);
				echo "<div class='message'><br>
				<p	class='titre'>La sous-rubrique suivante est modifiée:</p>
				<p class='t4'><b>".$designation_cat."</b>-".$designation_rub."-<i>".$_POST['designation_ss']."</i></p>					
				<br>
				<p class='t4'><a href='admin_cata.php' class='amodo'>Retour gestion catalogue</a>&nbsp;&nbsp;<a href='bureau.php'>Retour bureau</a></p></div><br><br>";
			}
			else
			{ //formulaire
				$id_ssrub=htmlentities($_POST['ssrubrique'], ENT_QUOTES, "UTF-8");
				if (!($requete1=mysql_query("SELECT * FROM `sous_rubrique` WHERE `id_ss_rub`='$id_ssrub'") )) {
					die('Erreur : ' . mysql_error());
				}
				$ligne=mysql_fetch_row($requete1) ;
				$id_rubS=$ligne[1];
				$designation_ss=stripslashes($ligne[2]);
				$detail_ss=stripslashes($ligne[3]);
				if (!($requete1=mysql_query("SELECT * FROM `rubrique` WHERE `id_rubrique`='$id_rubS'") )) {
					die('Erreur : ' . mysql_error());
				}
				$ligne=mysql_fetch_row($requete1) ;
				$designation_rubS=$ligne[2];
				$id_cat=$ligne[1];
				if (!($requete2=mysql_query("SELECT `designation` FROM `catalogue` WHERE `id_cat`='$id_cat'") )) {
					die('Erreur : ' . mysql_error());
				}
				$ligne2=mysql_fetch_row($requete2);
				$designation_catS=stripslashes($ligne2[0]);
				$listerubrique='';
				if (!($requete=mysql_query("SELECT * FROM `rubrique` ORDER BY `designation` ASC ") )) {
					die('Erreur : ' . mysql_error());
				}
				while($ligne=mysql_fetch_row($requete))
				{
					$designation_rub=stripslashes($ligne[2]);
					$id_rub=$ligne[0];
					$id_cat=$ligne[1];
					if (!($requete2=mysql_query("SELECT `designation` FROM `catalogue` WHERE `id_cat`='$id_cat'") )) {
						die('Erreur : ' . mysql_error());
					}
					$ligne2=mysql_fetch_row($requete2);
					$designation_cat=stripslashes($ligne2[0]);
					$listerubrique=$listerubrique."<option value=\"$id_rub\">$designation_rub <b>($designation_cat)</b></option>";
				}
				echo "<div class='message'><br>
				<form method=\"post\" action=\"admin_cata.php?action=modifier&amp;id_ss_rub=".$id_ssrub."\" enctype=\"multipart/form-data\">
				<p class='titre'>Modification du service:</p><br>
				<p class='t2'>Rubrique de cette sous-rubrique:<select name='rubrique'>
				<option value=\"$id_rubS\">$designation_rubS <b>($designation_catS)</b></option>
				".$listerubrique."</select></p>
				<p class='t2'>Désignation de la sous-rubrique: <input name='designation_ss' value=\"".$designation_ss."\" size=50 maxLength=90></p>
				<p class='t2'>Détail de la sous-rubrique: <input name='detail' value=\"".$detail_ss."\" size=80 maxLength=100></p>
				<p class='t4'><input type='submit' value=' Modifier '></p><br></form></div>";
			}
		}
		elseif($_GET['action']=='modifierrub')
		{// modifier rubrique
			if($_POST['designation_rub']!=null)
			{ // enregestrement
				$id_rub=htmlentities($_GET['id_rub'], ENT_QUOTES, "UTF-8");
				$designation_rub=htmlentities($_POST['designation_rub'], ENT_QUOTES, "UTF-8");
				$id_cat=htmlentities($_POST['id_cat'], ENT_QUOTES, "UTF-8");
				if (!($requete=mysql_query("UPDATE `rubrique` SET `designation` = '$designation_rub', `id_cat` ='$id_cat' WHERE `id_rubrique` ='$id_rub' LIMIT 1") )) {
					die('Erreur : ' . mysql_error());
				}
				if (!($requete2=mysql_query("SELECT `designation` FROM `catalogue` WHERE `id_cat`='$id_cat'") )) {
					die('Erreur : ' . mysql_error());
				}
				$ligne2=mysql_fetch_row($requete2);
				$designation_cat=stripslashes($ligne2[0]);
				echo "<div class='message'><br>
				<p class='titre'>La Rubrique suivante est modifiée:</p><br>
				<p class='t4'><b>".$designation_cat."</b>-".$_POST['designation_rub']."</p>					
				<br>
				<p class='t4'><a href='admin_cata.php' class='amodo'>Retour gestion catalogue</a>&nbsp;&nbsp;
				<a href='bureau.php'>Retour bureau</a></p></div><br><br>";
			}
			else
			{ //formulaire
				$id_rub=htmlentities($_POST['id_rub'], ENT_QUOTES, "UTF-8");
				if (!($requete1=mysql_query("SELECT * FROM `rubrique` WHERE `id_rubrique`='$id_rub'") )) {
					die('Erreur : ' . mysql_error());
				}
				$ligne=mysql_fetch_row($requete1) ;
				$id_rub=$ligne[0];
				$designation_rub=stripslashes($ligne[2]);
				$id_catS=stripslashes($ligne[1]);
				if (!($requete1=mysql_query("SELECT * FROM `catalogue` WHERE `id_cat`='$id_catS'") )) {
					die('Erreur : ' . mysql_error());
				}
				$ligne=mysql_fetch_row($requete1) ;
				$designation_catS=$ligne[1];
				$listecatalogue='';
				if (!($requete=mysql_query("SELECT * FROM `catalogue` ORDER BY `designation` ASC ") )) {
					die('Erreur : ' . mysql_error());
				}
				while($ligne=mysql_fetch_row($requete))
				{
					$designation_cat=stripslashes($ligne[1]);
					$id_cat=$ligne[0];
					$listecatalogue=$listecatalogue."<option value=\"$id_cat\">$designation_cat</option>";
				}
				echo "<div class='message'><br>
				<form method=\"post\" action=\"admin_cata.php?action=modifierrub&amp;id_rub=".$id_rub."\" enctype=\"multipart/form-data\">
				<p class='titre'>Modification de rubrique:</p><br>
				<p class='t2'>Catalogue de cette rubrique: <select name='id_cat'>
				<option value=\"$id_catS\"><b>$designation_catS </b></option>
				".$listecatalogue."</select></p>				
				<p class='t2'>Désignation rubrique: <input name='designation_rub' value='".$designation_rub."' size=70 maxLength=100></p>
				<p class='t4'><input type='submit' value=' Modifier ' ></p><br></form></div>";
			}
		}
		elseif($_GET['action']=='supprimerrub')
		{
			$id_rub=htmlentities($_POST['id_rub'], ENT_QUOTES, "UTF-8");
			if (!($nombre=mysql_num_rows(mysql_query("SELECT * FROM `sous_rubrique` WHERE `id_rubrique` = '$id_rub'")))) {
				die('Erreur : ' . mysql_error());
			}
			if ($nombre!=0)
			{ // peut pas effacé une sous rubrique existe encore dans cette rubrique
				echo "<div class='message'><br>
				<p class='titre'>Cette rubrique à encore $nombre sous-rubriques. Il faut d'abord les réaffecter ou les supprimer.</p><br>
				<p class='t4'><a href='admin_cata.php' class='amodo'>Retour gestion catalogue</a>&nbsp;&nbsp;
				<a href='bureau.php'>Retour bureau</a></p></div><br><br>";
			}
			else
			{
				if (!(mysql_query("DELETE FROM `rubrique` WHERE `id_rubrique` = '$id_rub' LIMIT 1"))) {
					die('Erreur : ' . mysql_error());
				}
				echo "<div class='message'><br>
				<p class='titre'>La rubrique numéro ".$id_rub." est supprimée.</p><br>
				<p class='t4'><a href='admin_cata.php' class='amodo'>Retour gestion catalogue</a>&nbsp;&nbsp;
				<a href='bureau.php'>Retour bureau</a></p></div><br><br>";
			}
		}
		elseif($_GET['action']=='supprimer')
		{
			$id_ssrub=htmlentities($_POST['ssrubrique'], ENT_QUOTES, "UTF-8");
			if (!(mysql_query("DELETE FROM `sous_rubrique` WHERE `id_ss_rub` = '$id_ssrub' LIMIT 1"))) {
				die('Erreur : ' . mysql_error());
			}
			if (!(mysql_query("DELETE FROM `services` WHERE `id_ss_rub` = '$id_ssrub'"))) {
				die('Erreur : ' . mysql_error());
			}
			echo "<div class='message'><br>
				<p class='titre'>La sous-rubrique numéro ".$id_ssrub." est supprimée du catalogue et des profils.</p><br>
				<p class='t4'><a href='admin_cata.php' class='amodo'>Retour gestion catalogue</a>&nbsp;&nbsp;
				<a href='bureau.php'>Retour bureau</a></p></div><br><br>";
		}
		else
		{
			//modif rubrique
			$listessrubrique='';
			if (!($requete=mysql_query("SELECT * FROM `sous_rubrique` ORDER BY `designation` ASC ") )) {
				die('Erreur : ' . mysql_error());
			}
			while($ligne=mysql_fetch_row($requete))
			{
				$designation_ss_rub=stripslashes($ligne[2]);
				$id_ss_rub=$ligne[0];
				$id_rub=$ligne[1];
				if (!($requete2=mysql_query("SELECT * FROM `rubrique` WHERE `id_rubrique`='$id_rub'") )) {
					die('Erreur : ' . mysql_error());
				}
				$ligne2=mysql_fetch_row($requete2);
				$designation_rub=stripslashes($ligne2[2]);
				$id_cat=$ligne2[1];
				if (!($requete3=mysql_query("SELECT `designation` FROM `catalogue` WHERE `id_cat`='$id_cat'") )) {
					die('Erreur : ' . mysql_error());
				}
				$ligne3=mysql_fetch_row($requete3);
				$designation_cat=stripslashes($ligne3[0]);
				$listessrubrique=$listessrubrique."<option value=\"$id_ss_rub\">$designation_ss_rub ($designation_rub - <b>$designation_cat</b>)</option>";
			}
			$listerubrique='';
			if (!($requete=mysql_query("SELECT * FROM `rubrique` ORDER BY `designation` ASC ") )) {
				die('Erreur : ' . mysql_error());
			}
			while($ligne=mysql_fetch_row($requete))
			{
				$designation_rub=stripslashes($ligne[2]);
				$id_rub=$ligne[0];
				$id_cat=$ligne[1];
				if (!($requete2=mysql_query("SELECT `designation` FROM `catalogue` WHERE `id_cat`='$id_cat'") )) {
					die('Erreur : ' . mysql_error());
				}
				$ligne2=mysql_fetch_row($requete2);
				$designation_cat=stripslashes($ligne2[0]);
				$listerubrique=$listerubrique."<option value=\"$id_rub\">$designation_rub <b>($designation_cat)</b></option>";
			}
			$listecatalogue='';
			if (!($requete=mysql_query("SELECT * FROM `catalogue` ORDER BY `designation` ASC ") )) {
				die('Erreur : ' . mysql_error());
			}
			while($ligne=mysql_fetch_row($requete))
			{
				$designation_cat=stripslashes($ligne[1]);
				$id_cat=$ligne[0];
				$listecatalogue=$listecatalogue."<option value=\"$id_cat\">$designation_cat</option>";
			}
			echo "			
				<br><div class='message'><br>
				<p class='titre'> Modification des sous-rubriques:</p>
				<br>
			<form method=\"post\" action=\"admin_cata.php?action=ajouter\" enctype=\"multipart/form-data\">
				<p class='t2'><b>Ajouter une sous_rubrique (service):</b></p>
				<p class='t2'>Dans quel rubrique doit apparaitre cette sous-rubrique ? <select name='rubrique'>".$listerubrique."</select><br>
				Désignation sous-rubrique:<input name='ssrubrique' value='' size=50 maxLength=80></p>
				<p class='t4'><input type='submit' value=' Ajouter ' ></p>
				</form>";
			// modif ss rub
			echo "<hr width='70%'>
			<form method=\"post\" action=\"admin_cata.php?action=modifier\" enctype=\"multipart/form-data\">
				<p class='t2'><b>Modifier une sous-rubrique (service):</b></p>
				<p class='t2'>Quelle sous-rubrique est à modifier ?	<select name='ssrubrique'>".$listessrubrique."</select></p>
				<p class='t4'><input type='submit' value=' Modifier ' ></p>
				</form>";
			// supprimer un service
			echo "<hr width='70%'>
			<form method=\"post\" action=\"admin_cata.php?action=supprimer\" enctype=\"multipart/form-data\">
				<p class='t2'><b>Supprimer une sous-rubrique (service):</b></p>
				<p class='t2'>Quel sous-rubrique est à supprimer ? <select name='ssrubrique'>".$listessrubrique."</select></p>
				<p class='t4'><input type='submit' value=' Supprimer ' ></p>
				</form><br></div>";
			echo "			
				<br><div class='message'><br>
				<p class='titre'> Modification des rubriques:</p>
				<br>
				<form method=\"post\" action=\"admin_cata.php?action=modifierrub\" enctype=\"multipart/form-data\">
				<p class='t2'><b>Modifier une rubrique:</b><select name='id_rub'>".$listerubrique."</select>
				<input type='submit' value=' Modifier ' ></p></form>
				<hr width='70%'>";
			// ajouter une rubrique
			echo "
			<form method=\"post\" action=\"admin_cata.php?action=ajoutrub\" enctype=\"multipart/form-data\">
			<p class='t2'><b>Ajouter une rubrique:</b></p>			
			<p class='t2'>Dans quel catalogue doit apparaitre cette rubrique? <select name='catalogue'>".$listecatalogue."</select><br>
				Nom de cette rubrique:<input name='rubrique' value='' size=50 maxLength=80></p>
				<p class='t4'><input type='submit' value=' Ajouter ' ></p>
			</form>
			<hr width='70%'>";
			// supprimer une rubrique
			echo "
			<form method=\"post\" action=\"admin_cata.php?action=supprimerrub\" enctype=\"multipart/form-data\">
			<p class='t2'><b>Supprimer une rubrique:</b><select name='id_rub'>".$listerubrique."</select>
			<input type='submit' value=' Supprimer ' ></p></form><br></div><br>";
			//modifier  un service
			
		}
			echo "<br></div>";
	} 
	//delai depassé
	else
	{
		header ("location:troptard.php");
		session_destroy();
	}
}
// pas de sesion
else
{
	header ("location:404.php");
	session_destroy();
}
mysql_close($connexion); 
include ("fin.php");	
?>
