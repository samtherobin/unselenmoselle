<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/
 
if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
{								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade`, `mdp` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		$mdp=$ligne[1];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
		switch ($grade)
		{
			case 'SELISTE' : include("lien/seliste.php");break;
			case 'MODERATEUR' :include("lien/seliste.php"); include("lien/moderateur.php");break;
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page
		echo "<br><div class=\"corps\"><br>";
		if($_POST['mdpancien']!=NULL) //on enregistre
		{
			$mdpancien=md5(htmlentities($_POST['mdpancien'], ENT_QUOTES, "UTF-8"));
			$mdpnouveau=md5(htmlentities($_POST['mdpnouveau'], ENT_QUOTES, "UTF-8"));;
			$mdpnouveau2=md5(htmlentities($_POST['mdpnouveau2'], ENT_QUOTES, "UTF-8"));;
			$nombre=strlen($_POST['mdpnouveau']);
			if($mdp==$mdpancien)
			{
				if(($mdpnouveau==$mdpnouveau2)&&($nombre>5))
				{
					if (!($requete=mysql_query("UPDATE `selistes` SET `mdp` = '$mdpnouveau' WHERE `id_seliste` ='$id_seliste' LIMIT 1") )) {
						die('Erreur : ' . mysql_error());
					}
					echo "
					<p class='titre'>Mot de passe mis à jour merci.</p>
					<br><br>
					<p><a href='bureau.php' alt='Bureau'>Retour au bureau</a></p><br><br>";
				}
				else
				{
					echo "
					<p class='titre'>Le nouveau mot de passe est différent de la confirmation! Ou il est trop court ( < 5 caractères).</p>
					<br><br>
					<p><a href='changemdp.php' >Retour changement de mot de passe</a>&nbsp;&nbsp;<a href='bureau.php' alt='Bureau'>Retour au bureau</a></p><br><br>";
				}
			}
			else
			{
				echo "
					<p class='titre'>Le mot de passe actuel saisie ne correspond pas à celui utilisé..</p>
					<br><br>
					<p><a href='changemdp.php' >Retour changement de mot de passe</a>&nbsp;&nbsp;<a href='bureau.php' alt='Bureau'>Retour au bureau</a></p><br><br>";		
			}
		}
		else   //formulaire
		{		
			echo "<form method=\"post\" action=\"changemdp.php\" enctype=\"multipart/form-data\">	
			<p class='titre'>Changement de mot de passe:</p><br>
			<table width=\"90%\" cellpadding=\"5\">
			<tr>
			<td width=\"50%\"><p class=\"t3\">Ancien mot de passe:</p></td>
			<td class=\"t2\" width=\"50%\"><input type=\"password\" name=\"mdpancien\" size=\"30\"></td>
			</tr>
			<tr>
			<td><p class=\"t3\">Nouveau mot de passe:</p></td>
			<td class=\"t2\"><input type=\"password\" name=\"mdpnouveau\" size=\"30\"></td>
			</tr>
			<tr>
			<td><p class=\"t3\">Confirmez votre nouveau mot de passe:</p></td>
			<td class=\"t2\"><input type=\"password\" name=\"mdpnouveau2\" size=\"30\"></td>
			</tr>
			<tr>
			<td colspan=\"2\" class=\"t1\"><input type=\"submit\" value=\" Sauvegarder \" ></td>
			</tr>
			</table>			
			</form>";
		}
		echo "<br></div>";	
	}
	else
	{ 	 // délai dépassé
		header ("location:troptard.php");
		session_destroy();
	}
 }
else
{ 	 // pas de session
	header ("location:404.php");
	session_destroy();
}
mysql_close($connexion);
include ("fin.php");	
?>