<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/

if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
 {								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade`, `nbr_cha` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		$nbr_cha=$ligne[1];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
		switch ($grade)
		{
			case 'SELISTE' : include("lien/seliste.php");break;	
			case 'MODERATEUR' :include("lien/seliste.php"); include("lien/moderateur.php");break;
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page
		echo "<br><div class='corps'><br><table summary=\"\" border='1' class='tablevu' width='100%'>
		<tr>
			<th><p>Version</p></th>
			<th><p>Date</p></th>
			<th><p>Modifications</p></th>
		</tr>
		<tr>
			<td class='t1'><p>3.1.6</p></td>
			<td class='t2'><p>05/2014</p></td>
			<td class='t2'><p class='left'>
			- Bug: Sous rubrique avec plus de texte.<br>
			- Préparatif : Mise à jour semi automatique.<br>
			- 2eme page en supplément.<br>
			- Centrage automatique du bloc coloré (Ckeditor)<br>
			<span class='pasimportant'>Modifications mineurs:<br>
			- Correction d'orthographe ( photos.php) 
			</span>
			</p></td>
		</tr>
		<tr>
			<td class='t1'><p>3.1.5</p></td>
			<td class='t2'><p>04/2014</p></td>
			<td class='t2'><p class='left'>
			- Affichage du nom de famille en parmètre.<br>
			- Nom affiché uniquement au modos.<br>
			- Email pour inscription différent de l'email générique.<br>
			- Préparation pour Mise a jour automatique.<br>
			<span class='pasimportant'>Modifications mineurs:<br>
			- Changement du texte pour le nom dans l'edition de profil.<br>
			- Correction d'un message d'erreur lors de la deconnexion.
			</span>
			</p></td>
		</tr>
		<tr>
			<td class='t1'><p>3.1.4</p></td>
			<td class='t2'><p>02-03/2014</p></td>
			<td class='t2'><p class='left'>
			- Mails automatique aux modérateurs.<br>
			- Page en supplément pour réglement ou statut.<br>
			<span class='pasimportant'>Modifications mineurs:<br>
			- Supression automatique des anciennes sauvegardes.<br>
			</span>
			</p></td>
		</tr>
		<tr>
			<td class='t1'><p>3.1.3</p></td>
			<td class='t2'><p>01/2014</p></td>
			<td class='t2'><p class='left'>
			- Inscription des non Internautes.<br>
			<span class='pasimportant'>Modifications mineurs:<br>
			- Correction du bug serveur privé.<br>
			- Correction du bug annonce externe.<br>
			- Suppresion du calcul de solde sur l'activité.<br>
			</span>
			</p></td>
		</tr>
		<tr>
			<td class='t1'><p>3.1.2</p></td>
			<td class='t2'><p>12/2013</p></td>
			<td class='t2'><p class='left'>
			- Fonction pour ne pas afficher les soldes comptable.<br>
			<span class='pasimportant'>Modifications mineurs:<br>
			- Ajout d'une url fixe (Blog).<br>
			</span>
			</p></td>
		</tr>
		<tr>
			<td class='t1'><p>3.1.1</p></td>
			<td class='t2'><p>11/2013</p></td>
			<td class='t2'><p class='left'>
			- Ajout gestion d'un compte bancaire.<br>
			- Export des petites annonces.<br>
			- Ajout d'un échange manuel.<br>
			- Validation automatique sans comptable.<br>
			- Séparation de la page selistes + création page modo.<br>
			<span class='pasimportant'>Modifications mineurs:<br>
			- Correction liste des services.<br>
			- Ajout du numéro de téléphone sur l'export catalogue.<br>
			- Correction de faute d'orthographe.<br>
			</span>
			</p></td>
		</tr>
		<tr>
			<td class='t1'><p>3.1.0</p></td>
			<td class='t2'><p>10/2013</p></td>
			<td class='t2'><p class='left'>
			- Administration partie public et modérateurs subdivisé(Admin).<br>
			- Temporisation petite annonce et actualité.<br>
			- Paramétrage des temporisations.<br>
			<span class='pasimportant'>Modifications mineurs:<br>
			- Bug UTf-8 accent corrigé.<br>
			</span>
			</p></td>
		</tr>
		<tr>
			<td class='t1'><p>3.0.1</p></td>
			<td class='t2'><p>08/2013-09/2013</p></td>
			<td class='t2'><p class='left'>
			- Bug de changement de mot de passe corrigé.<br>
			- Recherche par courriel (Modos).<br>
			<span class='pasimportant'>Modifications mineurs:<br>
			- Mise en forme cadre de connexion.<br>
			- Bug page séliste corrigé.<br></span>
			</p></td>
		</tr>
		<tr>
			<td class='t1'><p>3.0.0</p></td>
			<td class='t2'><p>01/2013-07/2013</p></td>
			<td class='t2'><p class='left'>
			- Passage en UTF-8<br>
			- Nouveau Design.<br>
			- Nouveau Menu.<br>
			- Instalation d'un editeur dans actualité et agenda.<br>
			- Calendrier agenda<br>
			- Carte des membres<br>
			- Gestion du catalogue simplfié en liste(Modos).<br>
			- Recherche automatique des demandes d'ajout catalogue (Modos).<br>
			- Gestion du catalogue simplfié en liste(Modos).<br>
			- Serveur privé pour stockage de fichier(Modos).<br>
			<span class='pasimportant'>Modifications mineurs:<br>
			- Amélioration de la liste des sélistes (Modos).<br>
			- Bug comptable réglé.<br>
			- Blocage des cotisations si délai trop court.<br>
			- Liste déroulante lors d'un enregistrement d'échange.<br>
			- Nouvelle gestion des photos avec Album.<br>
			- Petite annonce avec notification par courriel.<br>
			- Petite annonce des autres SEL par Courriel.<br>
			- Centralisation de l'aide.<br>
			- Page de messages.<br>
			- Connection par numéro.<br>
			- Affichage grisé des membres desactivés.<br></span>
			</p></td>
		</tr>
		<tr>
			<td class='t1'><p>2.1.6</p></td>
			<td class='t2'><p>12/2012</p></td>
			<td class='t2'><p class='left'>
			- Mise à jour des ages semi-auto<br>
			- Liste des villes.<br>
			- Ajout de desactivation de mail petite annonce.<br>
			- Differenciation entre agenda et actualité pour les mails.<br>
			- Modification du bureau.<br>
			<span class='pasimportant'>Modifications mineurs:<br>
			- Ajout bouton échange sur le profil<br>	</span>		
			</p></td>
		</tr>
		<tr>
			<td class='t1'><p>2.1.5</p></td>
			<td class='t2'><p>09-11/2012</p></td>
			<td class='t2'><p class='left'>
			- Modification de l'affichage des Recherches.<br>
			- Fonction des messages sauvegardés.<br>
			<span class='pasimportant'>Modifications mineurs:<br> 
			- Correction de bug modération agenda.</br>
			- Correction admin multi message.</br>
			- Correction de bug de l'agenda.</br></span>
			</p></td>
		</tr>
		<tr>
			<td class='t1'><p>2.1.4</p></td>
			<td class='t2'><p>08/2012</p></td>
			<td class='t2'><p class='left'>
			- Paramétrage Mot de passe.</br>
			- Paramétrage activation.</br>
			- Paramétrage maintenance.</br>
			</p></td>
		</tr>
		<tr>
			<td class='t1'><p>2.1.3</p></td>
			<td class='t2'><p>07/2012</p></td>
			<td class='t2'><p class='left'>
			- Nettoyage base de données automatisé.</br>
			- Catalogue remanié avec champs de recherche Multi critères.</br>
			<span class='pasimportant'>Modifications mineurs:<br> 
			- Design des icônes.<br>
			- Nettoyage multi message automatisé.<br>
			- Nettoyage services desactivé automatisé.<br>
			- Design des icônes.<br>
			- Balise du serveur.<br>
			- Récupération des fichiers par mail.<br>
			- Administration messagerie.<br>
			- Affichage du nombre d'email générés.</span>
			</p></td>
		</tr>
				<tr>
			<td class='t1'><p>2.1.2</p></td>
			<td class='t2'><p>05-06/2012</p></td>
			<td class='t2'><p class='left'>
			- Backup automatique.<br>
			<span class='pasimportant'>Modifications mineurs:<br> 
			- Paramétrage des modérateurs.<br>
			- Refonte page séliste.<br>
			- Amélioration design d'administration.<br>
			- Amélioration de gestion des photos.</span>
			</p></td>
		</tr>
		<tr>
			<td class='t1'><p>2.1.1</p></td>
			<td class='t2'><p>04/2012</p></td>
			<td class='t2'><p class='left'>
			- Refonte des Mails de masse.<br>
			<span class='pasimportant'>Modifications mineurs:<br> 
			- Ajout de la variable de Temporisation des mails.</span>
			</p></td>
		</tr>
		<tr>
			<td class='t1'><p>2.1.0</p></td>
			<td class='t2'><p>03/2012</p></td>
			<td class='t2'><p class='left'>
			- Fonction de desactivation de modules.<br>
			<span class='pasimportant'>Modifications mineurs:<br> 
			- Affichage et standardisation listes membres.</span>
			</p></td>
		</tr>
		<tr>
			<td class='t1'><p>2.0.3</p></td>
			<td class='t2'><p>09/201</p></td>
			<td class='t2'><p class='left'>
			- Creation boite à idée<br>
			<span class='pasimportant'>Modifications mineurs:<br> 
			- Non affichage des membres inactif dans la catalogue(bug).</span>
			</p></td>
		</tr>
		<tr>
			<td class='t1'><p>2.0.2</p></td>
			<td class='t2'><p>06/2011</p></td>
			<td class='t2'><p class='left'>
			-Changement de la gestion des mails<br>
			<span class='pasimportant'>Modifications mineurs:<br> 
			- Intégration commentaire des services dans le catalogue.</span>
			</p></td>
		</tr>
		<tr>
			<td class='t1'><p>2.0.1</p></td>
			<td class='t2'><p>2010-2011</p></td>
			<td class='t2'><p class='left'>
			- Passage en Version Libre.<br>
			- Mise en paramétrage.
			</p></td>
		</tr>
		<tr>
			<td class='t1'><p>1.2.5</p></td>
			<td class='t2'><p>11/2010</p></td>
			<td class='t2'><p class='left'>
			- Simplification des codes aléatoires.<br>
			- Ajout du quartier.<br>
			- Détail du catalogue obligatoire.
			</p></td>
		</tr>
		</tr>
				<tr> 
			<td class='t1'><p>1.2.4</p></td> 
			<td class='t2'><p>04-05/2010</p></td> 
			<td class='t2'><p class='left'> 
			<span class='pasimportant'>Modifications mineurs:<br> 
			- Page de paramètrage ( titre reinscription..)<br> 
			- Fonction d'edition de page autres sel.<br> 
			- Personalisation des services.
			</p></td> 
		</tr> 
		<tr> 
			<td class='t1'><p>1.2.3</p></td> 
			<td class='t2'><p>02/2010-03/2010</p></td> 
			<td class='t2'><p class='left'> 
			- Design cadrage blanc.<br> 
			<span class='pasimportant'>Modifications mineurs:<br> 
			- Réinscription.<br> 
			- Mise à jour de la charte suppression du réglement<br> 
			- Mise à jour date de connection par un lien d'email.<br> 
			- Mise à jour des signatures.<br> 
			- Modification de l'emplacement de la date de naissance.
			</p></td> 
		</tr> 
		<tr> 
			<td class='t1'><p>1.2.2</p></td> 
			<td class='t2'><p>01/2010</p></td> 
			<td class='t2'><p class='left'> 
			<span class='pasimportant'>Modifications mineurs:<br> 
			- Réinscription.<br> 
			- Page d'aide rédactionnel.<br> 
			- Correction date des automessage de validation.<br> 
			- Ajout date de la demande sur les profils.<br> 
			- Mise en place d'une image sur les profils(homme/femme).
			</p></td> 
		</tr> 
		<tr> 
			<td class='t1'><p>1.2.1</p></td> 
			<td class='t2'><p>12/2009</p></td> 
			<td class='t2'><p class='left'> 
			<span class='pasimportant'>Modifications mineurs:<br> 
			- Modification du message dans le multimessage.<br> 
			</p></td> 
		</tr> 
		<tr> 
			<td class='t1'><p>1.2.0</p></td> 
			<td class='t2'><p>11/2009</p></td> 
			<td class='t2'><p class='left'> 
			- Creation de trois grades ( Admin, moderateur et Seliste)
			<span class='pasimportant'>Modifications mineurs:<br> 
			- Page selistes admin sur 2 lignes.<br> 
			- Messages d'information sur l'agenda.<br> 
			- 7 Actualités au lieu de 5 d'affiché.<br> 
			- Date inscription du séliste affiché sur le profil.
			</p></td> 
		</tr> 
		<tr> 
			<td class='t1'><p>1.1.6</p></td> 
			<td class='t2'><p>10/2009</p></td> 
			<td class='t2'><p class='left'><span class='pasimportant'>Modifications mineurs:<br> 
			- Défiltrage des photos.<br> 
			- Modification de l'administration de l'agenda.<br> 
			- Création de la présentation public animée.<br> 
			- Lien pour gere sa liste d'amis<br> 
			- Bouton Envoyer un message en haut du profil.
			
			</p></td> 
		</tr> 
		<tr> 
			<td class='t1'><p>1.1.5</p></td> 
			<td class='t2'><p>08-09/2009</p></td> 
			<td class='t2'><p class='left'>Agenda avec répliques.
			<br><span class='pasimportant'>Modifications mineurs:<br> 
			- Log Administratif.<br> 
			- Page services l'état ne resté pas affiché.<br> 
			- Intitulé adresse email des mails automatiques.
			</p></td> 
		</tr> 
		<tr> 
			<td class='t1'><p>1.1.4</p></td> 
			<td class='t2'><p>07/2009</p></td> 
			<td class='t2'><p class='left'>4 Thèmes de couleurs.
			<br><span class='pasimportant'>Modifications mineurs:<br> 
			- Ordre des messages sur le bureau et la messagerie inversé.<br> 
			- Nouveaux design page annonces.<br> 
			- Thème mal voyants.<br> 
			- Préremplissage du message dans les annonces.<br> 
			- Affichage du message de reponse.<br> 
			- Perte de mot de passe automatisé.<br> 
			- Echange refusé de plus d'un mois non affiché.<br> 
			- Date inscription sur la page d'aceuil (partie public).<br> 
			- Multi message catalogue.</p></td> 
		</tr> 
		<tr> 
			<td class='t1'><p>1.1.3</p></td> 
			<td class='t2'><p>06/2009</p></td> 
			<td class='t2'><p class='left'>Lien dans les mails avec renvoi.<br> 
			<span class='pasimportant'>Modifications mineurs:<br> 
			- Agrandissement du chat.<br> 
			- Inversion liste sélistes.<br> 
			- Agrandissement liste échanges.<br> 
			- Statistiques évolution et graphique.<br> 
			- Condition d'utilisation mise à jour.<br> 
			- Correction d'orthographe.<br> 
			- Message sur ch'ti annonce.</span></p></td> 
		</tr> 
		<tr> 
			<td class='t1'><p>1.1.2</p></td> 
			<td class='t2'><p>05/2009</p></td> 
			<td class='t2'><p class='left'>Titre des mails avec prenom et numero.</p></td> 
		</tr> 
		<tr> 
			<td class='t1'><p>1.1.1</p></td> 
			<td class='t2'><p>04/2009</p></td> 
			<td class='t2'><p class='left'>Mémo administratif, statistique avec graphique.</p></td> 
		</tr> 
		<tr> 
			<td class='t1'><p>1.1.0</p></td> 
			<td class='t2'><p>03/2009</p></td> 
			<td class='t2'><p class='left'>Finalisation</p></td> 
		</tr> 
		</table><br><br></div><br>
		";
	}
	else
	{ 	 // délai dépassé
		header ("location:troptard.php");
		session_destroy();
	}
 }
else
{ 	 // pas de session
	header ("location:404.php");
	session_destroy();
}
mysql_close($connexion); 
echo "</center>";
include ("fin.php");	
?>
