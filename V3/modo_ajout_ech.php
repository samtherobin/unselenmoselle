<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/

if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
{								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade`, `prenom`, `email`, `grains` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		$prenom=$ligne[1];
		$email_seliste=$ligne[2];
		$grains_seliste=$ligne[3];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
		switch ($grade)
		{
			case 'SELISTE' : header("location:404.php");break;	
			case 'MODERATEUR' :include("lien/seliste.php"); include("lien/moderateur.php");break;
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;				
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page
		echo "<br><div class=\"corps\"><br>";
		if($_GET['action']=='ajouter')
		{ // ajouter un echange
			$id1=htmlentities($_POST['id1'], ENT_QUOTES, "UTF-8");
			$id2=htmlentities($_POST['id2'], ENT_QUOTES, "UTF-8");
			$grains_ech=htmlentities($_POST['grains'], ENT_QUOTES, "UTF-8");
			$ssrubrique=htmlentities($_POST['ssrubrique'], ENT_QUOTES, "UTF-8");
			$mois=htmlentities($_POST['mois'], ENT_QUOTES, "UTF-8");
			$jour=htmlentities($_POST['jour'], ENT_QUOTES, "UTF-8");
			$annee=htmlentities($_POST['annee'], ENT_QUOTES, "UTF-8");
			$detail=htmlentities($_POST['detail'], ENT_QUOTES, "UTF-8");
			
			if(($id1!=null)&&($grains_ech!=null)&&($mois!=null)&&($jour!=null)&&($annee!=null))
			{
				$time_eve= mktime(2, $minutes,0,$mois,$jour,$annee); 
				if (!(mysql_query("INSERT INTO `echanges` VALUES ('', '$id1', '$id2', '$time', '$grains_ech', '$ssrubrique', 'VAL','$detail','$time_eve','$id_seliste')"))) {
					die('Erreur : ' . mysql_error());
				}
				echo "<br><p class='t1'>L'échange a bien été enregistré il n'est pas encore validé en comptabilité.</p><br><br>
						<p class='t4'><a href='modo_ajout_ech.php'>Retour aux échanges</a>&nbsp;&nbsp;&nbsp;<a href='bureau.php' title='Bureau'>Retour sur mon bureau</a></p><br><br>";
			}
			else   // tout les champs ne sont pas complétés
			{
				echo "<br><p span class='rouge t1'>TOUT les champs doivent être complétés</p><br><br>
					<p class='t4'><a href='modo_ajout_ech.php'>Retour aux échanges</a>&nbsp;&nbsp;&nbsp;<a href='bureau.php' title='Bureau'>Retour sur mon bureau</a></p><br><br>";
			}
		}						
		else
		{ // formulaire
			echo "
			<form method=\"post\" action=\"modo_ajout_ech.php?action=ajouter\" enctype=\"multipart/form-data\">
			<table summary=\"\" class='tablevu' width=\"90%\">
			<tr>
				<th colspan='3'><p align='center'>Ajouter un échange:</p></th>
			</tr>
			<tr>
				<td class=t1>
				
				<select name=\"catalogue\" onChange=\"document.location='modo_ajout_ech.php?catalogue='+this.value\">
				<option value=\"\"> --- Catalogue ---</option>";
				// menu déroulant catalogue	
				if (!($requete=mysql_query("SELECT `id_cat`, `designation` FROM `catalogue` ORDER BY `designation` ASC"))) {
					die('Erreur : ' . mysql_error());
				}
				while($ligne=mysql_fetch_row($requete))
					{
						echo "<option value='".$ligne[0]."' ";
						if($_GET['catalogue']==$ligne[0])
						{
							echo "selected";
						}
						echo " >".$ligne[1]."</option>";
					}
				echo "</select>";
					// menu déroulant rubrique
				if( $_GET['catalogue']!=null)
				{
					$id_cat=$_GET['catalogue'];
					echo "<select name='rubrique' 
					onChange=\"document.location='modo_ajout_ech.php?catalogue=$id_cat&amp;rubrique='+this.value\">
					<option value=\"\"> -- Rubrique --</option> ";
					if (!($requete1=mysql_query("SELECT `id_rubrique`, `designation` FROM `rubrique` where `id_cat`='$id_cat' ORDER BY `designation` ASC"))) {
						die('Erreur : ' . mysql_error());
					}
					while($ligne=mysql_fetch_row($requete1))
					{
						echo "<option value='".$ligne[0]."'";
						if($_GET['rubrique']==$ligne[0])
						{
							echo "selected";
						}
							echo ">".$ligne[1]."</option>";
					}
				echo "</select>";
					}
					
				// menu déroulant sous  rubrique
				if( $_GET['rubrique']!=null)
				{
					$id_cat=$_GET['catalogue'];
					$id_rub=$_GET['rubrique'];
					$annee=date('Y');
					$mois=date('m');
					echo "<select name='ssrubrique'
					onChange=\"document.location='modo_ajout_ech.php?catalogue=$id_cat&amp;rubrique=$id_rub&amp;ssrub='+this.value\">
					<option value=\"\"> -- Sous Rubrique --</option> ";
					if (!($requete3=mysql_query("SELECT `id_ss_rub`, `designation` FROM `sous_rubrique` where `id_rubrique`='$id_rub' ORDER BY `designation` ASC"))) {
						die('Erreur : ' . mysql_error());
					}
					while($ligne=mysql_fetch_row($requete3))
				  	{
						echo "<option value='".$ligne[0]."' ";
						if($_GET['ssrub']==$ligne[0])
						{
							echo "selected";
						}
						echo ">".$ligne[1]."</option>";
					}
					echo "</select>";
				}
				if($_GET['ssrub']!=null)
				{
					$listeactif=='';
					if (!($requete=mysql_query("SELECT `id_seliste`, `prenom` FROM `selistes` WHERE `valide`='OUI' ORDER BY `id_seliste` ASC "))) {
						die('Erreur : ' . mysql_error());
					}
					while($ligne=mysql_fetch_row($requete))
					{
						$id_membre= $ligne[0];
						$prenom_membre=stripslashes($ligne[1]);
						$listeactif=$listeactif."<option value='$id_membre'>$prenom_membre($id_membre)</option>";
					}
				// rapatriement parametre
					
					echo "
					</td>
					</tr>
					<tr>
						<td class=t2><p>Séliste qui fournit le service et sera crédité :<select name='id1'>".$listeactif."</select>
						</p></td>
					</td>
					<tr>
						<td class=t2><p>Séliste qui demande le service et sera débité :<select name='id2'>".$listeactif."</select>
						</p></td>
					</tr>
					<tr>
					<td class='t2'>Date de l'échange:
					<input type='text' name='jour' size='2' maxlength='2' /> /
					<input type='text' name='mois' size='2' maxlength='2' value='".$mois."' /> /
					<input type='text' name='annee' size='4' maxlength='4' value='".$annee."' /></td>
					</tr>
					<tr>
						<td class=t2><p>L'échange a pour valeur <input name='grains' size=5 maxLength=5> $monnaie.</p></td>
					</tr>
					<tr>
						<td class=t2><p>Détail: <input name='detail' size=100 maxLength=200></p></td>
					</tr>
					<tr>
					<td colspan='3' class='t3'><input type='submit' value=' Sauvegarder ' ></td>
					</tr>"; 	
				}	
			echo "</table></form><br>";
		}
		echo "</div>";
	}
	else
	{ 	 // délai dépassé
		header ("location:troptard.php");
		session_destroy();
	}
 }
else
{ 	 // pas de session
	header ("location:404.php");
	session_destroy();
}
mysql_close($connexion);
include ("fin.php");	
?>
