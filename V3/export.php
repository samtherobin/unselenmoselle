<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/

if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
 {								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade`, `grains`, `prenom`, `nbr_mp`, `signature` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		$grains= $ligne[1];	
		$prenom= $ligne[2];			
		$nbr_mp= $ligne[3];
		$signature=$ligne[4];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
		switch ($grade)
		{
			case 'SELISTE' : include("lien/seliste.php");break;	
			case 'MODERATEUR' :include("lien/seliste.php"); include("lien/moderateur.php");break;
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page
		// le fichier doit deja exister
		$date=date('_M',$time);
		$date2=date('j/m/y',$time);
		$file = ("catalogue$date.xls"); 
		//on ouvre le fichier 
		if(!$myfile = fopen($file, "w"))     
		{
			 print("erreur: ");
			 print("'$filename' n'existe pas!\n");
			 exit;
		}
		fputs($myfile,"<table summary=\"\" border=\"1\" >
		<tr><td colspan=3 style='background-color:#008202;text-align=center'><p style='text-align=center; color:#ffffff; font-size:20pt'><b>Catalogue des services de ".$nom." du ".$date2."</b></p></td></tr>");
		if (!($requete=mysql_query("SELECT * FROM `catalogue` ORDER BY `designation` ASC"))) {
			die('Erreur : ' . mysql_error());
		}	
		while($ligne=mysql_fetch_row($requete))
		{
			$id_cat= $ligne[0];
			$designation_cat= utf8_decode($ligne[1]);
			fputs($myfile,"
			<tr>
			<td colspan=3 style='background-color:#73b8ff'><p align=center style='font-size:18pt'><b>$designation_cat</b></p></td>
			</tr>");
			//chercher les rubriques
			if (!($requete2=mysql_query("SELECT * FROM `rubrique` WHERE `id_cat`='$id_cat' ORDER BY `designation` ASC"))) {
				die('Erreur : ' . mysql_error());
			}	
			while($ligne2=mysql_fetch_row($requete2))
			{
				$id_rubrique= $ligne2[0];
				$designation_rub= utf8_decode($ligne2[2]);
				fputs($myfile,"
				<tr>
				<td colspan=3 style='background-color:#cdffce;'><p align=center style='align=center; font-size:16pt; color:#000000'><b>$designation_rub</b></p></td>
				</tr>
				<tr>
				<td width='150px' style='background-color:#c5e4ff; width:200px'><p align=center><b>Offres:</b></p></td>
				<td style='background-color:#c5e4ff; width:400px'><p align=center><b>Services:</b></p></td>
				<td style='background-color:#c5e4ff; width:200px'><p align=center><b>Demandes:</b></p></td>
				</tr>");
				//chercher les sous rubriques
				if (!($requete3=mysql_query("SELECT * FROM `sous_rubrique` WHERE `id_rubrique`='$id_rubrique' ORDER BY `designation` ASC"))) {
					die('Erreur : ' . mysql_error());
				}	
				while($ligne3=mysql_fetch_row($requete3))
				{
					$id_ss_rubrique= $ligne3[0];
					$designation_ss_rub= utf8_decode(stripslashes($ligne3[2]));
					if (!($requete4=mysql_query("SELECT `id_seliste`,`detail` FROM `services` WHERE `id_ss_rub`='$id_ss_rubrique' AND `etat`='PRO' ORDER BY `time` ASC"))) {
						die('Erreur : ' . mysql_error());
					}
					if (!($requete5=mysql_query("SELECT `id_seliste`,`detail` FROM `services` WHERE `id_ss_rub`='$id_ss_rubrique' AND `etat`='DEM' ORDER BY `time` ASC"))) {
						die('Erreur : ' . mysql_error());
					}
					fputs($myfile,"<tr>
					<td><p><i>");
					// affichage des selistes qui propose
					while($ligne4=mysql_fetch_row($requete4))
					{
						$id_seliste_pro=$ligne4[0];
						$detail_pro=utf8_decode(stripslashes($ligne4[1]));
						if (!($requete6=mysql_query("SELECT `prenom`,`tel` FROM `selistes` WHERE `id_seliste`='$id_seliste_pro'"))) {
							die('Erreur : ' . mysql_error());
						}
						$ligne6=mysql_fetch_row($requete6);
						$prenom_pro=$ligne6[0];
						$tel_pro=$ligne6[1];
						fputs($myfile,"$prenom_pro($id_seliste_pro)-<i>$tel_pro</i><br>$detail_pro<br>");
					}				
					fputs($myfile,"</i></p></td>
					<td style='background-color:#d9edff'><p align=center>$designation_ss_rub</p></td>
					<td><p><i>");
					// affichage des seliste qui demande
					while($ligne5=mysql_fetch_row($requete5))
					{
						$id_seliste_dem=$ligne5[0];
						$detail_dem=utf8_decode(stripslashes($ligne5[1]));
						if (!($requete7=mysql_query("SELECT `prenom`,`tel` FROM `selistes` WHERE `id_seliste`='$id_seliste_dem'"))) {
							die('Erreur : ' . mysql_error());
						}
						$ligne7=mysql_fetch_row($requete7);
						$prenom_dem=$ligne7[0];
						$tel_dem=$ligne7[1];
						fputs($myfile,"$prenom_dem($id_seliste_dem)-<i>$tel_pro</i><br>$detail_dem<br>");
					}
					fputs($myfile,"</i></p></td>
					</tr>");
				}
			}
		}
		fputs($myfile,"</table>");
		//fermeture fichier
		fclose($myfile);   //on ferme le fichier
		mysql_close();  // on ferme la connexion
		echo "<br><br><div class='corps'><br><br>
		<p class='titre'>Exportation du catalogue.</p><br>
		<p class='t4'>- Mise à jour du catalogue Réussie - <br>
		<a href='catalogue$date.xls' class='aphoto'>Cliquez ici pour l'ouvrir & le télécharger sur votre ordinateur.<br><img src='images/excel.gif' width='50'></a></p>
		<br></div><br><br>";
		include ("fin.php");
		
	}
	else
	{ 	// trop tard
		header ("location:404.php");
		session_destroy();
	}
}
else
{ 	// voleur
	header ("location:404.php");
	session_destroy();
}
?>