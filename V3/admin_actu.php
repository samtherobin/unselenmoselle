<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/

if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
{								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade`, `nbr_art`, `prenom` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		$nbr_art=$ligne[1];
		$prenom=$ligne[2];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
		switch ($grade)
		{
			case 'SELISTE' : header("location:404.php");break;
			case 'MODERATEUR' :include("lien/seliste.php"); include("lien/moderateur.php");break;
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page
		
		if ($_GET['action']=='voir') 
		{
			if (!($requete=mysql_query("SELECT COUNT(*) FROM selistes WHERE valide='OUI' AND `mail_actu`='OUI'"))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete);
			$nbrmail=$ligne[0];
			echo "<br><br><div class='corps'><br>
			<p class='titre'>Les actualités proposées à gérer:</p><br>
			";
			if (!($requete=mysql_query("SELECT * FROM `articles` WHERE `publication`='NON' ORDER BY `timestamp` DESC "))) {
				die('Erreur : ' . mysql_error());
			}
			while($ligne=mysql_fetch_row($requete))
			{
				$id_article=$ligne[0];
				$id_ecriveur=$ligne[1];
				$timestamp=$ligne[2];
				$date=date('d/m/y à H\h',$timestamp);	
				$titre= stripslashes($ligne[3]);
				$article= stripslashes($ligne[4]);
				$message=$article;
				include("smileyscompagnie.php");
				$article=$message;
				if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`='$id_ecriveur'")))) {
					die('Erreur : ' . mysql_error());
				}
				$prenom_ecriveur=$recup[0];		
				if (!($nb_replique=mysql_num_rows(mysql_query("SELECT * FROM `articles_repliques` WHERE `id_article`='$id_article'")))) {
					die('Erreur : ' . mysql_error());
				}
				echo "
				<div class='message'>
				<p class='t1'>".$titre." </p>
				<p>Actualité écrit par <a href=\"profil.php?id=".$id_ecriveur."\">".$prenom_ecriveur."(".$id_ecriveur.")</a> le ".$date."</p><br>
				<hr width=70%><span class='t2'>".$article."</span><hr width=70%><br><p>
				<a class='amodo' href=\"admin_actu.php?action=replique&amp;id=".$id_article."\">".$nb_replique." Avis sur cette actualité</a>&nbsp;&nbsp;
				<a class='amodo' href=\"admin_actu.php?action=edition&amp;id=".$id_article."\"><img src='images/editer.gif'> Edition </a>&nbsp;&nbsp;
				<a class='amodo' href=\"admin_actu.php?action=publication&amp;mail=non&amp;id=".$id_article."\"><img src='images/lettre.jpg'> Publication SANS Mail</a>&nbsp;&nbsp;
				<a class='amodo' href=\"admin_actu.php?action=publication&amp;id=".$id_article."\"><img src='images/lettre.jpg'> Publication et $nbrmail courriels envoyés</a>&nbsp;&nbsp;
				<a class='amodo' href=\"admin_actu.php?action=supprimer&amp;id=".$id_article."\"><img src='images/croix.jpg'> Supprimer</a></p>
				<br></div><br>";	
			}
			echo"<br></div>";
		}
		// formulaire commentaire article
		elseif ($_GET['action']=='replique') 
		{
			$id_article=$_GET['id'];
			if (!($requete=mysql_fetch_row(mysql_query("SELECT `publication` FROM `articles` WHERE `id_article`=$id_article")))) {
				die('Erreur : ' . mysql_error());
			}
			$publication=$requete[0];
			if($publication=='NON')
			{
				if (!($requete=mysql_query("SELECT * FROM `articles` WHERE `id_article`='$id_article'"))) {
					die('Erreur : ' . mysql_error());
				}
				$cpt_req;
				$ligne=mysql_fetch_row($requete);
				$id_article= $ligne[0];
				$id_ecriveur= $ligne[1];
				$timestamp= $ligne[2];
				$date=date('d/m/y à H\h',$timestamp);	
				$titre=stripslashes($ligne[3]);
				$article=stripslashes($ligne[4]);
				$message=$article;
				include("smileyscompagnie.php");
				$article=$message;
				if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`=$id_ecriveur")))) {
					die('Erreur : ' . mysql_error());
				}
				$prenom_ecriveur=$recup[0];		
				echo "<br><br><div class='corps'><br>
				<p class='titre'>Les actualités proposées à gérer:</p><br>
				<div class='message'>
				<p class='t1'>".$titre." </p>
				<p>Actualité écrit par <a href=\"profil.php?id=".$id_ecriveur."\">".$prenom_ecriveur."(".$id_ecriveur.")</a> le ".$date."</p><br>
				<hr width=70%><p class='t2'>".$article."</p><hr width=70%><br><p>
				<a class='amodo' href=\"admin_actu.php?action=commentaire&amp;id=".$id_article."\">".$nb_replique." Poster un avis sur cette actualité</a>&nbsp;&nbsp;
				<a class='amodo' href=\"admin_actu.php?action=edition&amp;id=".$id_article."\"><img src='images/editer.gif'> Edition </a>&nbsp;&nbsp;
				<a class='amodo' href=\"admin_actu.php?action=publication&amp;mail=non&amp;id=".$id_agenda."\"><img src='images/lettre.jpg'> Publication SANS Mail</a>&nbsp;&nbsp;
				<a class='amodo' href=\"admin_actu.php?action=publication&amp;id=".$id_article."\"><img src='images/lettre.jpg'> Publication et $nbrmail courriels envoyés</a>&nbsp;&nbsp;
				<a class='amodo' href=\"admin_actu.php?action=supprimer&amp;id=".$id_article."\"><img src='images/croix.jpg'> Supprimer</a></p>
				<br></div><br>";
				// recup des repliques
				if (!($requete=mysql_query("SELECT * FROM `articles_repliques` WHERE `id_article`='$id_article' ORDER BY `timestamp` ASC"))) {
					die('Erreur : ' . mysql_error());
				}	
				while($ligne=mysql_fetch_row($requete))
				{
					$id_ecriveur= $ligne[2];
					$timestamp= $ligne[3];
					$date=date('d/m/y à H\h i\m\i\n',$timestamp);	
					$message=stripslashes($ligne[4]);
					include("smileyscompagnie.php");
					if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`=$id_ecriveur")))) {
						die('Erreur : ' . mysql_error());
					}
					$prenom_ecriveur=$recup[0];		
					echo "
					<div class='message'><p class=\"pasimportant\">Réponse de <a href='profil.php?id=".$id_ecriveur."'>".$prenom_ecriveur."(".$id_ecriveur.")</a> le ".$date."</p><br>
					<p>".$message."</p>
					<br></div><br>";
				}
				echo"	
				<br></div><br>";
			}
		}
		// formulaire commentaire article
		elseif ($_GET['action']=='commentaire') 
		{
			$id_article=$_GET['id'];
			if (!($requete=mysql_fetch_row(mysql_query("SELECT `publication` FROM `articles` WHERE `id_article`=$id_article")))) {
				die('Erreur : ' . mysql_error());
			}
			$publication=$requete[0];
			if($publication=='NON')
			{
				if (!($recup=mysql_fetch_row(mysql_query("SELECT `titre` FROM `articles` WHERE `id_article`=$id_article")))) {
					die('Erreur : ' . mysql_error());
				}
				$titre=stripslashes($recup[0]);
				echo "<br><br><div class='corps'><br>
				<form method='post' action='admin_actu.php?action=envoye&amp;id=$id_article' enctype='multipart/form-data'>
				<input type='hidden' name='id_article' value=$id_article >
				<p class='t1'>Répondre à l'article : \" ".$titre." \"</p>
				<p class='t1'>
				<textarea name='message' cols='80' rows='10'></textarea>
				<br><input type='submit' value=' Répondre '></form><br></div><br>";
			}
		}
		// envoyer la replique
		elseif ($_GET['action']=='envoye') 
		{
			$id_article=$_GET['id'];
			$message=nl2br(htmlentities($_POST['message'], ENT_QUOTES, "UTF-8"));
			if (!($requete=mysql_fetch_row(mysql_query("SELECT `publication` FROM `articles` WHERE `id_article`=$id_article")))) {
				die('Erreur : ' . mysql_error());
			}
			$publication=$requete[0];
			if($publication=='NON')
			{
				if (!mysql_query("INSERT INTO `articles_repliques` VALUES ('', '$id_article', '$id_seliste', '$time', '$message')")) {
					die('Requête invalide : ' . mysql_error());
				}
				echo "<br><br><div class='corps'><br>			
				<p class='titre'>Votre message a bien été envoyé.</p>	<br>
				<p><a class='amodo' href='admin_actu.php?action=voir'>Retour gestion actualités</a>&nbsp;&nbsp;
				<a href='bureau.php'>Retour au bureau</a></p><br></div>";
			}				
		}
		// publication d'un article
		elseif ($_GET['action']=='publication') 
		{
			$id_article=$_GET['id'];
			if (!($requete=mysql_fetch_row(mysql_query("SELECT `publication`, `titre`, `article` FROM `articles` WHERE `id_article`='$id_article'")))) {
				die('Erreur : ' . mysql_error());
			}
			$publication=$requete[0];
			$titre=$requete[1];		
			$message_mail=stripslashes($requete[2]);
			include("smileysmail.php");
			$message2=$message_mail;
			if($publication=='NON')
			{
			    // Supprimer les replique quand publication:
				if (!(mysql_query("DELETE FROM `articles_repliques` WHERE `id_article`='$id_article'"))) {
					die('Erreur : ' . mysql_error());
				}
				if (!(mysql_query("UPDATE `articles` SET `timestamp` = '$time', `publication` ='OUI' WHERE `id_article`=$id_article LIMIT 1") )) {
					die('Erreur : ' . mysql_error());
				}
				// l'enregistré dans le log 
				if (!mysql_query("INSERT INTO `log` VALUES ('', '$id_seliste', '$id_article', '$time', 'article_PUBLIE', '$titre')")) {
					die('Requête invalide : ' . mysql_error());
				}
				// envoi du mail:
				if (!($requete=mysql_query("SELECT `prenom`, `email` FROM `selistes` WHERE `valide`='OUI' AND `mail_actu`='OUI'"))) {
					die('Erreur : ' . mysql_error());
				}
				// selection de celui qui à écrit l'article
				if (!($requete2=mysql_fetch_row(mysql_query("SELECT `id_seliste` FROM `articles` WHERE `id_article`='$id_article'")))) {
					die('Erreur : ' . mysql_error());
				}
				$id_ecrit=$requete2[0];	
				if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste` =$id_ecrit")))) {
					die('Erreur : ' . mysql_error());
				}
				$prenom_ecrit=stripslashes($recup[0]);
				$titre=html_entity_decode($titre,ENT_QUOTES,"UTF-8");$i=0;
				$titre=stripslashes($titre);
				$message="[".$nom."]Actualité: ".$titre."";
				$headers = "From: Actualite $nom <$email>\n";
				$headers .= "Reply-To: $email\n";
				$headers .= "X-Sender: $email\n";
				$headers .= "X-Author: Actualité $nom\n";
				$headers .= "X-Priority:1\n";
				$headers .= "X-Mailer: PHP\n";
				$headers .= "Return_Path: <$email>\n";
				$headers .='Content-Type: text/html; charset="UTF-8"'."\n";
				if($_GET['mail']==null)
				{					
					while($ligne=mysql_fetch_row($requete))
					{
						$i++;
						$prenom_dest=stripslashes($ligne[0]);
						$email_dest=$ligne[1];
						// envoi du mail 						
						$mel ="
						<html><head><title></title></head><body>
						<p>Bonjour ".$prenom_dest.", une nouvelle actualité écrite par <a href=http://".$site."/index.php?vers=profil.php?id=".$id_ecrit.">".$prenom_ecrit."(".$id_ecrit.")</a>, validé par <a href=http://".$site."/index.php?vers=profil.php?id=".$id_seliste.">".$prenom."(".$id_seliste.")</a> vient d'être publiée.
						<br><br><b>".$titre."</b>
						<br><hr width=90%><br>".$message2."
						<br><hr width=90%>
						<br>Pour la lire sur le site <a href=http://".$site."/index.php?vers=actualites.php?action=lire> cliquez-ici </a>
						<br>Cordialement, <br> ".$nom.".
						<br><br><i>Si vous ne souhaitez plus avoir ces alertes:
						<br><a href=http://".$site."/index.php?vers=compte.php> Cliquez ici </a> ou via la page notifications (en haut à gauche), pour desactiver ces alertes.</i></p></body></html>";
						sw_mail($email_dest,$message,$mel,$headers);
					}
				}
				else
				{
					$i=0;
				}
			}
			echo "<br><br><div class='corps'>
			<br><p class='titre'>Actualité publiée et $i Emails envoyés.</p>	
			<br>
			<p><a class='amodo' href='admin_actu.php?action=voir'>Retour gestion actualités</a>&nbsp;&nbsp;
			<a href='bureau.php'>Retour au bureau</a></p><br></div>";
		}
		// edition d'une article
		elseif ($_GET['action']=='edition') 
		{
			$id_article=$_GET['id'];
			if (!($requete=mysql_fetch_row(mysql_query("SELECT * FROM `articles` WHERE `id_article`=$id_article")))) {
				die('Erreur : ' . mysql_error());
			}
			$id_ecriveur=$requete[1];
			$timestamp=$requete[2];
			$titre=stripslashes($requete[3]);
			$article=stripslashes($requete[4]);
			$article=str_replace("<br />","",$article);	
			if (!($recup=mysql_fetch_row(mysql_query("SELECT `prenom` FROM `selistes` WHERE `id_seliste`=$id_ecriveur")))) {
				die('Erreur : ' . mysql_error());
			}
			$prenom_ecriveur=stripslashes($recup[0]);		
			echo "<br><br><div class='corps'><br><script type=\"text/javascript\" src=\"ckeditor/ckeditor.js\"></script>
			<script type=\"text/javascript\" src=\"ckeditor/config.js\"></script>
			<form method='post' action='admin_actu.php?action=env_edition&amp;id=$id_article' enctype='multipart/form-data'>
			
			<input type='hidden' name='id_article' value=$id_article >
					<p class='titre'>Edition de l'actualité \"".$titre."\"</p>	
					<br><p>Actu proposé par: <a href='profil.php?id=".$id_ecriveur."'>".$prenom_ecriveur."(".$id_ecriveur.")</a></p><br>
					<p>Titre:<br><input type='text' name='titre' size='50' maxlength='50' value=\"".$titre."\" /></p>
					<p>Actualité:<center><br><textarea name='article' class=\"ckeditor\" id=\"editeur\" name=\"editeur\" cols='80' rows='30'>".$article."</textarea></center></p><br>
					<p><input type='submit' value=' Enregistrer les modifications '></p><br>
					<p><a class='amodo' href='admin_actu.php?action=voir'>Revoir les actualités à gérer</a>&nbsp;&nbsp;&nbsp;&nbsp;
					<a href='bureau.php' title='bureau'>Retour bureau</a></p><br>
					</form><br></div>";
		}
		//envoi de l'edition d'un article
		elseif ($_GET['action']=='env_edition') 
		{
			$id_article=$_GET['id'];
			$id_article_post=$_POST['id_article'];
			if($id_article==$id_article_post)
			{
				// transformation des bbcodes et smileys
				$message=$_POST['article'];
				$article=$message;
				$titre=nl2br(htmlentities($_POST['titre'], ENT_QUOTES, "UTF-8"));;
				if (!($requete=mysql_query("UPDATE `articles` SET `article` = '$article', `titre` = '$titre' WHERE `id_article` ='$id_article' LIMIT 1"))) {
					die('Erreur : ' . mysql_error());
				}
				echo "<br><br><div class='corps'><br>	
				<p class='titre'>Edition enregistrée.</p><br>
				<p><a class='amodo' href='admin_actu.php?action=voir'>Retour gestion actualités</a>&nbsp;&nbsp;
				<a href='bureau.php'>Retour au bureau</a></p><br></div>";
			}
			// post correspond pas au get
			else
			{
				header ("location:404.php");
				session_destroy();
			}
		}
		//suppresion  d'un article
		elseif (($_GET['action']=='supprimer')&&(($grade=='MODERATEUR')||($grade=='ADMIN')))
		{
			$id_article=$_GET['id'];
			if($_POST['justificatif']==NULL)
			{
				echo "<br><br><div class='corps'><br>
				<form method='post' action='admin_actu.php?action=supprimer&amp;id=$id_article' enctype='multipart/form-data'>
				<input type='hidden' name='id_article' value=$id_article >
				<p class='titre'>Justifier la suppression de l'actualité:<br>
				<textarea name='justificatif' cols='80' rows='6'></textarea><br>
				<input type='submit' value=' Supprimer '>				
				</form></p><br>
				<p><a class='amodo' href='admin_actu.php?action=voir'>Revoir les actualités à gérer</a>&nbsp;&nbsp;&nbsp;&nbsp;
					<a href='bureau.php' title='bureau'>Retour bureau</a></p><br>
					</form><br></div>";
			}
			else
			{
				$justificatif=nl2br(htmlentities($_POST['justificatif'], ENT_QUOTES, "UTF-8"));
				$id_article=$_POST['id_article'];
				if($id_article==$_GET['id'])
				{
					// l'enregistré dans le log 
					if (!mysql_query("INSERT INTO `log` VALUES ('', '$id_seliste', '$id_article', '$time', 'article_SUPPRIME', '$justificatif')")) {
						die('Requête invalide : ' . mysql_error());
					}
					// supprimer de la base.
					if (!(mysql_query("UPDATE `articles` SET `publication` = 'SUP' WHERE `id_article` =$id_article LIMIT 1") )) {
						die('Erreur : ' . mysql_error());
					}
					echo "<br><br><div class='corps'><br>	
				<p class='titre'>Actualité supprimée!</p><br>
				<p><a class='amodo' href='admin_actu.php?action=voir'>Retour gestion actualités</a>&nbsp;&nbsp;
				<a href='bureau.php'>Retour au bureau</a></p><br></div>";
				}
			}
		}					
		//rien dans l'action
		else
		{
			header ("location:404.php");
			session_destroy();
		}
	}
	else
	{ 	 //délai dépassé
		header ("location:troptard.php");
		session_destroy();
	}
 }
else
{ 	 // pas de session
	header ("location:404.php");
	session_destroy();
}
mysql_close($connexion); 
include ("fin.php");	
?>
