<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/

if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
{								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{
		
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
		switch ($grade)
		{
			case 'SELISTE' : include("lien/seliste.php");break;	
			case 'MODERATEUR' :include("lien/seliste.php"); include("lien/moderateur.php");break;
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page
		// d'abord on recupére le parametre pour savoir si on affiche le solde ou pas et le nom de famille
		if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'affsolde'")))) {
			die('Erreur : ' . mysql_error());
		}
		$affsolde=stripslashes($recup[0]);
		if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'affnom'")))) {
			die('Erreur : ' . mysql_error());
		}
		$affnom=stripslashes($recup[0]);
		echo "<br><div class='corps'><br>";
			// rapatriement parametre
		if (!($recup=mysql_fetch_row(mysql_query("SELECT `texte` FROM `variables` WHERE `variable` = 'monnaie'")))) {
			die('Erreur : ' . mysql_error());
		}
		$monnaie=stripslashes($recup[0]);	
		if(isset($_GET['id']))
		{
			//voir la fiche
			$id_url=$_GET['id'];
			if (!($requete=mysql_query("SELECT *  FROM `selistes` WHERE `id_seliste`='$id_url'"))) {
				die('Erreur : ' . mysql_error());
			}	
			$ligne=mysql_fetch_row($requete) ;
			$id_sel= $ligne[0];
			$email=$ligne[1];
			$grade_sel= $ligne[2];
			$connection=$ligne[3];
			$grains_sel=$ligne[4];
			$prenom_sel=stripslashes($ligne[5]);
			$nom_sel=stripslashes($ligne[6]);
			$sexe_sel=$ligne[7];
			// transformation en image
			if($sexe_sel=='H')
			{
				$sexe_sel="<img width=30 src='images/homme.gif'>";
			}
			elseif($sexe_sel=='F')
			{
				$sexe_sel="<img width=30 src='images/femme.gif'>";
			}
			else
			{
				$sexe_sel="<img width=30 src='images/couple.gif'>";
			}
			$age_sel=$ligne[8];
			$cdp_sel=$ligne[9];
			$ville_sel=stripslashes($ligne[10]);
			$pays_sel=stripslashes($ligne[11]);
			$tel_sel=$ligne[12];
			$date_inscription=date('d/m/y',$ligne[13]);
			$reinscription=$ligne[14];
			$mdp=$ligne[15];
			$valide=$ligne[16];
			//    signature 17
			$mp=$ligne[18];	
			$art=$ligne[19];	
			$cha=$ligne[20];	
			$telport=$ligne[25];	
			$adresse=stripslashes($ligne[26]);	
			$profil=stripslashes($ligne[27]);	
			$quartier=stripslashes($ligne[30]);	
			if (!($requete2=mysql_query("SELECT avg(`nbr_mp`), avg(`nbr_art`), avg(`nbr_cha`) FROM `selistes` WHERE `valide`='OUI'"))) {
				die('Erreur : ' . mysql_error());
			}	
			$ligne2=mysql_fetch_row($requete2) ;
			$avg_mp= $ligne2[0];
			$avg_art= $ligne2[1];
			$avg_cha= $ligne2[2];		
		}
		if($id_sel!=null)
		{
			// time
			$seconde_connect=$time-$connection;
			if ($seconde_connect<180) //3minutes = online 
			{
				$seconde_connect='Connecté';
			}
			else
			{ 
				if($seconde_connect<3600)
				{
					$seconde_connect=floor($seconde_connect/60)."Min";
				}
				else
				{
					if($seconde_connect<86400)
					{
						$seconde_connect=floor($seconde_connect/60/60)."H";
					}
					else
					{
						$seconde_connect=floor($seconde_connect/60/60/24);
						if($seconde_connect==1){ $pluriel="Jour"; } else { $pluriel="Jours";}
						$seconde_connect=$seconde_connect." ".$pluriel;
					}
				}
			}
			
			if($valide=="NON")
			{
				echo "<p class='titre'>Le profil de \"".$prenom_sel."(".$id_sel.")\" est désactivé.</p><br>
				<p>Bonjour, ".$prenom_sel." est parti sans laisser d'adresse.. Une enquête est tou'<span class='vert'>verte.</span><br>
				<img src=images/wanted.png></p>";

			}
			else
			{
				if($id_sel==$id_seliste)
				{
					echo "<p><a href=\"amis.php?action=ajouter&amp;id=".$id_sel."\"><img src=images/telephone.gif width='20'> M'ajouter dans ma liste d'ami</a>&nbsp;&nbsp;
					<a href=\"services.php?action=edition\"><img src=images/editer.gif width='15'> Ajouter un service à mon profil</a>
					<a href=\"editer.php?action=edition\"><img src=images/editer.gif width='15'> Editer mes données personelles</a></p>";
				}
				else
				{
					echo "<p><a href=\"amis.php?action=ajouter&amp;id=".$id_sel."\"><img src=images/telephone.gif width='20'> Ajouter ".$prenom_sel." dans ma liste d'ami</a>&nbsp;&nbsp;
					<a href=\"messagerie.php?action=ecrire&amp;id=".$id_sel."\"><img src=images/lettre.jpg width='20'> Envoyer un message à ".$prenom_sel."</a>&nbsp;&nbsp;
					<a href=\"echanges.php\"><img src=images/coche.jpg width='10'> Ajouter un échange avec ".$prenom_sel."</a></p>";				
				}
				echo "	<br><p class='titre'> ".$prenom_sel."(".$id_sel.") </p><br>";
			}
			$inscription=date('d/m/y à H\h i\m\i\n',$inscription);
			if( $valide=='OUI')
			{ 
				// recup photo
				if (!($requete=mysql_query("SELECT `nom`, `commentaire` FROM `photos` WHERE `id_seliste`='$id_url' ORDER BY `ordre` ASC"))) {
					die('Erreur : ' . mysql_error());
				}	
				$galerie="<div class=\"highslide-gallery\">
				<script type=\"text/javascript\" src=\"scripts/highslide-with-gallery.js\"></script>
				<link rel=\"stylesheet\" type=\"text/css\" href=\"scripts/highslide.css\">
				<script type=\"text/javascript\">
				hs.graphicsDir = 'scripts/graphics/';
				hs.align = 'center';
				hs.transitions = ['expand', 'crossfade'];
				hs.outlineType = 'glossy-dark';
				hs.wrapperClassName = 'dark';
				hs.fadeInOut = true;
				hs.dimmingOpacity = 0.90;
				hs.addSlideshow({interval: 5000, repeat: false, useControls: true, fixedControls: 'fit', 
				overlayOptions: {opacity: 0.5, position: 'bottom center', hideOnMouseOut: true}});</script>";
				while($ligne=mysql_fetch_row($requete))
				{
					$nom_photo= $ligne[0];
					$commentaire_photo= $ligne[1];
					$galerie=$galerie."						
					<a class='aphoto' href=\"photos/".$nom_photo.".jpg\" class=\"highslide\" onclick=\"return hs.expand(this)\"><img height=100px src=\"photos/mini/".$nom_photo.".jpg\" title=\"Clic pour élargir\"></a>
					<div class=\"highslide-caption\"><p class='gris'>".$commentaire_photo."</p></div>";
				}
				$galerie=$galerie."</div>";
				echo "
				<div class='message'><p>".$galerie."</p></div><br>
				
				<div class='message' width='60%'><p class='titre'>Informations générales:</p>
				<table width='100%'><tr><td class='50%'>
				
					<table width='100%'><tr>
											<td class='50%'><p class='t3 gris'>Sexe:</p></td>
											<td class='50%'><p class='t2'>".$sexe_sel."</p></td>
										</tr>
										";if($nom_sel!="0"){ echo "
										<tr>
											<td class='50%'><p class='t3 gris'>Prénom:</p></td>
											<td class='50%'><p class='t2'><b>";if(($grade=='MODERATEUR')||($grade=='ADMIN')||($affnom=='OUI')){ echo $nom_sel." ";} echo $prenom_sel."</b></p></td>
										</tr>";} echo "
										";if($age_sel!="0"){ echo "
										<tr>
											<td class='50%'><p class='t3 gris'>Age:</p></td>
											<td class='50%'><p class='t2'><b>".$age_sel."</b></p></td>
										</tr>";} echo "
										";if($tel_sel!=null){ echo "
										<tr>
											<td class='50%'><p class='t3 gris'>Téléphone:<br><img src=\"images/telephone.gif\" alt=\"telephone\"></p></td>
											<td class='50%'><p class='t2'><b>".$tel_sel." <br>".$telport."</b><br></p></td>
										</tr>";} 
										if($ville_sel!=null){ echo "
										<tr>
											<td class='50%'><p class='t3 gris'>Adresse:</p></td>
											<td class='50%'><p class='t2'><b>".$adresse."<br>".$quartier."";if($cdp_sel!='0'){ echo "<br>".$cdp_sel;} echo " ".$ville_sel."<br>(".$pays_sel.")</b></p></td>
										</tr> "; } echo "
					</table>
				</td>
				<td>
				
					<table width='100%'>
						";
						if($affsolde=='OUI'){echo "<tr>
							<td class='50%'><p class='t3 gris'>$monnaie:</p></td>
							<td class='50%'><p class='t2'><b>".$grains_sel."</b></p></td>
						</tr>";}
						if(($affsolde=='NON')&&($grade!='SELISTE')){echo "<tr>
							<td class='50%'><p class='t3 bleu'>$monnaie:</p></td>
							<td class='50%'><p class='t2'><b>".$grains_sel."</b></p></td>
						</tr>";}
						echo "
						<tr>
							<td class='50%'><p class='t3 gris'>Dernière connexion:</p></td>
							<td class='50%'><p class='t2'><b>".$seconde_connect."</b></p></td>
						</tr>
						<tr>
							<td class='50%'><p class='t3 gris'>Inscrit depuis:</p></td>
							<td class='50%'><p class='t2'><b>".$date_inscription."</b></p></td>
						</tr>
						<tr>
							<td class='50%'><p class='t3 gris'>Statut:</p></td>
							<td class='50%'><p class='t2'><b>".$grade_sel."</b></p></td>
						</tr>					
					</table>
				</td>
				</tr></table>
				<p class='t1'>Présentation:</p><p>".$profil."</p>
				</div><br>
				
				<div class='message'>			
				<p class='titre'>&nbspServices proposés:&nbsp</p>
				<table summary=\"\" border='0' width='100%' cellpadding='2'>";
				// service proposé
				if (!($requete1=mysql_query("SELECT `id_rubrique`,`time`,`detail`,`description`, `id_service` FROM `services` WHERE `id_seliste`='$id_sel' AND `etat`='PRO' order by `time`"))) {
				    die('Erreur : ' . mysql_error());
				}
				while($ligne=mysql_fetch_row($requete1))
				{
					$id_rub= $ligne[0];
					$time_rub=date('d/m/y',$ligne[1]); 
					$detail= stripslashes($ligne[2]);
					$message= $detail;
					include("smileyscompagnie.php");
					$detail=$message;
					$designation_ssrub=stripslashes($ligne[3]);
					$id_service = $ligne[4];
					if (!($requete3=mysql_query("SELECT `designation`, `id_cat` FROM `rubrique` WHERE `id_rubrique`='$id_rub'"))) {
					    die('Erreur : ' . mysql_error());
					}
					$ligne3=mysql_fetch_row($requete3);
					$designation_rub=stripslashes($ligne3[0]);
					$id_cat=$ligne3[1];
					if (!($requete4=mysql_query("SELECT `designation` FROM `catalogue` WHERE `id_cat`='$id_cat'"))) {
					    die('Erreur : ' . mysql_error());
					}
					$ligne4=mysql_fetch_row($requete4);
					$designation_cat=stripslashes($ligne4[0]);
					echo "<tr><td width='70%'><p class='t2'><img width='20' src='images/flechedroite.png'>&nbsp;&nbsp;&nbsp;<b><span class='teinte2'>".$designation_cat." - ".$designation_rub." - ".$designation_ssrub."</b></span>
					<br><span class='pasimportant'><i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Ajouté le $time_rub)</i></span></p></td>
					<td rowspan=2 class=t3><a href=\"proposer.php?action=demande&amp;id_service=$id_service\">Faire une demande</a></td>
					</tr>
					<tr><td><p class='pasimportant'><i><b>".$message."</b></i></p></td></tr>
					<tr><td colspan='2'><hr width='60%'></td></tr>";
				}
				echo "</table>
				</div><br><div class='message'>
				<p class='titre'>&nbspServices demandés:&nbsp</p>
				<table summary=\"\" border='0' width='100%' cellpadding='2'>
				<tr>
				<td colspan='2'>
				</tr>";
				// service demandé
				if (!($requete1=mysql_query("SELECT `id_rubrique`,`time`,`detail`,`description`,`id_service` FROM `services` WHERE `id_seliste`='$id_sel' AND `etat`='DEM' order by `time`"))) {
				    die('Erreur : ' . mysql_error());
				}
				while($ligne=mysql_fetch_row($requete1))
				{
					$id_rub= $ligne[0];
					$time_rub=date('d/m/y',$ligne[1]); 
					$detail= stripslashes($ligne[2]);
					$message= $detail;
					include("smileyscompagnie.php");
					$detail=$message;
					$designation_ssrub=stripslashes($ligne[3]);
					$id_service= $ligne[4];
					if (!($requete3=mysql_query("SELECT `designation`, `id_cat` FROM `rubrique` WHERE `id_rubrique`='$id_rub'"))) {
					    die('Erreur : ' . mysql_error());
					}
					$ligne3=mysql_fetch_row($requete3);
					$designation_rub=stripslashes($ligne3[0]);
					$id_cat=$ligne3[1];
					if (!($requete4=mysql_query("SELECT `designation` FROM `catalogue` WHERE `id_cat`='$id_cat'"))) {
					    die('Erreur : ' . mysql_error());
					}
					$ligne4=mysql_fetch_row($requete4);
					$designation_cat=stripslashes($ligne4[0]);
					echo "<tr><td><p class='t2'><img width='20' src='images/flechedroite.png'>&nbsp;&nbsp;&nbsp;<b><span class='teinte1'>".$designation_cat." - ".$designation_rub." - ".$designation_ssrub."</b></span>
					<br><span class='pasimportant'><i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Ajouté le $time_rub)</i></span></p></td>
					<td rowspan=2 class=t3><p><a href=\"demander.php?action=propose&amp;id_service=$id_service\">Proposer mes services</a></p>
					</td></tr>
					<tr><td><p class='pasimportant'><i><b>".$message."</b></i></p></td></tr>
					<tr><td colspan='2'><hr width='60%'></td></tr>";
				}
				echo "</table></div>";
			}
			echo "<br>";
			if(($grade=='MODERATEUR')||($grade=='ADMIN'))
			{	
				if (!($requete=mysql_query("SELECT texte FROM `variables` where `variable` = 'annee'"))) {
					die('Erreur : ' . mysql_error());
				}
				$ligne=mysql_fetch_row($requete) ;
				$anneeBase= $ligne[0];	
				if($anneeBase==$reinscription) { $etatcotis="Séliste réinscrit"; } else { $etatcotis="Séliste non réinscrit";}
				echo "<div class='message'><p class='titre'>Visible uniquement pour les modérateurs:</p>
				<table summary=\"\" width=\"100%\" border='0'>
				<tr>
					<td width='40%'><p class='t3'><b>Paramètre de validation:</b></p></td>
					<td width='20%' colspan='2'><p class='t2'>".$valide."</p></td>
				</tr>
				<tr>
					<td width='40%'><p class='t3'><b>Paramètre année:</b></p></td>
					<td width='20%' colspan='2'><p class='t2'>".$etatcotis." </b></p></td>
				</tr>
				</tr>
				<tr>
					<td width='40%'><p class='t3'><b>Nombre d'articles écrit:</b></p></td>
					<td width='20%'><p class='t2'>".$art."</p></td>
					<td width='40%'><p class='t2'>Moyenne des membres: ".$avg_art."</p></td>
				</tr>
				<tr>
					<td><p class='t3'><b>Nombre de messages privés:</b></p></td>
					<td><p class='t2'>".$mp."</p></td>
					<td><p class='t2'>Moyenne: ".$avg_mp."</p></td>
				</tr>
				<tr>
					<td><p class='t3'><b>Nombre de messages sur le chat:</b></td>
					<td><p class='t2'>".$cha."</td>
					<td><p class='t2'>Moyenne: ".$avg_cha."</td>
				</tr>
				<tr>
					<td><p class='t3'><b>Email:</b></td>
					<td><p class='t2'>".$email."</td>
				</tr>
				</table>
				<p><a class='amodo' href='activite.php?id=$id_sel'>Voir ses derniers échanges.</a></p>
				</div><br>";	
			}
			if($grade=='ADMIN')
			{
				echo "<div class='message'>";
				if($_GET['action']=='passage')
				{
					if (!($requete=mysql_query("UPDATE `selistes` SET `reinscription` = '$anneeBase' WHERE `id_seliste` ='$id_sel' LIMIT 1") )) {
						die('Erreur : ' . mysql_error());
					}
					echo "<p>Année de cotisation corrigé</p>";
				}
				echo "<p class='titre'>Visible uniquement pour les admins:</p><br>
				<p>Mot de passe : <a class='aadmin' href=\"initmdp.php?action=initmdp&amp;id_sel=".$id_sel."\">Réinitialiser son mot de passe</a><br>(Lui envoi un mail, affiche le mot de passe généré, le valide, corrige l'année de cotisation)<br><hr width='70%'>
				<br><b>Paramètre de validation: ".$valide."</b>
				<br><b>Paramètre année: ".$reinscription." </b>(Si \"".$anneeBase."\" il a payé sa cotisation il est réinscrit)<br>
				<br><a class='aadmin' href=\"admin_editer.php?action=editer&amp;id=".$id_sel."\">Editer sa fiche</a>&nbsp;&nbsp;
				<a class='aadmin' href=\"profil.php?action=passage&amp;id=".$id_sel."\">Cotisation payée (met le paramètre \"annee\" à jour)</a>
				<p><br></div>";
			}
		}
		else
		{// fiche existe pas 
			echo "<p>Bonjour, un avis de recherche est lancé mais personne ne la trouvé!<br>
			<img src=images/wanted.png></p>";
		}
		echo "<br></div>";
	} 
	else
	{ 	// délai dépassé
		header ("location:troptard.php");
		session_destroy();
	}
	mysql_close($connexion);
}
else
{ 	// voleur
	header ("location:404.php");
	session_destroy();
	
}
 
include ("fin.php");	
?>
