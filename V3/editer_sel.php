<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/

if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
{								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
		switch ($grade)
		{
			case 'SELISTE' : include("lien/seliste.php");break;	
			case 'MODERATEUR' :include("lien/seliste.php"); include("lien/moderateur.php");break;
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page
		//debut de l'edition
		if($_GET['action']=='editer') //on enregistre les modifs
		{	
			echo "<br><div class=\"corps\"><br>
			<form method=\"post\" action=\"editer_sel.php?action=enregistre\" enctype=\"multipart/form-data\">
			<table summary=\"\" border='1' width='90%' cellpadding='5'>
			<tr>
			<td colspan='2' bgcolor='#FF0000'><p>Sauvegardez toutes les 10 minutes!</p></td>
			</tr>
			<tr>
			<th colspan='2'>Aide pour la rédaction:<br>
			<span class='pasimportant'>Exemple: <br>
			Passage à la ligne: &lt;br&gt; <br>
			Lien:&lt;a href=http://www.".$site."&gt; ".$site." &lt;/a&gt;<br>
			Gras: &lt;b&gt; <b>mon texte</b>&lt;/b&gt;<br>
			Italique:&lt;i&gt;<i>mon texte</i>&lt;/i&gt;<br>
			Souligné:&lt;u&gt;<u>mon texte</u>&lt;/u&gt;<br>
			Couleur: &lt;span class=rouge&gt;<span class='rouge'>mon texte</span>&lt;/span&gt;</span>
			</th>
			</tr>";		
			
			
			if (!($requete=mysql_query("SELECT * FROM `autresel` "))) {
				die('Erreur : ' . mysql_error());
			}
			while($ligne = mysql_fetch_array($requete))
			{
			
				$nom=$ligne[0];
				$description=str_replace('<br />', "", $ligne[1]);
			//	$description=stripslashes($decription);
				echo "
				<tr>
					<th>".$nom."</th>
					<td class='t1'><textarea name='$nom' cols=70 rows=10 >".$description."</textarea></td>
				</tr>";			
			}
			
			echo  "
			<tr>
			<td colspan='3' class='t3'><input type='submit' value=' Sauvegarder ' ></td>
			</tr>
			</table>			
			</form></div>";
		}
		else //on enregistre
		{
			
				echo "<br><div class=\"corps\"><br>
				<table summary=\"\" border='0' width='40%'>
				<tr>
				<th> Mise à jour des données :</td></tr>";
				
			if($_GET['action']=='enregistre')
			{ //ok pour enregistre
				if (!($requete=mysql_query("SELECT `nom` FROM `autresel` "))) {
					die('Erreur : ' . mysql_error());
				}
				while($ligne = mysql_fetch_row($requete))
				{
				
					$nom=$ligne[0];
					$description=nl2br($_POST[''.$nom.'']);
					if (!($maj=mysql_query("UPDATE `autresel` SET `description` = '$description' WHERE `nom` ='$nom' LIMIT 1"))) {
						die('Erreur : ' . mysql_error());
					}
					echo "
						<tr><td class='t1'>".$nom." mis à jour</td></tr>";			
				}
				echo "
				<tr><td class='t2'> FINI ! </td></tr>
				<tr><td class='t2'>	<a href='sel.php' title='autresel'>Voir le résultat</a></td> 
					</tr>
					</table></div>";
			}
			else
			{ 	//pirate
				header ("location:404.php");
				session_destroy();			
			}
		}
		//fin edition
	} 
	//delai depassé
	else
	{
		header ("location:troptard.php");
		session_destroy();
	}
}
// pas de sesion
else
{
	header ("location:404.php");
	session_destroy();
}
mysql_close($connexion); 
include ("fin.php");	
?>
