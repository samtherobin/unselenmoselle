<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/
if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
{								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade`,  `mail_multi`, `mail_dem`,`mail_mp`, `mail_actu`, `mail_age`, `mail_pet` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		$mail_multi=$ligne[1];
		$mail_dem=$ligne[2];
		$mail_mp=$ligne[3];
		$mail_actu=$ligne[4];
		$mail_age=$ligne[5];
		$mail_pet=$ligne[6];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
		switch ($grade)
		{
			case 'SELISTE' : include("lien/seliste.php");break;
			case 'MODERATEUR' :include("lien/seliste.php"); include("lien/moderateur.php");break;
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page
		echo "<br><div class=\"corps\"><br>";
		if($_POST['mail_mp']!=NULL) //on enregistre
		{
			$mail_multi=htmlentities($_POST['mail_multi'], ENT_QUOTES, "UTF-8");
			$mail_dem=htmlentities($_POST['mail_dem'], ENT_QUOTES, "UTF-8");
			$mail_mp=htmlentities($_POST['mail_mp'], ENT_QUOTES, "UTF-8");
			$mail_actu=htmlentities($_POST['mail_actu'], ENT_QUOTES, "UTF-8");
			$mail_age=htmlentities($_POST['mail_age'], ENT_QUOTES, "UTF-8");
			$mail_pet=htmlentities($_POST['mail_pet'], ENT_QUOTES, "UTF-8");
			if (!($requete=mysql_query("UPDATE `selistes` SET `mail_multi` = '$mail_multi', `mail_dem` = '$mail_dem', `mail_mp` = '$mail_mp', `mail_actu` = '$mail_actu', `mail_age` = '$mail_age', `mail_pet` = '$mail_pet' WHERE `id_seliste` ='$id_seliste' LIMIT 1"))) {
				die('Erreur : ' . mysql_error());
			}
			$cpt_req++;
			echo "
			<p class='titre'> Votre compte est mis à jour merci.</p>	
			<br>
			<p><a href='bureau.php' title='Bureau'>Retour bureau</a></p><br>";
		}
		else   //formulaire
		{		
			echo "<form method=\"post\" action=\"compte.php\" enctype=\"multipart/form-data\">
			<p class='titre'>Recevoir les notifications par courriel lorsque:</p>
			<table  border='0' cellpadding=\"4\">
			<tr>
				<td width=\"60%\"><p class=\"t3\"><b>Je reçois une demande urgente:</b></p></td>
				<td width=\"40%\"><p class=\"t2\"><input type=\"radio\" value=\"OUI\" ";if($mail_dem=='OUI'){echo "checked ";} echo "name=\"mail_multi\"> Oui<br><input type=\"radio\" value=\"NON\" ";if($mail_dem=='NON'){echo "checked ";} echo "name=\"mail_multi\">Non</p>
				</td>
			</tr>
			<tr>
			<td><p class=\"t3\"><b>Je recois une demande/proposition de service:</b></p></td>
			<td><p class=\"t2\"><input type=\"radio\" value=\"OUI\" ";if($mail_dem=='OUI'){echo "checked ";} echo "name=\"mail_dem\"> Oui<br><input type=\"radio\" value=\"NON\" ";if($mail_dem=='NON'){echo "checked ";} echo "name=\"mail_dem\"> Non</p></td>
			</tr>
			<tr>
			<td><p class=\"t3\"><b>Je recois un message personnel:</b></p></td>
			<td><p class=\"t2\"><input type=\"radio\" value=\"OUI\" ";if($mail_mp=='OUI'){echo "checked ";} echo "name=\"mail_mp\"> Oui<br><input type=\"radio\" value=\"NON\" ";if($mail_mp=='NON'){echo "checked ";} echo "name=\"mail_mp\"> Non</p></td>
			</tr>
			<tr>
			<td><p class=\"t3\"><b>Une nouvelle actualité ou un compte-rendu est publiée:</b></p></td>
			<td><p class=\"t2\"><input type=\"radio\" value=\"OUI\" ";if($mail_actu=='OUI'){echo "checked ";} echo "name=\"mail_actu\"> 
			Oui<br><input type=\"radio\" value=\"NON\" ";if($mail_actu=='NON'){echo "checked ";} echo "name=\"mail_actu\"> Non</p></td>
			</tr>
						<tr>
			<td><p class=\"t3\"><b>Un nouvel évenement dans l'agenda est publié:</b></p></td>
			<td><p class=\"t2\"><input type=\"radio\" value=\"OUI\" ";if($mail_age=='OUI'){echo "checked ";} echo "name=\"mail_age\">Oui<br>
			<input type=\"radio\" value=\"NON\" ";if($mail_age=='NON'){echo "checked ";} echo "name=\"mail_age\"> Non</p></td>
				</td>
			</tr>
						<tr>
			<td><p class=\"t3\"><b>Une nouvelle petite annonce est publiée:</b></p></td>
			<td><p class=\"t2\"><input type=\"radio\" value=\"OUI\" ";if($mail_pet=='OUI'){echo "checked ";} echo "name=\"mail_pet\">Oui<br><input type=\"radio\" value=\"NON\" ";if($mail_pet=='NON'){echo "checked ";} echo "name=\"mail_pet\"> Non</p></td>
				</td>
			</tr>
			<tr>
			<td colspan=\"2\"><p class=\"t1\"><input type=\"submit\" value=\" Sauvegarder \" ></p></td>
			</tr>
			</table>			
			</form>";
		}
		echo "<br></div>";
		
	}
	else
	{ 	 // délai dépassé
		header ("location:troptard.php");
		session_destroy();
	}
 }
else
{ 	 // pas de session
	header ("location:404.php");
	session_destroy();
}
mysql_close($connexion);
include ("fin.php");	
?>