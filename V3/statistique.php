<?php 
session_start(); 
/*   CETTE PARTIE EST INTERDITE DE MODIFICATION ET N'EST PAS AFFICHEE SUR VOTRE SITE
© Copyright  Créateur Initial du projet: Eolange Fabien [http://www.reveland.fr] 
Liste des contributeurs disponible sur la page "contributions.php"
Adresse du créateur initial et des sources: http://www.reveland.fr
Email: webmaster@reveland.fr
Ce logiciel est un programme informatique servant à gérer les membres d'une association de service d'échange local. Nommé S.E.L.
Ce logiciel est régi par la licence CeCILL-B soumise au droit français et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier ou redistribuer ce programme sous les conditions de la licence CeCILL-B telle que diffusée par le CEA,le CNRS et l'INRIA sur le site "http://www.cecill.info".
En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée.
Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédant successifs.
A cet égard  l'attention de l'utilisateur est attirée sur les risques associés au chargement,  à l'utilisation,  à la modification et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant  des  connaissances  informatiques approfondies.
Les utilisateurs sont donc invités à charger  et  tester  l'adéquation  du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-B, et que vous en avez accepté les termes. 
© Copyright by [Eolange-F.P.] www.reveland.fr  -->
*/

if ( (isset($_SESSION['id_seliste']))&&(isset ($_SESSION['session'])) )
{								  
    $id_seliste=$_SESSION['id_seliste'];
	$session=$_SESSION['session'];	
	$grade_ses=$_SESSION['grade'];	
	$theme=$_SESSION['theme'];
	include("debut.php");
	// test si session corespond a l'id    
	if (!($requete1=mysql_query("SELECT `id_seliste`, `timestamp` FROM `session` WHERE `id_ses_php` = '$session'") )) {
		die('Erreur : ' . mysql_error());
	}
	$time=time();		 
	$ligne=mysql_fetch_row($requete1) ;
	$id_seliste_base= $ligne[0];
	$timeavant= $ligne[1];
	$tpsattente=$time-$timeavant;  
	if(($tpsattente<1300)&&($id_seliste==$id_seliste_base)) //Si une ligne alors il est identifié  et si moin de 15 minutes de connection (900secondes)
	{	
		// recup de ses infos
		if (!($requete=mysql_query("UPDATE `session` SET `timestamp` = '$time' WHERE `id_ses_php` ='$session' LIMIT 1") )) {
			die('Erreur : ' . mysql_error());
		}
		if (!($requete1=mysql_query("SELECT `grade` FROM `selistes` WHERE `id_seliste`='$id_seliste'") )) {
			die('Erreur : ' . mysql_error());
		}
		$ligne=mysql_fetch_row($requete1) ;
		$grade= $ligne[0];
		if (!($query = mysql_query("SELECT * FROM `messagerie` WHERE `destinataire`='$id_seliste' AND `rubrique`='AFF'"))) {
			die('Erreur : ' . mysql_error());
		}
		$nbrmess = mysql_num_rows($query);
		switch ($grade)
		{
			case 'SELISTE' : include("lien/seliste.php");break;	
			case 'MODERATEUR' :include("lien/seliste.php"); include("lien/moderateur.php");break;
			case 'ADMIN' :include("lien/seliste.php"); include("lien/admin.php");break;			
			default: echo ("Erreur interne contactez le webmaster");	
		}
		// Debut de la page
		if($_GET['action']=='seliste')
		{	//nombre de mail envoyé
			echo "<br><br><div class='corps'><br><div class='message'><br>
			<p class='titre'>Sélistes actifs (valides):</p><br>			
			<table summary=\"\" class='tablevu' border='1' width='100%' cellpadding='5'>
			";	
			if (!($requete=mysql_query("SELECT COUNT(`mail_actu`)  FROM `selistes` WHERE `valide`='OUI' AND `mail_actu`='OUI' AND `id_seliste`>'5'"))) {
				die('Erreur : ' . mysql_error());
			}
			if (!($requete1=mysql_query("SELECT COUNT(`mail_actu`)  FROM `selistes` WHERE `valide`='OUI' AND `mail_actu`='NON' AND `id_seliste`>'5'"))) {
				die('Erreur : ' . mysql_error());
			}
			if (!($requete2=mysql_query("SELECT COUNT(`id_seliste`) FROM `selistes` WHERE `valide`='OUI' AND `id_seliste`>'5'") )) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete) ;
			$nbr_mail=$ligne[0];
			$ligne=mysql_fetch_row($requete1) ;
			$nbr_mail2=$ligne[0];
			$nbr_sel=$nbr_mail+$nbr_mail2;
			$ligne=mysql_fetch_row($requete2) ;
			$nbr_coti=$ligne[0];
			// on preleve pas le 1 le 2 et le 5
			echo "<tr>
			<td><p class=t3><b>Mails envoyés quand une actualité est publiée :</b></p></td><td><p class=t2>".$nbr_mail." (".$nbr_mail2." le refuse(nt)) / ".$nbr_sel."</p></td>
			</tr>";
			echo "<tr>
			<td><p class=t3><b>Nombre de cotisations de $monnaie prélevées:</b></p></td><td><p class=t2>".$nbr_coti." <br><span class='pasimportant'>(Sans les comptes 1 à 5)</span></p></td>
			</tr>";
			// seliste connecté 30jours
			$time2=$time-(30*24*60*60);
			if (!($requete=mysql_query("SELECT COUNT(`id_seliste`)  FROM `selistes` WHERE `connection`>'$time2' AND `valide`='OUI' AND `id_seliste`>'5'"))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete) ;
			$nbr_conn=$ligne[0];			
			echo "<tr>
			<td><p class=t3><b>Sélistes connectés il y a moins de 30 jours :</b></p></td><td><p class=t2>".$nbr_conn."/ ".$nbr_sel."</p></td>
			</tr></table><br></div>";
			
			
		echo "<br><div class='message'><br>
		<p class='titre'>Analyse des totaux par catégories de sélistes :</p><br>
		<table width=100% border=1 class='tablevu'>
		<tr><td><p class='t1'>Catégorie</p></td><td><p class='t1'>Total</p></td></tr>";
		
		$total=0;
		
		// Adhérents actifs
		if (!($requete1=mysql_query("SELECT count(`id_seliste`) FROM `selistes` WHERE `id_seliste`>5 AND `valide`='OUI'"))) {
			die('Erreur : ' . mysql_error());
		}
		if ($ligne=mysql_fetch_row($requete1)) {
			$nbselistes=$ligne[0];
		}

		if (!($requete1=mysql_query("SELECT sum(`grains`) FROM `selistes` WHERE `id_seliste`>5 AND `valide`='OUI'"))) {
			die('Erreur : ' . mysql_error());
		}
		if ($ligne=mysql_fetch_row($requete1)) {
			echo "<tr><td><p class=t3><b>$nbselistes adh&eacute;rents valides</b></p></td>
			<td class=t2><p>$ligne[0] $monnaie</p></td></tr>";
			$total=$total+$ligne[0];
		}
		mysql_free_result($requete1);
		
		// Adhérents inactifs
		if (!($requete1=mysql_query("SELECT count(`id_seliste`) FROM `selistes` WHERE `id_seliste`>5 AND NOT `valide`='OUI'"))) {
			die('Erreur : ' . mysql_error());
		}
		if ($ligne=mysql_fetch_row($requete1)) {
			$nbselistes=$ligne[0];
		}

		if (!($requete1=mysql_query("SELECT sum(`grains`) FROM `selistes` WHERE `id_seliste`>5 AND NOT `valide`='OUI'"))) {
			die('Erreur : ' . mysql_error());
		}
		if ($ligne=mysql_fetch_row($requete1)) {
			echo "<tr><td><p class=t3><b>$nbselistes adh&eacute;rents non valides</b></p></td>
			<td><p class=t2>$ligne[0] $monnaie</p></td></tr>";
			$total=$total+$ligne[0];
		}
		mysql_free_result($requete1);
		
		// Autres comptes
		if (!($requete1=mysql_query("SELECT count(`id_seliste`) FROM `selistes` WHERE `id_seliste`<'6' AND NOT `id_seliste`='1'"))) {
			die('Erreur : ' . mysql_error());
		}
		if ($ligne=mysql_fetch_row($requete1)) {
			$nbselistes=$ligne[0];
		}

		if (!($requete1=mysql_query("SELECT sum(`grains`) FROM `selistes` WHERE `id_seliste`<'6' AND NOT `id_seliste`='1'"))) {
			die('Erreur : ' . mysql_error());
		}
		if ($ligne=mysql_fetch_row($requete1)) {
			echo "<tr><td><p class=t3><b>$nbselistes autres comptes (2 à 5)</b></p></td>
			<td><p class=t2>$ligne[0] $monnaie</p></td></tr>";
			$total=$total+$ligne[0];
		}
		mysql_free_result($requete1);
		
		// Compte Rijsel
		if (!($requete1=mysql_query("SELECT `grains` FROM `selistes` WHERE `id_seliste`=1"))) {
			die('Erreur : ' . mysql_error());
		}
		if ($ligne=mysql_fetch_row($requete1)) {
			echo "<tr><td><p class=t3><b>".$nom."</b></p></td>
			<td><p class=t2>$ligne[0] $monnaie</p></td></tr>";
			$total=$total+$ligne[0];
		}
		mysql_free_result($requete1);
		
		echo "<tr><td><p class='t1'>TOTAL</p></td><td><p class='t1'>$total $monnaie</p></td></tr></table><br></div>";
			
		/////////////////////////////////////////////////
		//  plus grand message privé publieur et chat :	
		//message privé
		if (!($requete1=mysql_query("SELECT MAX(nbr_mp) AS nbr_mp  FROM `selistes` "))) {
			die('Erreur : ' . mysql_error());
		}	
		$ligne=mysql_fetch_row($requete1);
		$max_mp=$ligne[0];
		if (!($requete1=mysql_query("SELECT `id_seliste`, `prenom` FROM `selistes` WHERE `nbr_mp`='$max_mp' LIMIT 1"))) {
			die('Erreur : ' . mysql_error());
		}	
		$ligne=mysql_fetch_row($requete1);
		$id_sel_max_mp=$ligne[0];
		$prenom_max_mp=stripslashes($ligne[1]);
		// actu
		if (!($requete1=mysql_query("SELECT MAX(nbr_art) AS nbr_act  FROM `selistes` "))) {
			die('Erreur : ' . mysql_error());
		}	
		$ligne=mysql_fetch_row($requete1);
		$max_art=$ligne[0];
		if (!($requete1=mysql_query("SELECT `id_seliste`, `prenom` FROM `selistes` WHERE `nbr_art`='$max_art' LIMIT 1"))) {
			die('Erreur : ' . mysql_error());
		}	
		$ligne=mysql_fetch_row($requete1);
		$id_sel_max_art=$ligne[0];
		$prenom_max_art=stripslashes($ligne[1]);
		// chat
		if (!($requete1=mysql_query("SELECT MAX(nbr_cha) AS nbr_cha  FROM `selistes` "))) {
			die('Erreur : ' . mysql_error());
		}	
		$ligne=mysql_fetch_row($requete1);
		$max_cha=$ligne[0];
		if (!($requete1=mysql_query("SELECT `id_seliste`, `prenom` FROM `selistes` WHERE `nbr_cha`='$max_cha' LIMIT 1"))) {
			die('Erreur : ' . mysql_error());
		}	
		$ligne=mysql_fetch_row($requete1);
		$id_sel_max_cha=$ligne[0];
		$prenom_max_cha=stripslashes($ligne[1]);
		echo "<br><div class='message'>
		<p class='titre'>Les records du SEL:</p><br>
		<table class='tablevu' border='1' width='10%' cellpadding='3'>
			<tr>
			<td><p class=t3>Le plus de messages privés envoyés:</p></td>
			<td><p class=t2>$prenom_max_mp($id_sel_max_mp) avec $max_mp messages envoyés.</p></td>
			</tr>
			<tr>
			<td><p class=t3>Le plus d'actualités envoyées:</p></td>
			<td><p class=t2>$prenom_max_art($id_sel_max_art) avec $max_art actualités.</p></td>
			</tr>
			<tr>
			<td><p class=t3>Le plus de messages sur le chat:</p></td>
			<td><p class=t2>$prenom_max_cha($id_sel_max_cha) avec $max_cha messages.</p></td>
			</tr></table><br></div>";
			
			
			// affichage des villes 
			// selection que les valides avec un tri des 15 plus importants
			if (!($requete4=mysql_query("SELECT COUNT(`id_seliste`), `ville` FROM `selistes` WHERE `valide`='OUI' AND `ville`!='' GROUP BY `ville` Order by COUNT(`id_seliste`) DESC LIMIT 15 ") )) {
				die('Erreur : ' . mysql_error());
			}
			echo "<br><div class='message'><br>
			<p class='titre'>Les 15 villes les plus représentées:</p><br>
			<table class='tablevu' border='1' width='90%' cellpadding='5'>
			<tr>
				<td><p class='t1'>Nombre de selistes:</p></th>
				<th><p class='t1'>Ville:</p></th>
			</tr>";
			while($ligne=mysql_fetch_row($requete4))
				{
					$nbre=$ligne[0];
					$Ville=stripslashes($ligne[1]);
				echo
				"<tr>
					<td><p class=t1>$nbre</p></td>
					<td><p class=t2>$Ville</p></td>
				</tr>";
			}		
			echo "</table><br></div><br></div>";			
		}
		elseif($_GET['action']=='rijsel')
		{	//cotisation rapporté
			echo "<br><br><div class='corps'><br>
			<div class='message'><br><p class='titre'>Crédits (Cotisations en ".$monnaie."):</p><br>
			<table summary=\"\" class='tablevu' border='1' width='100%' cellpadding='3'>
			<tr>
			<td colspan='5'><p class=t1>Tableau mensuel:</p></td>
			</tr>
			<tr>
			<td><p class=t1>Date:</p></td><td><p class=t1>Montant:</p></td><td><p class=t1>Evolution:</p></td><td><p class=t1>Nombre de cotisations :</p></td><td><p class=t1>Valeur:</p></td>
			</tr>";			
			if (!($requete=mysql_query("SELECT SUM(`grains`), `time`, COUNT( `id_echange` )  FROM `echanges` WHERE `id_ss_rub`='478' GROUP BY `time` ORDER BY `time` asc"))) {
				die('Erreur : ' . mysql_error());
			}
			$gr_coti_mois=0;
			while($ligne=mysql_fetch_row($requete))
				{
					$gr_coti= $ligne[0];
					$time_coti=$ligne[1];
					$nbr_coti=$ligne[2];
					$time_coti=date('d/m/y à H\h i\m\i\n',$time_coti);
					$valeur=$gr_coti/$nbr_coti;
					if($gr_coti_mois!=null)
					{
						$progression=100-(($gr_coti*100)/$gr_coti_mois);
						$progression=$progression*(-1);
						$progression=ROUND($progression,2);
					}
					else
					{
						$progression=0;
					}					
					echo "
						<tr>
								<td><p class=t2>Le ".$time_coti."</p></td>
								<td><p class=t2>".$gr_coti."</p></td>
								<td><p class=t3>".$progression." %</p></td>
								<td><p class=t2>".$nbr_coti."</p></td>
								<td><p class=t3>".$valeur."</p></td>
						</tr>";
					$gr_coti_mois=$gr_coti;
				}	
			echo "</table><br></div><br>";	

			
			echo "<div class='message'><br>
			<p class='titre'>Crédits et débits:</p><br>
			<table summary=\"\" class='tablevu' border='1' width='50%' cellpadding='5'>
			<tr>
			<td colspan='2'><p class='t1'>Débits et crédits sur 1 mois (de aujourd'hui à -30jours):</p></td>
			</tr>";					
			$time2=$time-2629743;
			if (!($requete=mysql_query("SELECT sum(`grains`) FROM `echanges` where `etat`='OKI' AND `id_seliste_fou`='1' AND `time`>$time2"))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete);
			$gr_rec= $ligne[0];
			if (!($requete=mysql_query("SELECT sum(`grains`) FROM `echanges` where `etat`='OKI' AND `id_seliste_dem`='1' AND `time`>$time2"))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete);
			$gr_dep= $ligne[0];
			$gr_tot=$gr_rec-$gr_dep;
			echo "
				<tr>
					<td><p class=t3>Crédits:</p></td>
					<td><p class=t2>".$gr_rec." ".$monnaie."</p></td>
				</tr>
				<tr>
					<td><p class=t3>Débits:</p></td>
					<td><p class=t2>".$gr_dep." ".$monnaie."</p></td>
				</tr>
				<tr>
					<td><p class=t1>Total:</p></td>
					<td><p class=t1>".$gr_tot." ".$monnaie."</p></td>
				</tr>";
			echo "</table><br><br>";	
			echo "<table class='tablevu' summary=\"\" border='1' width='100%' cellpadding='5'>
			<tr>
			<td colspan='2'><p class='titre'>Débits et crédits sur 6 mois:</p></td>
			</tr>";					
			$time2=$time-(6*2629743);
			if (!($requete=mysql_query("SELECT sum(`grains`) FROM `echanges` where `etat`='OKI' AND `id_seliste_fou`='1' AND `time`>$time2"))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete);
			$gr_rec= $ligne[0];
			if (!($requete=mysql_query("SELECT sum(`grains`) FROM `echanges` where `etat`='OKI' AND `id_seliste_dem`='1' AND `time`>$time2"))) {
				die('Erreur : ' . mysql_error());
			}
			$ligne=mysql_fetch_row($requete);
			$gr_dep= $ligne[0];
			$gr_tot=$gr_rec-$gr_dep;
			echo "
				<tr>
					<td><p class=t3>Crédits:</p></td>
					<td><p class=t2>".$gr_rec." ".$monnaie."</p></td>
				</tr>
				<tr>
					<td><p class=t3>Débits:</p></td>
					<td><p class=t2>".$gr_dep." ".$monnaie."</p></td>
				</tr>
				<tr>
					<td><p class=t1>Total:</p></td>
					<td><p class=t1>".$gr_tot." ".$monnaie."</p></td>
				</tr>";
			echo "</table><br></div><br></div>";				
			
		}
		elseif($_GET['action']=='echange')
		{		//echanges
				/* debut tableau */
				$time1=$time-(2629743);		// 1 mois en seconde	
				if (!($requete=mysql_query("SELECT SUM(`grains`), COUNT( `id_echange` )  FROM `echanges` WHERE `date_ech`>'$time1' AND `etat`='OKI' AND NOT `id_ss_rub`='478'"))) {
					die('Erreur : ' . mysql_error());
				}
				while($ligne=mysql_fetch_row($requete))
					{
						$gr_ech1= $ligne[0];
						$nbr_ech1=$ligne[1];
					}
					// 2 eme mois
				$time2=$time1-(2629743);		// 1 mois en seconde	
				if (!($requete=mysql_query("SELECT SUM(`grains`), COUNT( `id_echange` )  FROM `echanges` WHERE `date_ech`>'$time2' AND `date_ech`<'$time1' AND `etat`='OKI' AND NOT `id_ss_rub`='478'"))) {
					die('Erreur : ' . mysql_error());
				}
				while($ligne=mysql_fetch_row($requete))
					{
						$gr_ech2= $ligne[0];
						$nbr_ech2=$ligne[1];
					}
					// 3eme mois
				$time3=$time2-(2629743);		// 1 mois en seconde	
				if (!($requete=mysql_query("SELECT SUM(`grains`), COUNT( `id_echange` )  FROM `echanges` WHERE `date_ech`>'$time3' AND `date_ech`<'$time2' AND `etat`='OKI' AND NOT `id_ss_rub`='478'"))) {
					die('Erreur : ' . mysql_error());
				}
				while($ligne=mysql_fetch_row($requete))
					{
						$gr_ech3= $ligne[0];
						$nbr_ech3=$ligne[1];
					}
					// 4eme mois
				$time4=$time3-(2629743);		// 1 mois en seconde	
				if (!($requete=mysql_query("SELECT SUM(`grains`), COUNT( `id_echange` )  FROM `echanges` WHERE `date_ech`>'$time4' AND `date_ech`<'$time3' AND `etat`='OKI' AND NOT `id_ss_rub`='478'"))) {
					die('Erreur : ' . mysql_error());
				}
				while($ligne=mysql_fetch_row($requete))
					{
						$gr_ech4= $ligne[0];
						$nbr_ech4=$ligne[1];
					}
					// 5eme mois
				$time5=$time4-(2629743);		// 1 mois en seconde	
				if (!($requete=mysql_query("SELECT SUM(`grains`), COUNT( `id_echange` )  FROM `echanges` WHERE `date_ech`>'$time5' AND  `date_ech`<'$time4' AND `etat`='OKI' AND NOT `id_ss_rub`='478'"))) {
					die('Erreur : ' . mysql_error());
				}
				while($ligne=mysql_fetch_row($requete))
					{
						$gr_ech5= $ligne[0];
						$nbr_ech5=$ligne[1];
					}
					// 6eme mois
				$time6=$time5-(2629743);		// 1 mois en seconde	
				if (!($requete=mysql_query("SELECT SUM(`grains`), COUNT( `id_echange` )  FROM `echanges` WHERE `date_ech`>'$time6' AND  `date_ech`<'$time5' AND `etat`='OKI' AND NOT `id_ss_rub`='478'"))) {
					die('Erreur : ' . mysql_error());
				}
				while($ligne=mysql_fetch_row($requete))
					{
						$gr_ech6= $ligne[0];
						$nbr_ech6=$ligne[1];
					}
				// calcul hauteur tableau
				$maxi=max($nbr_ech1,$nbr_ech2,$nbr_ech3,$nbr_ech4,$nbr_ech5,$nbr_ech6);
				$maxi2=$maxi*2;
				$Hnbr_ech1=$nbr_ech1*2;
				$Hnbr_ech2=$nbr_ech2*2;
				$Hnbr_ech3=$nbr_ech3*2;
				$Hnbr_ech4=$nbr_ech4*2;
				$Hnbr_ech5=$nbr_ech5*2;
				$Hnbr_ech6=$nbr_ech6*2;
				$date=date('d/m/y',$time);
				$date1=date('d/m/y',$time1);
				$date2=date('d/m/y',$time2);
				$date3=date('d/m/y',$time3);
				$date4=date('d/m/y',$time4);
				$date5=date('d/m/y',$time5);
				$date6=date('d/m/y',$time6);

				
				// affichage tableau
				echo "<br><br><div class='corps'><br>
				<div class='message'>
				<p class='titre'>Graphique d'évolution du nombre des échanges sur 6 mois.
					<table summary=\"\" border='0' width='100%' cellpadding='0'><tr><td>
					<tr height=$maxi2>
						<td><p>Max: $maxi échanges.</p></td>
						<td valign='bottom' width='14%' ><hr class='hrstat' width=100% size=$Hnbr_ech1></td>
						<td valign='bottom' width='14%' ><hr class='hrstat' width=100% size=$Hnbr_ech2></td>
						<td valign='bottom' width='14%' ><hr class='hrstat' width=100% size=$Hnbr_ech3></td>
						<td valign='bottom' width='14%' ><hr class='hrstat' width=100% size=$Hnbr_ech4></td>
						<td valign='bottom' width='14%' ><hr class='hrstat' width=100% size=$Hnbr_ech5></td>
						<td valign='bottom' width='14%' ><hr class='hrstat' width=100% size=$Hnbr_ech6></td>
					</tr>
					<tr height=50>
						<td><p>Nombre:<br><span class='pasimportant'>($monnaie)</span></p></td>
						<td  width='14%' class=t2 ><p>$nbr_ech1<br><span class='pasimportant'>($gr_ech1)<br>$date à $date1</span></p></td>
						<td  width='14%' class=t2 ><p>$nbr_ech2<br><span class='pasimportant'>($gr_ech2)<br>$date1 à $date2</span></p></td>
						<td  width='14%' class=t2 ><p>$nbr_ech3<br><span class='pasimportant'>($gr_ech4)<br>$date2 à $date3</span></p></td>
						<td  width='14%' class=t2 ><p>$nbr_ech4<br><span class='pasimportant'>($gr_ech4)<br>$date3 à $date4</span></p></td>
						<td  width='14%' class=t2 ><p>$nbr_ech5<br><span class='pasimportant'>($gr_ech5)<br>$date4 à $date5</span></p></td>
						<td  width='14%' class=t2 ><p>$nbr_ech6<br><span class='pasimportant'>($gr_ech6)<br>$date5 à $date6</span></p></td>
					</tr></table><br></div><br>";	
			
			/* fin tableau */ 
			echo "<br><div class='message'><br>
			<p class='titre'>Nombre total d'échanges depuis la création du site:  (sans la cotisation)</p>
			<br>";		
			$time2=$time-(2629743);		// 1 mois en seconde	
			if (!($requete=mysql_query("SELECT SUM(`grains`), COUNT( `id_echange` )  FROM `echanges` WHERE `etat`='OKI' AND NOT `id_ss_rub`='478'"))) {
				die('Erreur : ' . mysql_error());
			}
			while($ligne=mysql_fetch_row($requete))
				{
					$gr_ech= $ligne[0];
					$nbr_ech=$ligne[1];
					echo "<p class='t1'>Nombre de quetsches échangées : ".$gr_ech."</p>
					<p>Nombre d'échanges : ".$nbr_ech."</p>";
				}			
			// top 5 echange
			echo "</div><br><div class='message'><br><p class='titre'>Le top 15 des échanges: (sans cotisation ni ancienne compta.)</p>
			<table border='0'>
			<tr>
					<td colspan=3><p class='t1'>Désignation échange:</p></td>
					<td><p class=t1>Nombre d'échanges</p></td>
			</tr>";	
			if (!($requete=mysql_query("SELECT `id_ss_rub`, count(`id_ss_rub`) FROM `echanges` GROUP BY `id_ss_rub` order by count(`id_ss_rub`) desc  LIMIT 2,15"))) {
				die('Erreur : ' . mysql_error());
			}
			while($ligne=mysql_fetch_row($requete))
				{
					$id_ss_rub= $ligne[0];
					$nbr_id_ss_rub=$ligne[1];
					if (!($requete2=mysql_query("SELECT `designation`, `id_rubrique` FROM `sous_rubrique` WHERE `id_ss_rub`='$id_ss_rub'"))) {
						die('Erreur : ' . mysql_error());
					}
					$ligne2=mysql_fetch_row($requete2);
					$designation_ss_rub= stripslashes($ligne2[0]);
					$id_rub= stripslashes($ligne2[1]);					
					// recherche des catalogue et rubrique
					if (!($requete3=mysql_query("SELECT `designation`, `id_cat` FROM `rubrique` WHERE `id_rubrique`='$id_rub'") )) {
						die('Erreur : ' . mysql_error());
					}
					$ligne3=mysql_fetch_row($requete3);
					$designation_rub=stripslashes($ligne3[0]);
					$id_cat=$ligne3[1];
					if (!($requete4=mysql_query("SELECT `designation` FROM `catalogue` WHERE `id_cat`='$id_cat'") )) {
						die('Erreur : ' . mysql_error());
					}
					$ligne4=mysql_fetch_row($requete4);
					$designation_cat=stripslashes($ligne4[0]);					
					echo "
						<tr>
							<td><p class=t1>".$designation_cat."</p></td>
							<td><p class=t4>".$designation_rub."</p></td>
							<td><p class=t4>".$designation_ss_rub."</p></td>
							<td><p class=t1>".$nbr_id_ss_rub."</p></td>
						</tr>";
				}	
			echo "</table></div><br></div>";			
		}
		elseif($_GET['action']=='inscrit')
		{
			$time=time();
			$tpsmois1=$time-2629744;
			// inscrit
			if (!($mois1=mysql_fetch_row(mysql_query("SELECT COUNT(id_seliste) FROM `selistes` WHERE `inscription`>'$tpsmois1'")))) {
				die('Erreur : ' . mysql_error());
			}
			$nbrmois1=$mois1[0];	
			// inscrit validé
			if (!($mois1=mysql_fetch_row(mysql_query("SELECT COUNT(id_seliste) FROM `selistes` WHERE `inscription`>'$tpsmois1' AND `valide`='OUI'")))) {
				die('Erreur : ' . mysql_error());
			}
			$nbrmois1val=$mois1[0];	
			// 2eme mois			
			$tpsmois2=$tpsmois1-2629744;
			if (!($mois2=mysql_fetch_row(mysql_query("SELECT COUNT(id_seliste) FROM `selistes` WHERE `inscription`>'$tpsmois2'")))) {
				die('Erreur : ' . mysql_error());
			}
			$nbrmois2=$mois2[0]-$nbrmois1;
			if (!($mois2=mysql_fetch_row(mysql_query("SELECT COUNT(id_seliste) FROM `selistes` WHERE `inscription`>'$tpsmois2' AND `valide`='OUI'")))) {
				die('Erreur : ' . mysql_error());
			}
			$nbrmois2val=$mois2[0]-$nbrmois1val;	
			// 3 eme mois 
			$tpsmois3=$tpsmois2-2629744;
			if (!($mois3=mysql_fetch_row(mysql_query("SELECT COUNT(id_seliste) FROM `selistes` WHERE `inscription`>'$tpsmois3'")))) {
				die('Erreur : ' . mysql_error());
			}
			$nbrmois3=$mois3[0]-$nbrmois2;
			if (!($mois3=mysql_fetch_row(mysql_query("SELECT COUNT(id_seliste) FROM `selistes` WHERE `inscription`>'$tpsmois3' AND `valide`='OUI'")))) {
				die('Erreur : ' . mysql_error());
			}
			$nbrmois3val=$mois3[0]-$nbrmois2val;
			// 4 eme mois 
			$tpsmois4=$tpsmois3-2629744;
			if (!($mois4=mysql_fetch_row(mysql_query("SELECT COUNT(id_seliste) FROM `selistes` WHERE `inscription`>'$tpsmois4'")))) {
				die('Erreur : ' . mysql_error());
			}
			$nbrmois4=$mois4[0]-$nbrmois3;
			if (!($mois4=mysql_fetch_row(mysql_query("SELECT COUNT(id_seliste) FROM `selistes` WHERE `inscription`>'$tpsmois4' AND `valide`='OUI'")))) {
				die('Erreur : ' . mysql_error());
			}
			$nbrmois4val=$mois4[0]-$nbrmois3val;
			// 5 eme mois 
			$tpsmois5=$tpsmois4-2629744;
			if (!($mois5=mysql_fetch_row(mysql_query("SELECT COUNT(id_seliste) FROM `selistes` WHERE `inscription`>'$tpsmois5'")))) {
				die('Erreur : ' . mysql_error());
			}
			$nbrmois5=$mois5[0]-$nbrmois4;
			if (!($mois5=mysql_fetch_row(mysql_query("SELECT COUNT(id_seliste) FROM `selistes` WHERE `inscription`>'$tpsmois5' AND `valide`='OUI'")))) {
				die('Erreur : ' . mysql_error());
			}
			$nbrmois5val=$mois5[0]-$nbrmois4val;	
			// 6 eme mois 
			$tpsmois6=$tpsmois5-2629744;
			if (!($mois6=mysql_fetch_row(mysql_query("SELECT COUNT(id_seliste) FROM `selistes` WHERE `inscription`>'$tpsmois6'")))) {
				die('Erreur : ' . mysql_error());
			}
			$nbrmois6=$mois6[0]-$nbrmois5;
			if (!($mois6=mysql_fetch_row(mysql_query("SELECT COUNT(id_seliste) FROM `selistes` WHERE `inscription`>'$tpsmois6' AND `valide`='OUI'")))) {
				die('Erreur : ' . mysql_error());
			}
			$nbrmois6val=$mois6[0]-$nbrmois5val;				
			// nombre de non validé 
			$nonvalidmois1=$nbrmois1-$nbrmois1val;
			$nonvalidmois2=$nbrmois2-$nbrmois2val;
			$nonvalidmois3=$nbrmois3-$nbrmois3val;
			$nonvalidmois4=$nbrmois4-$nbrmois4val;
			$nonvalidmois5=$nbrmois5-$nbrmois5val;
			$nonvalidmois6=$nbrmois6-$nbrmois6val;
			// total
			$totalmois=$nbrmois1+$nbrmois2+$nbrmois3+$nbrmois4+$nbrmois5+$nbrmois6;
			$totalmoisval=$nbrmois1val+$nbrmois2val+$nbrmois3val+$nbrmois4val+$nbrmois5val+$nbrmois6val;
			$nonvalidmois=$nonvalidmois1+$nonvalidmois2+$nonvalidmois3+$nonvalidmois4+$nonvalidmois5+$nonvalidmois6;
			
			// pourcentage
			$prcentm1=round((100*$nbrmois1val/$nbrmois1),1);
			$prcentm2=round((100*$nbrmois2val/$nbrmois2),1);
			$prcentm3=round((100*$nbrmois3val/$nbrmois3),1);	
			$prcentm4=round((100*$nbrmois4val/$nbrmois4),1);
			$prcentm5=round((100*$nbrmois5val/$nbrmois5),1);
			$prcentm6=round((100*$nbrmois6val/$nbrmois6),1);
						
			$pourcentageval=round((100*$totalmoisval/$totalmois),1);
			$pourcentagepasval=round((100*$nonvalidmois/$totalmois),1);
			//affichage
			
			echo "<br><br><div class='corps'><br>
			<table summary=\"\" class='tablevu' border='1' width='90%' cellpadding='5'>
			<tr>
			<td colspan='4'><p class='titre'>Evolution des inscrits:</p></td>
			</tr>
			<tr>
			<td class=t1><p>Temps</p></td>
			<td class=t1><p>Nombre d'inscrits :</p></td>
			<td class=t1><p>Nombre qui l'ont validé (actifs):</p></td>
			<td class=t1><p>Nombre qui ne l'ont pas validé (inactifs):</p></td>
			</tr>
			<tr>
			<td class=t2><p>Ce mois-ci:</p></td>
			<td class=t3><p>".$nbrmois1."</p></td>
			<td class=t3><p><b>".$nbrmois1val."<br>(".$prcentm1."%)</b></p></td>
			<td class=t3><p>".$nonvalidmois1."</p></td>
			</tr>
			<tr>
			<td class=t2><p>Le mois dernier:</p></td>
			<td class=t3><p>".$nbrmois2."</p></td>
			<td class=t3><p><b>".$nbrmois2val."<br>(".$prcentm2."%)</b></p></td>
			<td class=t3><p>".$nonvalidmois2."</p></td>
			</tr>
			<tr>
			<td class=t2><p>Il y a 3 mois:</p></td>
			<td class=t3><p>".$nbrmois3."</p></td>
			<td class=t3><p><b>".$nbrmois3val."<br>(".$prcentm3."%)</b></p></td>
			<td class=t3><p>".$nonvalidmois3."</p></td>
			</tr>
			<tr>
			<td class=t2><p>Il y a 4 mois:</p></td>
			<td class=t3><p>".$nbrmois4."</p></td>
			<td class=t3><p><b>".$nbrmois4val."<br>(".$prcentm4."%)</b></p></td>
			<td class=t3><p>".$nonvalidmois4."</p></td>
			</tr>
			<tr>
			<td class=t2><p>Il y a 5 mois:</p></td>
			<td class=t3><p>".$nbrmois5."</p></td>
			<td class=t3><p><b>".$nbrmois5val."<br>(".$prcentm5."%)</b></p></td>
			<td class=t3><p>".$nonvalidmois5."</p></td>
			</tr>
			<tr>
			<td class=t2><p>Il y a 6 mois:</p></td>
			<td class=t3><p>".$nbrmois6."</p></td>
			<td class=t3><p><b>".$nbrmois6val."<br>(".$prcentm6."%)</b></p></td>
			<td class=t3><p>".$nonvalidmois6."</p></td>
			</tr>
			<tr>
			<td class=t1><p>Cumul:</p></td>
			<td class=t1><p>".$totalmois."</p></td>
			<td class=t1><p><b>".$totalmoisval." <br>(".$pourcentageval."%)</b></p></td>
			<td class=t1><p>".$nonvalidmois." <br>(".$pourcentagepasval."%)</p></td>
			</tr>			
			</table></div>";		
			
		}
		else
		{ 	//menu
			echo "<br><br><div class='corps'><br>
			<p class='titre'>Statistiques de ".$nom.":</p>
			<br><p>
			<a href=\"statistique.php?action=rijsel\">Compte ".$nom." (crédits et débits de ".$monnaie.")</a>&nbsp;&nbsp;
			<a href=\"statistique.php?action=echange\">Echanges du mois</a>&nbsp;&nbsp;
			<a href=\"statistique.php?action=seliste\">Sélistes (mails, actifs...)</a>&nbsp;&nbsp;
			<a href=\"statistique.php?action=inscrit\">Evolution inscriptions</a></p><br></div>";		
		}
	} 
	//delai depassé
	else
	{
		header ("location:troptard.php");
		session_destroy();
	}
}
// pas de sesion
else
{
	header ("location:404.php");
	session_destroy();
}
mysql_close($connexion); 
include ("fin.php");	
?>
